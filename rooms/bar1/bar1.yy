
{
    "name": "bar1",
    "id": "a6ed63e8-ae40-4015-9428-20c695cdcbeb",
    "creationCodeFile": "RoomCreationCode.gml",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "4ecbc891-b819-4305-a394-b3f0db534167",
        "67ca9d88-0f79-423a-bbd5-05022301af0a",
        "c6ef5d1c-8a55-47d2-a1f8-a83c14bed4cd",
        "8887f0e7-3c20-459a-8219-7031f9fc3078",
        "f6a3ce28-1d74-45ae-8bc1-7b52f4ead392",
        "925e49cf-a4ca-4d5a-9a2e-e05808a68ff2",
        "5e03382c-a135-4552-a4da-e83ae292a7ba",
        "02cf5730-c081-47bb-b61e-ed249c2c5c3d",
        "a633bc58-5878-47d5-bd8f-c038351671b7",
        "4bed0d1b-299e-42c7-a2d8-9f4ed2e44f8f",
        "606982d6-7078-4bcc-93be-8a56033fb95f"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Collisions",
            "id": "c5fbeaef-062b-4572-a530-f966939bc6ca",
            "depth": -300,
            "grid_x": 24,
            "grid_y": 24,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_5C49451","id": "8887f0e7-3c20-459a-8219-7031f9fc3078","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5C49451","objId": "7e112a55-f75c-47df-9092-27fd121f56af","properties": null,"rotation": 0,"scaleX": 2.5,"scaleY": 3.041667,"mvc": "1.0","x": 468,"y": 235},
{"name": "inst_7CCAC4DB","id": "f6a3ce28-1d74-45ae-8bc1-7b52f4ead392","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7CCAC4DB","objId": "7e112a55-f75c-47df-9092-27fd121f56af","properties": null,"rotation": 0,"scaleX": 3,"scaleY": 3,"mvc": "1.0","x": 0,"y": 144},
{"name": "inst_A707BCB","id": "925e49cf-a4ca-4d5a-9a2e-e05808a68ff2","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_A707BCB","objId": "7e112a55-f75c-47df-9092-27fd121f56af","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 232,"y": 199},
{"name": "inst_606A45E6","id": "5e03382c-a135-4552-a4da-e83ae292a7ba","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_606A45E6","objId": "7e112a55-f75c-47df-9092-27fd121f56af","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 112,"y": 248},
{"name": "inst_58AC5824","id": "02cf5730-c081-47bb-b61e-ed249c2c5c3d","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_58AC5824","objId": "7e112a55-f75c-47df-9092-27fd121f56af","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 120,"y": 190},
{"name": "inst_4B0F2323","id": "a633bc58-5878-47d5-bd8f-c038351671b7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4B0F2323","objId": "7e112a55-f75c-47df-9092-27fd121f56af","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 528,"y": 264},
{"name": "inst_24D23A3B","id": "4bed0d1b-299e-42c7-a2d8-9f4ed2e44f8f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_24D23A3B","objId": "7e112a55-f75c-47df-9092-27fd121f56af","properties": null,"rotation": 0,"scaleX": 17,"scaleY": 1,"mvc": "1.0","x": 192,"y": 384},
{"name": "inst_74DCBCFB","id": "606982d6-7078-4bcc-93be-8a56033fb95f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_74DCBCFB","objId": "7e112a55-f75c-47df-9092-27fd121f56af","properties": null,"rotation": 0,"scaleX": 5.708333,"scaleY": 0.9583333,"mvc": "1.0","x": 0,"y": 384}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Warps",
            "id": "2089b069-dec7-4bdb-ac31-75df9aa3143a",
            "depth": -200,
            "grid_x": 24,
            "grid_y": 24,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_32068894","id": "67ca9d88-0f79-423a-bbd5-05022301af0a","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_32068894.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_32068894","objId": "69ae36b7-4794-41af-8645-5350bad360a3","properties": null,"rotation": 0,"scaleX": 2,"scaleY": 1,"mvc": "1.0","x": 157,"y": 390}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": true,
            "visible": true
        },
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Instances",
            "id": "65dc94de-b952-423f-bd82-76e71d723eae",
            "depth": -100,
            "grid_x": 24,
            "grid_y": 24,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_3791BE20","id": "4ecbc891-b819-4305-a394-b3f0db534167","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3791BE20","objId": "53d24348-b6a5-4090-836f-b1d2de36b4ca","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 224,"y": 64},
{"name": "inst_753AE1ED","id": "c6ef5d1c-8a55-47d2-a1f8-a83c14bed4cd","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_753AE1ED.gml","creationCodeType": ".gml","ignore": false,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_753AE1ED","objId": "bf13f51f-a0c6-4491-a01f-d833b0f7f6af","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.0","x": 384,"y": 216}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": true,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "bar_image",
            "id": "c1494666-9e79-4faf-8fec-a41df5d8d963",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 999,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "9cfca21a-1ea1-4192-aab1-068b4c5c2033",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": true,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "beb97272-8270-4618-b61e-3531e2dd4697",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "31a1248f-248e-4331-9ac4-83a555dc2414",
        "Height": 400,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 600
    },
    "mvc": "1.0",
    "views": [
{"id": "808452e2-1959-4465-801a-b274df6a9794","hborder": 300,"hport": 800,"hspeed": -1,"hview": 400,"inherit": false,"modelName": "GMRView","objId": "53d24348-b6a5-4090-836f-b1d2de36b4ca","mvc": "1.0","vborder": 200,"visible": true,"vspeed": -1,"wport": 1200,"wview": 600,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "daf0509d-1633-4bdb-adb2-f3f549718e90","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "358fea40-1917-4826-b717-9799b0a84c22","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "0f78abfb-fea5-469f-9548-5d5032be9aa6","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "d687b3d9-851d-423b-9e9c-08eda1d08f07","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "93cd5cb9-f2ce-46d0-9229-b3d8613c03b1","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "987e511e-d7f7-485e-8b0a-956961595991","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "a4f7533f-adb6-46e5-a837-b45b136743c9","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "e4ac632b-6a7f-4edc-a4a2-6f3bf99c3255",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}
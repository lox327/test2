{
    "id": "65764902-42bf-4315-8fd8-51b4a1965873",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objMod_acid",
    "eventList": [
        {
            "id": "6493f7bb-1954-4dd5-93ba-0f2d3d2f2589",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "65764902-42bf-4315-8fd8-51b4a1965873"
        },
        {
            "id": "e37da103-7594-4277-a126-c8e7d52a317b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c0353dfc-c14d-417e-b283-64ff3ecacc3b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "65764902-42bf-4315-8fd8-51b4a1965873"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7af82463-cca2-45a9-87e3-98eb03ace7c9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8669a1c8-d9c3-46c5-99e3-bb5fb4033f7c",
    "visible": true
}
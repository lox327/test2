{
    "id": "bc5b7ff9-774b-4a5b-82c8-5ee557d5f6c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDebris1",
    "eventList": [
        {
            "id": "b2106d83-3e25-4f84-abd5-513f7e0ca60e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "bc5b7ff9-774b-4a5b-82c8-5ee557d5f6c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
    "visible": true
}
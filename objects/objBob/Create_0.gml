hasInfo = false;

//DIALOG for [quests]
dialog = [];
dialogLine = 0;
prox = 50;
name = "bob";

d_bob0 = 
addDialog("there's trouble around here down in the south, can you help us out?", false, avatarBar, false, self);

d_bob1 = 
addDialog("thanks for doing the dirty work, things will be better around here now.", true, avatarBar, false, self);
addDialog("the mod shop is a good place to enhance your items, check it out.", true, avatarBar, false, self);
addDialog("it's down here, just south a bit.", false, avatarBar, false, self);


/*
//QUEST
/// @description map for storing quests

//list for storing quest objs
global.questsJake = ds_list_create();

//map of quests to store in list
var quest = ds_map_create();
ds_map_add(quest, "name",	"quest1");
ds_map_add(quest, "reward",	"mod1");
ds_map_add(quest, "desc",	"1...is wanted for: blah");
ds_map_add(quest, "location",	"north");
ds_map_add(quest, "available",	true);
ds_map_add(quest, "completed",	false);
ds_list_add(global.questsJake, quest);

quest = ds_map_create();
ds_map_add(quest, "name",	"quest2");
ds_map_add(quest, "reward",	"mod2");
ds_map_add(quest, "desc",	"2...is wanted for: mehh");
ds_map_add(quest, "location",	"south");
ds_map_add(quest, "available",	false);
ds_map_add(quest, "completed",	false);
ds_list_add(global.questsJake, quest);

quest = ds_map_create();
ds_map_add(quest, "name",	"quest3");
ds_map_add(quest, "reward",	"mod3");
ds_map_add(quest, "desc",	"3...is wanted for: murr");
ds_map_add(quest, "location",	"south2");
ds_map_add(quest, "available",	false);
ds_map_add(quest, "completed",	false);
ds_list_add(global.questsJake, quest);
*/
{
    "id": "c866eb5a-faea-46f2-8f3a-cd01196e536c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBob",
    "eventList": [
        {
            "id": "fe49e1ab-31e0-4f6d-aab1-9e873e3ce8dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c866eb5a-faea-46f2-8f3a-cd01196e536c"
        },
        {
            "id": "1721e66d-3b2f-443f-95ff-b5546f8c8fa3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c866eb5a-faea-46f2-8f3a-cd01196e536c"
        },
        {
            "id": "14b326ed-db40-4eff-b2d9-3eab55b1c615",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c866eb5a-faea-46f2-8f3a-cd01196e536c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
    "visible": true
}
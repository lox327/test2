/// @description handle cursor movement across items; handle equipping of item
if (!display) exit;

//LEFT
if (keyboard_check_pressed(vk_left)) {
	if (slot_selected != 0) {
		slot_selected--;
	}
	else {
		//at beginning - do nothing or wrap around...
	}
}

//RIGHT
if (keyboard_check_pressed(vk_right)) {
	if (slot_selected != ds_grid_width(global.item_index)-1) {
		slot_selected++;
	}
	else {
		//at end - do nothing or wrap around...
	}
}


//ENTER - equips item
//need to swap char item slot with item selected in inv
//highlight which char item slot type is being selected: weapon, mod, whatever
//need process to implement what the equipped item is, and convey that to real world
if (keyboard_check_pressed(vk_enter)) {
	//toggle equipped for sel item
	global.item_index[#slot_selected, stat.equipped] = !global.item_index[#slot_selected, stat.equipped];
	
	/*if (global.item_index[#slot_selected, stat.equipped]) {
		global.item_index[#slot_selected, stat.equipped] = false;
		objPlayer.currentGun = noone;
	}
	else {
		global.item_index[#slot_selected, stat.equipped] = true;
		objPlayer.currentGun = global.item_index[#slot_selected, stat.type];
	}*/
	
	//must go through inv list and determine what is equipped
	var curr_size = ds_grid_width(global.item_index);
	for (var i = 0; i < curr_size; i++) {
			//update player GUN stats with what current equipped gun has
			if (global.item_index[#slot_selected, stat.type] != "Mod") {
				//if (global.item_index[#slot_selected, stat.equipped]) {
				//	show_debug_message("equip");
				//	objPlayer.secondaryGun = global.item_index[#slot_selected, stat.type];
				//}
				//if (objPlayer.currentGun != noone) objPlayer.secondaryGun =  global.item_index[#slot_selected, stat.type];

				objPlayer.weaponEnergyUse = global.item_index[#slot_selected, stat.weaponEnergyUse];
				objPlayer.weaponEnergyMax = global.item_index[#slot_selected, stat.weaponEnergyTotal];
				objPlayer.bulletDmg =		global.item_index[#slot_selected, stat.damage_low];
				objPlayer.currentGun =		global.item_index[#slot_selected, stat.type];
				objPlayer.mod1 =			global.item_index[#slot_selected, stat.mod1];
				objPlayer.gunSprite =		global.item_index[#slot_selected, stat.spr_sheet];
				objPlayer.gunSpriteIndex =	global.item_index[#slot_selected, stat.spr_index];
				//objPlayer.fireRate = global.item_index[#slot_selected, stat.fireRate];
				//objPlayer.bulletMax = global.item_index[#slot_selected, stat.bulletMax];
			}
			
			//update player equipped gun with mod selected in inv
			//can only equip mod if the gun you have equipped has a modslot
			//if () {
			//	global.item_index[#slot_selected, stat.equipped] = !global.item_index[#slot_selected, stat.equipped];
			//	break;
			//}
			//update player MOD stats with what current equipped mod has
			else if (objPlayer.mod1 == 1) {
				//objPlayer.mod1 = global.item_index[#slot_selected, stat.mod1];
				//objPlayer.mod2 = global.item_index[#slot_selected, stat.mod2];
				objPlayer.modifier =	global.item_index[#slot_selected, stat.modifier];
				objPlayer.bulletType =	global.item_index[#slot_selected, stat.mod_amount];
				objPlayer.mod_desc =	global.item_index[#slot_selected, stat.mod_desc];
				
				//MODS details
				switch ( objPlayer.bulletType ) {
					case objMod_fire:
						objPlayer.dot_duration =	global.item_index[#slot_selected, stat.dot_duration];
						objPlayer.dot_amount =		global.item_index[#slot_selected, stat.dot_amount];
						break;
					
					case objMod_shock:
						objPlayer.slow_duration =	global.item_index[#slot_selected, stat.slow_duration];
						objPlayer.slow_amount =		global.item_index[#slot_selected, stat.slow_amount];
						break;
					
					case objMod_acid:
						objPlayer.splash_rad =		global.item_index[#slot_selected, stat.splash_rad];
						objPlayer.splash_amount =	global.item_index[#slot_selected, stat.splash_amount];
						break;
					
				}

			}
			else {
				show_debug_message("no mod slot");
				global.item_index[#slot_selected, stat.equipped] = !global.item_index[#slot_selected, stat.equipped];
			}
			break;
			
		//mod unequipped
		/*else if ( global.item_index[#slot_selected, stat.type] == "Mod" ) {
			show_message("mod removed...");
			objPlayer.mod1 = 0;
			break;
		}*/
	}
}
show_debug_message(objPlayer.gunSprite)

/// @description Insert description here
// You can write your code in this editor
if (!display) exit;

//set background transparent
draw_set_alpha(0.70);
draw_rectangle_color(0, 0, objPlayer.x+500, objPlayer.y+500, c_black, c_black, c_black, c_black, false);

//back to normal
draw_set_alpha(1.0);

//local var for inv_name grid
var inv_name = global.item_index;

//draw player avatar
draw_sprite(char2,0,objPlayer.x-200,objPlayer.y);

//draw UI bg
draw_sprite(spr_inv_ui,0,objPlayer.x+100,objPlayer.y+40);


//draw player info
draw_text(objPlayer.x-250,objPlayer.y-155,"credits: " + string(objPlayer.invCredits));

//draw empty gun slot under player
draw_sprite(spr_slot,0,objPlayer.x-250,objPlayer.y+150);

//draw inv_name slots, items
for ( var i = 0; i < ds_grid_width(inv_name); i++ ) {
	//draw empty inv_name slots
	draw_sprite(spr_slot,0,objPlayer.x - 100 + (i * 32),objPlayer.y+10);
	
	//draw item sprite - draw in inv if not equipped; draw on char if equipped
	if ( inv_name[#i, stat.owned] == true ) { //for now, using owned == true as placeholder for player having bought the item from shop
		if ( inv_name[#i, stat.equipped] == false) draw_sprite(inv_name[#i, stat.spr_sheet],inv_name[#i, stat.spr_index],objPlayer.x - 100 + (i * 32),objPlayer.y+10);
		else {
			//draw on empty gun slot
			if (global.item_index[#i, stat.type] != "Mod") {
				draw_sprite(inv_name[#i, stat.spr_sheet],inv_name[#i, stat.spr_index],objPlayer.x-250,objPlayer.y+150);
			}
			
			//draw on mod slot next to gun
			else {
				//draw_sprite(inv_name[#i, stat.spr_sheet],inv_name[#i, stat.spr_index],objPlayer.x-168,objPlayer.y+150);
				draw_sprite_ext(inv_name[#i, stat.spr_sheet],inv_name[#i, stat.spr_index],objPlayer.x-218, objPlayer.y+150,.5,.5,1,c_white,1);
			}
			
			//draw empty mod slot
			if ( inv_name[#i, stat.mod1] == 1 ) draw_sprite(spr_mod_empty,0,objPlayer.x-218, objPlayer.y+150);
			//if ( inv_name[#i, stat.mod2] == 1 ) draw_sprite(spr_mod_empty,0,objPlayer.x-218, objPlayer.y+150);
		}
		
	}
		
}

//draw cursor
draw_sprite(spr_cursor,0,objPlayer.x - 100+(slot_selected*(32)),objPlayer.y+10);

//draw large item sprite
if (inv_name[#slot_selected, stat.owned] == true) draw_sprite(inv_name[#slot_selected, stat.spr_sheet],inv_name[#slot_selected, stat.spr_index],objPlayer.x+175,objPlayer.y+50);

//draw empty mod slot on large item sprite
if ( inv_name[#slot_selected, stat.mod1] == 1 ) draw_sprite(spr_mod_empty,0,objPlayer.x+205,objPlayer.y+50);


//draw char equipped slots
//draw empty inv_name slots
//draw_sprite(spr_slot,0,objPlayer.x-200,objPlayer.y+150);


//TEXT
//draw item info text of selected item
var text_x = -50;
var text_y = 100;
if ( inv_name[#slot_selected, stat.owned] == true ) {
	var text1 = inv_name[#slot_selected, stat.name];
	var type = inv_name[#slot_selected, stat.type];
	//var text3 = inv_name[#slot_selected, stat.description];
	
	draw_set_font(fnt_med_reg);
	draw_text(objPlayer.x+150,objPlayer.y+text_y,string_upper(text1));
	
	draw_set_font(fnt_GUI);
	draw_text(objPlayer.x+150,objPlayer.y+text_y+20,string_upper(getNameFromType(type)));
	//draw_text(objPlayer.x+100,objPlayer.y+text_y+40,text3);
	
	//gun stats
	draw_set_halign(fa_left);
	if ( inv_name[#slot_selected, stat.modifier] == 0) { //this is a gun
		var dmg_low = inv_name[#slot_selected, stat.damage_low];
		var dmg_high = inv_name[#slot_selected, stat.damage_high];
		var weaponEnergyUse = inv_name[#slot_selected, stat.weaponEnergyUse];
		var weaponEnergyTotal = inv_name[#slot_selected, stat.weaponEnergyTotal];
		draw_text(objPlayer.x-100,objPlayer.y+50,"damage_low: " + string(dmg_low));
		draw_text(objPlayer.x-100,objPlayer.y+50+20,"damage_high: " + string(dmg_high));
		draw_text(objPlayer.x-100,objPlayer.y+50+40,"weapon energy use: " + string(weaponEnergyUse));
		draw_text(objPlayer.x-100,objPlayer.y+50+60,"weapon energy total: " + string(weaponEnergyTotal));
	}

	//desc text for mods, more to follow
	else { //this is a mod
		var text5 = inv_name[#slot_selected, stat.modifier];
		var text6 = inv_name[#slot_selected, stat.mod_desc];
		draw_text(objPlayer.x-100,objPlayer.y+text_y-50,text5);
		draw_text_ext(objPlayer.x-100,objPlayer.y+text_y-30,text6,15,150);
		
		//need script to pull all mod info from the various modtypes, ghetto for now
		//get mod_type (mod_amount)
		var mod_amount = inv_name[#slot_selected, stat.mod_amount];
		
		if (mod_amount == objMod_fire ) {
			var mod_duration = inv_name[#slot_selected, stat.dot_duration];
			var amount = inv_name[#slot_selected, stat.dot_amount];
		}
		else if (mod_amount == objMod_acid ) {
			var mod_duration = inv_name[#slot_selected, stat.splash_rad];
			var amount = inv_name[#slot_selected, stat.splash_amount];
		}
		else if ( mod_amount == objMod_shock ) {
			var mod_duration = inv_name[#slot_selected, stat.slow_duration];
			var amount = inv_name[#slot_selected, stat.slow_amount];	
		}
		
		draw_text(objPlayer.x-100,objPlayer.y+text_y+35,"duration: " + string(mod_duration));
		draw_text(objPlayer.x-100,objPlayer.y+text_y+50,"amount: " + string(amount));
	}
}

/*
//draw item info text of selected item
if ( inv_name[#slot_selected, stat.owned] == true ) {
	var text1 = inv_name[#slot_selected, stat.name];
	var text2 = inv_name[#slot_selected, stat.description];
	var text3 = inv_name[#slot_selected, stat.price];
	var equipped = inv_name[#slot_selected, stat.equipped];
	draw_text(objPlayer.x-100,objPlayer.y+50,text1);
	draw_text(objPlayer.x-50,objPlayer.y+50,text2);
	draw_text(objPlayer.x+100,objPlayer.y+50,text3);
	
	if (equipped) draw_text(objPlayer.x-100,objPlayer.y+75,"*equipped");
}
*/
{
    "id": "3da8c296-45f5-47ea-81e0-fa013643322c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objInvMgr2",
    "eventList": [
        {
            "id": "d27b476d-6a66-4ceb-8e3b-841e6586f58d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3da8c296-45f5-47ea-81e0-fa013643322c"
        },
        {
            "id": "cf3618ba-c903-4c43-9a93-bc2b6803619f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3da8c296-45f5-47ea-81e0-fa013643322c"
        },
        {
            "id": "a3a8c48e-38d2-45c8-b1f6-c20fa7c72536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 36,
            "eventtype": 9,
            "m_owner": "3da8c296-45f5-47ea-81e0-fa013643322c"
        },
        {
            "id": "4c8ea60e-c808-4cfa-a85b-ca3bf2443ad8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3da8c296-45f5-47ea-81e0-fa013643322c"
        },
        {
            "id": "6754364d-50fd-4192-8b16-75a396a6bed4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 9,
            "m_owner": "3da8c296-45f5-47ea-81e0-fa013643322c"
        },
        {
            "id": "27bdb94b-a8f5-4530-ad46-fb3e3405d856",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 9,
            "m_owner": "3da8c296-45f5-47ea-81e0-fa013643322c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
{
    "id": "e3fbb4f8-ffbf-49e2-b26b-1662dd92bafe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBloodController",
    "eventList": [
        {
            "id": "240edd63-5c08-4233-a760-42fa09aa93df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e3fbb4f8-ffbf-49e2-b26b-1662dd92bafe"
        },
        {
            "id": "63a98bde-eebc-41f4-9980-b563c88853cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "e3fbb4f8-ffbf-49e2-b26b-1662dd92bafe"
        },
        {
            "id": "2bc818b7-6f90-4e68-9391-32677d1f36c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e3fbb4f8-ffbf-49e2-b26b-1662dd92bafe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
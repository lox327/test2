{
    "id": "a8beaa03-b9bc-4dc5-9805-918daea35c6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objKey3",
    "eventList": [
        {
            "id": "f9523bba-52c0-4100-ba91-79777f364cf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a8beaa03-b9bc-4dc5-9805-918daea35c6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b5d5993d-2a2e-4ea3-a438-77ee3d53f071",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b8fa866-d47c-4878-be2d-98bdb7d57aed",
    "visible": true
}
{
    "id": "0c01e080-5a30-4baa-9619-eba4c18d589c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLaser",
    "eventList": [
        {
            "id": "aeb3e826-69f8-49e2-80a6-7d542c69449e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c01e080-5a30-4baa-9619-eba4c18d589c"
        },
        {
            "id": "62ff74ab-0370-4937-88c8-614babfebdf8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c01e080-5a30-4baa-9619-eba4c18d589c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7e112a55-f75c-47df-9092-27fd121f56af",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "23d7be55-5fa1-49fa-8e8f-7942448bcb6c",
    "visible": true
}
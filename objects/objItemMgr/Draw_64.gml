display_set_gui_size( camera_get_view_width(view_camera[0]), camera_get_view_height(view_camera[0]) );

//draw screen info text
draw_text(100, 100, "item shop");
draw_text(100, 120, "credits available: " + string(objPlayer.invCredits));
draw_text(65, 215, "mod: 0x678");
draw_text(65, 240, "(active)");

draw_text(65, 375, "mod: 0x234");
draw_text(65, 400, "(passive)");

var dx = 650;
var dy = 625;
var dy2 = 700;

//item desc in right pane
//hard coding for now...
//can call func and pass in objItem...
if ( objItem1.selected ) {
	if (objItem1.available) {
		draw_sprite(invSel, 0, objItem1.x, objItem1.y);
		draw_text_ext(dx, dy, objItem1.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy, "[consumes " + objItem1.itemEnergy + "]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objItem2.selected ) {
	if (objItem2.available) {
		draw_sprite(invSel, 0, objItem2.x, objItem2.y);
		draw_text_ext(dx, dy, objItem2.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy2, "[consumes " + string(objItem2.itemEnergy) + " energy]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objItem3.selected ) {
	if (objItem3.available) {
		draw_sprite(invSel, 0, objItem3.x, objItem3.y);
		draw_text_ext(dx, dy, objItem3.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy2, "[consumes " + string(objItem3.itemEnergy) + " energy]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objItem4.selected ) {
	if (objItem4.available) {
		draw_sprite(invSel, 0, objItem4.x, objItem4.y);
		draw_text_ext(dx, dy, objItem4.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy2, "[consumes " + string(objItem4.itemEnergy) + " energy]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objItem5.selected ) {
	if (objItem5.available) {
		draw_sprite(invSel, 0, objItem5.x, objItem5.y);
		draw_text_ext(dx, dy, objItem5.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy2, "[consumes " + string(objItem5.itemEnergy) + " energy]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objItemMgr.selected ) {
	draw_sprite(invSel, 0, objItemMgr.x, objItemMgr.y);
	draw_text_ext(dx, dy, "exit...", line_break, word_wrap);
}
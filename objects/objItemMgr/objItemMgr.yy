{
    "id": "5d08e54c-ed75-4c68-bdc2-ead7effdc523",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objItemMgr",
    "eventList": [
        {
            "id": "f05b5c22-ba3c-4ed7-aa1f-9ff27d9ee0d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "5d08e54c-ed75-4c68-bdc2-ead7effdc523"
        },
        {
            "id": "f75c898a-e4f2-42ff-886b-4ef2875874b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5d08e54c-ed75-4c68-bdc2-ead7effdc523"
        },
        {
            "id": "aee9610f-1402-4056-95e3-d3c5f5827086",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5d08e54c-ed75-4c68-bdc2-ead7effdc523"
        },
        {
            "id": "78c161aa-d07e-4f4c-8c75-b3726788e4ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5d08e54c-ed75-4c68-bdc2-ead7effdc523"
        },
        {
            "id": "0e539711-30ee-42e4-a465-d4a7c8312850",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 5,
            "m_owner": "5d08e54c-ed75-4c68-bdc2-ead7effdc523"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e399d58f-9add-412f-a080-fdea66996e3f",
    "visible": true
}
//code for managing the state of the items: in stock, buy...
//EXIT
if ( objItemMgr.selected ) {
	//code for exiting room
	room_goto(global.currentRoom);
	objItem1.selected = true;
	objPlayer.x = prevX;
	objPlayer.y = prevY;
	
	objPlayer.playerMove = true;
	newFade.doTransition = false;
	exit;
}

if (ds_list_size(global.inv) >= 3) {
	show_message("inv full");
	exit;
}

//temp index var to assign item obj to reduce duplicate code
var index;

//move to switch
if ( objItem1.selected ) {
	index = objItem1;
}

else if ( objItem2.selected ) {
	index = objItem2;
}

else if ( objItem3.selected ) {
	index = objItem3;
}

else if ( objItem4.selected ) {
	index = objItem4;
}

else if ( objItem5.selected ) {
	index = objItem5;
}

//code for equipping this item, or going to room...
if (index.available) {
	addInv(index);
	index.available = false;
}
else show_message("item not available");

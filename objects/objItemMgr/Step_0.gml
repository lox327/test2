/// @description handle input movement across items
if (selected) {
	//image_index = 1;
} else {
	//image_index = 0;
}

if (keyboard_check_pressed(vk_left)) {
	if (objItem1.selected) {
		//wrap around
		//objItem1.selected = false;
		//objItem3.selected = true;
	}
	else if (objItem2.selected) {
		objItem2.selected = false;
		objItem1.selected = true;
	}
	else if (objItem3.selected) {
		//objItem3.selected = false;
		//objItem2.selected = true;
	}
	else if (objItem4.selected) {
		objItem4.selected = false;
		objItem3.selected = true;
	}
	else if (objItem5.selected) {
		objItem5.selected = false;
		objItem4.selected = true;
	}
}
if (keyboard_check_pressed(vk_right)) {
	if (objItem1.selected) {
		objItem1.selected = false;
		objItem2.selected = true;
	}
	else if (objItem2.selected) {
		//objItem2.selected = false;
		//objItem1.selected = true;
	}
	else if (objItem3.selected) {
		objItem3.selected = false;
		objItem4.selected = true;
	}
	else if (objItem4.selected) {
		objItem4.selected = false;
		objItem5.selected = true;
	}
	else if (objItem5.selected) {
		//objItem5.selected = false;
		//objItem4.selected = true;
	}
}


//down
if (keyboard_check_pressed(vk_down)) {
	if (objItem1.selected) {
		objItem1.selected = false;
		objItem3.selected = true;
	}
	else if (objItem2.selected) {
		objItem2.selected = false;
		objItem4.selected = true;
	}
	//exit
	else if (objItem3.selected) {
		objItem3.selected = false;
		objItemMgr.selected = true;
	}
	else if (objItem4.selected) {
		objItem4.selected = false;
		objItemMgr.selected = true;
	}
	else if (objItem5.selected) {
		objItem5.selected = false;
		objItemMgr.selected = true;
	}
}


//up
if (keyboard_check_pressed(vk_up)) {
	if (objItem3.selected) {
		objItem3.selected = false;
		objItem1.selected = true;
	}
	else if (objItem4.selected) {
		objItem4.selected = false;
		objItem2.selected = true;
	}
	else if (objItemMgr.selected) {
		objItemMgr.selected = false;
		objItem3.selected = true;
	}
}
{
    "id": "bf13f51f-a0c6-4491-a01f-d833b0f7f6af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBar",
    "eventList": [
        {
            "id": "aac5f0c3-7ee4-4136-a4c4-f3f3d8aea356",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf13f51f-a0c6-4491-a01f-d833b0f7f6af"
        },
        {
            "id": "522c3213-3e8a-45a9-9620-9e9096e431d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf13f51f-a0c6-4491-a01f-d833b0f7f6af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
    "visible": true
}
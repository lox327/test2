hasInfo = true;

//DIALOG
dialog = [];
dialogLine = 0;
prox = 100;
name = "bar1";

//dialog tree, can be array?
d_bar0 = 
addDialog("you look like shit, what'd you get into last night?", true, avatarBar, false, self);
//player
addDialog("I...", true, avatarPlayer, true, self);
addDialog("I can't remember...", true, avatarPlayer, true, self);

addDialog("the east is blocked off unless you have a key.", true, avatarBar, false, self);
addDialog("you're gonna need to go west and clear that room to get the key.", true, avatarBar, false, self);
//player
addDialog("ok, and then you'll tell me what I need to know? ...", true, avatarPlayer, true, self);

addDialog("we'll see if you make it back...", false, avatarBar, false, self);


d_bar1 = 
addDialog("wow, you made it back. I'm surprised.", true, avatarBar, false, self);
addDialog("did you get the key?", true, avatarBar, false, self);
//player
addDialog("sure did. now tell me, what should I expect when I go east??", true, avatarPlayer, true, self);

addDialog("HELL...", false, avatarBar, false, self);


d_bar2 = 
addDialog("you've restored power to the station", false, avatarBar, false, self);


d_bar3 = 
addDialog("all done", false, avatarBar, false, self);
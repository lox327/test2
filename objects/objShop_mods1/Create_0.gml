///@description Shop Guy3, Mods n Stuff
///@description Shop Items, using sprite sheet: spr_sheet_name
event_inherited();

#region inv specific fields
spr_sheet_name = spr_items3;
spr_avatar_name = spr_shopguy3;
inv_name = "global.inv_name3";
itemShopTotal = ItemShop3.total;

shopName = "Cayde-6"
shopDesc = "Mods n Stuff";
shopText = shopName + " has lived many lives. As master gunsmith for The City, he supplies only the best.";

sep = "";
repeat(string_length(shopName)+1) {sep +=  "_"};
#endregion

//items to populate shop with
enum ItemShop3 {
	mod1,
	mod2,
	mod3,
	mod4,
	total
}

inv_name = ds_grid_create(itemShopTotal, stat.total);

#region add items to grid
inv_name[#ItemShop3.mod1, stat.name] = "Hellfire";
inv_name[#ItemShop3.mod1, stat.description] = "It goes MOD1!";
inv_name[#ItemShop3.mod1, stat.damage_low] = 0;
inv_name[#ItemShop3.mod1, stat.damage_high] = 0;
inv_name[#ItemShop3.mod1, stat.weaponEnergyUse] = 0;
inv_name[#ItemShop3.mod1, stat.weaponEnergyTotal] = 0;
inv_name[#ItemShop3.mod1, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop3.mod1, stat.armor] = 0;
inv_name[#ItemShop3.mod1, stat.mod1] = 0;
inv_name[#ItemShop3.mod1, stat.mod2] = 0;
inv_name[#ItemShop3.mod1, stat.modifier] = "fx";
inv_name[#ItemShop3.mod1, stat.mod_amount] = objMod_fire;
inv_name[#ItemShop3.mod1, stat.mod_desc] = "this mod will add burn damage to your bullets";
inv_name[#ItemShop3.mod1, stat.slow_duration] = 0;
inv_name[#ItemShop3.mod1, stat.slow_amount] = 0;
inv_name[#ItemShop3.mod1, stat.dot_duration] = 349;
inv_name[#ItemShop3.mod1, stat.dot_amount] = 3;
inv_name[#ItemShop3.mod1, stat.splash_rad] = 0;
inv_name[#ItemShop3.mod1, stat.splash_amount] = 0;
inv_name[#ItemShop3.mod1, stat.owned] = false;
inv_name[#ItemShop3.mod1, stat.available] = true;
inv_name[#ItemShop3.mod1, stat.equipped] = false;
inv_name[#ItemShop3.mod1, stat.type] = "Mod";
inv_name[#ItemShop3.mod1, stat.price] = 10;
inv_name[#ItemShop3.mod1, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop3.mod1, stat.spr_index] = ItemShop3.mod1;

inv_name[#ItemShop3.mod2, stat.name] = "Ion Storm";
inv_name[#ItemShop3.mod2, stat.description] = "It goes MOD3!";
inv_name[#ItemShop3.mod2, stat.damage_low] = 0;
inv_name[#ItemShop3.mod2, stat.damage_high] = 0;
inv_name[#ItemShop3.mod2, stat.weaponEnergyUse] = 0;
inv_name[#ItemShop3.mod2, stat.weaponEnergyTotal] = 0;
inv_name[#ItemShop3.mod2, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop3.mod2, stat.armor] = 0;
inv_name[#ItemShop3.mod2, stat.mod1] = 0;
inv_name[#ItemShop3.mod2, stat.mod2] = 0;
inv_name[#ItemShop3.mod2, stat.modifier] = "fx";
inv_name[#ItemShop3.mod2, stat.mod_amount] = objMod_shock;
inv_name[#ItemShop3.mod2, stat.mod_desc] = "this mod will add a slow effect to your bullets";
inv_name[#ItemShop3.mod2, stat.slow_duration] = 299;
inv_name[#ItemShop3.mod2, stat.slow_amount] = 0.5;
inv_name[#ItemShop3.mod2, stat.dot_duration] = 0;
inv_name[#ItemShop3.mod2, stat.dot_amount] = 0;
inv_name[#ItemShop3.mod2, stat.splash_rad] = 0;
inv_name[#ItemShop3.mod2, stat.splash_amount] = 0;
inv_name[#ItemShop3.mod2, stat.owned] = false;
inv_name[#ItemShop3.mod2, stat.available] = true;
inv_name[#ItemShop3.mod2, stat.equipped] = false;
inv_name[#ItemShop3.mod2, stat.type] = "Mod";
inv_name[#ItemShop3.mod2, stat.price] = 15;
inv_name[#ItemShop3.mod2, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop3.mod2, stat.spr_index] = ItemShop3.mod2;

inv_name[#ItemShop3.mod3, stat.name] = "Serpent";
inv_name[#ItemShop3.mod3, stat.description] = "It goes MOD3!";
inv_name[#ItemShop3.mod3, stat.damage_low] = 0;
inv_name[#ItemShop3.mod3, stat.damage_high] = 0;
inv_name[#ItemShop3.mod3, stat.weaponEnergyUse] = 0;
inv_name[#ItemShop3.mod3, stat.weaponEnergyTotal] = 0;
inv_name[#ItemShop3.mod3, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop3.mod3, stat.armor] = 0;
inv_name[#ItemShop3.mod3, stat.mod1] = 0;
inv_name[#ItemShop3.mod3, stat.mod2] = 0;
inv_name[#ItemShop3.mod3, stat.modifier] = "fx";
inv_name[#ItemShop3.mod3, stat.mod_amount] = objMod_acid;
inv_name[#ItemShop3.mod3, stat.mod_desc] = "this mod will add a corrision effect to your weapon";
inv_name[#ItemShop3.mod3, stat.slow_duration] = 0;
inv_name[#ItemShop3.mod3, stat.slow_amount] = 0;
inv_name[#ItemShop3.mod3, stat.dot_duration] = 0;
inv_name[#ItemShop3.mod3, stat.dot_amount] = 0;
inv_name[#ItemShop3.mod3, stat.splash_rad] = 500;
inv_name[#ItemShop3.mod3, stat.splash_amount] = 5;
inv_name[#ItemShop3.mod3, stat.owned] = false;
inv_name[#ItemShop3.mod3, stat.available] = true;
inv_name[#ItemShop3.mod3, stat.equipped] = false;
inv_name[#ItemShop3.mod3, stat.type] = "Mod";
inv_name[#ItemShop3.mod3, stat.price] = 15;
inv_name[#ItemShop3.mod3, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop3.mod3, stat.spr_index] = ItemShop3.mod3;

inv_name[#ItemShop3.mod4, stat.name] = "Mod4";
inv_name[#ItemShop3.mod4, stat.description] = "It goes MOD4!";
inv_name[#ItemShop3.mod4, stat.damage_low] = 0;
inv_name[#ItemShop3.mod4, stat.damage_high] = 0;
inv_name[#ItemShop3.mod4, stat.armor] = 0;
inv_name[#ItemShop3.mod4, stat.mod1] = 0;
inv_name[#ItemShop3.mod4, stat.mod2] = 0;
inv_name[#ItemShop3.mod4, stat.modifier] = "base";
inv_name[#ItemShop3.mod4, stat.mod_amount] = 1.8;
inv_name[#ItemShop3.mod4, stat.mod_desc] = "this mod will increase the weapons base stats";
inv_name[#ItemShop3.mod4, stat.slow_duration] = 0;
inv_name[#ItemShop3.mod4, stat.slow_amount] = 0;
inv_name[#ItemShop3.mod4, stat.dot_duration] = 0;
inv_name[#ItemShop3.mod4, stat.dot_amount] = 0;
inv_name[#ItemShop3.mod4, stat.splash_rad] = 0;
inv_name[#ItemShop3.mod4, stat.splash_amount] = 0;
inv_name[#ItemShop3.mod4, stat.owned] = false;
inv_name[#ItemShop3.mod4, stat.available] = true;
inv_name[#ItemShop3.mod4, stat.equipped] = false;
inv_name[#ItemShop3.mod4, stat.type] = "Mod";
inv_name[#ItemShop3.mod4, stat.price] = 20;
inv_name[#ItemShop3.mod4, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop3.mod4, stat.spr_index] = ItemShop3.mod4;

#endregion

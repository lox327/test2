{
    "id": "997535ba-2849-47c2-b694-2a6c4e34c849",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDoorBlock",
    "eventList": [
        {
            "id": "cb382362-11f3-44d9-9fc4-17cd9f8c7e6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "997535ba-2849-47c2-b694-2a6c4e34c849"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7e112a55-f75c-47df-9092-27fd121f56af",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "f76c95aa-8c9b-4c87-9943-91e445a10410",
    "visible": true
}
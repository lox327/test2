{
    "id": "1cdb0179-9159-4925-88bf-a49a2ab4e1c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPower",
    "eventList": [
        {
            "id": "a151114f-7438-4ce2-b6ad-c50a23f3c0d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1cdb0179-9159-4925-88bf-a49a2ab4e1c0"
        },
        {
            "id": "62ac54b5-ef8c-430d-a842-443128408a1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "1cdb0179-9159-4925-88bf-a49a2ab4e1c0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dc660d83-2c6f-4969-a243-303fad7f4859",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0944e57-27a1-42c2-b377-2fb896465088",
    "visible": true
}
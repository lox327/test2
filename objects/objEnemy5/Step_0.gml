event_inherited();

if (hp <= 0 && enemyState != EnemyState.DEAD) {
	//enemyState = EnemyState.DEAD;
	//timer = deadTime;
	sprite_index = enemy5Dead;
	
	
}

if (hp <= 0) {
		if (timer <= 0) instance_destroy();
}

//image_index = direction;
//event_user(0);//Choose a State
if (proximity(sightRange))
{
switch (enemyState) {
    #region MOVE
	case EnemyState.MOVE: 
    { 
		//SPRITE SEL based on dir towards player
		if (self.x > objPlayer.x) {
			sprite_index = enemy5Left;
			dir = "left";
		}
		else {
			sprite_index = enemy5Right;
			dir = "right";
		}
		
		//if player is directly above/below enemy, want to change sprite to up/down
		if ( abs(self.x - objPlayer.x) < 100 ) {
			if (self.y > objPlayer.y) {
				sprite_index = enemy5Up;
				dir = "up";
			}
			else {
				sprite_index = enemy5Down;
				dir = "down";
			}
		}
		
		
		//MOVE
		mp_potential_step_object(objPlayer.x, objPlayer.y, spd, objCollision);
		
		
		//ATTACK - based on distance, % from rand
		if (distance_to_object(objPlayer) < attackDistance) {
			enemyState = EnemyState.ATTACK1;
		}
		
		//CHASE - based on distance, % from rand
		else if (distance_to_object(objPlayer) < chaseRange) {
			//timer = 10;
			//enemyState = EnemyState.CHASE;
		}
        break; 
    } 
	#endregion
	
	#region EVADE
    case EnemyState.EVADE: 
    { 
		mp_potential_step_object(objPlayer.x, objPlayer.y, evadeSpeed, objCollision);
		sprite_index = enemy5Idle; //evade sprite/animation
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.IDLE;
        }
        break; 
    } 
	#endregion
	
	#region IDLE
	case EnemyState.IDLE: 
    { 
        //EVADE
		/*if (distance_to_object(objPlayer) < evadeDistance) {
			if (!evadeCheck) {
				evadeCheck = true;
				if (choose(0,1) == 1) { //evade 50% of the time
					timer = 30;
					enemyState = EnemyState.EVADE;
				}
			}
		}*/
		
		//EVADE when low health, should do by percentage; should evade always happen?
		if(hp <= evadeLimit) {
			if (distance_to_object(objPlayer) < evadeDistance) {
				if (!evadeCheck) {
					evadeCheck = true;
					if (choose(0,1) == 1) { //evade 50% of the time
						timer = 30;
						enemyState = EnemyState.EVADE;
					}
				}
			}
		}
		
		//do another action 
        mp_potential_step(objPlayer.x, objPlayer.y, 0, true);
		sprite_index = enemy5Idle;
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.MOVE;
			evadeCheck = false;
        }
        break; 
    } 
	#endregion
	
	#region ATTACK1_IDLE
	case EnemyState.ATTACK1_IDLE: 
    { 
        //do another action 
		if (timer <= 0) {
			//timer for fired bullet is up; if bullet/burst max has been reached, we IDLE. otherwise, shoot again
			if (self.currBullet < self.bulletMax1) {
				enemyState = EnemyState.ATTACK1;
			}
			else {
				currBullet = 0;		//reset bullet counter
				timer = choose(80,120);		//how long to IDLE for before MOVE
				enemyState = EnemyState.IDLE;
				sprite_index = enemy5Idle;

			}
		}
		
        break; 
    } 
	#endregion
	
	#region ATTACK2_IDLE
	case EnemyState.ATTACK2_IDLE: 
    { 
        //do another action 
		sprite_index = enemy5Idle;
		if (timer <= 0) {
			if (self.currBullet < self.bulletMax2) //timer for fired bullet is up; if bullet/burst max has been reached, we IDLE. otherwise, shoot again
			{ 
				enemyState = EnemyState.ATTACK2;
			}
			else {
				self.currBullet = 0;	//reset bullet counter
				timer = 100;			//how long to IDLE for before next burst
				enemyState = EnemyState.IDLE;
			}
		}
		
        break; 
    } 
	#endregion
	
	#region STUN
	case EnemyState.STUN: 
    { 
        //do another action 
		sprite_index = enemy2Stun;
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = choose(EnemyState.MOVE, EnemyState.IDLE);
        }
        break; 
    } 
	#endregion
	
	#region CHASE
	case EnemyState.CHASE: 
    { 
		//sprite_index = enemy5Attack;
		mp_potential_step(objPlayer.x, objPlayer.y, spd*2, true);
        timer = 100;
        enemyState = EnemyState.MOVE;
        break; 
    } 
	#endregion
	
	#region ATTACK1 burst fire attack
	case EnemyState.ATTACK1: 
    { 
		currBullet++;
			 if (dir = "left")	sprite_index = enemy5Attack1_left;
		else if (dir = "right") sprite_index = enemy5Attack1_right;
		else if (dir = "up")	sprite_index = enemy5Attack1_up;
		else if (dir = "down")	sprite_index = enemy5Attack1_down;
		
		instance_create_layer(self.x,self.y,"Instances",enemyBullet);
		
		timer = pulseTime; //time between bullets
        enemyState = EnemyState.ATTACK1_IDLE; //pause in between bullets to create burst fire effect
		
		break;
    }
	#endregion
	
	//must update to not use motion set
	//update to use script
	#region DEAD
	case EnemyState.DEAD:
	{
		
	//EnemyState_DEAD(enemy5Dead);
	
	motion_set(point_direction(objPlayer.x, objPlayer.y, x, y), spd*.5);
	if (timer <= 0)
        { 
			motion_set(0,0);
			instance_destroy();
			
			with (instance_create_layer(x,y,"Instances",objEnemyDead)) {
				id.sprite_index = enemy5Dead;
			}
        }
		
	}
	#endregion

} 
timer--;
}
{
    "id": "cb5169ad-500b-4c10-8cd7-5aa29d3131da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemy5",
    "eventList": [
        {
            "id": "7df598b3-aaad-41fd-a730-c96d9bbb6e01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cb5169ad-500b-4c10-8cd7-5aa29d3131da"
        },
        {
            "id": "4e7fcc37-156a-47b4-a15b-dbe86a586af3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "cb5169ad-500b-4c10-8cd7-5aa29d3131da"
        },
        {
            "id": "1c82ad55-a057-4372-9790-3b7e03f09189",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb5169ad-500b-4c10-8cd7-5aa29d3131da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0353dfc-c14d-417e-b283-64ff3ecacc3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
    "visible": true
}
//show_debug_message(hitAngle);
event_inherited();

if (hp <= 0 && enemyState != EnemyState.DEAD) {
	enemyState = EnemyState.DEAD;
	timer = deadTime;
	repeat (100) instance_create_layer(x, y, "Instances", objBlood);

}

//image_index = direction;
//event_user(0);//Choose a State
//if (abs(self.x - objPlayer.x) <= self.sightRange)
if (proximity(sightRange)) {
switch (enemyState) {
    #region MOVE
	case EnemyState.MOVE: 
    { 
		#region SPRITE SEL based on dir towards player
		if (self.x > objPlayer.x) {
			sprite_index = enemy2Left;
			dir = "left";
		}
		else {
			sprite_index = enemy2Right;
			dir = "right";
		}
		
		//if player is directly above/below enemy, want to change sprite to up/down
		if ( abs(self.x - objPlayer.x) < 15 ) {
			if (self.y > objPlayer.y) {
				sprite_index = enemy2Up;
				dir = "up";
			}
			else {
				sprite_index = enemy2Down;
				dir = "down";
			}
		}
		#endregion
		
		//MOVE
		mp_potential_step_object(objPlayer.x, objPlayer.y+25, spd, objCollision);
		
		//avoid colliding/overlapping with other enemy
		if (distance_to_object(enemyParent) > 20) {
			//timer = 10;
			//self.x += sign(spd);
			//enemyState = EnemyState.IDLE;
			//mp_potential_step(objPlayer.x, objPlayer.y, spd);

		}
		
		//ATTACK - based on distance, % from rand
		if (distance_to_object(objPlayer) < attackRange) {
			timer = 20; //reset timer to short num to represent attack length
			if (random(100) > 25) enemyState = EnemyState.ATTACK1;
			else enemyState = EnemyState.ATTACK1;
		}
		
		//CHASE - based on distance, % from rand
		else if (distance_to_object(objPlayer) < chaseRange) {
			//timer = 100;
			enemyState = EnemyState.CHASE;
		}
		
		//random time for next idle state
        if (timer <= 0) {
			timer = choose(25,50);
            enemyState = EnemyState.IDLE;
        }
        break; 
    } 
    #endregion
	
	#region IDLE
	case EnemyState.IDLE: 
    { 
        //return to MOVE after IDLE
		sprite_index = enemy2Idle;
		if (timer <= 0) { //switch action
			timer = 100;
            enemyState = EnemyState.MOVE;
        }
        break; 
    } 
	#endregion
	
	#region STUN
	case EnemyState.STUN: 
    { 
        //do another action 
		sprite_index = enemy2Stun;
		
		if (timer <= 0) { //switch action
            timer = 100;
            enemyState = choose(EnemyState.MOVE, EnemyState.IDLE);
        }
        break; 
    } 
	#endregion
	
	#region CHASE
	case EnemyState.CHASE: 
    { 
		//sprite_index = enemy2Attack;
		mp_potential_step_object(objPlayer.x, objPlayer.y+25, spd*2, objCollision);
        if (distance_to_object(objPlayer) < attackRange) {
			//timer = 20; //reset timer to short num to represent attack length
			if (random(100) > 25) enemyState = EnemyState.ATTACK1;
			else enemyState = EnemyState.ATTACK1;
		}
		
		else if (timer <= 0) { //switch action
            timer = 100;
            enemyState = EnemyState.IDLE;

        }
        break; 
    } 
	#endregion
	//need to combine attacks into one attack state to reduce duplicate code
	
	case EnemyState.ATTACK1: {
		//SPRITE SEL based on dir towards player
			 if (dir = "left")	sprite_index = enemy2Attack1_left;
		else if (dir = "right") sprite_index = enemy2Attack1_right;
		else if (dir = "up")	sprite_index = enemy2Attack1_up;
		else if (dir = "down")	sprite_index = enemy2Attack1_down;
		//show_debug_message(dir);
		EnemyState_ATTACK(sprite_index,dir);
		
        break; 
    }
	//bigger attack, more dmg + idle for longer?
	
	#region ATTACK2
	case EnemyState.ATTACK2: 
    { 
		//SPRITE SEL based on dir towards player
		/*	 if (dir = "left")	sprite_index = enemy2Attack1_left;
		else if (dir = "right") sprite_index = enemy2Attack1_right;
		else if (dir = "up")	sprite_index = enemy2Attack1_up;
		else if (dir = "down")	sprite_index = enemy2Attack1_down;*/
		EnemyState_ATTACK(sprite_index);
		
        break;  
    }
	#endregion
	
	#region HIT
	case EnemyState.HIT: {
		EnemyState_KNOCKBACK(enemy2Hit);
		break;	
	}
	#endregion
	
	#region DEAD
	//must update to not use motion set
	case EnemyState.DEAD: {
		if (timer <= 0) {
			instance_destroy();
			
			with (instance_create_layer(x,y,"Instances",objEnemyDead)) {
				id.sprite_index = enemy2Dead;
			}
	    }
		
		/*motion_set(point_direction(objPlayer.x, objPlayer.y, x, y), spd*.5);
		if (timer <= 0) {
			motion_set(0,0);
			instance_destroy();
			
			with (instance_create_layer(x,y,"Instances",objEnemyDead)) {
				id.sprite_index = enemy2Dead;
			}
	    }*/
	}
	#endregion
} 
timer--;
}
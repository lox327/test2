{
    "id": "7d562d56-cd30-4f8c-a360-7ce463876015",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTurret",
    "eventList": [
        {
            "id": "b413d3a3-4ce4-4875-ac66-65134cc4dc0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7d562d56-cd30-4f8c-a360-7ce463876015"
        },
        {
            "id": "570003d9-6bdd-4d81-94ae-ec1c78c947f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7d562d56-cd30-4f8c-a360-7ce463876015"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "dc660d83-2c6f-4969-a243-303fad7f4859",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b271a52-57fd-45d1-9dda-609f8f4f91da",
    "visible": true
}
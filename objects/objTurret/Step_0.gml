/// @description Insert description here
// You can write your code in this editor
//-burst fire
//-continue shooting while in range, timer
event_inherited();

//FIRE
if (proximity(range) && !isFiring) {
	image_speed = 2;
	isFiring = true;
	
}

//STOP FIRE
else if (!proximity(range) && isFiring) {
	//reverse animation
	image_speed = -2;
	isFiring = false;
}

//fire bullets at end of animation
if (isFiring) {
	//instance_create_layer(x,y,"Instances",enemyBullet);
	if (animation_end()) {
		image_speed = 0;
	}
	if (image_speed == 0 && timer <= 0) {
		instance_create_layer(x,y-25,"Instances",enemyBullet);
		currBullet--;
		
		if (currBullet == 0) {
			timer = bulletRest;
			currBullet = bulletMax;
		}
		else timer = burstDelay;
	}
}
//stop animating for rev back to index = 0
else if (image_speed < 0) {
	if (image_index <.1) image_speed = 0;
}
timer--;
{
    "id": "64cb9e95-de9c-4e79-be17-9451f287b529",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBashBox",
    "eventList": [
        {
            "id": "4e9377c2-9fe5-4e4a-b33d-d751005ad784",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "64cb9e95-de9c-4e79-be17-9451f287b529"
        },
        {
            "id": "e5bb608c-7673-46ce-80ac-02f2967d7b6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dc660d83-2c6f-4969-a243-303fad7f4859",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "64cb9e95-de9c-4e79-be17-9451f287b529"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38a55b98-ce3c-45e3-be56-00e45726d0d8",
    "visible": true
}
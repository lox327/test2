{
    "id": "51927fcc-0776-4d8a-8f3f-090dbc13d459",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLight21",
    "eventList": [
        {
            "id": "68f2f349-7276-4978-9d53-d2f5e61ca01f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "51927fcc-0776-4d8a-8f3f-090dbc13d459"
        },
        {
            "id": "14c72f7d-f90f-4b84-954d-78c2b56435bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 9,
            "m_owner": "51927fcc-0776-4d8a-8f3f-090dbc13d459"
        },
        {
            "id": "6cd74112-1e6b-48f3-8b88-4114d6124c16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 9,
            "m_owner": "51927fcc-0776-4d8a-8f3f-090dbc13d459"
        },
        {
            "id": "b159d4a6-0cef-4d0f-9518-f5016f193529",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "51927fcc-0776-4d8a-8f3f-090dbc13d459"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "24cf4884-af7c-4b5c-af49-83638c278d82",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
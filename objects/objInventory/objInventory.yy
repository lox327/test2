{
    "id": "df511ddf-144a-4603-98d5-463b2e83f776",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objInventory",
    "eventList": [
        {
            "id": "a0ba5a4b-bd71-4410-a143-8a6fbb1e9cd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df511ddf-144a-4603-98d5-463b2e83f776"
        },
        {
            "id": "18702baa-64a5-4de0-9e8d-f1528bd4a893",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 36,
            "eventtype": 9,
            "m_owner": "df511ddf-144a-4603-98d5-463b2e83f776"
        },
        {
            "id": "c55c34f3-f01b-4d15-b1b7-b8ea9075530b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "df511ddf-144a-4603-98d5-463b2e83f776"
        },
        {
            "id": "9b9fbabe-b6f3-4a6e-bafe-9fb30168bd8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "df511ddf-144a-4603-98d5-463b2e83f776"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
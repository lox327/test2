#region xy coords
display = false;
depth = objPlayer.depth-9999;

inv_slots = 17;
inv_slots_width = 8;
inv_slots_height = 3;

spr_inv_UI = spr_inv_ui;
cell_size = 32;
inv_UI_width = 288;
inv_UI_height = 192;

gui_width = display_get_gui_width(); //need to fix
gui_height = display_get_gui_height(); //need to fix

default_zoom_width = camera_get_view_width(view_camera[0]);
default_zoom_height = camera_get_view_height(view_camera[0]);

inv_UI_x = gui_width*2 - inv_UI_width/2; //need to fix
inv_UI_y = gui_height*2 - inv_UI_height/2; //need to fix

info_x = objPlayer.x+10;
info_y = objPlayer.y;

slots_x = info_x;
slots_y = info_y + 40;

spr_inv_items_cols = sprite_get_width(spr_inventory_items)/cell_size;
spr_inv_items_rows = sprite_get_height(spr_inventory_items)/cell_size;
#endregion


#region data structure stuff
//player info
//0 - GOLD
//1 - SILVER
//2 - COPPER
//3 - NAME

//create
ds_player_info = ds_grid_create(2,4);

//fields
ds_player_info[# 0, 0] = "Gold";
ds_player_info[# 0, 1] = "Silver";
ds_player_info[# 0, 2] = "Copper";
ds_player_info[# 0, 3] = "Name";

//values
ds_player_info[# 1, 0] = irandom_range(0,5);	//gold
ds_player_info[# 1, 1] = irandom_range(0,50);	//silver
ds_player_info[# 1, 2] = irandom_range(0,99);	//copper
ds_player_info[# 1, 3] = "Mario1";				//name

//inv info
//0 - ITEM
//1 - AMOUNT

//create
ds_inventory = ds_grid_create(2,inv_slots);


//inv items
enum itemm {
	none	= 0,
	tomato	= 1,
	potato	= 2,
	carrot	= 3,
	chili	= 4,
	bucket	= 5,
	onion	= 6,
	apple	= 7,
	peach	= 8,
	glue	= 9,
	height	= 10,
}

//add an item, hard code 
//ds_inventory[# 0, 0] = item.carrot;
//ds_inventory[# 1, 0] = 2;

//add an item, random
for (var i = 0; i < 10; i++) {
	ds_inventory[# 0, i] = irandom_range(1,itemm.height-1);
	ds_inventory[# 1, i] = irandom_range(1,5);
}
#endregion
//add an item as you buy it or walk over it

//info_x = 
//info_y =

show_debug_message("ds")
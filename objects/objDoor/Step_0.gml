/// @description Insert description here
// You can write your code in this editor
/// @description block for doors that open depending on game state
//...passed in from creation code
//show_debug_message(checkpoint);
if ( global.checkpoint1 ) instance_destroy(self);
depth = objPlayer.depth + 1;

if (global.btnAction) {
	
	if (abs(objPlayer.x - x ) > prox) return;
	if (abs(objPlayer.y - y ) > prox) return;

	image_speed = 3;	
}

if (image_index == sprite_get_number(sprite_index) ) instance_destroy();
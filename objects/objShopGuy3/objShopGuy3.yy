{
    "id": "f436f533-3511-47ff-a1fd-c1e68929b1e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShopGuy3",
    "eventList": [
        {
            "id": "025c6fa2-9520-41a2-8f05-b4806eeace9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f436f533-3511-47ff-a1fd-c1e68929b1e9"
        },
        {
            "id": "21c3f710-87fa-4e8f-a255-501521271354",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f436f533-3511-47ff-a1fd-c1e68929b1e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1895ab3e-de03-489b-879e-f1431c33cba5",
    "visible": true
}
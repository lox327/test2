{
    "id": "871b0f5f-29a2-41f2-aa6c-f81a8dfbde92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShopGuy5",
    "eventList": [
        {
            "id": "97944eac-6e20-49a9-8883-484fdd39ef59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "871b0f5f-29a2-41f2-aa6c-f81a8dfbde92"
        },
        {
            "id": "805d684a-4312-4a42-98ac-81f2bd1fe28a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "871b0f5f-29a2-41f2-aa6c-f81a8dfbde92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fcfbb12a-c950-42e7-84d8-84f0a4c2df25",
    "visible": true
}
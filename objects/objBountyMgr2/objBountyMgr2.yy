{
    "id": "0f67f611-3c2c-4bcb-a80d-d9a00616836f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBountyMgr2",
    "eventList": [
        {
            "id": "20d45e0d-176a-408a-be61-1534514aec30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0f67f611-3c2c-4bcb-a80d-d9a00616836f"
        },
        {
            "id": "1564c56f-dcd7-4cda-a25d-884fea7cd117",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "0f67f611-3c2c-4bcb-a80d-d9a00616836f"
        },
        {
            "id": "8d555986-5af6-4847-ac2f-724272bc1f62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0f67f611-3c2c-4bcb-a80d-d9a00616836f"
        },
        {
            "id": "ac017b3b-03bf-4055-b2b0-b9412a4d289a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "0f67f611-3c2c-4bcb-a80d-d9a00616836f"
        },
        {
            "id": "789451d4-7e70-480c-b6f3-0216ab2d617d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 9,
            "m_owner": "0f67f611-3c2c-4bcb-a80d-d9a00616836f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e399d58f-9add-412f-a080-fdea66996e3f",
    "visible": true
}
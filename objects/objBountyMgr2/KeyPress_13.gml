/// @description manage action button

//if (global.b_checkpoint1)

//EXIT
if ( objBountyMgr2.selected ) {
	//code for exiting room
	room_goto(global.currentRoom);
	objBounty1.selected = true;
	objPlayer.x = prevX; //need to capture prev x,y correctly
	objPlayer.y = prevY;
	
	objPlayer.playerMove = true;
	exit;
}

//only want to set bounty to active if no other bounties are active
for (var i = 0; i < ds_list_size(global.bounties); i++) {
	val = ds_list_find_value(global.bounties, i);
	var active = ds_map_find_value(val, "active");
	var complete = ds_map_find_value(val, "complete");
	if (active && !complete) exit;
	else if (!active && complete) exit;
}

/*
//should we have more than 1 bounty at a time?
if (ds_list_size(global.inv) >= 3) {
	show_message("inv full");
	exit;
}
*/

//for now, only allowing one active bounty at a time
//need for loop to block updating bounties to active
//if (ds_list_find_value(global.bounties, index)) return;

//update (add) bounty list based on which bounty was selected
var index;
if ( objBounty1.selected ) {
	index = 0;
}

else if ( objBounty2.selected ) {
	index = 1;
}

else if ( objBounty3.selected ) {
	index = 2;
}

val = ds_list_find_value(global.bounties, index);	//get the whole map at index of bounties list

//update map
if (val[? "active"] == false) {
	val[? "active"] = true;
	objPlayer.hasBounty = true;
}
//else val[? "active"] = false;

if (val[? "complete"] == true && val[? "active"] == true ) {
	val[? "active"] = false;
	objPlayer.invCredits += val[? "reward"];		//add reward to player
	objPlayer.hasBounty = false;
}
ds_list_set(global.bounties, index, val);			//update bounties list



//update player bounty inv with global bounty
//do we need to do this if only 1 bounty active at a time? maybe not...
//ds_list_add(objPlayer.playerBounties, ds_list_find_value(global.bounties, index));

//data check
//val = ds_list_find_value(global.bounties, index);
//show_message(val);
//show_message(ds_list_size(objPlayer.playerBounties));
/// @description Insert description here
// You can write your code in this editor
//if (global.m_checkpoint1) {
if (distance_to_object(enemyParent) > threshold) {
	instance_destroy();
	instance_deactivate_layer("Barriers");
}

//need to only do this once...
//simplify
//activate trigger depending on which side char is approaching from
if (leftApproach) {
	if (!triggered && objPlayer.x > x) {
		triggered = true;
		instance_activate_layer("Barriers");
	
		//var i = instance_create_layer(x-100,y,"Instances",objBarrier);
		//i.image_angle = 320;
		//var ii = instance_create_layer(x+300,y,"Instances",objBarrier);
		//ii.image_angle = 120;
	}

} else if (!triggered && objPlayer.x < x) {
		triggered = true;
		instance_activate_layer("Barriers");
	
		//var i = instance_create_layer(x-100,y,"Instances",objBarrier);
		//i.image_angle = 320;
		//var ii = instance_create_layer(x+300,y,"Instances",objBarrier);
		//ii.image_angle = 120;
	}
/// @description lerp func for shotgun bullets to drag and disappear
var kb_friction = 0.05;
speed = lerp(speed, 0, kb_friction);

if (speed > -1.5) {
	instance_destroy();
}
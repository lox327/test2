{
    "id": "dc756be1-4176-4181-acbf-bd32c967c354",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBulletShotgun",
    "eventList": [
        {
            "id": "1075078c-94b8-40fd-8904-442dc67b68ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc756be1-4176-4181-acbf-bd32c967c354"
        },
        {
            "id": "41cdacd8-10cb-4be7-96c4-10015cebc473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc756be1-4176-4181-acbf-bd32c967c354"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7af82463-cca2-45a9-87e3-98eb03ace7c9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "daaf660d-4f56-454c-bc17-da0bf2f59fee",
    "visible": true
}
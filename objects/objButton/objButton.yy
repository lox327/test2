{
    "id": "13e71b96-58a8-47ed-ac40-bc6ecac498a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objButton",
    "eventList": [
        {
            "id": "cbbc6290-0bbd-42cf-bad7-0f8445e2b323",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13e71b96-58a8-47ed-ac40-bc6ecac498a9"
        },
        {
            "id": "2909b8a4-fef6-40d5-9d7c-d1922f654cb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "13e71b96-58a8-47ed-ac40-bc6ecac498a9"
        },
        {
            "id": "b746543b-8933-49c6-a84a-98f98ccde0ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "13e71b96-58a8-47ed-ac40-bc6ecac498a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d9f3746-45c7-42ec-afe5-7bc7604df280",
    "visible": true
}
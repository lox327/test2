/// @description Insert description here
// You can write your code in this editor
if (position_meeting(mouse_x, mouse_y, id)) image_alpha = 0.8;
else image_alpha = 1;

if (position_meeting(mouse_x, mouse_y, id) && mouse_check_button(mb_left)) {
	//show_message("mouse");
	if (item_id == "item1") health += 20;
	ds_list_delete(global.inv2, inv_id);
	
	if (instance_exists(objButton)) with (objButton) instance_destroy();
	objMenu.active = false;
}
{
    "id": "b5d5993d-2a2e-4ea3-a438-77ee3d53f071",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objParent",
    "eventList": [
        {
            "id": "8218ad53-3426-4e85-abae-7ac712333234",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b5d5993d-2a2e-4ea3-a438-77ee3d53f071"
        },
        {
            "id": "b3382fbf-42a7-457e-82f6-ff9f9a68352d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b5d5993d-2a2e-4ea3-a438-77ee3d53f071"
        },
        {
            "id": "05e550c5-a19b-47d6-a270-f2b32aba764b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "762cde50-f7cd-4924-89c4-3277b50303cd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b5d5993d-2a2e-4ea3-a438-77ee3d53f071"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
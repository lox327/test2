/// @description parent contains collision code, should be switch on type; type comes from create of each item
switch(type) {
	case "energy":
		if (objPlayer.weaponEnergy < objPlayer.weaponEnergyMax) {
			var diff = objPlayer.weaponEnergyMax - objPlayer.weaponEnergy;
			if (diff < objEnergy.value) objPlayer.weaponEnergy += diff;
			else objPlayer.weaponEnergy += objEnergy.value;	//need to add formula for adding in between values for < 25
		}
		break;
	
	case "key1":
		global.checkpoint1 = 1;
		break;
	case "key2":
		global.checkpoint2 = 1;
		break;
	case "key3":
		global.checkpoint3 = 1;
		break;	
	
	case "health":
		if (health < objPlayer.healthMax) {
			var diff = objPlayer.healthMax - health;
			if (diff < objHealth.value) health += diff;
			else health += objHealth.value;	//need to add formula for adding in between values for < 25
		}
		break;
	
	case "artifact":
		//if (objPlayer.currency < objPlayer.currencyMax) objPlayer.currency += 25;	//need to add formula for adding in between values for < 25
		objPlayer.invCredits += objArtifact.value;
		break;
		
	default:
		//add to inv?
		show_message("added to inv");
		break;
	
}

instance_destroy();
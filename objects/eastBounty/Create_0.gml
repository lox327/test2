///@description get active status for bounty, activate elements
val = ds_list_find_value(global.bounties, 2);

if (ds_map_find_value(val, "active")) {
	objEnemy1.isActive = true;
	instance_create_layer(300, 300,"Instances",objTank);
	//...more bounty objs set to isActive true here as well?
	//...unlock doors?
	//...place other world items?
}
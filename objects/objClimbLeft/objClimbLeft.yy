{
    "id": "df220df7-bc0a-4692-8cd9-65c2734926d3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objClimbLeft",
    "eventList": [
        {
            "id": "60f8c36d-1ab2-49f1-8820-4ae5beb8295f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df220df7-bc0a-4692-8cd9-65c2734926d3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "36bf373c-5420-4e9c-8cae-93cb1d62afae",
    "visible": false
}
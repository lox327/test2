{
    "id": "8e95694f-14a9-4b71-a4a1-d4f85129f691",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShopGuy6",
    "eventList": [
        {
            "id": "c221c40a-852b-4c22-8f4d-c219de029237",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e95694f-14a9-4b71-a4a1-d4f85129f691"
        },
        {
            "id": "ba8d1c82-b251-4161-a781-d0ed44c105e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e95694f-14a9-4b71-a4a1-d4f85129f691"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "68c47d7e-3234-424e-8eb2-6ea0653a8a90",
    "visible": true
}
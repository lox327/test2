event_inherited();

if (hp <= 0 && enemyState != EnemyState.DEAD) {
	enemyState = EnemyState.DEAD;
	timer = deadTime;
	
}
//image_index = direction;
//event_user(0);//Choose a State
if (proximity(sightRange))
{
switch (enemyState) {
    case EnemyState.MOVE: 
    { 
		//SPRITE SEL based on dir towards player
		if (self.x > objPlayer.x) sprite_index = enemy4Left;
		else sprite_index = enemy4Right;
		if (self.y > objPlayer.y) sprite_index = enemy4Up;
		else sprite_index = enemy4Down;
		
		//MOVE
		mp_potential_step_object(objPlayer.x, objPlayer.y, spd, objCollision);
		
		
		//ATTACK - based on distance, % from rand
		if (distance_to_object(objPlayer) < attackDistance) {
			//timer = 100;
			//enemyState = EnemyState.ATTACK2;
			var num = random(100);
			if ( num > 50) enemyState = EnemyState.ATTACK3_CHARGE;
			//else if (num > 50)  enemyState = EnemyState.ATTACK2;
			else enemyState = EnemyState.ATTACK1;
			//enemyState = EnemyState.ATTACK4;
		}
		
		//CHASE - based on distance, % from rand
		else if (distance_to_object(objPlayer) < chaseRange) {
			//timer = 10;
			//enemyState = EnemyState.CHASE;
		}
        break; 
    } 
	
    case EnemyState.EVADE: 
    { 
		mp_potential_step_object(objPlayer.x, objPlayer.y, evadeSpeed, objCollision);
		sprite_index = enemy4Idle; //evade sprite/animation
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.IDLE;
        }
        break; 
    } 
	
	case EnemyState.IDLE: 
    { 
        //EVADE
		/*if (distance_to_object(objPlayer) < evadeDistance) {
			if (!evadeCheck) {
				evadeCheck = true;
				if (choose(0,1) == 1) { //evade 50% of the time
					timer = 30;
					enemyState = EnemyState.EVADE;
				}
			}
		}*/
		
		//EVADE when low health, should do by percentage; should evade always happen?
		if(hp <= evadeLimit) {
			if (distance_to_object(objPlayer) < evadeDistance) {
				if (!evadeCheck) {
					evadeCheck = true;
					if (choose(0,1) == 1) { //evade 50% of the time
						timer = 30;
						enemyState = EnemyState.EVADE;
					}
				}
			}
		}
		
		//do another action 
        mp_potential_step(objPlayer.x, objPlayer.y, 0, true);
		sprite_index = enemy4Idle;
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.MOVE;
			evadeCheck = false;
        }
        break; 
    } 
	case EnemyState.ATTACK1_IDLE: 
    { 
        //do another action 
		sprite_index = enemy4Idle;
		if (timer <= 0) {
			//timer for fired bullet is up; if bullet/burst max has been reached, we IDLE. otherwise, shoot again
			if (self.currBullet < self.bulletMax1) {
				enemyState = EnemyState.ATTACK1;
			}
			else {
				currBullet = 0;		//reset bullet counter
				timer = 100;		//how long to IDLE for before MOVE
				enemyState = EnemyState.IDLE;
			}
		}
		
        break; 
    } 
	case EnemyState.ATTACK2_IDLE: 
    { 
        //do another action 
		sprite_index = enemy4Idle;
		if (timer <= 0) {
			if (self.currBullet < self.bulletMax2) //timer for fired bullet is up; if bullet/burst max has been reached, we IDLE. otherwise, shoot again
			{ 
				enemyState = EnemyState.ATTACK2;
			}
			else {
				self.currBullet = 0;	//reset bullet counter
				timer = 100;			//how long to IDLE for before next burst
				enemyState = EnemyState.IDLE;
			}
		}
		
        break; 
    } 
	case EnemyState.STUN: 
    { 
        //do another action 
		sprite_index = enemy2Stun;
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = choose(EnemyState.MOVE, EnemyState.IDLE);
        }
        break; 
    } 
	case EnemyState.CHASE: 
    { 
		//sprite_index = enemy4Attack;
		mp_potential_step(objPlayer.x, objPlayer.y, spd*2, true);
        timer = 100;
        enemyState = EnemyState.MOVE;
        break; 
    } 
	
	//burst fire attack
	case EnemyState.ATTACK1: 
    { 
		currBullet++;
		sprite_index = enemy4Attack1;
		instance_create_layer(self.x,self.y,"Instances",enemyBullet);
		
		timer = pulseTime; //time between bullets
        enemyState = EnemyState.ATTACK1_IDLE; //pause in between bullets to create burst fire effect
		
		break;
    }
	case EnemyState.ATTACK2: 
    { 
		self.currBullet++;
		sprite_index = enemy4Attack2;
		instance_create_layer(self.x,self.y,"Instances",enemyBullet2);
		
		timer = 25; //time between bullets
        enemyState = EnemyState.ATTACK2_IDLE; //pause in between bullets to create burst fire effect
		
		break;
		
    }
	
	//charge big boi attack, moves to ATTACK3 when done charging
	case EnemyState.ATTACK3_CHARGE: 
    { 
		sprite_index = enemy4Attack3_charge;
		
        if (animation_end(sprite_index)) //switch action
        { 
            enemyState = EnemyState.ATTACK3;
        }
		
		break;
		
    }
	//big boi attack
	case EnemyState.ATTACK3: 
    { 
		sprite_index = enemy4Attack2;
		instance_create_layer(self.x,self.y,"Instances",enemyBullet3);
		
		timer = chargeCooldown; //time between attack
        enemyState = EnemyState.IDLE;
		
		break;
		
    }

	
	//must update to not use motion set
	//update to use script
	case EnemyState.DEAD:
	{
		
	//EnemyState_DEAD(enemy4Dead);
	show_message("dead")
	instance_destroy();
			
			with (instance_create_layer(x,y,"Instances",objEnemyDead)) {
				id.sprite_index = enemy4Dead;
			}
			exit;
	motion_set(point_direction(objPlayer.x, objPlayer.y, x, y), spd*.5);
	if (timer <= 0)
        { 
			motion_set(0,0);
			instance_destroy();
			
			with (instance_create_layer(x,y,"Instances",objEnemyDead)) {
				id.sprite_index = enemy4Dead;
			}
        }
		
	}
} 
timer--;
}
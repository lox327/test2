{
    "id": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemyBulletParent",
    "eventList": [
        {
            "id": "c4e0af51-7962-4e9b-a083-8a0aa45347a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59a2ee32-5e01-48ac-8de2-3c48733daaff",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335"
        },
        {
            "id": "9ee6563f-b920-4f14-9cec-d1e1f97390a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e112a55-f75c-47df-9092-27fd121f56af",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335"
        },
        {
            "id": "ae219fd2-2a4d-4491-9015-3b21dd0bd603",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335"
        },
        {
            "id": "342c0446-e376-45b5-a32c-6f0468cadeb2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dc660d83-2c6f-4969-a243-303fad7f4859",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
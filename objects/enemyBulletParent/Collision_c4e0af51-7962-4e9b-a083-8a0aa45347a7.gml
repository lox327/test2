/// @description hitmask for full sprite of player
health -= bulletDmg;

objPlayer.state = playerState.HIT;
objPlayer.hitAngle = point_direction(enemyBulletParent.x, enemyBulletParent.y, objPlayer.x, objPlayer.y);
show_debug_message(objPlayer.hitAngle)

instance_destroy();
{
    "id": "14a51c0d-d6fa-40d6-8b7a-c1e54fe2e454",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBashBox2a",
    "eventList": [
        {
            "id": "62e5ecc4-9c3b-414e-9fd1-8a788a7e0438",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "14a51c0d-d6fa-40d6-8b7a-c1e54fe2e454"
        },
        {
            "id": "d6cd03f2-d5bf-4130-99ef-ddbb658aff95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dc660d83-2c6f-4969-a243-303fad7f4859",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "14a51c0d-d6fa-40d6-8b7a-c1e54fe2e454"
        },
        {
            "id": "b4fa1b63-f0c0-465a-8bec-e63cebc8b764",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c0353dfc-c14d-417e-b283-64ff3ecacc3b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "14a51c0d-d6fa-40d6-8b7a-c1e54fe2e454"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4f19fbfa-967a-4c1d-8293-97fe21a08531",
    "visible": true
}
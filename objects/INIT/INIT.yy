{
    "id": "2db64dd8-6cb7-4428-b80e-94b473d0def7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "INIT",
    "eventList": [
        {
            "id": "b7c1625c-7777-43fa-858d-ebdf3f825247",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 48,
            "eventtype": 9,
            "m_owner": "2db64dd8-6cb7-4428-b80e-94b473d0def7"
        },
        {
            "id": "859c55ca-846c-4c86-92c5-5eeb6a72d6b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2db64dd8-6cb7-4428-b80e-94b473d0def7"
        },
        {
            "id": "4763d3ef-16e4-4c1a-bdcc-7a8d0884bbbb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2db64dd8-6cb7-4428-b80e-94b473d0def7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
{
    "id": "ce93f93f-84af-4b7f-b0e8-9f8bb156fe82",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTemp1",
    "eventList": [
        {
            "id": "5f2cdca4-4c81-421d-a32b-193dcc072507",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce93f93f-84af-4b7f-b0e8-9f8bb156fe82"
        },
        {
            "id": "ea59125b-224f-4d53-860b-a6a953b72ddf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce93f93f-84af-4b7f-b0e8-9f8bb156fe82"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7e112a55-f75c-47df-9092-27fd121f56af",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5479fa57-7291-41d0-8050-ba9894976f1a",
    "visible": true
}
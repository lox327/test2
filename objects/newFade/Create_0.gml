guiWidth = display_get_gui_width();
guiHeight = display_get_gui_height();

blackAlpha = 0;
//higher = faster
fadeSpeedSlow = 0.0100;
fadeSpeedFast = 0.0250;

spawnRoom = -1;
spawnX = 0;
spawnY = 0;
doTransition = false;

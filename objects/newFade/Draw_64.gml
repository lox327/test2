/// @description 
if(doTransition){
	//fade out
	if(room != spawnRoom){
		//blackAlpha += (isBuilding2()) ? fadeSpeedFast : fadeSpeedSlow; //condition ? exprT : exprF
		blackAlpha += 1;
		if(blackAlpha >= 1){ room_goto(spawnRoom) }
	//fade out
	} else {
		//blackAlpha -= (isBuilding2()) ? fadeSpeedFast : fadeSpeedSlow; //condition ? exprT : exprF;
		blackAlpha -= 1;
		if(blackAlpha <= 0){ doTransition = false; objPlayer.playerMove = true;}
	}
	
	//Draw Black Fade
	draw_set_alpha(blackAlpha);
	draw_rectangle_colour(0,0, guiWidth, guiHeight, c_black, c_black, c_black, c_black, false);
	draw_set_alpha(1);
}

//camera.x = objPlayer.x;
//camera.y = objPlayer.y;
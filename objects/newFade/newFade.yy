{
    "id": "74f56176-dcb1-4d36-84d3-32b38eba6c43",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "newFade",
    "eventList": [
        {
            "id": "8ceed7ae-30d2-4b38-9623-36eacd61ebd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74f56176-dcb1-4d36-84d3-32b38eba6c43"
        },
        {
            "id": "68a1b011-06fb-4777-991c-048688f658a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "74f56176-dcb1-4d36-84d3-32b38eba6c43"
        },
        {
            "id": "a09491cd-c3e9-4720-bcd5-050690910376",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "74f56176-dcb1-4d36-84d3-32b38eba6c43"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
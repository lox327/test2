/// @description map for storing bounties

//list for storing item objs
global.items = ds_list_create();

//map of bounties to store in list
var item = ds_map_create();
ds_map_add(item, "name",	"item1");
ds_map_add(item, "wanted",	"handsome jack");	//nickname
ds_map_add(item, "wanted_name","sparrow, jack");	//full name
ds_map_add(item, "reward",	50);
ds_map_add(item, "desc",	"1...is wanted for: blah");
ds_map_add(item, "extDesc",	"1...is skilled in blah");
ds_map_add(item, "difficulty","medium");
ds_map_add(item, "location",	"north");
ds_map_add(item, "active",	false); //item is active
ds_map_add(item, "complete",	false);	//item hasbeen completed
ds_map_add(item, "turnin",	false);	//item has been turned in
ds_map_add(item, "spriteAvatar", itemAvatar1);
ds_map_add(item, "spriteitem", item1);
ds_list_add(global.items, item);

item = ds_map_create();
ds_map_add(item, "name",	"item2");
ds_map_add(item, "wanted",	"ugly sam");
ds_map_add(item, "wanted_name","durden, sam");
ds_map_add(item, "reward",	35);
ds_map_add(item, "desc",	"2...is wanted for: mehh");
ds_map_add(item, "extDesc",	"2...is skilled in mehh");
ds_map_add(item, "difficulty","easy");
ds_map_add(item, "location",	"south");
ds_map_add(item, "active",	false);
ds_map_add(item, "complete",	false);
ds_map_add(item, "turnin",	false);
ds_map_add(item, "spriteAvatar", itemAvatar2);
ds_map_add(item, "spriteitem", item2);
ds_list_add(global.items, item);

item = ds_map_create();
ds_map_add(item, "name",	"item3");
ds_map_add(item, "wanted",	"dirty marv");
ds_map_add(item, "wanted_name","cobb, marv");
ds_map_add(item, "reward",	75);
ds_map_add(item, "desc",	"3...is wanted for: murr");
ds_map_add(item, "extDesc",	"3...is skilled in murr");
ds_map_add(item, "difficulty","easy");
ds_map_add(item, "location",	"south2");
ds_map_add(item, "active",	false);
ds_map_add(item, "complete",	false);
ds_map_add(item, "turnin",	false);
ds_map_add(item, "spriteAvatar", itemAvatar3);
ds_map_add(item, "spriteitem", item3);
ds_list_add(global.items, item);

//val = ds_list_find_value(bounties, 1);
//show_message(ds_map_find_value(val, "name"));
//show_message(ds_list_find_value(bounties, 2));
event_inherited();

hp = irandom_range(4,6);
hpMax = hp;
spd = 1.0;
flash = 0;

state = "Inactive";
isActive = false; //bounty enemy only active when bounty is active
sightRange = 350;
aggressiveness = 0;
attackRange = sprite_width/2 + objPlayer.sprite_width/2;


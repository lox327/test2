if (!isActive) return;

if (hp <= 0)  instance_destroy();
//image_index = direction;
//event_user(0);//Choose a State
if (abs(self.x - objPlayer.x) <= self.sightRange)
{
switch (enemyState) {
    case EnemyState.MOVE: 
    { 
        //MOVE
		mp_potential_step(objPlayer.x, objPlayer.y, spd, true);
		
		//SPRITE SEL
		if (self.x > objPlayer.x) sprite_index = enemy1Left;
		else sprite_index = enemy1Right;
		if (self.y > objPlayer.y) sprite_index = enemy1Up;
		else sprite_index = enemy1Down;
		
		//avoid colliding/overlapping with other enemy
		if (distance_to_object(enemyParent) > 20) {
			//timer = 10;
			//self.x += sign(spd);
			//enemyState = EnemyState.IDLE;
			//mp_potential_step(objPlayer.x, objPlayer.y, spd);

		}
		
		//ATTACK - based on distance, % from rand
		if (distance_to_object(objPlayer) < 50) {
			timer = 20; //reset timer to short num to represent attack length
			if (random(100) > 25) enemyState = EnemyState.ATTACK2;
			else enemyState = EnemyState.ATTACK1;
		}
		
		//CHASE - based on distance, % from rand
		else if (distance_to_object(objPlayer) < 100) {
			//timer = 10;
			//enemyState = EnemyState.CHASE;
		}
		
		
        if (timer <= 0) {
		//if (distance_to_object(objPayer) > 128)	enemyState = EnemyState.IDLE;
            if (random(100) > 50) timer = 20;
			else timer = 50;
            enemyState = EnemyState.IDLE;
        }
        break; 
    } 
    case EnemyState.IDLE: 
    { 
        //do another action 
        mp_potential_step(objPlayer.x, objPlayer.y, 0, true);
		sprite_index = enemy1Idle;
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.MOVE;
        }
        break; 
    } 
	case EnemyState.STUN: 
    { 
        //do another action 
        mp_potential_step(objPlayer.x, objPlayer.y, 0, true);
		sprite_index = enemy1Stun;
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.MOVE;
        }
        break; 
    } 
	case EnemyState.CHASE: 
    { 
		//sprite_index = enemy1Attack;
		mp_potential_step(objPlayer.x, objPlayer.y, spd*2, true);
        timer = 100;
        enemyState = EnemyState.MOVE;
        break; 
    } 
	
	case EnemyState.ATTACK1: 
    { 
		sprite_index = enemy1Attack1;
		mp_potential_step_object(objPlayer.x, objPlayer.y, spd*5, self);
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.IDLE;

        }
        break; 
    }
	//bigger attack, more dmg + idle for longer?
	case EnemyState.ATTACK2: 
    { 
		sprite_index = enemy1Attack2;
		mp_potential_step_object(objPlayer.x, objPlayer.y, spd*5, self);
		if (timer <= 0) //switch action
        { 
            timer = 150;
            enemyState = EnemyState.IDLE;

        }
        break; 
    }
} 
timer--;
}
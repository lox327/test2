/// @description bounty boss
/*if (random(100) >=50) item = objHealth;
else item = objEnergy;
instance_create_layer(x, y, "Instances", item);*/
item = "";
var rand = random(100);

if (rand > 0 && rand <= 20) item = objHealth;
else if (rand > 20 && rand <= 40) item = objEnergy;
else if (rand > 40 && rand <= 60) item = objArtifact;
if (item != "") instance_create_layer(x, y, "Instances", item);

//STUFF TO DO TO UPDATE BOUNTY DETAILS
global.b_checkpoint1 = 1;
with (obJake) {
	obJake.hasInfo = true;
}

val = ds_list_find_value(global.bounties, 1);
val[? "complete"] = true

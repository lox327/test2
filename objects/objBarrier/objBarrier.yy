{
    "id": "dc295468-ee41-4eb8-ad16-c6d0ca399ac9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBarrier",
    "eventList": [
        {
            "id": "dfa73f19-c830-462a-bb5f-a1bb1ebb9de9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dc295468-ee41-4eb8-ad16-c6d0ca399ac9"
        },
        {
            "id": "0c325152-4915-41e6-8af0-7fdc2edc80ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dc295468-ee41-4eb8-ad16-c6d0ca399ac9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7e112a55-f75c-47df-9092-27fd121f56af",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "1507d081-5453-4856-9d86-eca5e93205f3",
    "visible": true
}
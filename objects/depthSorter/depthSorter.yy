{
    "id": "97199060-2fa0-44c9-800e-da0da4dfde0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "depthSorter",
    "eventList": [
        {
            "id": "3cb55c00-2973-4196-8ec4-d660fb89d4c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97199060-2fa0-44c9-800e-da0da4dfde0d"
        },
        {
            "id": "6190de81-a449-4e17-9356-2a24773cd823",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "97199060-2fa0-44c9-800e-da0da4dfde0d"
        },
        {
            "id": "2cea0d31-1e27-4976-b07c-c9c1f9612c7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "97199060-2fa0-44c9-800e-da0da4dfde0d"
        },
        {
            "id": "0d3dffd4-df5f-4d94-9971-54dcec6b55a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "97199060-2fa0-44c9-800e-da0da4dfde0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "58dc7138-89d9-427f-b2ec-0d2672bbc940",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
{
    "id": "1abc7aa8-ae4f-4f31-bccf-b61fcb08dc07",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBashBox2",
    "eventList": [
        {
            "id": "16d0aa04-9784-4ec9-bdc4-18b325af3b78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 8,
            "m_owner": "1abc7aa8-ae4f-4f31-bccf-b61fcb08dc07"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38a55b98-ce3c-45e3-be56-00e45726d0d8",
    "visible": true
}
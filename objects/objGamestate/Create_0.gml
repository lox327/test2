/// @description gamestate logic
debug = true;
state = "IN_GAME";
alpha = 1; //0 is transparent

transitionSpeed_room = 0.01;
transitionSpeed_building = 0.03;

currentRoom1 = noone;

pause = false;
allObjects[0,0] = noone;


//*** GAMESTATE (can be array?) ***
//CHECKPOINTS
global.checkpoint0 = 0;	//intro
global.checkpoint1 = 0;	//boss1 down
global.checkpoint2 = 1;
global.checkpoint3 = 0;

//MINI CHECKPOINTS
global.m_checkpoint1 = 0;
global.m_checkpoint2 = 0;
global.m_checkpoint3 = 0;

//BOUNTIES
global.b_checkpoint1 = 0; //first bounty, opens up gun shop
global.b_checkpoint2 = 0;
global.b_checkpoint3 = 0;

//DIALOG
global.d_checkpoint1 = 0;
global.d_checkpoint2 = 0;
global.d_checkpoint3 = 0;

//BOSSES
global.boss1 = 0;
global.boss1_name = "boss1";

global.boss2 = 0;
global.boss2_name = "boss2";

global.cutscene1 = 0;
global.cutscene2 = 0;
global.cutscene3 = 0;
//*** GAMESTATE ***

global.rm_east_x = 0;
global.rm_east_y = 0;


//GLOBALS
//static rooms: shops, bounties, char screen, inv screen which will not contain player moving or HUD draw
//can be list?
global.SHOPS[0] = rm_char;
global.SHOPS[1] = rm_bounty;
global.SHOPS[2] = rm_shop1;
global.SHOPS[3] = rm_shop2;
global.SHOPS[4] = rm_inv;

//building room: bar, gun shops, item shops, mod shops
global.BUILDINGS[0] = bar1;
global.BUILDINGS[1] = buildingItems1;
global.BUILDINGS[2] = buildingItems2;
global.BUILDINGS[3] = buildingItems3;

enum guns1 {
	NONE,	//0
	DEFAULT,//1
	PISTOL,	//2
	RIFLE,	//3
	SHOTGUN	//4
}

//** THINGS TO UNLOCK AS YOU PROGRESS

/*
-backpack
-gun shops
-mod shops
-pocket terminal for hacking atms to access bounties
-bar/jukebox unlock music selection
-hack level increase, can access more and different atms, hack doors and stuff?
	failed hack leads to alarm and bad guys
*/
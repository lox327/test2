/// @description manage action button

//update (remove) bounty list based on which bounty was selected
var index;
if ( objBounty1.selected ) {
	index = 0;
}

else if ( objBounty2.selected ) {
	index = 1;
}

else if ( objBounty3.selected ) {
	index = 2;
}

val = ds_list_find_value(global.bounties, index);	//get the whole map at index of bounties list
val[? "active"] = false;								//update map
ds_list_set(global.bounties, index, val);			//update bounties list

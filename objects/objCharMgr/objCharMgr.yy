{
    "id": "00450f94-4401-4890-a813-f592ea7311ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCharMgr",
    "eventList": [
        {
            "id": "1e9ed12f-2e7a-483a-b007-70bb04ae9eb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "00450f94-4401-4890-a813-f592ea7311ec"
        },
        {
            "id": "2d5f3030-a150-428f-8ba1-f94323f3141d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "00450f94-4401-4890-a813-f592ea7311ec"
        },
        {
            "id": "62e4dbc4-0908-4afc-bfae-d4425b126eb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "00450f94-4401-4890-a813-f592ea7311ec"
        },
        {
            "id": "7b212d69-2d80-4cfa-81dc-178a1fff84c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "00450f94-4401-4890-a813-f592ea7311ec"
        },
        {
            "id": "fc60a4a7-efd5-488f-809a-7f1282b8c300",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 9,
            "m_owner": "00450f94-4401-4890-a813-f592ea7311ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e399d58f-9add-412f-a080-fdea66996e3f",
    "visible": true
}
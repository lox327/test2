/// @description draw items to screen

display_set_gui_size( camera_get_view_width(view_camera[0]), camera_get_view_height(view_camera[0]) );

//draw bounty wanted images to the screen from global bounties list
var wanted_x = 125;
var wanted_y = 125;

for (var i = 0; i < 6; i++) {
	//get sprite
	//val = ds_list_find_value(global.bounties, i);
	//var spr = ds_map_find_value(val, "spriteBounty");
	//draw_sprite(spr, 0, (i*200)+250, wanted_y);
	draw_sprite(invEmpty, 0, (i*100)+250, wanted_y);
	
}
//
if ( objCharMgr.selected ) return;

//draw cursor around selected item (put into script?)
if (objBounty1.selected) {
	draw_sprite(invCursor, 0, 250, wanted_y);
}
else if (objBounty2.selected) {
	draw_sprite(invCursor, 0, 350, wanted_y);
}
else if (objBounty3.selected) {
	draw_sprite(invCursor, 0, 450, wanted_y);
}
//


//draw room text to the screen
/*draw_text(100, 100, "item shop");
draw_text(100, 120, "credits available: " + string(objPlayer.invCredits));
draw_text(65, 215, "mod: 0x678");
draw_text(65, 240, "(active)");

draw_text(65, 375, "mod: 0x234");
draw_text(65, 400, "(passive)");*/

//item desc in right pane
//can call func and pass in objItem...

/*for (var i = 0; i < ds_list_size(global.bounties); i++) {
	val = ds_list_find_value(global.bounties, i);

	for (var k = ds_map_find_first(val); !is_undefined(k); k = ds_map_find_next(val, k)) {
		var v = val[? k];
		//show_debug_message(v);
	}

}*/
//

//draw room text to the screen
var dx = 600;
var dy = 450;

var bx = 175;
var by = 475;

//draw bounty info to screen, should be loop from global.bounties, hard coding for now...
var index;
if ( objBounty1.selected && objBounty1.available ) {
	index = 0;
	//else draw_text_ext(dx, dy, "bounty not available...", line_break, word_wrap);
}

else if ( objBounty2.selected && objBounty2.available ) {
	index = 1;
	//else draw_text_ext(dx, dy, "bounty not available...", line_break, word_wrap);
}

else if ( objBounty3.selected && objBounty3.available ) {
	index = 2;
	//else draw_text_ext(dx, dy, "bounty not available...", line_break, word_wrap);
}
val = ds_list_find_value(global.bounties, index);

//set vars for bounty avatar and bounty map desc text info to display
var desc = ds_map_find_value(val, "desc");
var extDesc = ds_map_find_value(val, "extDesc");
var reward = ds_map_find_value(val, "reward");
var active = ds_map_find_value(val, "active");
var spr = ds_map_find_value(val, "spriteAvatar");
//...other bounty details

//draw bounty details to screen
draw_text_ext(dx, dy, desc, line_break, word_wrap);
draw_text_ext(dx, dy+50, "reward: " + reward, line_break, word_wrap);
draw_text_ext(dx, dy+100, "more details: " + extDesc, line_break, word_wrap);
draw_text_ext(dx, dy+150, "active: " + string(active), line_break, word_wrap);
draw_sprite(spr, 0, bx, by);
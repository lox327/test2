/// @description handle input movement
//cursor for exit
if (selected) {
	image_index = 1;
} else {
	image_index = 0;
}

//LEFT
if (keyboard_check_pressed(vk_left)) {
	if (objBounty1.selected) {
		//do nothing, or wrap around
		//objBounty1.selected = false;
		//objBounty3.selected = true;
	}
	else if (objBounty2.selected) {
		objBounty2.selected = false;
		objBounty1.selected = true;
	}
	else if (objBounty3.selected) {
		objBounty2.selected = true;
		objBounty3.selected = false;
	}
	
}

//RIGHT
if (keyboard_check_pressed(vk_right)) {
	if (objBounty1.selected) {
		objBounty1.selected = false;
		objBounty2.selected = true;
	}
	else if (objBounty2.selected) {
		objBounty2.selected = false;
		objBounty3.selected = true;
	}
	else if (objBounty3.selected) {
		//do nothing, or wrap around
		//objBounty2.selected = false;
		//objBounty3.selected = true;
	}
}

//DOWN
if (keyboard_check_pressed(vk_down)) {
	objCharMgr.selected = true;

	if (objBounty1.selected) {
		objBounty1.selected = false;
	}
	else if (objBounty2.selected) {
		objBounty2.selected = false;
	}
	//exit
	else if (objBounty3.selected) {
		objBounty3.selected = false;
	}
	
}

//UP
if (keyboard_check_pressed(vk_up)) {
	if (objCharMgr.selected) {
		objCharMgr.selected = false;
		objBounty1.selected = true;
	}
}
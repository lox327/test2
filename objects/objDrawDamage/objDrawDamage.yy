{
    "id": "53ef08c8-1c65-41cc-acfe-3eb4f4781b12",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDrawDamage",
    "eventList": [
        {
            "id": "c8aea8cf-c867-4453-b682-af985d04b3ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53ef08c8-1c65-41cc-acfe-3eb4f4781b12"
        },
        {
            "id": "a8030e87-9d9c-4e8e-8670-b105c43972eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "53ef08c8-1c65-41cc-acfe-3eb4f4781b12"
        },
        {
            "id": "02dd4cc7-2fb9-4fb3-b222-2dbfeae1c3cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "53ef08c8-1c65-41cc-acfe-3eb4f4781b12"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
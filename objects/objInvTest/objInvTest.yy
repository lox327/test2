{
    "id": "481b03be-7276-4602-97eb-807ad0deab6b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objInvTest",
    "eventList": [
        {
            "id": "734cef0a-541e-4177-b736-7c62fa5c1d38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "481b03be-7276-4602-97eb-807ad0deab6b"
        },
        {
            "id": "d2da89e5-edd0-45fc-a441-5ce127e3ccc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "481b03be-7276-4602-97eb-807ad0deab6b"
        },
        {
            "id": "58c0ab99-0d35-4138-9db7-58ace6e588f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 9,
            "m_owner": "481b03be-7276-4602-97eb-807ad0deab6b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
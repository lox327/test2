{
    "id": "8ebfcd5c-6822-4786-b3ad-ce4c751f03d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemyBullet3",
    "eventList": [
        {
            "id": "97579df8-321b-4b4e-a836-4f8fbcb0bb2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ebfcd5c-6822-4786-b3ad-ce4c751f03d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f3b693ec-27d8-4019-adca-7f447ce3e8fe",
    "visible": true
}
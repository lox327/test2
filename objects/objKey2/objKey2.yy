{
    "id": "8952e6eb-623b-4ac0-a11f-a8e53a7bb368",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objKey2",
    "eventList": [
        {
            "id": "7a08e8e4-94c7-4084-bf4c-bcf250b8e299",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8952e6eb-623b-4ac0-a11f-a8e53a7bb368"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b5d5993d-2a2e-4ea3-a438-77ee3d53f071",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b8fa866-d47c-4878-be2d-98bdb7d57aed",
    "visible": true
}
{
    "id": "23ceaee3-ff0f-432b-bd7d-0d434164a044",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemyBullet2",
    "eventList": [
        {
            "id": "f16d130a-751b-41db-81f0-f5b74e612426",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "23ceaee3-ff0f-432b-bd7d-0d434164a044"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a0408bc6-f74e-4b80-b4fc-4f3a4690fb5b",
    "visible": true
}
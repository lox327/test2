/// @description Insert description here
// You can write your code in this editor
image_angle = direction;
image_alpha = alpha;

if ( place_meeting(x, y, objFloor) ) {
	visible = false;
	if (speed > 0) {
		speed -= slowDown;
		alpha -= .01;
	}
	
	if ( !surface_exists(global.surface_blood) ) {
		global.surface_blood = surface_create(room_width, room_height);	
	}
	else {
		surface_set_target(global.surface_blood);
		draw_sprite_ext(blood, 0, x, y, image_xscale, image_yscale, image_angle, c_white, alpha);
		surface_reset_target();
		
		//surface_set_target(global.surface_blood);
		//gpu_set_blendmode_ext(bm_one, bm_inv_src_alpha) //in GM2
		//draw_sprite_ext(blood, 0, x, y, image_xscale, image_yscale, image_angle, c_white, alpha);
		//gpu_set_blendmode(bm_normal);  //in GM2, use the GMS version if you have that.
		//surface_reset_target();
	}
	
} 
else {
	visible = true;
}

if ( alpha <= 0 ) {
	instance_destroy();
}
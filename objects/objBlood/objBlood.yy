{
    "id": "157b9c7c-627c-4fe9-b154-21ee6e7f92d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBlood",
    "eventList": [
        {
            "id": "e6a679a9-007d-4016-8ae3-50359b2e31b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "157b9c7c-627c-4fe9-b154-21ee6e7f92d7"
        },
        {
            "id": "f16b8eaf-960e-4008-a555-1a62b387ea63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "157b9c7c-627c-4fe9-b154-21ee6e7f92d7"
        },
        {
            "id": "c3b32b69-b294-44a3-82a2-d566ad9b2f58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "157b9c7c-627c-4fe9-b154-21ee6e7f92d7"
        },
        {
            "id": "d978551c-9c71-40dd-b02d-ef42a7a4d72a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "157b9c7c-627c-4fe9-b154-21ee6e7f92d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59af6108-6d4a-425d-a47d-3418388af745",
    "visible": true
}
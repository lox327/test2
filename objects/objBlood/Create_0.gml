/// @description Insert description here
// You can write your code in this editor
slowDown = random_range(5.5, 8.5);
scale = random_range(.3, .5);
alpha = 1;

image_xscale = scale;
image_yscale = scale;
direction = random(360);
gravity = .5;

vspeed = random_range(6, -20);
hspeed = random_range(-8, 8);

alarm[0] = 180;
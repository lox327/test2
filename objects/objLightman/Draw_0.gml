draw_set_color(c_white);
draw_text(10,20,"Shader med normal map = "+string(normal)+"  (1 = on, 2 = off) \nALT - Turn Shader on/off \nMouseWheel - Height of light \nLeftMouseButton - Change colour of light");
draw_set_color(c_black);

if(normal) {
	var sampler = shader_get_sampler_index(sha_normalmap_angle, "s_normalmap")
	var bg = sprite_get_texture(spr_lightman_normal,0);
	var light_pos = shader_get_uniform(sha_normalmap_angle,"light_pos");
	var light_dif = shader_get_uniform(sha_normalmap_angle,"light_dif");
	var angle = shader_get_uniform(sha_normalmap_angle,"angle");

	shader_set(sha_normalmap_angle);

	texture_set_stage(sampler,bg);
	//shader_set_uniform_f(light_pos,mouse_x,mouse_y,lz);
	shader_set_uniform_f(light_pos,objLight21.x,objLight21.y,lz);
	shader_set_uniform_f(light_dif,R,G,B);
	shader_set_uniform_f(angle,degtorad(image_angle));

	draw_sprite_ext(sprite_index,0,x,y,1,1,image_angle,c_white,1);

	shader_reset();
} else {
	draw_sprite_ext(sprite_index,0,x,y,1,1,image_angle,c_white,1);
}

//image_angle += 0.1;
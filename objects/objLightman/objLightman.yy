{
    "id": "02ba6334-a3f8-4dbb-b81a-eae494bda3f0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLightman",
    "eventList": [
        {
            "id": "e959c80e-3e84-4af0-b5d1-586fdde7b4d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "02ba6334-a3f8-4dbb-b81a-eae494bda3f0"
        },
        {
            "id": "211c40b8-fa1e-46ff-817b-f100c58531e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "02ba6334-a3f8-4dbb-b81a-eae494bda3f0"
        },
        {
            "id": "0776834d-6c8d-4acf-9216-cdebeb53f6ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "02ba6334-a3f8-4dbb-b81a-eae494bda3f0"
        },
        {
            "id": "82e83e89-e743-4f7d-9080-8a360c2f36ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 60,
            "eventtype": 6,
            "m_owner": "02ba6334-a3f8-4dbb-b81a-eae494bda3f0"
        },
        {
            "id": "11f937bf-c307-4e27-b6cd-71225852a52d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 61,
            "eventtype": 6,
            "m_owner": "02ba6334-a3f8-4dbb-b81a-eae494bda3f0"
        },
        {
            "id": "c2eba115-b47d-404d-a2a5-10591440249d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "02ba6334-a3f8-4dbb-b81a-eae494bda3f0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38d8d713-a0ff-4424-ac60-9d03322f4db2",
    "visible": true
}
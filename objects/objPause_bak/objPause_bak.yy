{
    "id": "c791baee-8173-4e7f-8c25-fd7e994b632d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPause_bak",
    "eventList": [
        {
            "id": "d2db7ac0-0798-4b07-8ed5-acd9eebda53a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c791baee-8173-4e7f-8c25-fd7e994b632d"
        },
        {
            "id": "15b146d8-690b-4366-ae99-3db022fb877c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 67,
            "eventtype": 9,
            "m_owner": "c791baee-8173-4e7f-8c25-fd7e994b632d"
        },
        {
            "id": "88b25b53-90b0-43c4-b19f-d278ccbe77ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c791baee-8173-4e7f-8c25-fd7e994b632d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
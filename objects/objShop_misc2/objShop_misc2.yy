{
    "id": "a7697122-7a2d-4406-80df-7518359a996d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShop_misc2",
    "eventList": [
        {
            "id": "979707f3-d2f7-406e-aa4e-e4ebcd587231",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7697122-7a2d-4406-80df-7518359a996d"
        },
        {
            "id": "964e5854-1185-49a6-a13b-ed6469a8b77f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "a7697122-7a2d-4406-80df-7518359a996d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "99caee38-f03a-4937-b2be-4eae05270879",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
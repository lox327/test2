//code for managing the state of the items: in stock, buy...
//EXIT
if ( objItemMgr2.selected ) {
	//code for exiting room
	room_goto(global.currentRoom);
	objGun1.selected = true;
	objPlayer.x = prevX;
	objPlayer.y = prevY;
	
	objPlayer.playerMove = true;
	newFade.doTransition = false;
	exit;
}

if (ds_list_size(global.inv) >= 3) {
	show_message("inv full");
	exit;
}

//temp index var to assign item obj to reduce duplicate code
var index = objGun1;

//move to switch
if ( objGun1.selected ) {
	index = objGun1;
}

else if ( objGun2.selected ) {
	index = objGun2;
}

else if ( objGun3.selected ) {
	index = objGun3;
}

else if ( objGun4.selected ) {
	index = objGun4;
}


//code for equipping this item, or going to room...
if (true) {
	addInv(index);
	index.available = false;
}
else show_message("item not available");

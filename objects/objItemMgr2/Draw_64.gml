display_set_gui_size( camera_get_view_width(view_camera[0]), camera_get_view_height(view_camera[0]) );

//draw screen info text
draw_text(100, 100, "item shop");
draw_text(100, 120, "credits available: " + string(objPlayer.invCredits));
draw_text(65, 215, "mod: 0x678");
draw_text(65, 240, "(active)");

draw_text(65, 375, "mod: 0x234");
draw_text(65, 400, "(passive)");

var dx = 650;
var dy = 625;
var dy2 = 700;

//item desc in right pane
//hard coding for now...
//can call func and pass in objGun...
if ( objGun1.selected ) {
	if (objGun1.available) {
		draw_sprite(invSel, 0, objGun1.x, objGun1.y);
		draw_text_ext(dx, dy, objGun1.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy, "[consumes " + objGun1.itemEnergy + "]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objGun2.selected ) {
	if (objGun2.available) {
		draw_sprite(invSel, 0, objGun2.x, objGun2.y);
		draw_text_ext(dx, dy, objGun2.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy2, "[consumes " + string(objGun2.itemEnergy) + " energy]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objGun3.selected ) {
	if (objGun3.available) {
		draw_sprite(invSel, 0, objGun3.x, objGun3.y);
		draw_text_ext(dx, dy, objGun3.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy2, "[consumes " + string(objGun3.itemEnergy) + " energy]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objGun4.selected ) {
	if (objGun4.available) {
		draw_sprite(invSel, 0, objGun4.x, objGun4.y);
		draw_text_ext(dx, dy, objGun4.full_desc, line_break, word_wrap);
		draw_text_ext(dx, dy2, "[consumes " + string(objGun4.itemEnergy) + " energy]", line_break, word_wrap);
	}
	else draw_text_ext(dx, dy, "item not available...", line_break, word_wrap);
}

else if ( objItemMgr2.selected ) {
	draw_sprite(invSel, 0, objItemMgr2.x, objItemMgr2.y);
	draw_text_ext(dx, dy, "exit...", line_break, word_wrap);
}
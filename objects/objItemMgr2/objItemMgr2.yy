{
    "id": "440487ef-d11e-4c49-ad85-146d8b4655e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objItemMgr2",
    "eventList": [
        {
            "id": "ce02be86-bf41-4000-b1c6-081583d8ded9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "440487ef-d11e-4c49-ad85-146d8b4655e4"
        },
        {
            "id": "1e7ce891-1fe5-400c-845a-7c3e9df81272",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "440487ef-d11e-4c49-ad85-146d8b4655e4"
        },
        {
            "id": "772172c3-ade9-4d85-b1d2-f1f773b58795",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "440487ef-d11e-4c49-ad85-146d8b4655e4"
        },
        {
            "id": "ad6044d0-3dc6-4cea-9011-d484c195b2f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "440487ef-d11e-4c49-ad85-146d8b4655e4"
        },
        {
            "id": "9339f070-5bf9-4799-bf5b-638e4e01a6d3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 5,
            "m_owner": "440487ef-d11e-4c49-ad85-146d8b4655e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e399d58f-9add-412f-a080-fdea66996e3f",
    "visible": true
}
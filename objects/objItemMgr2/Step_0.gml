/// @description handle input movement across items
if (selected) {
	//image_index = 1;
} else {
	//image_index = 0;
}

if (keyboard_check_pressed(vk_left)) {
	if (objGun1.selected) {
		//wrap around
		//objGun1.selected = false;
		//objGun3.selected = true;
	}
	else if (objGun2.selected) {
		objGun2.selected = false;
		objGun1.selected = true;
	}
	else if (objGun3.selected) {
		objGun3.selected = false;
		objGun2.selected = true;
	}
	else if (objGun4.selected) {
		objGun4.selected = false;
		objGun3.selected = true;
	}

}
if (keyboard_check_pressed(vk_right)) {
	if (objGun1.selected) {
		objGun1.selected = false;
		objGun2.selected = true;
	}
	else if (objGun2.selected) {
		objGun2.selected = false;
		objGun3.selected = true;
	}
	else if (objGun3.selected) {
		objGun3.selected = false;
		objGun4.selected = true;
	}

}


//down
if (keyboard_check_pressed(vk_down)) {
	if (objGun1.selected) {
		objGun1.selected = false;
	}
	else if (objGun2.selected) {
		objGun2.selected = false;
	}
	//exit
	else if (objGun3.selected) {
		objGun3.selected = false;
	}
	else if (objGun4.selected) {
		objGun4.selected = false;
	}
	objItemMgr2.selected = true;

}


//up
if (keyboard_check_pressed(vk_up)) {
	if (objItemMgr2.selected) {
		objItemMgr2.selected = false;
		objGun1.selected = true;
	}
}
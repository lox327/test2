{
    "id": "3854197e-6d61-41a6-8608-1b2e8ea7312d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShopGuy4",
    "eventList": [
        {
            "id": "3bbe4a84-dba0-460f-81f6-0faad80670b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3854197e-6d61-41a6-8608-1b2e8ea7312d"
        },
        {
            "id": "5031b6d2-9213-4cb1-b78a-366d09bb9496",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3854197e-6d61-41a6-8608-1b2e8ea7312d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "10c585bf-2db2-49e7-8115-65340dc76da8",
    "visible": true
}
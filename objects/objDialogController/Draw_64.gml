/// @description draw the dialog box
if (!fetch) {
	draw_set_font(fnt_GUI);
	
	//draw our avatar box if available
	if (dialogAvatar != "") {
		if (dialogLeftAlign) {
			draw_sprite(dialogAvatar, 0, x_pos, y_pos - 60);
			draw_sprite(sprNamePlate, 0, x_pos, y_pos-15);
			draw_text(x_pos+12, y_pos-12, "player");
		}
		else {
			draw_sprite_ext(dialogAvatar, 0, x_pos + 170, y_pos - 110, 1, 1, 0, c_white, 1);	
			draw_sprite(sprNamePlate, 0, x_pos + 192, y_pos-15);
			draw_text(x_pos + 210, y_pos - 12, dialogObj.name);
		}
	}
	
	//draw our dialog box, longer if no dialogAvatar (used for text overlay)
	draw_set_halign(fa_left); //make sure font align is left
	draw_sprite(sprDialogBox2, 0, x_pos, y_pos);
	draw_text_ext( x_pos + 12, y_pos + 8, string_hash_to_newline(dialogOutput), line_sep, line_w );
	
	//show continue arrow
	if (dialogEnd) {
		//draw_sprite(dialogCursor, 0, x_pos + 165, y_pos +40);
		//instance_create_layer(x_pos, y_pos, "Instances", objEnergy)
	}
}
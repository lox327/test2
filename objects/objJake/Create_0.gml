//DIALOG
dialog = [];
dialogLine = 0;
prox = 100;

d_bar1 = 
addDialog("the east is blocked off unless you have a key.", true, avatarBar, false, self);
addDialog("you're gonna need to go west and clear that room to get the key.", true, avatarBar, false, self);
addDialog("ok, and then you'll tell me what I need to know? ...", true, avatarPlayer, true, self);

addDialog("we'll see if you make it back...", false, avatarBar, false, self);


d_bar2 = 
addDialog("wow, you made it back. I'm surprised.", true, avatarBar, false, self);
addDialog("did you get the key?", true, avatarBar, false, self);
addDialog("sure did. now tell me, what should I expect when I go east??", true, avatarPlayer, true, self);

addDialog("HELL...", false, avatarBar, false, self);

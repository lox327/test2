{
    "id": "7338d527-cd8a-40c4-8713-c3c8c803f9b2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objJake",
    "eventList": [
        {
            "id": "850ca55c-ec97-460d-a12a-7caf87c9a910",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7338d527-cd8a-40c4-8713-c3c8c803f9b2"
        },
        {
            "id": "7fb67dc6-5596-4af2-b31e-8659d825a219",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "7338d527-cd8a-40c4-8713-c3c8c803f9b2"
        },
        {
            "id": "fb78e610-6193-46fe-8d26-63cf59ecbd45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7338d527-cd8a-40c4-8713-c3c8c803f9b2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
    "visible": true
}
{
    "id": "befc1d62-8c68-43b3-ad0a-3bcb4b8400c3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objDebris2",
    "eventList": [
        {
            "id": "a6259895-9b32-4bbf-85fb-e919bb478e25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "befc1d62-8c68-43b3-ad0a-3bcb4b8400c3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
    "visible": true
}
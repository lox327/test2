{
    "id": "4f601341-0f11-4724-bfe0-506536eab9d5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShop_misc1",
    "eventList": [
        {
            "id": "e3049dfe-6658-4d54-a2dc-dce60df94e1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f601341-0f11-4724-bfe0-506536eab9d5"
        },
        {
            "id": "6ad46e7e-7aa3-4db1-b2e4-c0ffa6b23ac7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 35,
            "eventtype": 9,
            "m_owner": "4f601341-0f11-4724-bfe0-506536eab9d5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "99caee38-f03a-4937-b2be-4eae05270879",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
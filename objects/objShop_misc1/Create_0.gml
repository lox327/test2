///@description Shop Guy1, Misc Stuff
///@description Shop Items, using sprite sheet: spr_sheet_name
event_inherited();

#region inv specific fields
spr_sheet_name = spr_items1;
spr_avatar_name = spr_shopguy3;
inv_name = "global.inv_name1";
itemShopTotal = ItemShop1.total;

shopName = "Shop Guy1"
shopDesc = "Misc Stuff";
shopText = shopName + " has lived many lives. As master gunsmith for The City, he supplies only the best.";

sep = "";
repeat(string_length(shopName)+1) {sep +=  "_"};
#endregion

//items to populate shop with
enum ItemShop1 {
	bomb,
	gun,
	modification,
	total
}

inv_name = ds_grid_create(itemShopTotal, stat.total);

#region add items to grid
inv_name[#ItemShop1.bomb, stat.name] = "Bomb";
inv_name[#ItemShop1.bomb, stat.description] = "It goes BOOM!";
inv_name[#ItemShop1.bomb, stat.damage_low] = 0;
inv_name[#ItemShop1.bomb, stat.damage_high] = 0;
inv_name[#ItemShop1.bomb, stat.weaponEnergyUse] = 0;
inv_name[#ItemShop1.bomb, stat.weaponEnergyTotal] = 0;
inv_name[#ItemShop1.bomb, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop1.bomb, stat.armor] = 0;
inv_name[#ItemShop1.bomb, stat.mod1] = 0;
inv_name[#ItemShop1.bomb, stat.mod2] = 0;
inv_name[#ItemShop1.bomb, stat.modifier] = 0;
inv_name[#ItemShop1.bomb, stat.mod_amount] = 0;
inv_name[#ItemShop1.bomb, stat.mod_desc] = "";
inv_name[#ItemShop1.bomb, stat.slow_duration] = 0;
inv_name[#ItemShop1.bomb, stat.slow_amount] = 0;
inv_name[#ItemShop1.bomb, stat.dot_duration] = 0;
inv_name[#ItemShop1.bomb, stat.dot_amount] = 0;
inv_name[#ItemShop1.bomb, stat.splash_rad] = 0;
inv_name[#ItemShop1.bomb, stat.splash_amount] = 0;
inv_name[#ItemShop1.bomb, stat.owned] = false;
inv_name[#ItemShop1.bomb, stat.available] = true;
inv_name[#ItemShop1.bomb, stat.equipped] = false;
inv_name[#ItemShop1.bomb, stat.type] = "Item";
inv_name[#ItemShop1.bomb, stat.price] = 20;
inv_name[#ItemShop1.bomb, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop1.bomb, stat.spr_index] = ItemShop1.bomb;

inv_name[#ItemShop1.gun, stat.name] = "Gun";
inv_name[#ItemShop1.gun, stat.description] = "It goes BANG!";
inv_name[#ItemShop1.gun, stat.damage_low] = 0;
inv_name[#ItemShop1.gun, stat.damage_high] = 0;
inv_name[#ItemShop1.gun, stat.weaponEnergyUse] = 0;
inv_name[#ItemShop1.gun, stat.weaponEnergyTotal] = 0;
inv_name[#ItemShop1.gun, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop1.gun, stat.armor] = 0;
inv_name[#ItemShop1.gun, stat.mod1] = 0;
inv_name[#ItemShop1.gun, stat.mod2] = 0;
inv_name[#ItemShop1.gun, stat.modifier] = 0;
inv_name[#ItemShop1.gun, stat.mod_amount] = 0;
inv_name[#ItemShop1.gun, stat.mod_desc] = "";
inv_name[#ItemShop1.gun, stat.slow_duration] = 0;
inv_name[#ItemShop1.gun, stat.slow_amount] = 0;
inv_name[#ItemShop1.gun, stat.dot_duration] = 0;
inv_name[#ItemShop1.gun, stat.dot_amount] = 0;
inv_name[#ItemShop1.gun, stat.splash_rad] = 0;
inv_name[#ItemShop1.gun, stat.splash_amount] = 0;
inv_name[#ItemShop1.gun, stat.owned] = false;
inv_name[#ItemShop1.gun, stat.available] = true;
inv_name[#ItemShop1.gun, stat.equipped] = false;
inv_name[#ItemShop1.gun, stat.type] = "Item";
inv_name[#ItemShop1.gun, stat.price] = 20;
inv_name[#ItemShop1.gun, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop1.gun, stat.spr_index] = ItemShop1.gun;

inv_name[#ItemShop1.modification, stat.name] = "Mod";
inv_name[#ItemShop1.modification, stat.description] = "It goes MOD!";
inv_name[#ItemShop1.modification, stat.damage_low] = 0;
inv_name[#ItemShop1.modification, stat.damage_high] = 0;
inv_name[#ItemShop1.modification, stat.weaponEnergyUse] = 0;
inv_name[#ItemShop1.modification, stat.weaponEnergyTotal] = 0;
inv_name[#ItemShop1.modification, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop1.modification, stat.armor] = 0;
inv_name[#ItemShop1.modification, stat.mod1] = 0;
inv_name[#ItemShop1.modification, stat.mod2] = 0;
inv_name[#ItemShop1.modification, stat.modifier] = 0;
inv_name[#ItemShop1.modification, stat.mod_amount] = 0;
inv_name[#ItemShop1.modification, stat.mod_desc] = "";
inv_name[#ItemShop1.modification, stat.slow_duration] = 0;
inv_name[#ItemShop1.modification, stat.slow_amount] = 0;
inv_name[#ItemShop1.modification, stat.dot_duration] = 0;
inv_name[#ItemShop1.modification, stat.dot_amount] = 0;
inv_name[#ItemShop1.modification, stat.splash_rad] = 0;
inv_name[#ItemShop1.modification, stat.splash_amount] = 0;
inv_name[#ItemShop1.modification, stat.owned] = false;
inv_name[#ItemShop1.modification, stat.available] = true;
inv_name[#ItemShop1.modification, stat.equipped] = false;
inv_name[#ItemShop1.modification, stat.type] = "Item";
inv_name[#ItemShop1.modification, stat.price] = 20;
inv_name[#ItemShop1.modification, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop1.modification, stat.spr_index] = ItemShop1.modification;

#endregion
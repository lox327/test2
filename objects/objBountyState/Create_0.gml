/// @description map for storing bounties

//list for storing bounty objs
global.bounties = ds_list_create();

//map of bounties to store in list
var bounty = ds_map_create();
ds_map_add(bounty, "name",	"bounty1");
ds_map_add(bounty, "wanted",	"handsome jack");	//nickname
ds_map_add(bounty, "wanted_name","sparrow, jack");	//full name
ds_map_add(bounty, "reward",	50);
ds_map_add(bounty, "desc",	"1...is wanted for: blah");
ds_map_add(bounty, "extDesc",	"1...is skilled in blah");
ds_map_add(bounty, "difficulty","medium");
ds_map_add(bounty, "location",	"north");
ds_map_add(bounty, "active",	false); //bounty is active
ds_map_add(bounty, "complete",	false);	//bounty hasbeen completed
ds_map_add(bounty, "turnin",	false);	//bounty has been turned in
ds_map_add(bounty, "spriteAvatar", bountyAvatar1);
ds_map_add(bounty, "spriteBounty", bounty1);
ds_list_add(global.bounties, bounty);

bounty = ds_map_create();
ds_map_add(bounty, "name",	"bounty2");
ds_map_add(bounty, "wanted",	"ugly sam");
ds_map_add(bounty, "wanted_name","durden, sam");
ds_map_add(bounty, "reward",	35);
ds_map_add(bounty, "desc",	"2...is wanted for: mehh");
ds_map_add(bounty, "extDesc",	"2...is skilled in mehh");
ds_map_add(bounty, "difficulty","easy");
ds_map_add(bounty, "location",	"south");
ds_map_add(bounty, "active",	false);
ds_map_add(bounty, "complete",	false);
ds_map_add(bounty, "turnin",	false);
ds_map_add(bounty, "spriteAvatar", bountyAvatar2);
ds_map_add(bounty, "spriteBounty", bounty2);
ds_list_add(global.bounties, bounty);

bounty = ds_map_create();
ds_map_add(bounty, "name",	"bounty3");
ds_map_add(bounty, "wanted",	"dirty marv");
ds_map_add(bounty, "wanted_name","cobb, marv");
ds_map_add(bounty, "reward",	75);
ds_map_add(bounty, "desc",	"3...is wanted for: murr");
ds_map_add(bounty, "extDesc",	"3...is skilled in murr");
ds_map_add(bounty, "difficulty","easy");
ds_map_add(bounty, "location",	"south2");
ds_map_add(bounty, "active",	false);
ds_map_add(bounty, "complete",	false);
ds_map_add(bounty, "turnin",	false);
ds_map_add(bounty, "spriteAvatar", bountyAvatar3);
ds_map_add(bounty, "spriteBounty", bounty3);
ds_list_add(global.bounties, bounty);

//val = ds_list_find_value(bounties, 1);
//show_message(ds_map_find_value(val, "name"));
//show_message(ds_list_find_value(bounties, 2));
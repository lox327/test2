{
    "id": "f96df7a6-c255-4672-a871-30a87d20c70b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemyKnife",
    "eventList": [
        {
            "id": "123104d0-04dc-4102-8286-c4a6d2274c2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f96df7a6-c255-4672-a871-30a87d20c70b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cc5b3b54-15ed-4fba-8248-d6277c559cc0",
    "visible": true
}
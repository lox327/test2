{
    "id": "e3ce0589-9065-4d02-8021-fa4aa5339638",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemyBullet",
    "eventList": [
        {
            "id": "b4979ff3-57bf-40b7-b1ad-70604a2f02f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e3ce0589-9065-4d02-8021-fa4aa5339638"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bcc1ec8e-0c2e-490f-8e7a-9da48ab3a335",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "daaf660d-4f56-454c-bc17-da0bf2f59fee",
    "visible": true
}
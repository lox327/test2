///@description MOD effects for enemy
#region SHOCK
if (shock) {
	//sprite for shock
	//dot tick animation
	//sprite for dying
	//image_blend = c_red;

	if (dot_timer%100 == 0) {
		show_debug_message("shock tick");
		//hp -= dot_amount;	
	}
	
	//RESET
	if (dot_timer == 0) {
		//reset attrs to prev
		//sprite index
		shock = false;
		spd = prev_spd;
		show_debug_message("shock done");
		//image_blend = -1;
	}
	dot_timer--;
}
#endregion

#region FIRE
else if (fire) {
	//sprite for burning
	//dot tick animation
	//sprite for dying on fire
	if (dot_timer%100 == 0) {
		//show_debug_message("fire tick");
		hp -= dot_amount;
		damage_text(dot_amount);
	}
	
	//RESET
	if (dot_timer == 0) {
		//reset attrs to prev
		//sprite index
		fire = false;
		//image_blend = -1;
		//show_debug_message("fire done");
	}
	dot_timer--;
}
#endregion

#region ACID
else if (splash) {
	//show_debug_message("splash");
	//show_debug_message(splash_amount)
	getInstanceRange(splash_x, splash_y, enemyParent, splash_rad, splash_amount);
	
	splash = false;
	//image_blend = -1;
}
#endregion
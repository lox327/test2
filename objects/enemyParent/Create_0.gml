hp = irandom_range(4,6);
hpMax = hp;
spd = 1.5;
prev_spd = spd;
flash = 0;
isHit = false;
hitAngle = 0;
bashTime = 10;	//time for getting bashed
deadTime = 20;	//time for death animation
chaseRange = 100;
attackRange = 15;
boss = false;
dir = "";

//used for bounty system
//bounty enemies are set to inactive if player does not have that bounty
isActive = true;

state = "Inactive";
sightRange = 300;
aggressiveness = 0;
//attackRange = sprite_width/2 + objPlayer.sprite_width/2;

areaEnemy = false; //used for enemy that is in a locked area, once killed, trigger is destroyed

#region MOD STUFF
//SHOCK
slow_duration = 0;
slow_amount = 0;
shock = false;

//FIRE
dot_timer = 0;
dot_amount = 0;
fire = false;

//ACID
splash_rad = 0;
splash_amount = 0;
splash_x = 0;
splash_y = 0;
splash = false;

//can also have modType = "shock", "fire"...
#endregion

enum EnemyState
{ 
     MOVE,
	 IDLE,
	 ATTACK1_IDLE,
	 ATTACK2_IDLE,
     ATTACK1,
	 ATTACK2,
	 ATTACK3,
	 ATTACK3_CHARGE,
	 ATTACK4,
     CHASE,
	 STUN,
	 HIT,
	 EVADE,
	 INTRO,
	 DEAD,
	 TURRET,
	 WAIT
};
enemyState = EnemyState.IDLE;
timer = 100; 

{
    "id": "c0353dfc-c14d-417e-b283-64ff3ecacc3b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemyParent",
    "eventList": [
        {
            "id": "5e12f92f-272b-4849-94cb-cea9a68a3471",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "6faf6e10-6f71-4edc-bcf8-0404172c9363",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "d67f082b-7804-457b-9198-29481b73f9d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "03a0f2c9-7c42-4803-8781-f6fd1b01755c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "13390ee5-1872-466e-af1b-d2a2bc3e125f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "859e450a-bfca-414c-bda3-b1bf5616a0c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "73553047-867e-4e85-87e8-d69c862a04c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "002b6ee6-d200-4439-b4cf-601e42eb29da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "045cc223-8413-4692-b413-eea516e9893b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "1a5a6c10-a59a-417e-afb9-7df31dab7449",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7af82463-cca2-45a9-87e3-98eb03ace7c9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "6e83db3f-14a6-4282-970c-56f5f3376056",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "f83b0667-3ac9-493d-b7fa-70f336726314",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "59a2ee32-5e01-48ac-8de2-3c48733daaff",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "0cbbf25f-9d2f-4544-9796-1595e3157187",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64cb9e95-de9c-4e79-be17-9451f287b529",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "e0809f97-dd07-475a-921d-e4429cda6d68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        },
        {
            "id": "d9a82e70-1792-4cb5-8a46-cc249a0cd36e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "c0353dfc-c14d-417e-b283-64ff3ecacc3b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "58dc7138-89d9-427f-b2ec-0d2672bbc940",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
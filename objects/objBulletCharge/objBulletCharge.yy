{
    "id": "c723f4a8-c06d-4789-9171-917528d114ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objBulletCharge",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7af82463-cca2-45a9-87e3-98eb03ace7c9",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "397c745e-e86e-4f30-ae97-b93fc40f5c87",
    "visible": true
}
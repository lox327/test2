/// @description narration obj will trigger when prox() is true, no action button intended
if ( proximity(prox) && !triggered ) {
	triggered = true;
	if (global.checkpoint1 == 0) startDialog(self, d_narr0);
	
	//boss1 down
	else if (global.checkpoint1 == 1) {
		if (global.b_checkpoint1 == 0) startDialog(self, d_narr1);
	}
}

if ( !proximity(prox) && triggered ) {
	triggered = false;
	if (triggerOnce) instance_destroy();
}
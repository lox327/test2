/// @description if triggerOnce is true, obj will destroy after viewed; with this, only one narrX should be defined

hasInfo = true;
triggered = false;
triggerOnce = true;

//DIALOG for [quests]
dialog = [];
dialogLine = 0;
prox = 50;
name = "narr";

d_narr0 = 
addDialog("the UNITED STATES police force, like an army, is encamped around the island...", true, "", false, self);
//addDialog("there are no guards inside the prison, only prisoners and the world they have made.", true, "", false, self);
addDialog("the rules are simple: once you go in, you don't come out...", false, "", false, self);

d_narr1 = 
addDialog("I heard you took down boss1...", true, "", false, self);
addDialog("the bounty terminal is back online now. could be a nice way to get some gear.", false, "", false, self);

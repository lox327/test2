{
    "id": "ddfa99ea-ee8f-45d2-8393-dccb26a705e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objNarration",
    "eventList": [
        {
            "id": "bfe16746-ce9c-4b61-9ed5-d0cf951d517a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ddfa99ea-ee8f-45d2-8393-dccb26a705e0"
        },
        {
            "id": "25c35175-a47e-4a9d-bb68-182c821ef771",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "ddfa99ea-ee8f-45d2-8393-dccb26a705e0"
        },
        {
            "id": "f23fd5a0-a68f-40a8-bff3-b45aae74e00c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ddfa99ea-ee8f-45d2-8393-dccb26a705e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
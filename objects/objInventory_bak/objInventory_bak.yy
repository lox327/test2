{
    "id": "3000c226-e37d-4dfb-9b78-dd10ca04f487",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objInventory_bak",
    "eventList": [
        {
            "id": "298377f0-19df-4246-85af-ec323a172de9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3000c226-e37d-4dfb-9b78-dd10ca04f487"
        },
        {
            "id": "ab1d1125-ae7d-490c-a83c-d721b1e7303f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 36,
            "eventtype": 9,
            "m_owner": "3000c226-e37d-4dfb-9b78-dd10ca04f487"
        },
        {
            "id": "474eb640-3ea8-49e3-a09a-0c5b9905347e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3000c226-e37d-4dfb-9b78-dd10ca04f487"
        },
        {
            "id": "1b9e35f3-d6c9-4b97-b6cd-d5e647b959f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3000c226-e37d-4dfb-9b78-dd10ca04f487"
        },
        {
            "id": "61bb3e8f-a17c-4e1c-81d0-f0b405e27f23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3000c226-e37d-4dfb-9b78-dd10ca04f487"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
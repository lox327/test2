/// @description Insert description here
// You can write your code in this editor
if (!display) exit;

//draw empty inv slots
for (var i = 0; i<5; i++) {
	draw_sprite(gun1, 0, objPlayer.x + (i*50), objPlayer.y);
}
	
//draw current inv items
for (var i = 0; i<3; i++) {
	draw_sprite(gun2, 0, objPlayer.x + 5 + (i*50), objPlayer.y);
}
	
//draw cursor
draw_sprite(invCursor, 0, objPlayer.x, objPlayer.y);

//draw inv info to screen
var info_grid = ds_player_info;
var c = c_black;
draw_text_color(objPlayer.x, objPlayer.y, info_grid[# 0, 3] + ": " + info_grid[# 1,3], c, c, c, c, 1);

//draw inv values to screen
for (var i = 0; i < 3; i++) {
	draw_text_color(objPlayer.x, objPlayer.y + 50 + (i*50), info_grid[# 1, i] , c, c, c, c, 1);
}
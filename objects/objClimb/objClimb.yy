{
    "id": "36886c43-14fc-432a-a294-bff56ada12e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objClimb",
    "eventList": [
        {
            "id": "5f9fdbd0-a5f0-4e18-81c2-49b9e9bbcba8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "36886c43-14fc-432a-a294-bff56ada12e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "36bf373c-5420-4e9c-8cae-93cb1d62afae",
    "visible": false
}
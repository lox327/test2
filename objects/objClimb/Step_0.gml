/// @description slow down player on climbables (ladder, stairs)
if ( place_meeting(x,y,objPlayer) ) objPlayer.playerSpeed = objPlayer.playerSpeed_climb;
else objPlayer.playerSpeed = objPlayer.playerSpeed;

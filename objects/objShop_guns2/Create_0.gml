///@description Shop Guy2, Guns n Stuff
///@description Shop Items, using sprite sheet: spr_sheet_name
event_inherited();

#region inv specific fields
spr_sheet_name = spr_items4;
spr_avatar_name = spr_shopguy1;
inv_name = "global.inv_name2";
itemShopTotal = ItemShop4.total;

shopName = "Ikora Rey"
shopDesc = "Specialty Guns";
shopText = shopName + " has lived many lives. As master gunsmith for The City, he supplies only the best.";

sep = "";
repeat(string_length(shopName)+1) {sep +=  "_"};
#endregion

//items to populate shop with
enum ItemShop4 {
	gun1,
	gun2,
	gun3,
	total
}

inv_name = ds_grid_create(itemShopTotal, stat.total);

#region add items to grid
inv_name[#ItemShop4.gun1, stat.name] = "Venom";
inv_name[#ItemShop4.gun1, stat.description] = "It goes BANG1!";
inv_name[#ItemShop4.gun1, stat.damage_low] = 10;
inv_name[#ItemShop4.gun1, stat.damage_high] = 15;
inv_name[#ItemShop4.gun1, stat.weaponEnergyUse] = 5;
inv_name[#ItemShop4.gun1, stat.weaponEnergyTotal] = 100;
inv_name[#ItemShop4.gun1, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop4.gun1, stat.armor] = 0;
inv_name[#ItemShop4.gun1, stat.mod1] = 1;
inv_name[#ItemShop4.gun1, stat.mod2] = 0;
inv_name[#ItemShop4.gun1, stat.modifier] = 0;
inv_name[#ItemShop4.gun1, stat.mod_amount] = 0;
inv_name[#ItemShop4.gun1, stat.mod_desc] = "";
inv_name[#ItemShop4.gun1, stat.slow_duration] = 0;
inv_name[#ItemShop4.gun1, stat.slow_amount] = 0;
inv_name[#ItemShop4.gun1, stat.dot_duration] = 0;
inv_name[#ItemShop4.gun1, stat.dot_amount] = 0;
inv_name[#ItemShop4.gun1, stat.splash_rad] = 0;
inv_name[#ItemShop4.gun1, stat.splash_amount] = 0;
inv_name[#ItemShop4.gun1, stat.owned] = false;
inv_name[#ItemShop4.gun1, stat.available] = true;
inv_name[#ItemShop4.gun1, stat.equipped] = false;
inv_name[#ItemShop4.gun1, stat.type] = guns1.PISTOL;
inv_name[#ItemShop4.gun1, stat.price] = 20;
inv_name[#ItemShop4.gun1, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop4.gun1, stat.spr_index] = ItemShop4.gun1;

inv_name[#ItemShop4.gun2, stat.name] = "The Viper";
inv_name[#ItemShop4.gun2, stat.description] = "It goes BANG2!";
inv_name[#ItemShop4.gun2, stat.damage_low] = 8;
inv_name[#ItemShop4.gun2, stat.damage_high] = 12;
inv_name[#ItemShop4.gun2, stat.weaponEnergyUse] = 8;
inv_name[#ItemShop4.gun2, stat.weaponEnergyTotal] = 100;
inv_name[#ItemShop4.gun2, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop4.gun2, stat.armor] = 0;
inv_name[#ItemShop4.gun2, stat.mod1] = 1;
inv_name[#ItemShop4.gun2, stat.mod2] = 0;
inv_name[#ItemShop4.gun2, stat.modifier] = 0;
inv_name[#ItemShop4.gun2, stat.mod_amount] = 0;
inv_name[#ItemShop4.gun2, stat.mod_desc] = "";
inv_name[#ItemShop4.gun2, stat.slow_duration] = 0;
inv_name[#ItemShop4.gun2, stat.slow_amount] = 0;
inv_name[#ItemShop4.gun2, stat.dot_duration] = 0;
inv_name[#ItemShop4.gun2, stat.dot_amount] = 0;
inv_name[#ItemShop4.gun2, stat.splash_rad] = 0;
inv_name[#ItemShop4.gun2, stat.splash_amount] = 0;
inv_name[#ItemShop4.gun2, stat.owned] = false;
inv_name[#ItemShop4.gun2, stat.available] = true;
inv_name[#ItemShop4.gun2, stat.equipped] = false;
inv_name[#ItemShop4.gun2, stat.type] = guns1.RIFLE;
inv_name[#ItemShop4.gun2, stat.price] = 19;
inv_name[#ItemShop4.gun2, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop4.gun2, stat.spr_index] = ItemShop4.gun2;

inv_name[#ItemShop4.gun3, stat.name] = "Silencer";
inv_name[#ItemShop4.gun3, stat.description] = "It goes BANG3!";
inv_name[#ItemShop4.gun3, stat.damage_low] = 12;
inv_name[#ItemShop4.gun3, stat.damage_high] = 15;
inv_name[#ItemShop4.gun3, stat.weaponEnergyUse] = 10;
inv_name[#ItemShop4.gun3, stat.weaponEnergyTotal] = 100;
inv_name[#ItemShop4.gun3, stat.weaponEnergyRegen] = 0;
inv_name[#ItemShop4.gun3, stat.armor] = 0;
inv_name[#ItemShop4.gun3, stat.mod1] = 0;
inv_name[#ItemShop4.gun3, stat.mod2] = 0;
inv_name[#ItemShop4.gun3, stat.modifier] = 0;
inv_name[#ItemShop4.gun3, stat.mod_amount] = 0;
inv_name[#ItemShop4.gun3, stat.mod_desc] = "";
inv_name[#ItemShop4.gun3, stat.slow_duration] = 0;
inv_name[#ItemShop4.gun3, stat.slow_amount] = 0;
inv_name[#ItemShop4.gun3, stat.dot_duration] = 0;
inv_name[#ItemShop4.gun3, stat.dot_amount] = 0;
inv_name[#ItemShop4.gun3, stat.splash_rad] = 0;
inv_name[#ItemShop4.gun3, stat.splash_amount] = 0;
inv_name[#ItemShop4.gun3, stat.owned] = false;
inv_name[#ItemShop4.gun3, stat.available] = true;
inv_name[#ItemShop4.gun3, stat.equipped] = false;
inv_name[#ItemShop4.gun3, stat.type] = guns1.SHOTGUN;
inv_name[#ItemShop4.gun3, stat.price] = 25;
inv_name[#ItemShop4.gun3, stat.spr_sheet] = spr_sheet_name;
inv_name[#ItemShop4.gun3, stat.spr_index] = ItemShop4.gun3;
#endregion
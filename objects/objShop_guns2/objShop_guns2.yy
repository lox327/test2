{
    "id": "2ada2166-b707-46fd-9fcf-5c6ea084fab5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShop_guns2",
    "eventList": [
        {
            "id": "196c75c9-d5c3-4f2a-b090-7f07d22556a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2ada2166-b707-46fd-9fcf-5c6ea084fab5"
        },
        {
            "id": "2855c5bf-c150-4357-8098-ebd1a0733c30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "2ada2166-b707-46fd-9fcf-5c6ea084fab5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "99caee38-f03a-4937-b2be-4eae05270879",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
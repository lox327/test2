{
    "id": "8ebfcd5c-6822-4786-b3ad-ce4c751f03d1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "enemyBullet21",
    "eventList": [
        {
            "id": "97579df8-321b-4b4e-a836-4f8fbcb0bb2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8ebfcd5c-6822-4786-b3ad-ce4c751f03d1"
        },
        {
            "id": "1a04b951-225e-47e9-acad-1a4f3e96d0fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "8ebfcd5c-6822-4786-b3ad-ce4c751f03d1"
        },
        {
            "id": "2f545e91-4fc6-4048-ab33-d805ebd7fa13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "762cde50-f7cd-4924-89c4-3277b50303cd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8ebfcd5c-6822-4786-b3ad-ce4c751f03d1"
        },
        {
            "id": "d3e779a8-79a9-4db3-94a4-f689b9c1557f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e112a55-f75c-47df-9092-27fd121f56af",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8ebfcd5c-6822-4786-b3ad-ce4c751f03d1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a0408bc6-f74e-4b80-b4fc-4f3a4690fb5b",
    "visible": true
}
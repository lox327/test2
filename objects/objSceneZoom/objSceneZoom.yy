{
    "id": "55aca347-b518-44e0-af64-86d4eccdf5f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objSceneZoom",
    "eventList": [
        {
            "id": "2d010201-d872-458d-ab2c-36c23b1744c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 36,
            "eventtype": 9,
            "m_owner": "55aca347-b518-44e0-af64-86d4eccdf5f2"
        },
        {
            "id": "b36b9ee3-96a9-46c0-b20f-06d4cf17e6f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "55aca347-b518-44e0-af64-86d4eccdf5f2"
        },
        {
            "id": "1b67a276-6814-4dd9-a4af-e89c668d09d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "55aca347-b518-44e0-af64-86d4eccdf5f2"
        },
        {
            "id": "a0556ed6-02d5-4955-af97-152fab4372c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "55aca347-b518-44e0-af64-86d4eccdf5f2"
        },
        {
            "id": "1923ae60-7f76-4c66-993d-b7ff85e9349b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "55aca347-b518-44e0-af64-86d4eccdf5f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
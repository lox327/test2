/// @description draw horizontal black scene bars
if (!drawLetterbox) exit;
index = 0;
if (zoom) index = 25;

var view_w = camera_get_view_width(view_camera[0]);
var view_h = camera_get_view_height(view_camera[0]);

draw_set_color(c_black);
draw_rectangle(0,0,view_w, index, false);
draw_rectangle(0,view_h,view_w, view_h-index, false);
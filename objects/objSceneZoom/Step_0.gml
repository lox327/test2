/// @description zoom in on scene at trigger points
//show_debug_message("zoom")

var _objSceneZoom = instance_nearest(objPlayer.x, objPlayer.y, objSceneZoom);

if (_objSceneZoom.zoomDone1) {
	_index = 1;
	zoom = false;
	exit;
}


if (proximity(_objSceneZoom.prox)) {
	//show_debug_message("1");
	_index = -1;
	zoom = true;
}
else {
	//show_debug_message("2");
	_index = 1;
	zoom = false;
}

zoom_level = clamp(zoom_level + _index*.01, max_zoom, min_zoom);

//get current size
var view_w = camera_get_view_width(view_camera[0]);
var view_h = camera_get_view_height(view_camera[0]);

//get new sizes by interpolating current and target zoom size
var new_w = lerp(view_w, zoom_level * default_zoom_width, rate);
var new_h = lerp(view_h, zoom_level * default_zoom_height, rate);

//apply the new size
camera_set_view_size(view_camera[0],new_w, new_h);


//IS THIS NEEDED?
/*var vpos_x = camera_get_view_x(view_camera[0]);
var vpos_y = camera_get_view_y(view_camera[0]);

//change coords of cam so zoom is centered
var new_x = lerp(vpos_x, vpos_x+(view_w-zoom_level * default_zoom_width)/2, rate)+500;
var new_y = lerp(vpos_y, vpos_y+(view_h-zoom_level * default_zoom_height)/2, rate);

camera_set_view_pos(view_camera[0],new_x, new_y);

show_debug_message(new_x)*/
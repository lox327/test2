/// @description Insert description here
// You can write your code in this editor
zoom = false;
zoom_level = 1;
_index = 1;

//set the rate/speed of interpolation
rate = 0.05;

//min zoom amount
min_zoom = 1;

//max zoom amount, updated by CC
max_zoom = 0.75;

default_zoom_width = camera_get_view_width(view_camera[0]);
default_zoom_height = camera_get_view_height(view_camera[0]);

//draw letterbox
drawLetterbox = true;

prox = 150;

_test = true;
zoomDone1 = false;
//alarm[0] = room_speed * 3;


//DIALOG for text overview
dialog = [];
dialogLine = 0;

d_test = 
addDialog("things have been bad around here lately...", true, "", false, self);
addDialog("things have been bad around here lately222...", false, "", false, self);

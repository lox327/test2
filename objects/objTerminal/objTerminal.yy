{
    "id": "222dc350-7c51-46f1-a1ec-013755c96af3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTerminal",
    "eventList": [
        {
            "id": "e6cc6bd6-82bf-416f-ae37-e72a8e10f17a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "222dc350-7c51-46f1-a1ec-013755c96af3"
        },
        {
            "id": "9a78aeaf-49cf-4c56-9a03-894fc483fd36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "222dc350-7c51-46f1-a1ec-013755c96af3"
        },
        {
            "id": "96271b75-5844-47c8-842f-ce6807ea8ae8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "222dc350-7c51-46f1-a1ec-013755c96af3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7e112a55-f75c-47df-9092-27fd121f56af",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7f76e7b1-d384-4f60-b46b-403fae67af34",
    "visible": true
}
//used for bg parallax fx
//room needs a bg layer called "Background_para1"
var _id = layer_get_id("Background_para1");
if (_id != -1) {
	var _cam_x = camera_get_view_x(view_camera[0]);
	var _cam_y = camera_get_view_y(view_camera[0]);
	
	layer_x("Background_para1", _cam_x * paraDrag1);
	layer_y("Background_para1", _cam_y * paraDrag1);
}


//scene zoom
var _objSceneZoom = instance_nearest(x, y, objSceneZoom);

if (instance_exists(_objSceneZoom) && _objSceneZoom.zoom) {
		x += (_objSceneZoom.x - x ) * cameraDrag;
		y += (_objSceneZoom.y - y ) * cameraDrag;
}
else {
	x += (objPlayer.x - x ) * cameraDrag;
	y += (objPlayer.y - y ) * cameraDrag;
}


//screen shake
if (shake > 0) {
	x += random_range(-shake,shake);
	y += random_range(-shake,shake);
	shake *= shakeDecrease;
}
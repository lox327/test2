cameraDrag = .04; //smaller num = more drag, .04 default, .01 increments
paraDrag1 = .25;

x = objPlayer.x;
y = objPlayer.y;

shake = 0;
shakeDecrease = 0.90; //smaller num = small decrease; .05 increments

//shake intensity, whole num increment
defaultShake = 5;
smallShake = 3;
bigShake = 7;
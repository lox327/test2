/// @description Insert description here
// You can write your code in this editor
if ( keyboard_check_pressed(vk_delete) ) active = !active;
else exit;

if (active) {
	var w=0, h=0;
	//ds_list_size(global.inv2)
	for (var i = 0;i<3; i++) {
		//var btn = instance_create_depth(objPlayer.x, objPlayer.y, -10000, objButton);
		var btn = instance_create_layer(objPlayer.x+(w*96), objPlayer.y+(h*96), "Instances", objButton);
		btn.item_id = global.inv2[|i];
		btn.inv_id = i;
		w++;
		
		if (w >= (floor(window_get_height()/96))) {
			w = 0; h++; //move to next row	
		}
	}
	
}

else {
	if (instance_exists(objButton))	 {
		with(objButton) instance_destroy();
	}
}
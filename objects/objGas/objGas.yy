{
    "id": "56d464a8-66b7-438c-8b06-f434d0e3bac7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objGas",
    "eventList": [
        {
            "id": "ccc89f3d-34d1-4d5b-877c-cb66a2f6d2d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "56d464a8-66b7-438c-8b06-f434d0e3bac7"
        },
        {
            "id": "600cf072-c770-4702-8a5d-caf6ebd61f0f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "56d464a8-66b7-438c-8b06-f434d0e3bac7"
        },
        {
            "id": "311fb04e-78e2-44e0-8f13-039ba16afb3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "56d464a8-66b7-438c-8b06-f434d0e3bac7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c94b35fd-89b7-4a48-a5cc-642409162724",
    "visible": true
}
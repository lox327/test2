{
    "id": "99caee38-f03a-4937-b2be-4eae05270879",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShopMgr_Parent",
    "eventList": [
        {
            "id": "def3680a-9995-44df-b449-ecec9bf81f9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "99caee38-f03a-4937-b2be-4eae05270879"
        },
        {
            "id": "2e21d71e-ebe9-48f5-b39b-e32cef2de64b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99caee38-f03a-4937-b2be-4eae05270879"
        },
        {
            "id": "617941a4-3b1e-4ef0-b378-b5982c4b7520",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99caee38-f03a-4937-b2be-4eae05270879"
        },
        {
            "id": "58fbff31-eb6e-4e38-8f86-791fcb54f274",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "99caee38-f03a-4937-b2be-4eae05270879"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
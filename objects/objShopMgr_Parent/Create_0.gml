///@description Shop Items, using sprite sheet: spr_sheet_name
depth = objPlayer.depth-9999;
display = false;
slot_selected = 0;

//STATS for items 
enum stat {
	name,
	description,
	damage_low,
	damage_high,
	weaponEnergyUse,
	weaponEnergyTotal,
	weaponEnergyRegen,
	armor,
	mod1,
	mod2,
	modifier,
	mod_amount,
	mod_desc,
	slow_duration,
	slow_amount,
	dot_duration,
	dot_amount,
	splash_rad,
	splash_amount,
	owned,
	available,
	equipped,
	type,
	price,
	spr_sheet,
	spr_index,
	total
}

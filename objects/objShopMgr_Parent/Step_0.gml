///@description handle cursor movement across items
if (!display) exit;

#region cursor
//LEFT
if (keyboard_check_pressed(vk_left)) {
	if (slot_selected != 0) {
		slot_selected--;
	}
	else {
		//at beginning - do nothing or wrap around...
	}
}

//RIGHT
if (keyboard_check_pressed(vk_right)) {
	if (slot_selected != itemShopTotal-1) {
		slot_selected++;
	}
	else {
		//at end - do nothing or wrap around...
	}
}
#endregion

//ENTER
if (keyboard_check_pressed(vk_enter)) {
	//check if item available
	if ( inv_name[#slot_selected, stat.available] ) {
		//check for enough credits
		if ( objPlayer.invCredits >= inv_name[#slot_selected, stat.price] ) {
	
			//udpate item stat available to false
			inv_name[#slot_selected, stat.available] = false;
			inv_name[#slot_selected, stat.owned] = true;
	
			//subtract price of item from char credits
			objPlayer.invCredits -= inv_name[#slot_selected, stat.price];
	
			var curr_size = ds_grid_width(global.item_index);
	
			for (var i = 0; i < curr_size; i++) {
				//hard coding to add to 0 for now
				//global.item_index[#curr_size-1, i] = inv_name[#slot_selected, i];
				if ( global.item_index[#i, 0] == 0  ) { //if inv slot is not occupied
					//add shop item to inv one stat at a time, easier way for this?
					for (var j = 0; j < stat.total; j++) {
						global.item_index[#i, j] = inv_name[#slot_selected, j];
					}
					break;
				}
			}
			//ds_grid_resize(global.item_index,curr_size+1,stat.total);
	
		}
		else show_message("not enough"); //not enough credits
	} else show_message("not avail");	 //already purchased
	
}

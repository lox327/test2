/// @description close inv screen, reset slot sel to 0
// need to move to step and use global button instead of hard code
if (display) {
	display = false;
	slot_selected = 0;
}
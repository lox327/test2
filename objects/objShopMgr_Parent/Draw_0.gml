/// @description Insert description here
if (!display) exit;

//black out bg
//set transparent
draw_set_alpha(0.60);
draw_rectangle_color(0, 0, room_width, room_height, c_black, c_black, c_black, c_black, false);

//back to normal
draw_set_alpha(1.0);

//draw shop avatar
draw_sprite(spr_avatar_name,0,room_width/2-225,room_height/2);

//draw shop info
draw_set_color(c_white);
draw_set_font(fnt_small_bold);
draw_set_halign(fa_left);
draw_text(room_width/2-250,room_height/2+105,shopDesc);

draw_set_font(fnt_med_reg);
draw_text(room_width/2-250,room_height/2+102,sep);
draw_text(room_width/2-250,room_height/2+120,shopName);

draw_set_font(fnt_small_reg);
draw_text_ext(round(room_width/2-250),round(room_height/2+140),shopText,15,200);

//draw UI bg
draw_sprite(spr_inv_ui,0,room_width/2+90,room_height/2-30);

//draw player credits
draw_set_font(fnt_GUI);
draw_text(room_width/2+175,room_height/2-175,"credits: " + string(objPlayer.invCredits));
//draw_text(room_width/2+175,room_height/2-115,objPlayer.invCredits);

var slot_x = 100;
var slot_y = 115;
for ( var i = 0; i < itemShopTotal; i++ ) {
	//draw empty inv slots
	draw_sprite(spr_slot,0,room_width/2 - slot_x + (i * 32),room_height/2-slot_y);

	//draw item sprite - only draw sprite if it's available (hasn't been purchased)
	if ( inv_name[#i, stat.available] == true ) {
		draw_sprite(inv_name[#i, stat.spr_sheet],inv_name[#i, stat.spr_index],room_width/2 - slot_x + (i * 32),room_height/2-slot_y);
		
		//draw empty mod slot
		if ( inv_name[#i, stat.mod1] == 1 ) draw_sprite(spr_mod_empty,0,room_width/2 - slot_x + (i * 32),room_height/2-slot_y);
		if ( inv_name[#i, stat.mod2] == 1 ) draw_sprite(spr_mod_empty,0,room_width/2 - slot_x + (i * 32),room_height/2-slot_y+16);

	}
		
}

//draw cursor
draw_sprite(spr_cursor,0,room_width/2 - slot_x+(slot_selected*(32)),room_height/2-slot_y);

//draw large item
if (inv_name[#slot_selected, stat.available] == true) draw_sprite(inv_name[#slot_selected, stat.spr_sheet],inv_name[#slot_selected, stat.spr_index],room_width/2 + 150,room_height/2-15);


//TEXT
//draw item info text of selected item
var text_x = 250;
var text_y = 20;
if ( inv_name[#slot_selected, stat.available] == true ) {
	draw_set_font(fnt_GUI);
	var name = inv_name[#slot_selected, stat.name];
	var type = inv_name[#slot_selected, stat.type];
	var text3 = inv_name[#slot_selected, stat.description];
	var price = inv_name[#slot_selected, stat.price];
	
	draw_set_font(fnt_med_reg);
	draw_text(room_width/2+text_x-100,room_height/2+text_y,string_upper(name));
	
	draw_set_font(fnt_GUI);
	draw_text(room_width/2+text_x-100,room_height/2+text_y+20,string_upper(getNameFromType(type)));
	
	draw_text(room_width/2+text_x-100,room_height/2+text_y+35,text3);
	draw_text(room_width/2+text_x-100,room_height/2+text_y+50,"$" + string(price));
	
	//gun stats
	draw_set_halign(fa_left);
	if ( inv_name[#slot_selected, stat.modifier] == 0) {	
		var dmg_low = inv_name[#slot_selected, stat.damage_low];
		var dmg_high = inv_name[#slot_selected, stat.damage_high];
		var weaponEnergyUse = inv_name[#slot_selected, stat.weaponEnergyUse];
		var weaponEnergyTotal = inv_name[#slot_selected, stat.weaponEnergyTotal];
		draw_text(room_width/2-100,room_height/2,"damage_low: " + string(dmg_low));
		draw_text(room_width/2-100,room_height/2+15,"damage_high: " + string(dmg_high));
		draw_text(room_width/2-100,room_height/2+30,"weapon energy use: " + string(weaponEnergyUse));
		draw_text(room_width/2-100,room_height/2+45,"weapon energy total: " + string(weaponEnergyTotal));
	}

	//desc text for mods, more to follow
	else {
		var modifier = inv_name[#slot_selected, stat.modifier];
		var mod_desc = inv_name[#slot_selected, stat.mod_desc];
		
		draw_text(room_width/2-100,room_height/2,modifier);
		draw_text_ext(room_width/2-100,room_height/2+20,mod_desc,15,150);
		
		//need script to pull all mod info from the various modtypes, ghetto for now
		//get mod_type (mod_amount) - fx, or base
		var mod_amount = inv_name[#slot_selected, stat.mod_amount];
		
		if (mod_amount == objMod_fire ) {
			var mod_duration = inv_name[#slot_selected, stat.dot_duration];
			var amount = inv_name[#slot_selected, stat.dot_amount];
		}
		else if (mod_amount == objMod_acid ) {
			var mod_duration = inv_name[#slot_selected, stat.splash_rad];
			var amount = inv_name[#slot_selected, stat.splash_amount];
		}
		else if ( mod_amount == objMod_shock ) {
			var mod_duration = inv_name[#slot_selected, stat.slow_duration];
			var amount = inv_name[#slot_selected, stat.slow_amount];	
		}
		else { //this is base stat mod
			var mod_duration = "-";
			var amount = mod_amount;
		}
		
		draw_text(room_width/2-100,room_height/2+60,"duration: " + string(mod_duration));
		draw_text(room_width/2-100,room_height/2+75,"amount: " + string(amount));
	}
}

//show_debug_message(camera_get_view_x(view_camera[0]))
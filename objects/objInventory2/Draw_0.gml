/// @description Insert description here
// You can write your code in this editor
if (!display) exit;

info_x = objPlayer.x+10;
info_y = objPlayer.y;

slots_x = info_x;
slots_y = info_y + 60;

//draw inv back
draw_sprite_part_ext(spr_inv_ui,0,cell_size,0,inv_UI_width,inv_UI_height,objPlayer.x,objPlayer.y,1,1,c_white,1);

//draw inv info to screen
var info_grid = ds_player_info;
var c = c_black;
draw_text_color(objPlayer.x + 5, objPlayer.y, info_grid[# 0, 3] + ": " + info_grid[# 1,3], c, c, c, c, 1);

//draw inv values to screen
for (var i = 0; i < 3; i++) {
	draw_text_color(objPlayer.x + 150 + (i*50), objPlayer.y, info_grid[# 1, i] , c, c, c, c, 1);
}


//inv items
var ix, iy, xx, yy, sx, sy, curr_item, inv_grid;
ix = 0; iy = 0;	inv_grid = ds_inventory;
for (var i = 0; i < inv_slots; i++) {
	//xy of slot
	xx = slots_x + (cell_size + 2)*ix;
	yy = slots_y + (cell_size + 2)*iy;
	
	//item
	curr_item = inv_grid[# 0, i];
	sx = (curr_item mod spr_inv_items_cols) * cell_size;
	sy = (curr_item div spr_inv_items_rows) * cell_size;
	
	//draw slot and item
	/*draw_sprite_part_ext(
		spr_inv_ui, 0,	0, 0, cell_size, cell_size,
		xx, yy, 1, 1, c_white, 1);*/
	
	//draw empty slots
	draw_sprite_part_ext(
		spr_slot, 0, sx, sy, cell_size, cell_size,
		xx, yy, 1, 1, c_white, 1);
	
	//draw items
	if (inv_grid[# 2,i] == true) {
		draw_sprite_part_ext(
			spr_inventory_items, 0,	sx, sy, cell_size, cell_size,
			xx, yy, 1, 1, c_white, 1);
	}
		
	
	
	//draw item amount
	
	//increment
	ix = i mod inv_slots_width;
	iy = i div inv_slots_width;
	
}

//draw cursor	
draw_sprite(spr_cursor,0,slots_x+(slot_selected*(cell_size+2)),slots_y)

/*
//draw empty inv slots
for (var i = 0; i<5; i++) {
	draw_sprite(gun1, 0, objPlayer.x + (i*50), objPlayer.y);
}
	
//draw current inv items
for (var i = 0; i<3; i++) {
	draw_sprite(gun2, 0, objPlayer.x + 5 + (i*50), objPlayer.y);
}
	
//draw cursor
draw_sprite(invCursor, 0, objPlayer.x, objPlayer.y);

//draw inv info to screen
var info_grid = ds_player_info;
var c = c_black;
draw_text_color(objPlayer.x, objPlayer.y, info_grid[# 0, 3] + ": " + info_grid[# 1,3], c, c, c, c, 1);

//draw inv values to screen
for (var i = 0; i < 3; i++) {
	draw_text_color(objPlayer.x, objPlayer.y + 50 + (i*50), info_grid[# 1, i] , c, c, c, c, 1);
}
*/
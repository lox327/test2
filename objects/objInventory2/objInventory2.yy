{
    "id": "854c8e1e-d294-41cc-8720-86d7f53e2970",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objInventory2",
    "eventList": [
        {
            "id": "48dc3640-53c1-40dd-b301-0288df75e0cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "854c8e1e-d294-41cc-8720-86d7f53e2970"
        },
        {
            "id": "baec9797-deca-496e-93d0-0ee9a998b914",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 36,
            "eventtype": 9,
            "m_owner": "854c8e1e-d294-41cc-8720-86d7f53e2970"
        },
        {
            "id": "a530e557-cbd3-4e75-819f-e55b011111fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "854c8e1e-d294-41cc-8720-86d7f53e2970"
        },
        {
            "id": "8aa7f156-7fa5-444b-8b1d-3605cbf2fe9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "854c8e1e-d294-41cc-8720-86d7f53e2970"
        },
        {
            "id": "09c82569-9cbb-4fd7-8a28-ad924fde908d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "854c8e1e-d294-41cc-8720-86d7f53e2970"
        },
        {
            "id": "85d6a4e8-8e1d-4570-9557-71ed28d44b75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 34,
            "eventtype": 9,
            "m_owner": "854c8e1e-d294-41cc-8720-86d7f53e2970"
        },
        {
            "id": "d2fabe28-050c-4489-a072-09f0195b94aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 35,
            "eventtype": 9,
            "m_owner": "854c8e1e-d294-41cc-8720-86d7f53e2970"
        },
        {
            "id": "b44acf70-43a8-4652-a68e-d89c882bc47c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "854c8e1e-d294-41cc-8720-86d7f53e2970"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
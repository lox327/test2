/// @description handle cursor movement across items
if (!display) exit;

//LEFT
if (keyboard_check_pressed(vk_left)) {
	if (slot_selected != 0) {
		slot_selected--;
	}
	else {
		//at beginning - do nothing or wrap around...
	}
}

//RIGHT
if (keyboard_check_pressed(vk_right)) {
	if (slot_selected != 7) {
		slot_selected++;
	}
	else {
		//at end - do nothing or wrap around...
	}
}

//DOWN
if (keyboard_check_pressed(vk_down)) {
	slot_selected = 8
}

//UP
if (keyboard_check_pressed(vk_up)) {
	slot_selected = 0
}

//ENTER
if (keyboard_check_pressed(vk_enter)) {
	if (ds_inventory[# 2,slot_selected] == true) {
		ds_inventory[# 2,slot_selected] = false;
		//show_message(ds_grid_get(ds_inventory,2,slot_selected))
	}
}
		show_debug_message(slot_selected)

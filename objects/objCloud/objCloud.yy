{
    "id": "4c6eb0ec-fc23-4048-82be-7db43b42011e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objCloud",
    "eventList": [
        {
            "id": "f6ea6338-5d80-44aa-94a0-18047f9e0307",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4c6eb0ec-fc23-4048-82be-7db43b42011e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
    "visible": true
}
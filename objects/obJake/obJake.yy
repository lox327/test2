{
    "id": "20b86d6d-540d-4fd7-bfda-f5b8ba543ef9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obJake",
    "eventList": [
        {
            "id": "23921969-036f-4b69-bf7a-34aca95398b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "20b86d6d-540d-4fd7-bfda-f5b8ba543ef9"
        },
        {
            "id": "d07846f3-0d19-4fc3-80f0-005a9372d053",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "20b86d6d-540d-4fd7-bfda-f5b8ba543ef9"
        },
        {
            "id": "6f204bc1-ddc0-4df8-9a36-e0ba06987b07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "20b86d6d-540d-4fd7-bfda-f5b8ba543ef9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
    "visible": true
}
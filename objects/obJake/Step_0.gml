if ( !proximity(prox) ) exit;

if (global.btnAction) {
	//intro
	if (global.checkpoint1 == 0) startDialog(self, d_jake0);
	
	//boss2 down
	else if (global.checkpoint2 == 1) {
		startDialog(self, d_jake2);
	}
	
	//boss1 down
	else if (global.checkpoint1 == 1) {
		if (global.b_checkpoint1 == 0) startDialog(self, d_jake1);
		//bounty1 complete
		else startDialog(self, d_jake1_bounty);
	}
	
}
hasInfo = true;

//DIALOG for [quests]
dialog = [];
dialogLine = 0;
prox = 50;
name = "jake";

d_jake0 = 
addDialog("things have been bad around here lately...", false, avatarDirtbag1, false, self);

d_jake1 = 
addDialog("I heard you took down " + global.boss1_name + "...", true, avatarDirtbag1, false, self);
addDialog("the bounty terminal is back online now. could be a nice way to get some extra cash.", false, avatarDirtbag1, false, self);


d_jake2 = 
addDialog("I heard you took down " + global.boss2_name + "...", true, avatarDirtbag1, false, self);
addDialog("check the local shops for new gear.", false, avatarDirtbag1, false, self);


d_jake1_bounty = 
addDialog("finished your first bounty huh?", true, avatarDirtbag1, false, self);
addDialog("the board changes often, check back in every now and again...", false, avatarDirtbag1, false, self);




/*
//QUEST
/// @description map for storing quests

//list for storing quest objs
global.questsJake = ds_list_create();

//map of quests to store in list
var quest = ds_map_create();
ds_map_add(quest, "name",	"quest1");
ds_map_add(quest, "reward",	"mod1");
ds_map_add(quest, "desc",	"1...is wanted for: blah");
ds_map_add(quest, "location",	"north");
ds_map_add(quest, "available",	true);
ds_map_add(quest, "completed",	false);
ds_list_add(global.questsJake, quest);

quest = ds_map_create();
ds_map_add(quest, "name",	"quest2");
ds_map_add(quest, "reward",	"mod2");
ds_map_add(quest, "desc",	"2...is wanted for: mehh");
ds_map_add(quest, "location",	"south");
ds_map_add(quest, "available",	false);
ds_map_add(quest, "completed",	false);
ds_list_add(global.questsJake, quest);

quest = ds_map_create();
ds_map_add(quest, "name",	"quest3");
ds_map_add(quest, "reward",	"mod3");
ds_map_add(quest, "desc",	"3...is wanted for: murr");
ds_map_add(quest, "location",	"south2");
ds_map_add(quest, "available",	false);
ds_map_add(quest, "completed",	false);
ds_list_add(global.questsJake, quest);
*/
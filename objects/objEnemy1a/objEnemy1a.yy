{
    "id": "06dd7929-5814-4f14-b75b-2583c5ed096c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemy1a",
    "eventList": [
        {
            "id": "fcfac3e4-6289-432f-a9e5-8fe6d905289e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "06dd7929-5814-4f14-b75b-2583c5ed096c"
        },
        {
            "id": "c1d7878e-ecf3-4dea-857a-4fab0adbe241",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "06dd7929-5814-4f14-b75b-2583c5ed096c"
        },
        {
            "id": "95758df2-db50-4756-ab8a-61b91180a8d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "06dd7929-5814-4f14-b75b-2583c5ed096c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0353dfc-c14d-417e-b283-64ff3ecacc3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "980edd4c-24a9-4333-9959-0062d92d417f",
    "visible": true
}
{
    "id": "a1e05594-795e-4701-922e-149c7e377893",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objArm2",
    "eventList": [
        {
            "id": "28b56541-58ef-4c91-a3fb-1294abdafe0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a1e05594-795e-4701-922e-149c7e377893"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "81c2ab68-b483-4988-a1e3-7b7a34e16ef8",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d16b8db7-2939-41d6-b7ef-093786483ebe",
    "visible": true
}
{
    "id": "7db6851e-c266-4cbe-a164-f4cbc7197bce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShock",
    "eventList": [
        {
            "id": "142a934f-ab93-48b8-9a1d-e1fc8eb42a0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7db6851e-c266-4cbe-a164-f4cbc7197bce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8e51a1b4-c4e9-4df9-b5d6-99d1e8967c5e",
    "visible": true
}
///@description get active status for bounty, activate elements
val = ds_list_find_value(global.bounties, 1);

if (ds_map_find_value(val, "active")) {
	objEnemy1.isActive = true;
	objEnemy1a.isActive = true;
	//objPower2.isActive = true;
	instance_create_layer(1537, 392, "Instances", objTank);
	instance_create_layer(1500, 500, "Instances", objTank);
	
	objBob.hasInfo = true;
	//...more bounty objs or npcs set to isActive true here as well? 
}
/// @description draw player shadow
if (isShop()) exit;

draw_self(); //add prop for rooms where char should not display
draw_sprite(sprShadow, 0, x-13,y+16);

//draw_circle(x, y, ch_distance, true);

//should only show when in AIM mode?
#region crosshair
if (aimLock) {
	switch (dir) {
	case "up":
		draw_sprite(crosshair, 0, x,y-ch_distance);
	break;
	
	case "down":
		draw_sprite(crosshair, 0, x,y+ch_distance);
	break;
	
	case "left":
		draw_sprite(crosshair, 0, x-ch_distance,y-10);
	break;
	
	case "right":
		draw_sprite(crosshair, 0, x+ch_distance,y-10);
	break;
	
	//diag
	case "up_right":
		draw_sprite(crosshair, 0, x+ch_distance/ch_diag+10,y-ch_distance/ch_diag+10);
	break;
	
	case "down_right":
		draw_sprite(crosshair, 0, x+ch_distance/ch_diag,y+ch_distance/ch_diag);
	break;
	
	case "up_left":
		draw_sprite(crosshair, 0, x-ch_distance/ch_diag,y-ch_distance/ch_diag);
	break;
	
	case "down_left":
		draw_sprite(crosshair, 0, x-ch_distance/ch_diag,y+ch_distance/ch_diag);
	break;
	
}
}

#endregion

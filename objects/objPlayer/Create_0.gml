//setDepth();
randomize();
getGamepad();

//math for speed and diag speed
spd_const = 2;

playerSpeed = (sprite_width/image_number*0.05) + spd_const;
playerSpeed_climb = playerSpeed*.75;
walkSpeed = playerSpeed;
bashSpeed = walkSpeed*.25;
diagSpeed = round(playerSpeed * (sqrt(2)/2)) + .00;

if (global.gpConnected) {
	spd_const = 1.0;
	diagSpeed += .50;
}

xadd = 0;
yadd = 0;
xaddSpd = 0;
yaddSpd = 0;

charged = false;
charging = false;
chargeShot = false;

animation = (playerSpeed*0.1)*image_number;
//grid = sprite_width;
grid = 20;
//show_debug_message(grid)
dir = "right";
pause = false;

ch_distance = 125; //crosshair distance
ch_diag = 1.40;
aimLockSpeed = 1;
aimLockConst = .35;

//flash = 0;


//DEFAULTS
health = 90;
timer = 0;
armEnergy = 11;
playerMove = true;
chargeMax = 3;
chargeCount = 2;

#region gun and mod properties
//GUNS
currentGun =	guns1.DEFAULT;	//current gun selected
secondaryGun =  guns1.NONE;		//secondary gun selected
bulletType = objBullet;			//default bullet type, effected by mods (stun, fire...)
defaultEnergy = 100;			//current energy for default gun
defaultUsage = 5;				//how much to deplete the default gun (need name refactor)
weaponEnergy = 500;				//current energy for special gun
weaponEnergyUse = 5				//how much to deplete the special gun (need name refactor)

gunSprite = spr_gun_hud;
gunSpriteIndex = currentGun;

defaultMax = 100;
weaponEnergyMax = 100;

bulletDmg = 1;
fireRate = 10;		//for rifles
currentBullet = 0;	//for rifles
bulletMax = 3;		//for rifles
spread = 4;			//for shotguns
shoot_x = 0;
shoot_y = 0;

//MODS
mod1 = 0;			//mod slot full or empty
modifier = "";		//base, fx...
mod_desc = "";
mod_amount = 0;

//shock
slow_duration = 0;
slow_amount = 0;

//fire
dot_duration = 0;
dot_amount = 0;


//acid
splash_rad = 0;
splash_amount = 0;

grenadeDist = -1;

#endregion

//BOUNTIES (global.bounties from objBountyState)
hasBounty = false;
bountyMax = 2;
playerBounties = ds_list_create(); //list for player bounties
//ds_list_add(playerBounties, ds_list_find_value(global.bounties, 1));

//get the bounty map from the player bounty list
//val = ds_list_find_value(playerBounties, 0);
//if (ds_map_find_value(val, "active")) show_message(ds_map_find_value(val, "name"));


invCredits = 50;
invGun = 0;

//which inv to use?
inventoryList = ds_list_create();
invSlot[0] = 0;
global.inv = ds_list_create();
global.inv2 = ds_list_create();


//should be ENUM?
enum EquippedItems
{ 
     ARM,
	 GUN,
	 OTHER
};
/*
equippedList[EquippedItems.ARM] = noone; //arm
equippedList[EquippedItems.GUN] = noone; //gun
equippedList[EquippedItems.OTHER] = noone; //other

equippedList[EquippedItems.ARM] = objArm1;
equippedList[EquippedItems.GUN] = objGun2;
*/
//create in special INIT object?
//instance_create_layer(0, 0, "Instances", equippedList[0]); //arm
//instance_create_layer(200, 500, "Instances", equippedList[1]); //gun


//MAX

healthMax = 100;


//DIALOG
/*dialog = [];
dialogLine = 0;

d_intro_text = 
addDialog("hello world! this is a long message, lots of words. blah, whatever. im hungry af........", true, sprAvatar, true);
addDialog("how are you? lots more words, blah blah blah....................", false, sprAvatar2, false);

d_second_text = 
addDialog("lots more blah", false, sprAvatar2, false);

startDialog(self, d_second_text);
*/

//player/game state stuff
enum playerState {
	IDLE,
	SHOOT,
	ATTACK,
	HIT,
	KNOCKBACK,
	AIM
}

state = playerState.IDLE;
isShooting = false;
isHit = false;
hitAngle = 0;

//alarms
//alarm[1] = room_speed * 1;
/// @description draw character hud
//x_left, y_top, x_right, y_bot
//depth = 0;
display_set_gui_size( camera_get_view_width(view_camera[0]), camera_get_view_height(view_camera[0]) );

if (isShop()) exit;

left_align = 100/2;
right_align = 200/2;
xbar = 25/2;

var pc = (health / healthMax) * 100;
draw_healthbar(left_align, xbar*1, right_align, xbar*2, pc, c_black, c_red, c_red, 0, true, true);

var pc2 = (weaponEnergy / weaponEnergyMax) * 100;
draw_healthbar(left_align, xbar*2, right_align, xbar*3, pc2, c_black, c_blue, c_blue, 0, true, true);

var pc3 = (defaultEnergy / defaultMax) * 100;
draw_healthbar(left_align, xbar*3, right_align, xbar*3+2, pc3, c_black, c_gray, c_gray, 0, true, true);

//draw_sprite(spr_gun_hud, currentGun, 15, 10);
draw_sprite(gunSprite, gunSpriteIndex, 15, 10);

//draw bounty if active
if (hasBounty) draw_sprite_ext(bounty1, 0, 110, 12, .15, .15, 0, c_white, 1);

//fps mon
draw_text(580, 5, string(fps));
//draw_text(550, 20, string(fps_real));
																

//demo for 'special'attack
/*for (i = 0; i < chargeMax; i++) {
	var charge = 0;
	if (chargeCount > i) charge = 1;
	draw_sprite(sprSpecial, charge, 15*(i+1), 50);
}*/


//char currency
//draw_text(15, xbar*4, "credits: " + string(objPlayer.invCredits));
//draw_text(525, 15, string(fps) + "fps");

//DEBUG
if (0) {
	draw_text(400, 15*1, "health: " + string(health));
	draw_text(400, 15*2, "weapon energy: " + string(objPlayer.weaponEnergy));
	draw_text(400, 15*3, "credits: " + string(objPlayer.invCredits));
	
	draw_text(400, 15*5, "checkpoint1: " + string(global.checkpoint1));
	draw_text(400, 15*6, "checkpoint2: " + string(global.checkpoint2));
	draw_text(400, 15*7, "d_checkpoint1: " + string(global.d_checkpoint1));
	draw_text(400, 15*8, "d_checkpoint2: " + string(global.d_checkpoint2));
	
	//draw_text(400, 84, "test: " +   string(instance_find(objEnemy3,0)));
	//draw_text(10, 72, "start point x: " + string(sp.x));
	//draw_text(10, 84, "start point y: " + string(sp.y));
}


//}
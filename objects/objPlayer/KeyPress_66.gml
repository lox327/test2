/// @description key to display bounties
val = ds_list_find_value(playerBounties, 0);

//loop through and print out all details
if (ds_map_find_value(val, "active")) {
	show_debug_message("player bounties:");
	show_debug_message(ds_map_find_value(val, "name"));
}
else show_debug_message("no active bounties");
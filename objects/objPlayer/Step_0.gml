//show_debug_message("p")
//armEnergy++;

if (instance_exists(objGrenade)) {
	grenadeDist = point_distance(x,y,objGrenade.x,objGrenade.y);
	with (objGrenade) {
		x += 2;
		show_debug_message(dist);
		
		if (dist > 300/2) {
			instance_destroy();
			grenadeDist = -1;
		}
		else if (dist > 250/2) image_index = 0;
		else if (dist > 200/2) image_index = 1;
		else if (dist > 100/2) image_index = 2;
		else if (dist > 50/2) image_index = 1;

	}
}

//ensure that player depth is same as Instances layer for traveling to other rooms and back
if (layer_exists("Instances")) depth = layer_get_depth("Instances");

getInput();

//holding down charge button
//if (keyboard_check_direct(ord("Z"))) {
if (keyboard_check_direct(vk_space)) {
	//first time pressed, start the alarm
	if (alarm[0] == -1) {
		alarm[0] = 60;
		charging = true;
	}
	
	//if fully charged
	else if (alarm[0] == 0) {
		//show_debug_message("CHARGED")
		alarm[0] = -2;
		charged = true;
		charging = false;
	}
	
	//charging
	//sprite_index = enemy4Attack3_charge;
	//show_debug_message(armEnergy)
}

//releasing charge button
//else if (keyboard_check_released(ord("Z"))) {
else if (keyboard_check_released(vk_space)) {
	//do charge attack
	if (charged) {
		//show_debug_message("BANG")
		//chargeShot = true;
		state = playerState.SHOOT;
		
        if (animation_end(sprite_index)) //switch action
        { 
            //enemyState = EnemyState.ATTACK3;
        }
	}
	
	//reset alarm and charge flag
	//show_debug_message("rel")
	alarm[0] = -1;
	charging = false;
	chargeShot = false;
	//sprite_index = playerRight;
}


objHitmask.x = x;
objHitmask.y = y;

if (health <= 0) {
	game_restart();
	health = 100;
}

switch (state) {
	case playerState.IDLE: PlayerState_IDLE(); break;
	case playerState.SHOOT: PlayerState_SHOOT(); break;
	case playerState.ATTACK: PlayerState_BASH(); break;
	case playerState.HIT: PlayerState_KNOCKBACK(); break;
	case playerState.AIM: PlayerState_AIM(); break;
}


//increase default energy every step to regen, should use var
if (defaultEnergy < defaultMax) {
	if (timer <=0 ) defaultEnergy += .50; //defaultEnergy++;
	else timer--;
}

#region gunsel
//gpConnected
if (global.gpConnected || !global.gpConnected) {
	//switch, gun/bullet sel, controller
	if (wepSwitch) {
		if (currentGun == guns1.DEFAULT) {
			currentGun = guns1.PISTOL;
		}
		else if (currentGun == guns1.PISTOL) {
			currentGun = guns1.RIFLE;
		}
		else if (currentGun == guns1.RIFLE) {
			currentGun = guns1.SHOTGUN;
		}
		else if (currentGun == guns1.SHOTGUN) {
			currentGun = guns1.DEFAULT;
		}
	
	}
	if (aimLock) {
		//show_debug_message("aim");
	}
	
	
//no gp, default to keyboard
/*else {
	//switch, gun/bullet sel
	if (keyboard_check(ord("1"))) {
		currentGun = 1;
	}
	else if (keyboard_check(ord("2"))) {
		currentGun = 2;
	}
	else if (keyboard_check(ord("3"))) {
		currentGun = 3;
	}
	else if (keyboard_check(ord("4"))) {
		currentGun = 4;
	}*/
}
#endregion

//handle inventory
/*if (actInv) {
	if (room != 2) {
		//instance_deactivate_all(true);
		currentRoom = room;
		room_goto(inventory);
	}
	else {
		//instance_activate_all();
		room_goto(currentRoom);
	}
}*/



//close window
if (gameEnd) {
	game_end();
}


//no longer needed once char inv working...
/*if (printInventory) {
	if (ds_list_size(global.inv) > 0) {
		show_debug_message("[inventory]");
		for (i = 0; i < ds_list_size(global.inv); i++) {
			//show_message( ds_list_find_value(global.inv, i));
			show_debug_message(global.inv[| i].desc);
		}
	}
	else show_message("inv empty");
}*/

/*if (charScreen) {
	room_goto(rm_char);
}/*

/*
if (aimLock) {
	state = playerState.AIM;
}

if (aimLockRel) {
	state = playerState.IDLE;
	playerMove = true;
}/*

//if (bountyInv) {
//	val = ds_list_find_value(global.bounties, 1);
//	show_debug_message(ds_map_find_value(val, "reward"));
//}


//setDepth();

//show_debug_message(alarm[0]);
//show_debug_message(state);
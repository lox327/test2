{
    "id": "762cde50-f7cd-4924-89c4-3277b50303cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objPlayer",
    "eventList": [
        {
            "id": "fb8ebf2e-ff07-4e9d-afbe-7b1e2306cc25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "f05405c5-49fd-46a1-831f-c3b1aad9ad21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "51ae9fcc-6b3d-45fb-9ab4-411b65cd047e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "b03ae1bc-fcc2-4cb9-81be-9c7110abdae8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "51843cdb-fc66-45d9-81b7-4d99ca39a1b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "437f74b4-ce7b-4bf7-9696-5d756c65144f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 9,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "c5fb03c2-8d83-441b-ba8a-c5ad1afa1646",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "b69d2fb3-66e1-44fb-a1fd-04c2c20892c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 66,
            "eventtype": 9,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "7d6f6a9c-44a5-4e57-98af-4cebb2cf869a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "8581b92e-32f6-4e1e-bc7e-07bbf14edcd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "d30e08d1-a77d-486f-b4ab-d06ef1007226",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 2,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "5a8da6f5-9c36-479f-8f48-09e01b3197f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 10,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        },
        {
            "id": "e8c0528d-c745-4158-a128-44f8f93e9db5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "762cde50-f7cd-4924-89c4-3277b50303cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
    "visible": true
}
/// @description bullets for burst fire for RIFLES
PlayerState_SHOOT();

//create bullets for burst fire for RIFLES
b = instance_create_layer(shoot_x, shoot_y,"Instances",bulletType);
if (shoot_angle != 0) b.direction = image_angle+shoot_angle;

currentBullet++;
alarm[5] = fireRate;
if (currentBullet == bulletMax) {
	alarm[5] = -1;
	currentBullet = 0;
}
/// @description shock mod
//slow_duration,
//slow_amount,

event_inherited();

//slow_duration = 500;
//slow_amount = 50;
//dot_amount = 5;

slow_duration = objPlayer.slow_duration;
slow_amount = objPlayer.slow_amount;

stun = true;

bulletMax = 3;
bulletDmg = objPlayer.bulletDmg;
speed = -6;

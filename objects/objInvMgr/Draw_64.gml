/// @description draw char inv elements to the screen

//draw char inv screen text
draw_text(100, 40, "character inv");
draw_text(100, 65, "credits: " + string(objPlayer.invCredits));
draw_text(500, 40, "[inventory]");

//draw empty inv slots to screen (row1)
for (i = 0; i< 3; i++) {
	draw_sprite(emptySlot, 0, xinv+(i*100), yinv);
}

//draw empty inv slots to screen (row2)
for (i = 0; i< 3; i++) {
	draw_sprite(emptySlot, 0, xinv+(i*100), yinv+100);
}

//draw empty arm/gun? slots to screen
for (i = 0; i<modSlotSize ; i++) {
	draw_sprite(emptySlot, 0, xarm, yarm+(i*100));
	
	//draw arm slot text to the screen
	if (i == 0) draw_text(xarm+10, yarm+65+(i*100), "0x678");
	else if (i == 1) draw_text(xarm+10, yarm+65+(i*100), "0x234");
	else draw_text(xarm+10, yarm+65+(i*100), "0x459");
}

//draw gun slot
draw_sprite(emptySlot, 0, xgun, ygun);

//draw armor slot
draw_sprite(emptySlot, 0, xarmor, yarmor);

//draw tech1 slot
draw_sprite(emptySlot, 0, xtech1, ytech1);

//draw tech2 slot
draw_sprite(emptySlot, 0, xtech2, ytech2);

//draw arm that player has equipped
draw_sprite(object_get_sprite(objPlayer.equippedList[EquippedItems.ARM]), 0, 175, 125);

//draw gun that player has equipped
//draw_sprite(object_get_sprite(objPlayer.equippedList[EquippedItems.GUN]), 0, 200, 300);

//fill in inv and equipped slots with items
if (invSize > 0) {
	for (i = 0; i < invSize; i++) {
		//item is not equipped. should appear in inv
		if (!global.inv[| i].equipped) {
			draw_sprite(global.inv[| i].spriteName, global.inv[| i].selected, xinv+(i*100), yinv);
			
			//mark inv slot as taken
			slot[i] = 1;
		}
		
		//item is equipped, should appear in equipped slot
		else {
			draw_sprite(global.inv[| i].spriteName, global.inv[| i].selected, xarm, yarm+(i*100));
			
			//mark inv slot as taken
			slot[i] = 0;	
		}
		
		//item text to screen
		if (global.inv[| i].selected) {
			draw_text_ext(xtext, ytext, global.inv[| i].full_desc, line_break, word_wrap);			//item desc
			draw_text(xtext, ytext+100, "energy required: " + string(global.inv[| i].itemEnergy));	//energy req
			draw_text(xtext, ytext+125, "mod type: " + global.inv[| i].itemType);					//mod type
		}
	}
}


//draw energy bar
armCurrent = objPlayer.equippedList[0].currentEnergy;
armMax = objPlayer.equippedList[0].maxEnergy;

pc = (objPlayer.equippedList[0].currentEnergy / armMax) * 100;
draw_text(110, 450, string(objPlayer.equippedList[0].currentEnergy) + "/" + string(armMax));
draw_healthbar(90, 90, 115, 425, pc, c_black, c_green, c_green, 3, true, true); //hor: upper left x, upper left y, upper right x, lower right y

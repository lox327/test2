invSize = ds_list_size(global.inv);

selected = false;
if (invSize>0) global.inv[| 0].selected = true;
else selected = true;

//hard coding for now
global.inv[| 0].equipped = false;
global.inv[| 1].equipped = false;
global.inv[| 2].equipped = false;

image_speed = 0;

line_break = 20;
word_wrap = 350;

objPlayer.playerMove = false;

//slot array to manage which inv slots are free/taken
slot[0] = 0;
slot[1] = 0;
slot[2] = 0;

//coords for drawing slots
xtext = 700;
ytext = 450;

xinv = 550;
yinv = 150;

xarm = 353;
yarm = 130;

xgun = 160;
ygun = 513;

xarmor = 353;
yarmor = 513;

xtech1 = 160;
ytech1 = 675;

xtech2 = 353;
ytech2 = 675;

modSlotSize = array_length_1d(objPlayer.equippedList[0].modSlot);

pc = noone;
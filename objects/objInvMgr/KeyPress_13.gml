//EXIT
var diff = armMax - armCurrent;

if ( objInvMgr.selected ) {
	//code for exiting room
	//room_goto_previous();
	room_goto(global.currentRoom);
	//objPlayer.x = 200;
	//objPlayer.y = 750;
	
	objPlayer.playerMove = true;
	//instance_destroy(objItem1);
	//objPlayer.visible = true;
}

else if (global.inv[| 0].selected) {
	if (diff < global.inv[| 0].itemEnergy) show_message("not enough energy");
	else objPlayer.equippedList[0].currentEnergy += global.inv[| 0].itemEnergy;
	global.inv[| 0].equipped = !global.inv[| 0].equipped;
	//show_message(global.inv[| 0].equipped);
}

else if (global.inv[| 1].selected) {
	if (diff < global.inv[| 1].itemEnergy) show_message("not enough energy");
	else objPlayer.equippedList[0].currentEnergy += global.inv[| 1].itemEnergy;
	global.inv[| 1].equipped = !global.inv[| 1].equipped;
}

else if (global.inv[| 2].selected) {
	if (diff < global.inv[| 2].itemEnergy) show_message("not enough energy");
	else objPlayer.equippedList[0].currentEnergy += global.inv[| 2].itemEnergy;
	global.inv[| 2].equipped = !global.inv[| 2].equipped;
}

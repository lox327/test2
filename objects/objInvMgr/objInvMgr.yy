{
    "id": "4d134262-c90b-4899-9b19-c0f6950fffe4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objInvMgr",
    "eventList": [
        {
            "id": "718e96a2-9ec7-4151-a6ff-0e35e8e0c58f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4d134262-c90b-4899-9b19-c0f6950fffe4"
        },
        {
            "id": "b04c1f76-88fc-45a1-a231-5dc489fccf23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "4d134262-c90b-4899-9b19-c0f6950fffe4"
        },
        {
            "id": "dd675d3a-b725-4551-af64-5a5cbfec437e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4d134262-c90b-4899-9b19-c0f6950fffe4"
        },
        {
            "id": "099c9177-ba0f-42f9-9025-2d5f79b6c166",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "4d134262-c90b-4899-9b19-c0f6950fffe4"
        },
        {
            "id": "1cbecd25-c8c6-411b-bca3-b936504a6cdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 17,
            "eventtype": 9,
            "m_owner": "4d134262-c90b-4899-9b19-c0f6950fffe4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e399d58f-9add-412f-a080-fdea66996e3f",
    "visible": true
}
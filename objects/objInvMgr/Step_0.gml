/// @description handle inv slot movement
if (selected) {
	image_index = 1;
} else {
	image_index = 0;
}
if (invSize == 0) return;


//ARROW KEYS
if (keyboard_check_pressed(vk_left)) {
	if (global.inv[| 0].selected) {
		//do nothing
	}
	else if (slot[1] == 1 && global.inv[| 1].selected) {
		global.inv[| 1].selected = false;
		global.inv[| 0].selected = true;
	}
	else if (slot[2] == 1 && global.inv[| 2].selected) {
		global.inv[| 2].selected = false;
		global.inv[| 1].selected = true;
	}
	/*else if (objItem4.selected) {
		objItem4.selected = false;
		objItem3.selected = true;
	}
	else if (objItem5.selected) {
		objItem5.selected = false;
		objItem4.selected = true;
	}*/
}
else if (keyboard_check_pressed(vk_right)) {
	if (slot[1] == 1 && global.inv[| 0].selected) {
		global.inv[| 0].selected = false;
		global.inv[| 1].selected = true;
	}
	else if (slot[2] == 1 && global.inv[| 1].selected ) {
		global.inv[| 1].selected = false;
		global.inv[| 2].selected = true;
	}
	else if (slot[2] == 1 && global.inv[| 2].selected) {
		//do nothing
	}
	/*else if (objItem4.selected) {
		objItem4.selected = false;
		objItem5.selected = true;
	}
	else if (objItem5.selected) {
		//objItem5.selected = false;
		//objItem4.selected = true;
	}*/
}
else if (keyboard_check_pressed(vk_down)) {
	if (global.inv[| 0].selected) {
		global.inv[| 0].selected = false;
	}
	else if (slot[1] == 1 && global.inv[| 1].selected) {
		global.inv[| 1].selected = false;
	}
	else if (slot[2] == 1 && global.inv[| 2].selected) {
		global.inv[| 2].selected = false;
	}
	
	objInvMgr.selected = true;
	
}
else if (keyboard_check_pressed(vk_up)) {
	if (objInvMgr.selected) {
		objInvMgr.selected = false;
		global.inv[| 0].selected = true;
	}
	
}

//ARROW KEYS
{
    "id": "825983f2-7020-4454-9e2b-ccce8f3e247c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLightman2",
    "eventList": [
        {
            "id": "d5954f95-fc9d-4b20-8f3d-881e0af654e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "825983f2-7020-4454-9e2b-ccce8f3e247c"
        },
        {
            "id": "77c8e13b-2470-4e30-aa4d-43cfd8271b31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "825983f2-7020-4454-9e2b-ccce8f3e247c"
        },
        {
            "id": "999b1696-a638-4615-83f6-00c340aac8d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "825983f2-7020-4454-9e2b-ccce8f3e247c"
        },
        {
            "id": "b2d5d2bf-77c2-46a6-8bca-2f2aff36ba55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 60,
            "eventtype": 6,
            "m_owner": "825983f2-7020-4454-9e2b-ccce8f3e247c"
        },
        {
            "id": "9766948c-be98-468c-98cb-d3284effa7f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 61,
            "eventtype": 6,
            "m_owner": "825983f2-7020-4454-9e2b-ccce8f3e247c"
        },
        {
            "id": "ba52ae3b-ef39-4511-a9ce-478ba04a8536",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "825983f2-7020-4454-9e2b-ccce8f3e247c"
        },
        {
            "id": "d119f5e3-2d42-42b9-9752-8cdb96a389b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 18,
            "eventtype": 9,
            "m_owner": "825983f2-7020-4454-9e2b-ccce8f3e247c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3682f5e7-fe77-4da9-b98b-4543fb7f2e0d",
    "visible": true
}
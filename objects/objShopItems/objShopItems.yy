{
    "id": "1a74af11-7bb5-4f11-bcbf-46ae09b049cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShopItems",
    "eventList": [
        {
            "id": "5a525ecf-a478-4f1b-8d90-30cd35555c91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1a74af11-7bb5-4f11-bcbf-46ae09b049cd"
        },
        {
            "id": "8ebd438b-0dd2-46a0-9697-7cca21231ba1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 35,
            "eventtype": 9,
            "m_owner": "1a74af11-7bb5-4f11-bcbf-46ae09b049cd"
        },
        {
            "id": "562e77f0-3b09-454d-a431-36182ce5936a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1a74af11-7bb5-4f11-bcbf-46ae09b049cd"
        },
        {
            "id": "e319c46d-45ea-4066-9ab5-fc5c531a655d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "1a74af11-7bb5-4f11-bcbf-46ae09b049cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
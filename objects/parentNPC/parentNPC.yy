{
    "id": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "parentNPC",
    "eventList": [
        {
            "id": "c09c83e1-be8c-4194-bd40-ae9cfd97de06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7e503d6-24b4-41bd-b365-1924e36ba828"
        },
        {
            "id": "6a13fd45-c563-4c12-8836-fa7979119d31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f7e503d6-24b4-41bd-b365-1924e36ba828"
        },
        {
            "id": "622ce7b6-45d8-4f54-a477-16723ec2c572",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f7e503d6-24b4-41bd-b365-1924e36ba828"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "58dc7138-89d9-427f-b2ec-0d2672bbc940",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}
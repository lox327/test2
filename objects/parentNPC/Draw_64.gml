if (instance_exists(objShopMgr_Parent)) if (objShopMgr_Parent.display) exit;
if (instance_exists(objInvMgr2)) if (objInvMgr2.display) exit;

draw_self();

if (!hasInfo) exit;
var xnpc = self.x  - camera_get_view_x(view_camera[0]) - 25;
var ynpc = self.y  - camera_get_view_y(view_camera[0]) - 25;

draw_sprite(infobox, 0, xnpc, ynpc);

if ( proximity(prox) ) {
	//draw_text(xnpc, ynpc, "[hit enter]");
	draw_sprite(infobox, 1, xnpc, ynpc);
	//hoverfx(); //works in DRAW only for now, need way to draw outline

}

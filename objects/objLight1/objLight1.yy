{
    "id": "8cba93e4-9c21-4709-b106-c32d875fa18b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objLight1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "24cf4884-af7c-4b5c-af49-83638c278d82",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c75df6f6-fe0b-4eed-bc89-fe3ffb95d6d8",
    "visible": false
}
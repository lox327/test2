{
    "id": "40aad24f-ec64-4232-988e-643253649718",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objTylerItems",
    "eventList": [
        {
            "id": "d0413718-d17c-49d1-886d-d321e1784bfd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "40aad24f-ec64-4232-988e-643253649718"
        },
        {
            "id": "e1e3b5bb-cf70-4b5b-be22-205260463e8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "40aad24f-ec64-4232-988e-643253649718"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f7e503d6-24b4-41bd-b365-1924e36ba828",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e5cc1bda-ece7-4d4f-be06-cdcaf78c4a7d",
    "visible": true
}
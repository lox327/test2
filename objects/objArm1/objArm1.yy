{
    "id": "7a9ebe57-0832-4ca5-967c-a541ba1cc89b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objArm1",
    "eventList": [
        {
            "id": "598acb0a-1ce1-4b6f-a53a-56c4c88a91c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a9ebe57-0832-4ca5-967c-a541ba1cc89b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "81c2ab68-b483-4988-a1e3-7b7a34e16ef8",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a82baa86-5ab9-45ce-9377-cdbb53cedecd",
    "visible": true
}
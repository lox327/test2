event_inherited();

/// @description Insert description here
// You can write your code in this editor
modSlot[0] = 0;
modSlot[1] = 0;
modSlot[2] = 0;

//[slot num, slot type]
//0: open or active
//1: full, passive
modSlot2[0,0] = 0; //0: open, 1: full
modSlot2[0,1] = 0; //0: active
modSlot2[1,0] = 0;
modSlot2[1,1] = 0; //0: active
modSlot2[2,0] = 0;
modSlot2[2,1] = 1; //1: passive

currentEnergy = 0;
maxEnergy = 10;

desc = "arm1";
full_desc = "[" + desc + "] this cyberarm ..."
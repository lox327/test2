{
    "id": "2f3e2967-bdd3-4129-90e6-d56c7aaf3dab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objEnemy3",
    "eventList": [
        {
            "id": "070d1bf6-aaec-403c-b6d6-a0977006a4bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2f3e2967-bdd3-4129-90e6-d56c7aaf3dab"
        },
        {
            "id": "259dbc53-e4f0-4f32-b852-2b997a24f41b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "2f3e2967-bdd3-4129-90e6-d56c7aaf3dab"
        },
        {
            "id": "5f72e53a-fbe4-4c2a-911a-ff37b47783ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2f3e2967-bdd3-4129-90e6-d56c7aaf3dab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c0353dfc-c14d-417e-b283-64ff3ecacc3b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "95629af6-75c3-4a60-920e-e989ba868fe2",
    "visible": true
}
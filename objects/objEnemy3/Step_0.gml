//show_debug_message("e" + string(x))
event_inherited();

if (hp <= 0 && enemyState != EnemyState.DEAD) {
	enemyState = EnemyState.DEAD;
	timer = deadTime;
	
}
//image_index = direction;
//event_user(0);//Choose a State
//if (abs(self.x - objPlayer.x) <= self.sightRange)
if (proximity(sightRange)) {
switch (enemyState) {
    case EnemyState.MOVE: 
    { 
		//SPRITE SEL based on dir towards player
		if (self.x > objPlayer.x) sprite_index = enemy3Left;
		else sprite_index = enemy3Right;
		if (self.y > objPlayer.y) sprite_index = enemy3Up;
		else sprite_index = enemy3Down;
		
		//MOVE
		mp_potential_step_object(objPlayer.x, objPlayer.y, spd, objCollision);
		
		//avoid colliding/overlapping with other enemy
		if (distance_to_object(enemyParent) > 20) {
			//timer = 10;
			//self.x += sign(spd);
			//enemyState = EnemyState.IDLE;
			//mp_potential_step(objPlayer.x, objPlayer.y, spd);

		}
		
		//ATTACK - based on distance, % from rand
		if (distance_to_object(objPlayer) < 20) {
			timer = 200; //reset timer to short num to represent attack length
			if (random(100) > 25) enemyState = EnemyState.ATTACK1;
			else enemyState = EnemyState.ATTACK1;
		}
		
		//CHASE - based on distance, % from rand
		else if (distance_to_object(objPlayer) < chaseRange) {
			//timer = 100;
			enemyState = EnemyState.CHASE;
		}
		
		//random time for next idle state
        if (timer <= 0) {
			timer = choose(25,50);
            enemyState = EnemyState.IDLE;
        }
        break; 
    } 
    case EnemyState.IDLE: 
    { 
        //do another action 
		sprite_index = enemy3Idle;
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.MOVE;
        }
        break; 
    } 
	
	case EnemyState.ATTACK1_IDLE: 
    { 
        //do another action 
		sprite_index = enemy4Idle;
		if (timer <= 0) {
			//timer for fired bullet is up; if bullet/burst max has been reached, we IDLE. otherwise, shoot again
			if (self.currBullet < self.bulletMax1) {
				enemyState = EnemyState.ATTACK1;
			}
			else {
				currBullet = 0;		//reset bullet counter
				_index = 0;
				timer = 100;		//how long to IDLE for before MOVE
				enemyState = EnemyState.IDLE;
			}
		}
		
        break; 
    } 
	
	case EnemyState.STUN: 
    { 
        //do another action 
		sprite_index = enemy3Stun;
		
		if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = choose(EnemyState.MOVE, EnemyState.IDLE);
        }
        break; 
    } 
	case EnemyState.CHASE: 
    { 
		//sprite_index = enemy3Attack;
		mp_potential_step_object(objPlayer.x, objPlayer.y, spd*1.5, objCollision);
        if (distance_to_object(objPlayer) < attackRange) {
			//timer = 20; //reset timer to short num to represent attack length
			if (random(100) > 25) enemyState = EnemyState.ATTACK1;
			else enemyState = EnemyState.ATTACK2;
		}
		
		else if (timer <= 0) //switch action
        { 
            timer = 100;
            enemyState = EnemyState.IDLE;

        }
        break; 
    } 
	
	//need to combine attacks into one attack state to reduce duplicate code
	case EnemyState.ATTACK1: 
    { 
		currBullet++;
		sprite_index = enemy4Attack1;
		//instance_create_layer(x+_index,y+_index,"Instances",objTemp1);
		
		timer = 10; //time between bullets
        enemyState = EnemyState.ATTACK1_IDLE; //pause in between bullets to create burst fire effect
		_index += 50;
		
		break;
    }
	//bigger attack, more dmg + idle for longer?
	case EnemyState.ATTACK2: 
    { 
		//SPRITE SEL based on dir towards player
		if (self.x > objPlayer.x) image_xscale = -1;
		else image_xscale = 1;
		//if (self.y > objPlayer.y) sprite_index = enemy3Up;
		//else sprite_index = enemy3Down;
		
		EnemyState_ATTACK(enemy3Attack2);
        break; 
    }
	
	case EnemyState.HIT:
	{
		EnemyState_KNOCKBACK(enemy3Hit);
		break;	
	}
	
	//must update to not use motion set
	case EnemyState.DEAD:
	{
		
	motion_set(point_direction(objPlayer.x, objPlayer.y, x, y), spd*.5);
	if (timer <= 0)
        { 
			motion_set(0,0);
			instance_destroy();
			
			with (instance_create_layer(x,y,"Instances",objEnemyDead)) {
				id.sprite_index = enemy3Dead;
			}
        }
		
	}
} 
timer--;
}
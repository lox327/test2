/// @description Insert description here
// You can write your code in this editor
event_inherited();

hp = 20;
hpMax = hp;
spd = 1.0;
flash = 0;
attackDistance = 300;
chaseRange = 100;
boss = true;

bulletMax1 = 3;
bulletMax2 = 5;
currBullet = 0;
_index = 0;

state = "Inactive";
sightRange = 500;
aggressiveness = 0;
attackRange = sprite_width/2 + objPlayer.sprite_width/2;

//EVADE
evadeDistance = 75;
evadeSpeed = spd * -1.25;
evadeCheck = false; //only check for evade once per step, reset after IDLE
evadeLimit = 10; //evade when health is <= to evadeLimit
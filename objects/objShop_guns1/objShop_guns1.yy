{
    "id": "e6fefb30-1e84-4e09-8dcc-24d1a16019b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "objShop_guns1",
    "eventList": [
        {
            "id": "6ad34554-03c1-434b-a8c1-13578ac0862c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e6fefb30-1e84-4e09-8dcc-24d1a16019b9"
        },
        {
            "id": "bb1d68b9-0026-44da-bdbe-03de98d71e28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 33,
            "eventtype": 9,
            "m_owner": "e6fefb30-1e84-4e09-8dcc-24d1a16019b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "99caee38-f03a-4937-b2be-4eae05270879",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
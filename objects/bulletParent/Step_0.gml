/// @description code to handle shotgun bullets
if (objPlayer.currentGun == guns1.SHOTGUN) {
	var kb_friction = 0.05;
	speed = lerp(speed, 0, kb_friction);

	if (speed > -1.5) {
		instance_destroy();
	}
}

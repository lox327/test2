{
    "id": "7af82463-cca2-45a9-87e3-98eb03ace7c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bulletParent",
    "eventList": [
        {
            "id": "bb22105b-1fea-45c2-a888-1789838115de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7af82463-cca2-45a9-87e3-98eb03ace7c9"
        },
        {
            "id": "d3893dbd-3bff-4d6f-ae5c-ae39c1838d7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "7af82463-cca2-45a9-87e3-98eb03ace7c9"
        },
        {
            "id": "331103d0-1d8e-471b-a406-abeec4d73c38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7e112a55-f75c-47df-9092-27fd121f56af",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7af82463-cca2-45a9-87e3-98eb03ace7c9"
        },
        {
            "id": "325facc0-c830-4780-a043-e4a8e84e4e75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "dc660d83-2c6f-4969-a243-303fad7f4859",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7af82463-cca2-45a9-87e3-98eb03ace7c9"
        },
        {
            "id": "e1229d4a-81ec-4f2c-927b-ff78400f0d21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7af82463-cca2-45a9-87e3-98eb03ace7c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}
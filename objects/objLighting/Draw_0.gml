///@description objLightingParent allows you to add children to switch through, makes draw_sprite easier
if (!surface_exists(surf)) {
	var _cw = camera_get_view_width(view_camera[0]);
	var _ch = camera_get_view_height(view_camera[0]);
	surf = surface_create(_cw,_ch);
	
	surface_set_target(surf);
	draw_set_color(c_black);
	draw_set_alpha(0);
	draw_rectangle(0,0,_cw,_ch,false);
	surface_reset_target();
}

else {
	var _cw = camera_get_view_width(view_camera[0]);
	var _ch = camera_get_view_height(view_camera[0]);
	var _cx = camera_get_view_x(view_camera[0]);
	var _cy = camera_get_view_y(view_camera[0]);
		
	surface_set_target(surf);
	//draw black over whole screen...
	if (true) {
		draw_set_color(c_black);
		draw_set_alpha(black_val);
		draw_rectangle(0,0,_cw,_ch,false);
	}
	//gpu_set_blendmode(bm_subtract);
		
	with (objLightingParent) {
		var _sw = sprite_width/2;	
		var _sh = sprite_height/2;	
			
		switch (object_index) {
			case objLight1:
				gpu_set_blendmode(bm_subtract);
				draw_sprite_ext(spr_light,0,x-_cx,y - _cy,.75+random(flicker_val),.75+random(flicker_val),0,c_white,1);
				gpu_set_blendmode(bm_normal);
					
				gpu_set_blendmode(bm_add);
				draw_sprite_ext(spr_light,0,x-_cx,y - _cy,.75+random(flicker_val),.75+random(flicker_val),0,c_yellow,alpha_val);
				gpu_set_blendmode(bm_normal);

				break;
			
			case objLight2:
				gpu_set_blendmode(bm_subtract);
				draw_sprite_ext(spr_light,0,x-_cx,y - _cy,.75+random(flicker_val),.75+random(flicker_val),0,c_white,1);
				gpu_set_blendmode(bm_normal);
					
				gpu_set_blendmode(bm_add);
				draw_sprite_ext(spr_light,0,x-_cx,y - _cy,.75+random(flicker_val),.75+random(flicker_val),0,c_fuchsia,alpha_val);
				gpu_set_blendmode(bm_normal);

				break;
				
			case objLight21:
				gpu_set_blendmode(bm_subtract);
				draw_sprite_ext(spr_light2,0,x-_cx,y - _cy,1,1,0,c_white,1);
				gpu_set_blendmode(bm_normal);
				
				gpu_set_blendmode(bm_add);
				draw_sprite_ext(spr_light2,0,x-_cx,y - _cy,1,1,0,c_yellow,.5);
				gpu_set_blendmode(bm_normal);

				break;
				
		}
			
	}
		
	//gpu_set_blendmode(bm_normal);
	draw_set_alpha(1);
		
	surface_reset_target();
	draw_surface(surf,_cx,_cy);
		
}

///@arg knockback_sprite
var knockback_sprite = argument[0];
//init for when enemy is hit, used once
if (sprite_index != knockback_sprite) {
	isHit = true;
	kb_spd = spd*2;
	camera.shake = camera.smallShake;

	sprite_index = knockback_sprite;
	image_index = 0;
}
var kb_friction = 0.10;
move_and_collide(hitAngle, kb_spd);
kb_spd = lerp(kb_spd, 0, kb_friction);

if (kb_spd < .5) {
	timer = 50;
	enemyState = EnemyState.IDLE;
	isHit = false;
}
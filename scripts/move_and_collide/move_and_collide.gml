///@arg angle
///@arg spd
var angle = argument[0];
var spd = argument[1];

if ( place_meeting(x+(spd),y,objCollision) || place_meeting(x-(spd),y,objCollision)) {
	//while( !place_meeting(x+sign(xadd),y,objCollision) ) x += sign(xadd);
	timer = 50;
    enemyState = EnemyState.IDLE;
		
}
else x += lengthdir_x(spd,angle);
	
//y collisions
if ( place_meeting(x,y+(spd),objCollision) || place_meeting(x,y-(spd),objCollision) ) {
	//while( !place_meeting(x,y+sign(yadd),objCollision) ) y += sign(yadd);
	timer = 50;
	enemyState = EnemyState.IDLE;
		
}
else y += lengthdir_y(spd,angle);

//init for when player is hit, used once
if (sprite_index != playerHit_left && sprite_index != playerHit_right && sprite_index != playerHit_up && sprite_index != playerHit_down) {
	isHit = true;
	kb_spd = walkSpeed*1.5;
	camera.shake = camera.smallShake;
	if (false) sprite_index=playerHit_left;
	//show_debug_message(dir)

	if      (dir="left")	sprite_index=playerHit_left;
	else if (dir="right")	sprite_index=playerHit_right;
	else if (dir="up")		sprite_index=playerHit_up;
	else if (dir="down")	sprite_index=playerHit_down;

		 /*if (objPlayer.hitAngle >= 0 && objPlayer.hitAngle < 90) sprite_index=playerHit_left;
	else if (objPlayer.hitAngle >= 90 && objPlayer.hitAngle < 180) sprite_index=playerHit_right;
	else if (objPlayer.hitAngle >= 180 && objPlayer.hitAngle < 270) sprite_index=playerHit_up;
	else if (objPlayer.hitAngle >= 270 && objPlayer.hitAngle < 360) sprite_index=playerHit_down;*/
	
	image_index = 0;
	alarm[0] = room_speed * .125;
	isShooting = false;
}
var kb_friction = 0.10;
move_and_collide(hitAngle, kb_spd);
kb_spd = lerp(kb_spd, 0, kb_friction);

//if (animation_end()) {
//if (alarm[0] == 0) {
if (kb_spd < .5) {
	//kb_spd = 0;
	state = playerState.IDLE;
	sprite_index = playerIdle;
	isHit = false;
	aimLockRel = true;
	aimLock = false;
	//hitAngle = 0;
}
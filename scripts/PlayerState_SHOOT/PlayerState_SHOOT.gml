//show_debug_message(currentGun)
#region switch for weapon/bulletType
if (!isShooting) {	
	//super charge gun thing
	var prevBulletType = bulletType;
	
	//show_debug_message(weaponEnergy)
	if (charged) {
		bulletType = objBulletCharge;
		//weaponEnergyUse *= 2;
		charged = false;
	}
	
	if (currentGun == guns1.DEFAULT) {
		//bulletType = objBullet;
		spread = 0;
		if (defaultEnergy > -20) defaultEnergy -=15;
	}
	else if (currentGun == guns1.PISTOL) {
		//bulletType = objMod_fire;
		spread = 0;
		weaponEnergy -= weaponEnergyUse; //how much weapon energy to use
	}
	else if (currentGun == guns1.RIFLE) {
		//bulletType = objMod_shock;
		spread = 0;
		weaponEnergy -= weaponEnergyUse; //how much weapon energy to use
	}
	else if (currentGun == guns1.SHOTGUN) {
		//bulletType = objBulletShotgun;
		spread = 4; //angle for spread bullets, get from inv
		weaponEnergy -= weaponEnergyUse; //how much weapon energy to use
	}
}
#endregion
shoot_angle = 0;
spread_x = 0;
spread_y = 0;

switch(dir)	{
	#region straight
	case "up":
		if (!isShooting) {
			shoot_x = x;
			shoot_y = y-grid;
			spread_x = spread;
		}
		//shoot(playerUp, attackUp);
	break;
		
	case "down":
		if (!isShooting) {
			shoot_x = x;
			shoot_y = y+grid;
			spread_x = spread;
		}
		//shoot(playerDown, attackDown);
	break;
		
	case "left":
		if (!isShooting) {
			shoot_x = x-grid;
			shoot_y = y-grid/2;
			spread_y = spread;
		}
		//shoot(playerLeft, attackLeft);
	break;
		
	case "right":
		if (!isShooting) {
			shoot_x = x+grid;
			shoot_y = y-grid/2;
			spread_y = spread;
		}
		//shoot(playerRight, attackRight);
		
	break;
	#endregion
	
	//diag shooting
	//need new sprite for diag shooting like in zombies ate my neighbors
	
	#region diag
	case "up_right":
		if (!isShooting) {
			shoot_x = x+grid;
			shoot_y = y;
			shoot_angle = -135;
		}
		//shoot(playerRight, attackRight);
	break;
	
	case "up_left":
		if (!isShooting) {
			shoot_x = x+grid;
			shoot_y = y;
			shoot_angle = -45;
		}
		//shoot(playerLeft, attackLeft);
	break;
	
	case "down_right":
		if (!isShooting) {
			shoot_x = x+grid;
			shoot_y = y;
			shoot_angle = 135;
		}
		//shoot(playerRight, attackRight);
	break;
	
	case "down_left":
		if (!isShooting) {
			shoot_x = x+grid;
			shoot_y = y;
			shoot_angle = 45;
		}
		//shoot(playerLeft, attackLeft);
	break;
	#endregion
	
}

//create bullet based on gun type
//PISTOL (single shot)
if (currentGun == guns1.DEFAULT || currentGun == guns1.PISTOL) {
	b = instance_create_layer(shoot_x, shoot_y,"Instances",bulletType);
	if (shoot_angle != 0) b.direction = image_angle+shoot_angle;	//diag bullets
}

//RIFLE (pulse)
else if (currentGun == guns1.RIFLE) {
	if (alarm[5] == -1) {
		b = instance_create_layer(shoot_x, shoot_y,"Instances",bulletType);
		if (shoot_angle != 0) b.direction = image_angle+shoot_angle;
		currentBullet++;
		alarm[5] = fireRate;
	}
}
	
//SHOTGUN (spread)
else if (currentGun == guns1.SHOTGUN) {
	b1 = instance_create_layer(shoot_x, shoot_y,"Instances",bulletType);
	if (shoot_angle != 0) b1.direction = image_angle+shoot_angle;
	
	b2 = instance_create_layer(shoot_x-spread_x,shoot_y-spread_y,"Instances",bulletType);
	if (shoot_angle != 0) b2.direction = image_angle+shoot_angle+22.5;
	
	b3 = instance_create_layer(shoot_x+spread_x,shoot_y+spread_y,"Instances",bulletType);
	if (shoot_angle != 0) b3.direction = image_angle+shoot_angle-22.5;
}
	
state = playerState.IDLE;
bulletType = prevBulletType;
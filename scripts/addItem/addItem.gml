//addItem(ItemShop3.mod2, inv_name,"Mod2","It goes MOD2!",0,0,0,0,0,false,true,false,"Item",25,spr_sheet_name);

///@description adding item to inv grid
///@description should be able to avoid using arg12 as the enum index should work since each item will have unique sprite in the sheet
///@arg0 item_id
///@arg1 grid_id
///@arg2 name
///@arg3 description
///@arg4 damage_low
///@arg5 damage_high
///@arg6 armor
///@arg7 mod1
///@arg8 mod2
///@arg9 owned
///@arg10 available
///@arg11 equipped
///@arg12 type
///@arg13 price
///@art14 spr_sheet
///@art15 spr_index, or use item_id if unique sprites in sprite sheet

var item_id = argument0;
var grid_id = argument1;
grid_id[#item_id, stat.name] = argument2;
grid_id[#item_id, stat.description] = argument3;
grid_id[#item_id, stat.damage_low] = argument4;
grid_id[#item_id, stat.damage_high] = argument5;
grid_id[#item_id, stat.armor] = argument6;
grid_id[#item_id, stat.mod1] = argument7;
grid_id[#item_id, stat.mod2] = argument8;
grid_id[#item_id, stat.owned] = argument9;
grid_id[#item_id, stat.available] = argument10;
grid_id[#item_id, stat.equipped] = argument11;
grid_id[#item_id, stat.type] = argument12;
grid_id[#item_id, stat.price] = argument13;
grid_id[#item_id, stat.spr_sheet] = argument14;
grid_id[#item_id, stat.spr_index] = item_id;

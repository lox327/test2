///@arg attack_sprite
//init for when player bashes, used once
sprite_index = argument[0];
_dir = argument[1];

if (sprite_index != enemy2Attack1_left && sprite_index != enemy2Attack1_right && sprite_index != enemy2Attack1_up && sprite_index != enemy2Attack1_down) {
	image_index = 0;
		 /*if (dir == "left")	 sprite_index = enemy2Attack1_left;
	else if (dir == "right") sprite_index = enemy2Attack1_right;
	else if (dir == "up")	 sprite_index = enemy2Attack1_up;
	else if (dir == "down")  sprite_index = enemy2Attack1_down;*/
}


//create hitbox between image index 2 and 3
if ( image_index >= 2 && image_index <= 3 ) {

	with (instance_create_layer(x,y,"Instances",objBashBox2)) {
		#region create bash box and give initial dir
		switch(argument[1]) {
			case "up":
				x -= 25;
				y -= 20;
			break;
	
			case "down": 
				x -= 32;
				y += 32;
			break;
	
			case "left": 
				x -= 45;
			break;
			
			case "right": 
				image_xscale = other.image_xscale;
				x -= 5;
			break;
	
	
			//diag
			case "up_right":
				x += 16;
				y -= 32;
			break;
	
			case "up_left":
				x -= 64;
				y -= 32;
			break;
	
			case "down_right": 
				x += 16;
				y += 32;
			break;
	
			case "down_left":
				x -= 64;
				y += 32;
			break;
	
		}
		#endregion
		
		with (instance_place(x,y,objHitmask)) {
			if (objPlayer.state != playerState.HIT) {
				//should add id to list to manage who exactly has been hit
				objPlayer.state = playerState.HIT;
				health -= 5; //use var
				//isHit = true;
				//timer = bashTime; //timing of this with enemy IDLE is being weird
				objPlayer.hitAngle = point_direction(objEnemy2.x, objEnemy2.y, x, y);
			}
			
		}
	}
}

//reset enemy attr
if (animation_end()) {
	enemyState = EnemyState.IDLE;
	timer = 100;
	sprite_index = enemy2Idle;
	/*	 if (dir == "left")	 sprite_index = enemy2Idle_left;
	else if (dir == "right") sprite_index = enemy2Idle_right;
	else if (dir == "up")	 sprite_index = enemy2Idle_up;
	else if (dir == "down")  sprite_index = enemy2Idle_down;*/
}

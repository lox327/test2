///@arg1 prevSprite
///@arg2 attackDir
isShooting = true;

prevSprite = argument[0];
attackDir = argument[1];
		
if (sprite_index != attackDir) image_index = 0;
sprite_index=attackDir;


if (animation_end()) {
	state = playerState.IDLE;
	sprite_index = prevSprite;
	isShooting = false;
}
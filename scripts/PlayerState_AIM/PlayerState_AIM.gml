//playerMove = false;
sprite_index = attackLeft;
image_index = 0;
image_speed = 0;
aimLockSpeed = .5;

if (actShoot && !isShooting) {
	//default gun, doesn't use energy
	if (currentGun == guns1.DEFAULT) {
		if ( defaultEnergy > 0 ) PlayerState_SHOOT();
	}
	//all other guns use energy
	else if ( weaponEnergy > 0 ) PlayerState_SHOOT();
	isShooting = false;
}


if (aimLockRel) {
	//playerSpeed = 10;
	//playerMove = true;
	state = playerState.IDLE;
	isShooting = false;
	aimLockSpeed = 1;
}
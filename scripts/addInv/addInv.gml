if ( objPlayer.invCredits >= argument[0].price ) { //if player has enough funds to purchase items
	show_message("purchased: " + argument[0].desc);
		
	//subtract currency from player
	objPlayer.invCredits -=  argument[0].price;
		
	//put item in inventory list
	//ds_list_add(objPlayer.inventoryList, argument[0].desc);
	
	//add item obj to inv list
	ds_list_add(global.inv, argument[0]);
	
	//add item to the room to be accessed
	room_instance_add(rm_inv, 100, 100, argument[0]);
	
	//ds_list_add(objPlayer.inventoryList, argument[0]);
}
else show_message("not enough credits");


/// instance_furthest_range( x, y, obj, radius, amount );
///@arg0 x
///@arg1 y
///@arg2 obj
///@arg3 radius
///@arg4 amount

/// Finds the furthest instance from the x and y coordinate within the given radius

var _x, _y, _obj, _radius, _amount, _inst, _list, _num;

_x = argument0;
_y = argument1;
_obj = argument2;
_radius = argument3;
_amount = argument4;

_inst = noone;
_num = instance_number(_obj);

// First, make a list of all instances of the given type
for (var i = 0; i < _num; i++ )
    _list[i] = instance_find(_obj, i);

// Then, deactivate all instances outside the given radius
for (var i = 0; i < _num; i++ ) {
	if (_list[i] && point_distance(_list[i].x, _list[i].y, _x, _y) < _radius) {
		//show_debug_message(point_distance(_list[i].x, _list[i].y, _x, _y))
		_list[i].hp -= _amount;
		//damage_text_mult(_list[i].x, _list[i].y, _amount);
		damage_text(_amount, _list[i].x, _list[i].y);

	}
    //instance_deactivate_object(_list[i]);
}


// Then find the furthest instance that is still activated
//_inst = instance_furthest(_x, _y, _obj);

// Finally, activate all instances we just deactivated.
//instance_activate_object(_obj);

// And return the value
//return _inst;
#region GAMEPAD
if (global.gpConnected) {
	//b_num = gamepad_button_count(device);
	gamepad_set_axis_deadzone(device, 0.3);
	xadd = gamepad_axis_value(device, gp_axislh);
	yadd = gamepad_axis_value(device, gp_axislv);
	
	global.rh = gamepad_axis_value(device, gp_axisrh);
	global.rv = gamepad_axis_value(device, gp_axisrv);
	global.ia = point_direction(0,0,global.rh,global.rv);
	
	//show_debug_message(global.ia);
	//show_debug_message(x);
	//show_debug_message(global.ia);
	
	//config1
	if (0) {
		actAttack =	gamepad_button_check(device, gp_face1);
		actShoot = gamepad_button_check_pressed(device, gp_shoulderr);
		wepSwitch = gamepad_button_check_pressed(device, gp_shoulderl);
		//global.btnAction = gamepad_button_check_pressed(device, gp_face3);
	}
	//config2
	else {
		//actAttack = gamepad_button_check_pressed(device, gp_face2);
		//actShoot = gamepad_button_check_pressed(device, gp_face1);
		//wepSwitch = gamepad_button_check_pressed(device, gp_shoulderl);
		aimLock = gamepad_button_check_pressed(device, gp_shoulderr);
		aimLockRel = gamepad_button_check_released(device, gp_shoulderr);
		//global.btnAction = gamepad_button_check_pressed(device, gp_face3);
	}
	
	//gameEnd = gamepad_button_check_pressed(device, gp_select);
	gamePause = gamepad_button_check_pressed(device, gp_start);
	
	//printInventory = gamepad_button_check_pressed(device, gp_select);
	//charScreen = gamepad_button_check_pressed(device, gp_start);
	
}
#endregion

#region KEYBOARD
else {
	//ARROW KEYS
	/*moveUp = keyboard_check(vk_up);
	moveDown = keyboard_check(vk_down);
	moveLeft = keyboard_check(vk_left);
	moveRight = keyboard_check(vk_right);*/
	
	//WASD KEYS
	moveUp = keyboard_check(ord("W"));
	moveDown = keyboard_check(ord("S"));
	moveLeft = keyboard_check(ord("A"));
	moveRight = keyboard_check(ord("D"));
	
	xadd = moveRight - moveLeft;
	yadd = moveDown - moveUp;
	//xadd = keyboard_check(vk_right) - keyboard_check(vk_left);
	//yadd = keyboard_check(vk_down) - keyboard_check(vk_up);
	
	//actAttack = keyboard_check_pressed(vk_control);
	//actShoot = keyboard_check_pressed(vk_space);
	printInventory = keyboard_check_pressed(ord("I"));
	bountyInv = keyboard_check_pressed(ord("K"));
	charScreen = keyboard_check_pressed(ord("C"));
	//global.btnAction = keyboard_check_pressed(vk_enter);
	
	aimLock = keyboard_check_direct(ord("Q"));
	aimLockRel = keyboard_check_released(ord("Q"));
	
	//gameEnd = keyboard_check_pressed(vk_escape);
	//actInv = keyboard_check_pressed(vk_tab);
	//actPause = keyboard_check_pressed(vk_tab);

}
#endregion

#region GLOBAL controls
global.btnAction =	keyboard_check_pressed(vk_enter) || gamepad_button_check_pressed(device, gp_face3);		//action
actAttack =			keyboard_check_pressed(vk_control) || gamepad_button_check_pressed(device, gp_face2);	//bash
actShoot =			keyboard_check_pressed(vk_space) || gamepad_button_check_pressed(device, gp_face1);		//shoot
gameEnd =			keyboard_check_pressed(vk_escape) || gamepad_button_check_pressed(device, gp_select);	//end
wepSwitch =			keyboard_check_pressed(vk_alt) || gamepad_button_check_pressed(device, gp_shoulderl);	//wep cycle
#endregion


/*num = 0;
if keyboard_check_pressed(ord("P")) {
	//screen_save(working_directory + "\Screens\Screen_"+string(num)+".png")
	//num += 1;
	var surf;
	surf = surface_create(32, 32);
	surface_set_target(surf);
	draw_clear_alpha(c_black, 0);
	spr_custom = sprite_create_from_surface(surf, 0, 0, 32, 32, true, true, 16, 16);
	sprite_index = spr_custom;
	draw_sprite(sprite_index, image_index, x, y);
	//draw_sprite(spr_custom, 0, x, y-32);
	surface_reset_target();
	//surface_free(surf);
	//num += 1;
}*/
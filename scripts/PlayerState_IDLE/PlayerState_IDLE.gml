xaddSpd = xadd * playerSpeed * aimLockSpeed;
yaddSpd = yadd * playerSpeed * aimLockSpeed;

//to stop player movement when dialog
if (!playerMove) {
	return;
}


#region movement
//x collisions
if ( place_meeting(x+(xaddSpd),y,objCollision) ) {
	while( !place_meeting(x+sign(xadd),y,objCollision) ) x += sign(xadd);
		
}
else x += xaddSpd;
	
//y collisions
if ( place_meeting(x,y+(yaddSpd),objCollision) ) {
	while( !place_meeting(x,y+sign(yadd),objCollision) ) y += sign(yadd);
		
}
else y += yaddSpd;

	
//straight
if (abs(yaddSpd) > abs(xaddSpd)) {
	if (yaddSpd < 0) { //moving right
		sprite_index = drifter_up;	//drifter_up
		dir = "up";
	}
	else {
		sprite_index = drifter_down;	//drifter_down
		dir = "down";
	}
}

if (abs(xaddSpd) > abs(yaddSpd)) {
	if (xaddSpd < 0) { //moving right
		sprite_index = drifter_right;	//drifter_left
		image_xscale = -1;	
		dir = "left";
		
		//climb
		if ( place_meeting(x,y,objClimbLeft) ) {
			y -= 1;
		}
		else if ( place_meeting(x,y,objClimbRight) ) {
			y += 1;
		}
	}
	else {
		sprite_index = drifter_right;	//drifter_right
		dir = "right";
		image_xscale = 1;
		
		//climb
		if ( place_meeting(x,y,objClimbLeft) ) {
			y += 1;
		}
		else if ( place_meeting(x,y,objClimbRight) ) {
			y -= 1;
		}
		
	}
}

#region diagonal
//diag and standard movement, animation
if (yaddSpd < 0 && xaddSpd > 0 ) { //moving up/right
	//show_debug_message("UR")
	sprite_index = drifter_up;	//drifter_up
	dir = "up_right";
}
else if (yaddSpd < 0 && xaddSpd < 0 ) { //moving up/left
	//show_debug_message("UL")
	sprite_index = drifter_up;	//drifter_up
	dir = "up_left";
}
else if (yaddSpd > 0 && xaddSpd < 0 ) { //moving down/left
	sprite_index = drifter_down;	//drifter_up
	dir = "down_left";
}
else if (yaddSpd > 0 && xaddSpd > 0 ) { //moving down/right
	sprite_index = drifter_down;	//drifter_up
	dir = "down_right";
}

//diagonal speed
if ( xaddSpd != 0 && yaddSpd != 0 ) {
	playerSpeed = diagSpeed;
	//xaddSpd = xaddSpd / sqrt(2);
	//yaddSpd = yaddSpd / sqrt(2);
} else playerSpeed = walkSpeed;
#endregion


//bounds
if (x <= objPlayer.sprite_width/2)					x = objPlayer.sprite_width/2;
if (x >= room_width - objPlayer.sprite_width/2)		x = room_width - objPlayer.sprite_width/2;
if (y <= objPlayer.sprite_height/2)					y = objPlayer.sprite_height/2;
if (y >= room_height - objPlayer.sprite_height/2)	y = room_height - objPlayer.sprite_height/2;

#endregion

if (xaddSpd == 0 && yaddSpd == 0) {
	sprite_index = playerIdle;
	image_speed = 1;
}
//diff speed because left/right animation has less frames than up/down. hack for now
else if (dir == "left" || dir == "right") image_speed = animation;
else image_speed = 1.25;


//if (charging) sprite_index = drifter_left_shoot;
//else if (charged) sprite_index = drifter_left_shoot;
if (actShoot) {
//if (false) {
	image_index = -1;
	if (dir == "up" || dir == "up_right" || dir == "up_left") sprite_index = drifter_up_shoot;
	else if (dir == "down" || dir == "down_right" || dir == "down_left") sprite_index = drifter_down_shoot;
	else sprite_index = drifter_left_shoot;
}

#region FADE to room
//SWITCH to room (no fade)
/*var inst = instance_place(x,y,objWarp);

if(inst != noone){
	x = inst.targetX;
	y = inst.targetY;
	camera.x = inst.targetX;
	camera.y = inst.targetY;
	
	room_goto(inst.targetRoom);
}*/

var inst = instance_place(x,y,objWarp);
if(inst != noone){
	with(newFade){
		if(!doTransition){
			spawnRoom = inst.targetRoom;
			spawnX = inst.targetX;
			spawnY = inst.targetY;
			doTransition = true;
			objPlayer.playerMove = false;
		}
	}
}
#endregion

if (aimLock) {
	//state = playerState.AIM;
	aimLockSpeed = aimLockConst;
}

else if (aimLockRel) {
	aimLockSpeed = 1;
}
//show_debug_message(aimLock)

#region shoot/attack
if (actShoot && !isShooting) {
	//default gun, doesn't use energy
	if (currentGun == guns1.DEFAULT) {
		if ( defaultEnergy > 0 ) state = playerState.SHOOT;
	}
	//all other guns use energy
	else if ( weaponEnergy > 0 ) state = playerState.SHOOT;
}

else if ( actAttack ) state = playerState.ATTACK;
#endregion
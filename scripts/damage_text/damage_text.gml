///@description
//arg0 amount
//arg1 x
//arg2 y

_amount = argument[0];
var _x = x;
var _y = y;

if (argument_count == 3) {
	_x = argument[1];
	_y = argument[2];
}

instance_create_layer(_x,_y,"Instances",objDrawDamage);
objDrawDamage.num = _amount;
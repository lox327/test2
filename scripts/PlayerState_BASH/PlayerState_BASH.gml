//init for when player bashes, used once
if (sprite_index != playerBash_left && sprite_index != playerBash_right && sprite_index != playerBash_up && sprite_index != playerBash_down) {
	image_index = 0;
	if (dir == "left") sprite_index = playerBash_left;
	else if (dir == "right") sprite_index = playerBash_right;
	else if (dir == "up") sprite_index = playerBash_up;
	else if (dir == "down") sprite_index = playerBash_down;
}


//hitbox code
//1st param is frame that the hitbox starts (2)
//2nd param is frame that finishes the animation (3)
if ( image_index >= 2 && image_index <= 3 ) {

#region momentum code
switch(dir) {
	case "up": move_and_collide(90, bashSpeed);
	break;
	
	case "down": move_and_collide(270, bashSpeed);
	break;
	
	case "left": move_and_collide(180, bashSpeed);
	break;
	
	case "right": move_and_collide(0, bashSpeed);
	break;
	
	//diag
	case "up_right": move_and_collide(45, bashSpeed);
	break;
	
	case "up_left": move_and_collide(135, bashSpeed);
	break;
	
	case "down_right": move_and_collide(315, bashSpeed);
	break;
	
	case "down_left": move_and_collide(225, bashSpeed);
	break;
	
}
#endregion
	
	//create bash collision box
	with (instance_create_layer(x,y,"Instances",objBashBox2a)) {
		#region
		switch(objPlayer.dir) {
			case "up":
				x += 25;
				y -= 32;
			break;
	
			case "down": 
				//x -= 32;
				y += 32;
			break;
	
			case "left": 
				x -= 15;
			break;
			
			case "right": 
				image_xscale = other.image_xscale;
				x += 40;
			break;
	
	
			//diag
			case "up_right":
				x += 16;
				y -= 32;
			break;
	
			case "up_left":
				x -= 64;
				y -= 32;
			break;
	
			case "down_right": 
				x += 16;
				y += 32;
			break;
	
			case "down_left":
				x -= 64;
				y += 32;
			break;
	
		}
		#endregion
		
		
		with (instance_place(x,y,enemyParent)) {
			if (enemyState != EnemyState.HIT) {
				//should add id to list to manage who exactly has been hit
				enemyState = EnemyState.HIT;
				timer = bashTime; //timing of this with enemy IDLE is being weird
				hitAngle = point_direction(objBashBox2a.x, objBashBox2a.y, x, y);
			}
			
		}
	}
}

if (animation_end()) {
	state = playerState.IDLE;
	sprite_index = playerIdle;
}

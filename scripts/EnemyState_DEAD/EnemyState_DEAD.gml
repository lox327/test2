sprite = argument[0];
//show_debug_message(argument[0]);

motion_set(point_direction(objPlayer.x, objPlayer.y, x, y), spd*.5);
if (timer <= 0)
    { 
		motion_set(0,0);
		instance_destroy();
			
		with (instance_create_layer(x,y,"Instances",objEnemyDead)) {
			id.sprite_index = sprite;
		}
    }
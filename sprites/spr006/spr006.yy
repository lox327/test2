{
    "id": "aed0ae54-d582-4e67-bcab-26d8474c5d7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr006",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 0,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c52b53c-0d1c-4482-9e5d-8e873997c233",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aed0ae54-d582-4e67-bcab-26d8474c5d7a",
            "compositeImage": {
                "id": "02f3e82f-d1ee-4b93-a01d-0c07f0b4726d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c52b53c-0d1c-4482-9e5d-8e873997c233",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8c1ea00-d81c-4495-8e25-12481dce7bdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c52b53c-0d1c-4482-9e5d-8e873997c233",
                    "LayerId": "e381e7e9-2798-4d47-a7a8-5508d4baff55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "e381e7e9-2798-4d47-a7a8-5508d4baff55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aed0ae54-d582-4e67-bcab-26d8474c5d7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 13,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "bb5236df-e186-4ee6-80ff-453db19b6eae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Attack1_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ea42c7c-593a-4e5b-90e7-f58d3c96d63b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb5236df-e186-4ee6-80ff-453db19b6eae",
            "compositeImage": {
                "id": "0c9d698e-58e5-4c00-947d-42bfd4645cff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea42c7c-593a-4e5b-90e7-f58d3c96d63b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0422224-7556-473a-bef6-a06db41ae2ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea42c7c-593a-4e5b-90e7-f58d3c96d63b",
                    "LayerId": "9b271480-e3e6-4c15-85ff-e0f72f5c7816"
                }
            ]
        },
        {
            "id": "18ce7fe4-7075-47cd-b40a-4bc1a0bf4872",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb5236df-e186-4ee6-80ff-453db19b6eae",
            "compositeImage": {
                "id": "8d686f31-6d30-44b9-a562-aeb605f8d782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18ce7fe4-7075-47cd-b40a-4bc1a0bf4872",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15abaeaa-7103-4a50-b43c-46e6a6529323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18ce7fe4-7075-47cd-b40a-4bc1a0bf4872",
                    "LayerId": "9b271480-e3e6-4c15-85ff-e0f72f5c7816"
                }
            ]
        },
        {
            "id": "202defc9-00df-460b-8929-f71a922a986e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb5236df-e186-4ee6-80ff-453db19b6eae",
            "compositeImage": {
                "id": "a7c500c5-1d18-4844-8f99-c0289582cc31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "202defc9-00df-460b-8929-f71a922a986e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8643f9a0-b952-4074-981e-6899d5f37b5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "202defc9-00df-460b-8929-f71a922a986e",
                    "LayerId": "9b271480-e3e6-4c15-85ff-e0f72f5c7816"
                }
            ]
        },
        {
            "id": "d63fd2eb-afae-4950-8916-46e83cb986a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb5236df-e186-4ee6-80ff-453db19b6eae",
            "compositeImage": {
                "id": "b83bf49e-136a-4318-9f37-f0d78479dd1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63fd2eb-afae-4950-8916-46e83cb986a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec4b051e-98fc-4655-9e3a-027ab7cd3e7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63fd2eb-afae-4950-8916-46e83cb986a6",
                    "LayerId": "9b271480-e3e6-4c15-85ff-e0f72f5c7816"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "9b271480-e3e6-4c15-85ff-e0f72f5c7816",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb5236df-e186-4ee6-80ff-453db19b6eae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
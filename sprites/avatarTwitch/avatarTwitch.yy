{
    "id": "a605c720-6c7a-4128-823e-6921fd6cfaf1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarTwitch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 7,
    "bbox_right": 48,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22400b11-50f9-4750-930b-35df69435da4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a605c720-6c7a-4128-823e-6921fd6cfaf1",
            "compositeImage": {
                "id": "f221a7f2-16d0-481d-a5c9-7b72753263d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22400b11-50f9-4750-930b-35df69435da4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3126162e-714a-487f-b859-dd9725d43006",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22400b11-50f9-4750-930b-35df69435da4",
                    "LayerId": "8266c975-66bd-4af3-b941-5a5a594045bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "8266c975-66bd-4af3-b941-5a5a594045bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a605c720-6c7a-4128-823e-6921fd6cfaf1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": -195,
    "yorig": -74
}
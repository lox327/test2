{
    "id": "fd5f1114-f408-424a-8809-a55c18f077f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr007",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6bfd2a2-5bf8-4447-bb3e-086c8d0e62d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd5f1114-f408-424a-8809-a55c18f077f4",
            "compositeImage": {
                "id": "3eb29bd6-3fd1-4836-9bdf-74f8bdacb5fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6bfd2a2-5bf8-4447-bb3e-086c8d0e62d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27733f08-09c8-40de-ac3b-3a1934ee76d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6bfd2a2-5bf8-4447-bb3e-086c8d0e62d1",
                    "LayerId": "6bde9d5b-a476-4ea6-878a-d4f9e149626f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "6bde9d5b-a476-4ea6-878a-d4f9e149626f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd5f1114-f408-424a-8809-a55c18f077f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}
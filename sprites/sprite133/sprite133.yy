{
    "id": "9ee2cafd-d35e-4aa6-b79c-edb6a1833d47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite133",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cd347e0-5270-4112-b4b0-0c52490968db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9ee2cafd-d35e-4aa6-b79c-edb6a1833d47",
            "compositeImage": {
                "id": "7f039c53-07d6-47ca-9f50-b0df50aaa862",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd347e0-5270-4112-b4b0-0c52490968db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89a884f1-1411-4fe2-b6c4-97fae88a2d95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd347e0-5270-4112-b4b0-0c52490968db",
                    "LayerId": "8e06d459-a3db-42d8-997c-21c16ee43dd6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "8e06d459-a3db-42d8-997c-21c16ee43dd6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9ee2cafd-d35e-4aa6-b79c-edb6a1833d47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
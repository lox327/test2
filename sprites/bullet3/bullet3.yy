{
    "id": "36fc7366-69a6-4f61-b950-fc28191455e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bullet3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4f299df-73a2-45e4-a07d-a948f3354617",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36fc7366-69a6-4f61-b950-fc28191455e8",
            "compositeImage": {
                "id": "3a4564c5-2352-4332-97f5-2c2fa362acc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4f299df-73a2-45e4-a07d-a948f3354617",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "554f3874-b848-4453-883c-4f5ce0201b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4f299df-73a2-45e4-a07d-a948f3354617",
                    "LayerId": "6eb7e765-a03c-483b-a76b-932d85ea935e"
                }
            ]
        },
        {
            "id": "7575e125-d81d-46c8-8738-ff68c99389f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36fc7366-69a6-4f61-b950-fc28191455e8",
            "compositeImage": {
                "id": "df8b6691-7b4a-4c94-a825-19b503a633b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7575e125-d81d-46c8-8738-ff68c99389f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "274e58ff-d93e-44ba-adf4-316835e554b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7575e125-d81d-46c8-8738-ff68c99389f6",
                    "LayerId": "6eb7e765-a03c-483b-a76b-932d85ea935e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "6eb7e765-a03c-483b-a76b-932d85ea935e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36fc7366-69a6-4f61-b950-fc28191455e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}
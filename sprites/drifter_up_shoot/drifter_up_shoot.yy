{
    "id": "4913765d-7367-4907-ab57-7e60d6e2d020",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "drifter_up_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 19,
    "bbox_right": 35,
    "bbox_top": 36,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e11e4602-249c-47db-b3f0-053e89838faa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4913765d-7367-4907-ab57-7e60d6e2d020",
            "compositeImage": {
                "id": "838b3fea-e6bc-41e5-a737-4e1b33872cfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e11e4602-249c-47db-b3f0-053e89838faa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e1f02a9-8c74-4d5a-b8a1-3ef96e845585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e11e4602-249c-47db-b3f0-053e89838faa",
                    "LayerId": "95f7ce00-867a-407b-8d9c-a753470bfccf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "95f7ce00-867a-407b-8d9c-a753470bfccf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4913765d-7367-4907-ab57-7e60d6e2d020",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "d16b8db7-2939-41d6-b7ef-093786483ebe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "arm2b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 294,
    "bbox_left": 5,
    "bbox_right": 81,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "282f4362-0bed-4865-9e15-2955ecba3602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d16b8db7-2939-41d6-b7ef-093786483ebe",
            "compositeImage": {
                "id": "6997b83b-9ef2-4366-a9f2-e99196ab0f32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282f4362-0bed-4865-9e15-2955ecba3602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "baf825eb-258a-4241-b6a1-2fce3a90d9f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282f4362-0bed-4865-9e15-2955ecba3602",
                    "LayerId": "3b95bab8-6be9-4ae0-828c-93634cee5781"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "3b95bab8-6be9-4ae0-828c-93634cee5781",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d16b8db7-2939-41d6-b7ef-093786483ebe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 0,
    "yorig": 0
}
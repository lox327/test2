{
    "id": "3c667b82-4690-4fc4-9da5-a66a8280687e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aDeath",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a180f20-25f7-4465-9e7b-47ce838f1599",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c667b82-4690-4fc4-9da5-a66a8280687e",
            "compositeImage": {
                "id": "138be435-3c78-4ae1-88b1-058c957b3193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a180f20-25f7-4465-9e7b-47ce838f1599",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34d43f4d-34ee-46b2-953d-7136ec3b3d4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a180f20-25f7-4465-9e7b-47ce838f1599",
                    "LayerId": "7fcece5c-e233-40ed-b663-c7f9d0cf05c4"
                }
            ]
        },
        {
            "id": "cd261e5a-4242-49bf-90ca-dc6f9bf5ed37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c667b82-4690-4fc4-9da5-a66a8280687e",
            "compositeImage": {
                "id": "4dde0d5d-c931-493f-b454-de9c39983931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd261e5a-4242-49bf-90ca-dc6f9bf5ed37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fec08c3f-bdec-4915-b0e0-cc21d6fff0e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd261e5a-4242-49bf-90ca-dc6f9bf5ed37",
                    "LayerId": "7fcece5c-e233-40ed-b663-c7f9d0cf05c4"
                }
            ]
        },
        {
            "id": "c24db6e7-ad96-471c-b250-e65d65b89edd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c667b82-4690-4fc4-9da5-a66a8280687e",
            "compositeImage": {
                "id": "3dc93f7a-97e9-4968-aa10-496c0656beca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c24db6e7-ad96-471c-b250-e65d65b89edd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b690b9a3-9bb3-484d-9f18-3fbd6c461f6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c24db6e7-ad96-471c-b250-e65d65b89edd",
                    "LayerId": "7fcece5c-e233-40ed-b663-c7f9d0cf05c4"
                }
            ]
        },
        {
            "id": "4113f2c8-6883-44b2-a4fd-4db1c1682967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c667b82-4690-4fc4-9da5-a66a8280687e",
            "compositeImage": {
                "id": "c929dac5-6562-43d8-a57c-adcc267acc00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4113f2c8-6883-44b2-a4fd-4db1c1682967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d00bd6e5-e342-419d-a408-b9a35dc799dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4113f2c8-6883-44b2-a4fd-4db1c1682967",
                    "LayerId": "7fcece5c-e233-40ed-b663-c7f9d0cf05c4"
                }
            ]
        },
        {
            "id": "d83a0281-6ecc-4d6a-9537-f318783fd2b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c667b82-4690-4fc4-9da5-a66a8280687e",
            "compositeImage": {
                "id": "83e2f2d4-1bad-4d99-8331-c5d781b5b342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d83a0281-6ecc-4d6a-9537-f318783fd2b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c86c9f90-e92a-4634-90a5-c611f6ed7f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d83a0281-6ecc-4d6a-9537-f318783fd2b2",
                    "LayerId": "7fcece5c-e233-40ed-b663-c7f9d0cf05c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7fcece5c-e233-40ed-b663-c7f9d0cf05c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c667b82-4690-4fc4-9da5-a66a8280687e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
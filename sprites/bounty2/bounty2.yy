{
    "id": "d05811b2-0bf8-4a44-8a91-4698f3584beb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bounty2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ace4e93-6ae5-4494-8640-fb5e186f661c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d05811b2-0bf8-4a44-8a91-4698f3584beb",
            "compositeImage": {
                "id": "a295f116-9d8c-4088-8061-9c797d415867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ace4e93-6ae5-4494-8640-fb5e186f661c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2282881-15fb-46ab-a780-8edacab156d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ace4e93-6ae5-4494-8640-fb5e186f661c",
                    "LayerId": "d034040a-92ee-4d94-a4a8-8a6201b10c68"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "d034040a-92ee-4d94-a4a8-8a6201b10c68",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d05811b2-0bf8-4a44-8a91-4698f3584beb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
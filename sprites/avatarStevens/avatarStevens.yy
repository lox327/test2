{
    "id": "d8107385-6ba7-498d-a8c2-ff8c44c06549",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarStevens",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 4,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28748cd8-7e2e-4e23-8b76-24d204da090d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8107385-6ba7-498d-a8c2-ff8c44c06549",
            "compositeImage": {
                "id": "517c68fd-64b2-4817-98f0-7f901bebb423",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28748cd8-7e2e-4e23-8b76-24d204da090d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce414ae-6cbe-46ce-8b89-bf5dee1e22aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28748cd8-7e2e-4e23-8b76-24d204da090d",
                    "LayerId": "fae714b7-38e6-4ba1-959a-b87504f2b5dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "fae714b7-38e6-4ba1-959a-b87504f2b5dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8107385-6ba7-498d-a8c2-ff8c44c06549",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
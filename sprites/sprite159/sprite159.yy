{
    "id": "3eb9b7a8-f629-4ced-bc83-21ab255a8981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite159",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c02a88c-cb4b-4494-80bf-2e46a81410aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eb9b7a8-f629-4ced-bc83-21ab255a8981",
            "compositeImage": {
                "id": "34e07cd0-03bf-41f5-b1fe-c21dcd2231ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c02a88c-cb4b-4494-80bf-2e46a81410aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95747b18-04d5-43d0-ab34-0463bdc0ec9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c02a88c-cb4b-4494-80bf-2e46a81410aa",
                    "LayerId": "4db20e95-1750-4b57-8b9e-85b85175cc8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4db20e95-1750-4b57-8b9e-85b85175cc8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eb9b7a8-f629-4ced-bc83-21ab255a8981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
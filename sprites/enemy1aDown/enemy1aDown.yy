{
    "id": "28d784f4-8a74-47ca-b477-064ca8896ab8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d8d1f0e-5a10-4d3a-9d5c-e5f622777c2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d784f4-8a74-47ca-b477-064ca8896ab8",
            "compositeImage": {
                "id": "016657a9-4065-461e-874d-dc7d4fc5e373",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d8d1f0e-5a10-4d3a-9d5c-e5f622777c2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c20fd7ed-6132-4c27-b158-cc0868accdc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d8d1f0e-5a10-4d3a-9d5c-e5f622777c2f",
                    "LayerId": "0d36446a-66d8-41e0-b419-1ef8877cdd2d"
                }
            ]
        },
        {
            "id": "fb66bdb6-5f7a-4037-a377-7d85ca8d8ef6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28d784f4-8a74-47ca-b477-064ca8896ab8",
            "compositeImage": {
                "id": "b7ea9477-b3e4-4689-9f08-72edd146bd53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb66bdb6-5f7a-4037-a377-7d85ca8d8ef6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d3c6959-e742-4283-aef9-be015c81e83f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb66bdb6-5f7a-4037-a377-7d85ca8d8ef6",
                    "LayerId": "0d36446a-66d8-41e0-b419-1ef8877cdd2d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d36446a-66d8-41e0-b419-1ef8877cdd2d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28d784f4-8a74-47ca-b477-064ca8896ab8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "24b0799d-3bd8-4304-b88a-ec0c85b4fbf8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Attack1_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 16,
    "bbox_right": 43,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c2fddc0-1ec1-4462-beb8-a4bc73e26469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24b0799d-3bd8-4304-b88a-ec0c85b4fbf8",
            "compositeImage": {
                "id": "a4db985a-d172-40e3-bc4b-76a48989f4e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c2fddc0-1ec1-4462-beb8-a4bc73e26469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb985909-c6e8-43a7-9972-2db678ba29bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c2fddc0-1ec1-4462-beb8-a4bc73e26469",
                    "LayerId": "fe433e5d-6213-472e-9f31-51c7f1a4153e"
                }
            ]
        },
        {
            "id": "3902d87e-5ea5-4919-833e-3a5334b023af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24b0799d-3bd8-4304-b88a-ec0c85b4fbf8",
            "compositeImage": {
                "id": "e7ef85a5-e100-45ec-8487-d524a2e32b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3902d87e-5ea5-4919-833e-3a5334b023af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ebd4bfe-dcea-4c28-8234-7fc4f99e0111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3902d87e-5ea5-4919-833e-3a5334b023af",
                    "LayerId": "fe433e5d-6213-472e-9f31-51c7f1a4153e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "fe433e5d-6213-472e-9f31-51c7f1a4153e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24b0799d-3bd8-4304-b88a-ec0c85b4fbf8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
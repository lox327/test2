{
    "id": "da7ff5fc-694a-4e77-b0c4-085c55631900",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDirtbag1_smoking",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "386535c4-e341-4be5-b19b-c457ab47fd8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "e6a596c0-9b20-4d45-aff6-c6a7ce929376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "386535c4-e341-4be5-b19b-c457ab47fd8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b643cc70-c3f2-4996-be80-3e8ea6f4844b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "386535c4-e341-4be5-b19b-c457ab47fd8b",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "7041d9ae-b41c-4c97-bc0a-2178ad811607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "fb987124-9bb8-4dbb-ad09-6113b03d3642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7041d9ae-b41c-4c97-bc0a-2178ad811607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a118d25-2afe-483f-ba5e-aa711feea980",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7041d9ae-b41c-4c97-bc0a-2178ad811607",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "57c961ac-4caa-4417-8400-21596fc56518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "e148c766-293d-4b46-9b17-ab9a1946595b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57c961ac-4caa-4417-8400-21596fc56518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "829f21bd-8709-4d1a-91e7-c87b0c7fbe6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57c961ac-4caa-4417-8400-21596fc56518",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "c2876366-b880-419d-b0b9-c3f52bb4e050",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "7ee70ef1-eb4c-4d18-b234-1d63a7665438",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2876366-b880-419d-b0b9-c3f52bb4e050",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a09a0838-2b87-4d73-a13f-5cfd9a8566c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2876366-b880-419d-b0b9-c3f52bb4e050",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "0c677785-31a5-4e1c-b3a5-b3c862a1c867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "84caf6d0-8e0a-41ba-a0d0-390e670dcc93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c677785-31a5-4e1c-b3a5-b3c862a1c867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a81a465b-f0bd-4fa0-9388-a88fa83ea4b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c677785-31a5-4e1c-b3a5-b3c862a1c867",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "e622d634-2907-4f1a-be3a-b3d280219c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "33b73d68-37ae-48ad-9ee2-c169f1134194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e622d634-2907-4f1a-be3a-b3d280219c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c71aa54f-8090-40b3-80c9-c2c560d920f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e622d634-2907-4f1a-be3a-b3d280219c00",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "449a3e35-88f9-4d86-9af2-bbe5fcb467cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "79927250-a176-4be8-abf7-229f0b8c727d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449a3e35-88f9-4d86-9af2-bbe5fcb467cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ba188c0-5f7a-41a8-bf8c-7549d03579ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449a3e35-88f9-4d86-9af2-bbe5fcb467cc",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "2565cd57-55d3-4493-80fb-2b68360279e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "370f5991-a44a-476c-8dd5-67e9303a2acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2565cd57-55d3-4493-80fb-2b68360279e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfb1648f-8078-4f43-a94d-3929d26a054a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2565cd57-55d3-4493-80fb-2b68360279e1",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "428b1627-d470-4e8a-b4f1-a9eec238ec50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "fb24045a-017d-4334-a5d1-151f8f7df4b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "428b1627-d470-4e8a-b4f1-a9eec238ec50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc26601-65e0-4575-9cff-b81372c2b036",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "428b1627-d470-4e8a-b4f1-a9eec238ec50",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "7b7b9ed6-e9dd-4f91-b8b1-e6393f072b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "5f27a8c8-e9ce-4ccc-8d67-d6b129591329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b7b9ed6-e9dd-4f91-b8b1-e6393f072b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2de43db5-6b86-422c-baf6-56ce4e9ff9e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b7b9ed6-e9dd-4f91-b8b1-e6393f072b3b",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "aaca195f-6663-46a3-b533-795eaf6ef416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "b627e55c-ec1e-4b21-81da-5ea8ea1b57b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaca195f-6663-46a3-b533-795eaf6ef416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b4d8e36-da9b-4f5c-8a2a-bcae244aaf0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaca195f-6663-46a3-b533-795eaf6ef416",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        },
        {
            "id": "599a1428-8aa6-4c36-9253-3f05f9ffff63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "compositeImage": {
                "id": "375d52ac-f917-4657-8e38-f5e7b2ac8369",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599a1428-8aa6-4c36-9253-3f05f9ffff63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e36ef87-14ba-48d3-a9aa-f183a4b36b53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599a1428-8aa6-4c36-9253-3f05f9ffff63",
                    "LayerId": "de827bac-8d18-49b6-aaf3-dcc8a02378cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "de827bac-8d18-49b6-aaf3-dcc8a02378cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da7ff5fc-694a-4e77-b0c4-085c55631900",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 11,
    "yorig": 25
}
{
    "id": "963b2ebc-c492-4b47-ad27-f8a16dfa442a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9af0c3d9-bd6f-49a9-adf2-e7320f3b6721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963b2ebc-c492-4b47-ad27-f8a16dfa442a",
            "compositeImage": {
                "id": "de2e41c8-486b-4030-a065-d4e9952e4b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9af0c3d9-bd6f-49a9-adf2-e7320f3b6721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21765c03-cff4-4282-96b6-0c2cef9af997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9af0c3d9-bd6f-49a9-adf2-e7320f3b6721",
                    "LayerId": "81dfc91d-6081-44b8-a876-eb566ca6f740"
                }
            ]
        },
        {
            "id": "f37f1401-d000-49ed-85b3-90b49b295195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "963b2ebc-c492-4b47-ad27-f8a16dfa442a",
            "compositeImage": {
                "id": "c71d1d6a-3788-41b5-9e27-654b1917f208",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f37f1401-d000-49ed-85b3-90b49b295195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "627ff084-3b5d-4ba9-ae2f-e2a9e156609d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f37f1401-d000-49ed-85b3-90b49b295195",
                    "LayerId": "81dfc91d-6081-44b8-a876-eb566ca6f740"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "81dfc91d-6081-44b8-a876-eb566ca6f740",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "963b2ebc-c492-4b47-ad27-f8a16dfa442a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 21
}
{
    "id": "c7e1ed13-1e53-4f5a-b0ce-f38a1555c4f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bountyAvatar1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 203,
    "bbox_left": 12,
    "bbox_right": 67,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84a58673-d6bd-460f-b118-1d25f2635f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c7e1ed13-1e53-4f5a-b0ce-f38a1555c4f6",
            "compositeImage": {
                "id": "2d7cca71-5992-4e84-9f47-0aed247f8572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84a58673-d6bd-460f-b118-1d25f2635f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8c5a1e-6b73-4757-8437-055a61cf7dfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84a58673-d6bd-460f-b118-1d25f2635f45",
                    "LayerId": "dfa5bc8e-76b6-49f6-807c-a4d1531a14d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 212,
    "layers": [
        {
            "id": "dfa5bc8e-76b6-49f6-807c-a4d1531a14d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c7e1ed13-1e53-4f5a-b0ce-f38a1555c4f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}
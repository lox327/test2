{
    "id": "c20441a2-d65f-47bd-acd4-8d8524f944e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerHit_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b191e241-251a-490a-9bf6-11e5d693a592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c20441a2-d65f-47bd-acd4-8d8524f944e9",
            "compositeImage": {
                "id": "96a69dda-7903-40f6-b278-6c28621b9882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b191e241-251a-490a-9bf6-11e5d693a592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2981e13a-5d2d-4360-9ec1-061a959c8109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b191e241-251a-490a-9bf6-11e5d693a592",
                    "LayerId": "3b3af9fb-e169-494a-aa26-57cdb041b850"
                }
            ]
        },
        {
            "id": "dde61881-5b97-4e26-9c20-b4876474a504",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c20441a2-d65f-47bd-acd4-8d8524f944e9",
            "compositeImage": {
                "id": "51cbc966-3d5b-4f92-916a-716c9e07f9b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dde61881-5b97-4e26-9c20-b4876474a504",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f07e0e0-2f48-4276-89c7-9a7793917fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dde61881-5b97-4e26-9c20-b4876474a504",
                    "LayerId": "3b3af9fb-e169-494a-aa26-57cdb041b850"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "3b3af9fb-e169-494a-aa26-57cdb041b850",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c20441a2-d65f-47bd-acd4-8d8524f944e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
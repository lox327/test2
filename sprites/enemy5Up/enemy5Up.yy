{
    "id": "c963d293-8adc-457c-b09b-80ac6c608b3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 11,
    "bbox_right": 49,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ae8b9b1-4db4-475a-a3da-576cb11a16b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "compositeImage": {
                "id": "31450937-c2c4-44be-8f64-6cb2046067ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ae8b9b1-4db4-475a-a3da-576cb11a16b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a9e5c8-355f-4da9-8b1d-2ba49ed5df49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ae8b9b1-4db4-475a-a3da-576cb11a16b9",
                    "LayerId": "847c4bfc-793f-4ced-91f5-888dba995075"
                }
            ]
        },
        {
            "id": "a0b479fc-4be5-4b1b-8a11-c871b2ca5807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "compositeImage": {
                "id": "e6fcf1ae-6fc0-41a1-afd8-fefc234fc205",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b479fc-4be5-4b1b-8a11-c871b2ca5807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af30d692-cb01-4756-8c4f-71aa56e48bb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b479fc-4be5-4b1b-8a11-c871b2ca5807",
                    "LayerId": "847c4bfc-793f-4ced-91f5-888dba995075"
                }
            ]
        },
        {
            "id": "f653ebcb-620e-4813-9892-f49835b0b5b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "compositeImage": {
                "id": "c16cf1b4-ecd4-498d-b626-d0f00a1a11cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f653ebcb-620e-4813-9892-f49835b0b5b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdfe9847-b0fb-4193-a80e-26c6f7e6206c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f653ebcb-620e-4813-9892-f49835b0b5b5",
                    "LayerId": "847c4bfc-793f-4ced-91f5-888dba995075"
                }
            ]
        },
        {
            "id": "3bb88a5e-718c-424a-8be8-b84a4f734ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "compositeImage": {
                "id": "9d5a5335-2542-46d2-ad5d-01ad34a0ccb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bb88a5e-718c-424a-8be8-b84a4f734ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80bf6f76-c79b-409d-b1c9-0315962c4961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bb88a5e-718c-424a-8be8-b84a4f734ae6",
                    "LayerId": "847c4bfc-793f-4ced-91f5-888dba995075"
                }
            ]
        },
        {
            "id": "773f7f7e-71d7-4d89-9df3-2a4f86f54587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "compositeImage": {
                "id": "4567b821-a2b5-4ce5-b021-a4bd244a8f58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "773f7f7e-71d7-4d89-9df3-2a4f86f54587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70fbd8ce-eca1-4195-96a5-3b9952189180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "773f7f7e-71d7-4d89-9df3-2a4f86f54587",
                    "LayerId": "847c4bfc-793f-4ced-91f5-888dba995075"
                }
            ]
        },
        {
            "id": "10d537a6-8171-442f-8b98-8cfe287d9091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "compositeImage": {
                "id": "35fa084a-6f40-4c90-81e5-c279c8a4ba5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10d537a6-8171-442f-8b98-8cfe287d9091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd445adb-f7c1-4019-b5bd-373ecd5d021b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10d537a6-8171-442f-8b98-8cfe287d9091",
                    "LayerId": "847c4bfc-793f-4ced-91f5-888dba995075"
                }
            ]
        },
        {
            "id": "d810375c-2aee-411c-bfc4-f22571d43c50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "compositeImage": {
                "id": "1e92099f-1ab3-477c-bcf9-5fbd1c36a379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d810375c-2aee-411c-bfc4-f22571d43c50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff52ac2-a7d1-48a8-bb4f-dea8c4ffc066",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d810375c-2aee-411c-bfc4-f22571d43c50",
                    "LayerId": "847c4bfc-793f-4ced-91f5-888dba995075"
                }
            ]
        },
        {
            "id": "0012ce58-14af-4042-948d-2fc986050883",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "compositeImage": {
                "id": "4ad39f0e-7a38-4b7e-a260-6612db4737d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0012ce58-14af-4042-948d-2fc986050883",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da7869fd-b301-490f-be30-201ecf90bdda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0012ce58-14af-4042-948d-2fc986050883",
                    "LayerId": "847c4bfc-793f-4ced-91f5-888dba995075"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "847c4bfc-793f-4ced-91f5-888dba995075",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c963d293-8adc-457c-b09b-80ac6c608b3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "be642dad-cc54-4103-9378-f16ec35d0299",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGlow_blue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44569e84-7a9a-43be-865a-a178d96d721c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be642dad-cc54-4103-9378-f16ec35d0299",
            "compositeImage": {
                "id": "b468830c-4099-4962-8b2b-fd37ff06100d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44569e84-7a9a-43be-865a-a178d96d721c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3d9698a-8556-45f2-9f6e-154bcf7bb52b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44569e84-7a9a-43be-865a-a178d96d721c",
                    "LayerId": "d4c943a4-0091-42e1-a6e7-b0430ff44e7a"
                }
            ]
        },
        {
            "id": "bcd0d3d8-dd7a-462e-a270-fcf3a9d98541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be642dad-cc54-4103-9378-f16ec35d0299",
            "compositeImage": {
                "id": "4f363b26-3cca-4376-841d-187cfa9cbb7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcd0d3d8-dd7a-462e-a270-fcf3a9d98541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "177da362-64ba-45e1-ac3b-b6db636be891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcd0d3d8-dd7a-462e-a270-fcf3a9d98541",
                    "LayerId": "d4c943a4-0091-42e1-a6e7-b0430ff44e7a"
                }
            ]
        },
        {
            "id": "8ee2c309-b688-4f04-ba48-09c847b3c9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be642dad-cc54-4103-9378-f16ec35d0299",
            "compositeImage": {
                "id": "5d9a620a-7ae2-4904-b38c-bcd981028dd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ee2c309-b688-4f04-ba48-09c847b3c9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24cce2fc-d899-4b02-81cb-6b60d8f01c4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ee2c309-b688-4f04-ba48-09c847b3c9c3",
                    "LayerId": "d4c943a4-0091-42e1-a6e7-b0430ff44e7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d4c943a4-0091-42e1-a6e7-b0430ff44e7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be642dad-cc54-4103-9378-f16ec35d0299",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 32
}
{
    "id": "d443a1b2-e011-4e7b-910f-669b2e525ee2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jpgroundtiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 479,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e76a8ce-050e-48d7-b41c-935eb0fd2b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d443a1b2-e011-4e7b-910f-669b2e525ee2",
            "compositeImage": {
                "id": "a8d1501c-4b7e-4b77-8e9d-43592729fcb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e76a8ce-050e-48d7-b41c-935eb0fd2b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4fb054c-ebb7-4475-b5f6-718be6c07a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e76a8ce-050e-48d7-b41c-935eb0fd2b6e",
                    "LayerId": "c3bdc226-8a3a-43db-95fb-4ef661102363"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "c3bdc226-8a3a-43db-95fb-4ef661102363",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d443a1b2-e011-4e7b-910f-669b2e525ee2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "f7a2d24d-1a07-420a-9fdd-4b5a728e4b79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr011",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9435ec1d-a4cb-446e-bbc6-8cf5c6d449ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7a2d24d-1a07-420a-9fdd-4b5a728e4b79",
            "compositeImage": {
                "id": "e7663e52-3290-4202-9050-d18530049250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9435ec1d-a4cb-446e-bbc6-8cf5c6d449ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e92e3f9-a7f6-445b-8cb3-3492b0af2905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9435ec1d-a4cb-446e-bbc6-8cf5c6d449ef",
                    "LayerId": "048a167a-fa31-407b-b385-641c2911070d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "048a167a-fa31-407b-b385-641c2911070d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7a2d24d-1a07-420a-9fdd-4b5a728e4b79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
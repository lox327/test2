{
    "id": "f230c565-728f-4306-8279-f998301c4139",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr008",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "88494bf7-f5bb-4288-a44d-e7c4b90136ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f230c565-728f-4306-8279-f998301c4139",
            "compositeImage": {
                "id": "06025d33-297a-44b8-90f7-70c1282c0f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88494bf7-f5bb-4288-a44d-e7c4b90136ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e38442d7-b4e3-47e8-923d-0836ef6dbf6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88494bf7-f5bb-4288-a44d-e7c4b90136ba",
                    "LayerId": "359fe264-7dbd-4a14-998d-9a63cd763fc3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "359fe264-7dbd-4a14-998d-9a63cd763fc3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f230c565-728f-4306-8279-f998301c4139",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "b35af346-50a8-4b4b-b1d2-03fb625ada7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cars",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 24,
    "bbox_right": 459,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f8f5a4b-b7ec-4339-8d9c-d78dc0b1a45c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b35af346-50a8-4b4b-b1d2-03fb625ada7f",
            "compositeImage": {
                "id": "bd4fe521-d36e-486d-b62c-46260a30f2c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8f5a4b-b7ec-4339-8d9c-d78dc0b1a45c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34c98663-2d35-47a9-a974-c729ae9051ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8f5a4b-b7ec-4339-8d9c-d78dc0b1a45c",
                    "LayerId": "0b1a27d3-3cf3-4bed-9c4b-a475c31fd388"
                }
            ]
        }
    ],
    "gridX": 24,
    "gridY": 24,
    "height": 270,
    "layers": [
        {
            "id": "0b1a27d3-3cf3-4bed-9c4b-a475c31fd388",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b35af346-50a8-4b4b-b1d2-03fb625ada7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "cbae2117-7d61-488d-a0f8-1c7485ab701d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprGlow_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 12,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "30b77c89-db5b-4b9e-a1cf-d9645f8dbc79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbae2117-7d61-488d-a0f8-1c7485ab701d",
            "compositeImage": {
                "id": "76bc53f4-2589-4c87-a130-16a0ca388d26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b77c89-db5b-4b9e-a1cf-d9645f8dbc79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8e60e8-377e-4094-8fe4-b1c28603e3cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b77c89-db5b-4b9e-a1cf-d9645f8dbc79",
                    "LayerId": "74150164-1edd-4efd-b09e-6c19e2b59752"
                }
            ]
        },
        {
            "id": "d2a9cad4-8676-451a-82f0-a4cd6a5d1320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbae2117-7d61-488d-a0f8-1c7485ab701d",
            "compositeImage": {
                "id": "6d551b36-f9d9-4752-a1b2-6907e6aeda55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2a9cad4-8676-451a-82f0-a4cd6a5d1320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f5063f0-fa07-4d59-b78a-0f487b0bdced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2a9cad4-8676-451a-82f0-a4cd6a5d1320",
                    "LayerId": "74150164-1edd-4efd-b09e-6c19e2b59752"
                }
            ]
        },
        {
            "id": "10f44645-6855-4639-963b-82dfb803a7c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbae2117-7d61-488d-a0f8-1c7485ab701d",
            "compositeImage": {
                "id": "a39c6d31-6109-4f1c-86b3-3f61d83967b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10f44645-6855-4639-963b-82dfb803a7c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6fd0e16-aa59-4aad-b41e-9bd83b57812c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10f44645-6855-4639-963b-82dfb803a7c8",
                    "LayerId": "74150164-1edd-4efd-b09e-6c19e2b59752"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "74150164-1edd-4efd-b09e-6c19e2b59752",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbae2117-7d61-488d-a0f8-1c7485ab701d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 32
}
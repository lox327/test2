{
    "id": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "daa4c502-f012-4a5c-8b62-6603e33bff55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "67b9db4d-1a3a-4cb5-92a8-d341132b9540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daa4c502-f012-4a5c-8b62-6603e33bff55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07da6d98-28fe-45de-b1c6-2c522fe59585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daa4c502-f012-4a5c-8b62-6603e33bff55",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "d5a10274-714d-4eaa-b976-226b583b4031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "2097fb73-37b5-4a6d-9514-7c51fa183d18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a10274-714d-4eaa-b976-226b583b4031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9b2f83-9728-40ae-842a-55d93479fed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a10274-714d-4eaa-b976-226b583b4031",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "c5a03581-a9c6-449c-8032-03e1334aab63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "42f64753-7d83-42f4-91c7-908bb3651d2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a03581-a9c6-449c-8032-03e1334aab63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca691496-abb1-47e4-ba58-01fd4ef626f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a03581-a9c6-449c-8032-03e1334aab63",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "270cba7e-f3ec-4831-b9f2-d5197e80b611",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "4f1cf7e0-4953-4c12-b67c-f0bb1e8dd815",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270cba7e-f3ec-4831-b9f2-d5197e80b611",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c36d194-7c66-4142-81c7-62d2b64e3ce5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270cba7e-f3ec-4831-b9f2-d5197e80b611",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "fdb5bcb4-0ef6-4f8e-a767-7eea2c1a28b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "55e89544-72a4-4912-964e-aad7897c1a32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdb5bcb4-0ef6-4f8e-a767-7eea2c1a28b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b68b993-3d07-41ac-9d34-cb4e4d25b60c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdb5bcb4-0ef6-4f8e-a767-7eea2c1a28b3",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "1c53d1b1-b797-4845-b459-c80944664a65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "330dfa0f-c388-4cd9-aae6-d66494193664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c53d1b1-b797-4845-b459-c80944664a65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15f5ba3f-69bf-4600-88d3-2af1edaba35e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c53d1b1-b797-4845-b459-c80944664a65",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "108d98fc-de34-4be6-8d50-7d64a0866f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "44f06605-74d1-479d-a875-b4eac0c347ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "108d98fc-de34-4be6-8d50-7d64a0866f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e2e538f-4a34-4d50-b4e2-533ee033e35e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "108d98fc-de34-4be6-8d50-7d64a0866f35",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "83a92813-206f-4edd-9f81-968832117185",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "69d5fc9f-c51d-400b-9ada-14dd787418cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83a92813-206f-4edd-9f81-968832117185",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dea46e1-cce0-454d-9772-0fe14688258c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83a92813-206f-4edd-9f81-968832117185",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "e4cf442c-ea9e-48ec-9535-25f12023f822",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "9b0bd8e7-9f08-426e-bca1-6f0984a13330",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4cf442c-ea9e-48ec-9535-25f12023f822",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35c4d319-f2a9-42a7-8d10-aba04f71eeb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4cf442c-ea9e-48ec-9535-25f12023f822",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "2b4afbdd-0fb8-4959-af22-55830621cbb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "8c067c55-51ef-41ab-9039-8652188dbded",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b4afbdd-0fb8-4959-af22-55830621cbb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9dfdb68-ed70-4ce0-aa69-4943683f5667",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b4afbdd-0fb8-4959-af22-55830621cbb1",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "ac7aabc8-6e3e-462a-83fe-b737ad8a5823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "d4c2501b-8fed-4c4c-8934-1406cf7e70aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac7aabc8-6e3e-462a-83fe-b737ad8a5823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59f76aa6-d6fb-415d-90e8-48a5d9a406ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac7aabc8-6e3e-462a-83fe-b737ad8a5823",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        },
        {
            "id": "a85df4ce-129d-4b6f-a553-09112bfc1a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "compositeImage": {
                "id": "6d8fed85-74f5-4942-8d53-891d838daca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a85df4ce-129d-4b6f-a553-09112bfc1a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bf50d1e-069b-4da8-a5fd-529706486bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a85df4ce-129d-4b6f-a553-09112bfc1a9e",
                    "LayerId": "716e18cd-d9c5-4b00-aba3-7be073a68396"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "716e18cd-d9c5-4b00-aba3-7be073a68396",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa2f5855-ad48-4894-8ece-9bb2f6311eb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
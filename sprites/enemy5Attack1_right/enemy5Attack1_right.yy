{
    "id": "a07ab9c3-779e-4a06-8644-b02077a07aa5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Attack1_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 9,
    "bbox_right": 59,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e34d84c7-bfd7-4e5c-b53d-519e1d0dadd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a07ab9c3-779e-4a06-8644-b02077a07aa5",
            "compositeImage": {
                "id": "5f1fe776-da3b-4e73-bb2f-789f74c6365d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e34d84c7-bfd7-4e5c-b53d-519e1d0dadd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d7d70a-581a-4dcb-a9ff-58064747e0d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e34d84c7-bfd7-4e5c-b53d-519e1d0dadd8",
                    "LayerId": "774ba1bb-ea39-4916-8d15-526c883808fb"
                }
            ]
        },
        {
            "id": "869188c4-36fb-4432-a467-5f19d796827b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a07ab9c3-779e-4a06-8644-b02077a07aa5",
            "compositeImage": {
                "id": "8f657834-eea2-474c-95e4-ee52f2b768d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "869188c4-36fb-4432-a467-5f19d796827b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ec35227-6676-4056-ad15-4878d25d1fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "869188c4-36fb-4432-a467-5f19d796827b",
                    "LayerId": "774ba1bb-ea39-4916-8d15-526c883808fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "774ba1bb-ea39-4916-8d15-526c883808fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a07ab9c3-779e-4a06-8644-b02077a07aa5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
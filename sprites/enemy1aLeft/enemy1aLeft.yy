{
    "id": "138918e2-3ef9-4109-b85f-83e59c0670da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ec4e9e1-6992-4403-b2d4-32a0386c915c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "138918e2-3ef9-4109-b85f-83e59c0670da",
            "compositeImage": {
                "id": "8bd74312-eca1-4dff-a530-b15937475bd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ec4e9e1-6992-4403-b2d4-32a0386c915c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3120ce16-cf49-469c-ba1c-e60e85b20ec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ec4e9e1-6992-4403-b2d4-32a0386c915c",
                    "LayerId": "420699f8-d677-452b-9ffd-a7c238e7cf02"
                }
            ]
        },
        {
            "id": "04f7ec98-2113-42f5-8732-a48c26559a68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "138918e2-3ef9-4109-b85f-83e59c0670da",
            "compositeImage": {
                "id": "ba1e6323-f8fc-429a-bbf2-cc855f55ff82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04f7ec98-2113-42f5-8732-a48c26559a68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95ff8afd-661e-4005-913a-5104b64fad64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04f7ec98-2113-42f5-8732-a48c26559a68",
                    "LayerId": "420699f8-d677-452b-9ffd-a7c238e7cf02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "420699f8-d677-452b-9ffd-a7c238e7cf02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "138918e2-3ef9-4109-b85f-83e59c0670da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
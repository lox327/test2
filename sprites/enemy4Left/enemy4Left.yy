{
    "id": "b0d70454-5be2-41ea-994b-88b195677734",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy4Left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 17,
    "bbox_right": 54,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ef97bcfa-1b29-41ba-b496-78dd5c7fd43f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "compositeImage": {
                "id": "39eebf7e-fd88-4507-b82d-b83095bf9236",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef97bcfa-1b29-41ba-b496-78dd5c7fd43f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99ae988a-9934-471b-b4f6-1d8ddf5b770e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef97bcfa-1b29-41ba-b496-78dd5c7fd43f",
                    "LayerId": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5"
                }
            ]
        },
        {
            "id": "3d1e84dd-0cec-411a-ab36-c486b16c521b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "compositeImage": {
                "id": "78ca5fd5-96c4-4ab5-b188-b64dca0df53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d1e84dd-0cec-411a-ab36-c486b16c521b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17d5c71-f3d0-4bae-93c2-7bee1154106b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d1e84dd-0cec-411a-ab36-c486b16c521b",
                    "LayerId": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5"
                }
            ]
        },
        {
            "id": "ccc1b2c5-4170-44e5-b38b-02fb5efccb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "compositeImage": {
                "id": "7e8430d2-9187-4246-b792-d51f05248651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc1b2c5-4170-44e5-b38b-02fb5efccb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beff1628-4499-4905-ac71-5cfa350cb2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc1b2c5-4170-44e5-b38b-02fb5efccb2c",
                    "LayerId": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5"
                }
            ]
        },
        {
            "id": "9093dac4-8d09-4a59-94f3-6fb8b92c4df5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "compositeImage": {
                "id": "973c6ffd-35a1-4035-97f6-ca0734b40c42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9093dac4-8d09-4a59-94f3-6fb8b92c4df5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aab3efd7-1188-4694-97a1-7668c70cf74f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9093dac4-8d09-4a59-94f3-6fb8b92c4df5",
                    "LayerId": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5"
                }
            ]
        },
        {
            "id": "219535e2-3c30-4714-88b4-aa5150b91ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "compositeImage": {
                "id": "3a0a240b-f6e2-40e9-beea-8da9443c01b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "219535e2-3c30-4714-88b4-aa5150b91ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f2e87a3-3d8e-4988-ab5e-329954c0c051",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "219535e2-3c30-4714-88b4-aa5150b91ee7",
                    "LayerId": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5"
                }
            ]
        },
        {
            "id": "015f32c4-9827-4b06-b391-34b764d0f4b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "compositeImage": {
                "id": "365f73cc-d69a-478a-99d0-cc8bc14b78a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "015f32c4-9827-4b06-b391-34b764d0f4b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e41d6218-df13-43d3-8656-2a0988ee54b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "015f32c4-9827-4b06-b391-34b764d0f4b9",
                    "LayerId": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5"
                }
            ]
        },
        {
            "id": "53703f4a-c932-49a8-aa62-ed449a477645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "compositeImage": {
                "id": "40081f36-8c79-408b-93b0-fe574fbcd07b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53703f4a-c932-49a8-aa62-ed449a477645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d47b6a7-5a24-4707-be3e-689b4929007f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53703f4a-c932-49a8-aa62-ed449a477645",
                    "LayerId": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5"
                }
            ]
        },
        {
            "id": "62e0ee00-407a-4e06-99ee-719caa46996c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "compositeImage": {
                "id": "b5fe76f3-1ba2-41e0-8cea-1dbca60e8a79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e0ee00-407a-4e06-99ee-719caa46996c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8f1009b-e46b-4978-a472-0fd820897f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e0ee00-407a-4e06-99ee-719caa46996c",
                    "LayerId": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "0395ccf8-3c07-48d0-8e98-b30a7d6c2aa5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0d70454-5be2-41ea-994b-88b195677734",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "e9cf724e-c742-4f3e-9a29-a4bc138d551d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tank2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33a86aa6-d248-4e88-b700-944db4d658cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9cf724e-c742-4f3e-9a29-a4bc138d551d",
            "compositeImage": {
                "id": "a76d645e-b76c-4a78-b9ba-a0463cefe838",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33a86aa6-d248-4e88-b700-944db4d658cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "630a198c-9f3c-4ac8-8ee4-73a669465236",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33a86aa6-d248-4e88-b700-944db4d658cf",
                    "LayerId": "5bccef91-5054-4370-9c2e-5b29b15e97f7"
                }
            ]
        },
        {
            "id": "a31760f6-89e2-4c23-85bc-816cb8d1e02b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9cf724e-c742-4f3e-9a29-a4bc138d551d",
            "compositeImage": {
                "id": "ca452c4c-fd43-416c-ad86-10696370a444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a31760f6-89e2-4c23-85bc-816cb8d1e02b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00433623-6c7f-4071-8eaf-1ca2768b7f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a31760f6-89e2-4c23-85bc-816cb8d1e02b",
                    "LayerId": "5bccef91-5054-4370-9c2e-5b29b15e97f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "5bccef91-5054-4370-9c2e-5b29b15e97f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9cf724e-c742-4f3e-9a29-a4bc138d551d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 14
}
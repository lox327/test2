{
    "id": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 17,
    "bbox_right": 54,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3a6f2da-1278-46df-b0c3-f7351355449b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "compositeImage": {
                "id": "8dd70546-68a3-4f9a-9424-e446c77bddb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a6f2da-1278-46df-b0c3-f7351355449b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b5680d-8021-45fa-ad77-5e12938a82ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a6f2da-1278-46df-b0c3-f7351355449b",
                    "LayerId": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605"
                }
            ]
        },
        {
            "id": "3623d9d2-cafe-4446-8613-83120432678b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "compositeImage": {
                "id": "e45201de-bacd-48c5-b820-6233d666c554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3623d9d2-cafe-4446-8613-83120432678b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f15b95-cb54-41d9-9caa-13f0ff573114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3623d9d2-cafe-4446-8613-83120432678b",
                    "LayerId": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605"
                }
            ]
        },
        {
            "id": "837445de-bde0-41f1-b4e6-26466056f946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "compositeImage": {
                "id": "03cbb803-bc39-4398-98d2-861a97c4bea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "837445de-bde0-41f1-b4e6-26466056f946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8e89f26-4cdb-4577-bdfc-b0e284982e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "837445de-bde0-41f1-b4e6-26466056f946",
                    "LayerId": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605"
                }
            ]
        },
        {
            "id": "db92d661-e5c4-48f2-b735-307348ce467e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "compositeImage": {
                "id": "13a2e464-f72f-469e-8e68-ecfa9fc26376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db92d661-e5c4-48f2-b735-307348ce467e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f01964ac-bde2-4f6b-a951-373530b7d60e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db92d661-e5c4-48f2-b735-307348ce467e",
                    "LayerId": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605"
                }
            ]
        },
        {
            "id": "57b92f54-9c2a-4e04-a64f-49a4e3eb70b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "compositeImage": {
                "id": "d7ac5bde-52b9-4b3b-a821-93940d8ec5f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57b92f54-9c2a-4e04-a64f-49a4e3eb70b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac4f23d-b472-425c-8de3-e76691c58bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57b92f54-9c2a-4e04-a64f-49a4e3eb70b4",
                    "LayerId": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605"
                }
            ]
        },
        {
            "id": "87abb405-d4e6-451d-b292-2458d6db2979",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "compositeImage": {
                "id": "d1b5387a-dc02-49d9-a4ab-5a226ff3869d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87abb405-d4e6-451d-b292-2458d6db2979",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8acef7-bb78-4673-9c28-05c8952e8e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87abb405-d4e6-451d-b292-2458d6db2979",
                    "LayerId": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605"
                }
            ]
        },
        {
            "id": "e66b65fc-851a-4ab3-ab38-fd847c2415e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "compositeImage": {
                "id": "324fa03a-f9fe-4d1e-9989-57ca480c0a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e66b65fc-851a-4ab3-ab38-fd847c2415e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cf6e2ac-85c5-47ba-9b21-4a26bc8adae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e66b65fc-851a-4ab3-ab38-fd847c2415e8",
                    "LayerId": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605"
                }
            ]
        },
        {
            "id": "f056f8d8-f0c1-45ec-9a8c-30fc7819b469",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "compositeImage": {
                "id": "6e0a8227-6d26-4964-a04f-9fec680cbcc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f056f8d8-f0c1-45ec-9a8c-30fc7819b469",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1375f2-7e1a-44d1-8db5-3d9f03ebe030",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f056f8d8-f0c1-45ec-9a8c-30fc7819b469",
                    "LayerId": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "f8c49ecd-bb42-41d9-9d4a-4fe7882cc605",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "005d0f1a-c48e-4baf-82cf-3d664fb00574",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
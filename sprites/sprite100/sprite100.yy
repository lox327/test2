{
    "id": "15303c49-a930-4006-a9f7-62840c628c8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite100",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 269,
    "bbox_left": 0,
    "bbox_right": 473,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03b9afb1-5691-4109-8c67-2e553099c5ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15303c49-a930-4006-a9f7-62840c628c8b",
            "compositeImage": {
                "id": "c9a717e0-abd1-4bd4-ac3b-d0952bd76651",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b9afb1-5691-4109-8c67-2e553099c5ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db55202-ab70-4a03-8362-4a0f92e762b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b9afb1-5691-4109-8c67-2e553099c5ed",
                    "LayerId": "dcaf40e9-3d2a-4842-8a6c-954f38ade3c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "dcaf40e9-3d2a-4842-8a6c-954f38ade3c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15303c49-a930-4006-a9f7-62840c628c8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
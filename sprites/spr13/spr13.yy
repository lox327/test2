{
    "id": "2085417d-8423-4a30-a486-d5560e9721da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr13",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ab8e25d-27da-4759-8186-c4fd60358476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2085417d-8423-4a30-a486-d5560e9721da",
            "compositeImage": {
                "id": "145fa217-d617-49a7-a277-82bf3cb72f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab8e25d-27da-4759-8186-c4fd60358476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87eca04a-4e0e-47d1-bacc-c59fc86fe552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab8e25d-27da-4759-8186-c4fd60358476",
                    "LayerId": "59c2e8e0-2d4a-4f4b-97aa-7a26d5b304f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "59c2e8e0-2d4a-4f4b-97aa-7a26d5b304f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2085417d-8423-4a30-a486-d5560e9721da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "a0408bc6-f74e-4b80-b4fc-4f3a4690fb5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bullet2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cda1ea27-dc92-495b-bb23-8cb4ef4949a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0408bc6-f74e-4b80-b4fc-4f3a4690fb5b",
            "compositeImage": {
                "id": "4e2c29cd-5c1f-4c97-ab79-d2c81e87f8af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda1ea27-dc92-495b-bb23-8cb4ef4949a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4523891-a3ee-478f-a991-cb8c5d8a8a74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda1ea27-dc92-495b-bb23-8cb4ef4949a0",
                    "LayerId": "11db9e00-76c0-4694-b6d8-3d496c328c2e"
                }
            ]
        },
        {
            "id": "da3c9437-6168-41dd-82af-1822d92c40da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0408bc6-f74e-4b80-b4fc-4f3a4690fb5b",
            "compositeImage": {
                "id": "12a445cf-7876-495c-9f97-a3458c14873d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da3c9437-6168-41dd-82af-1822d92c40da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7396e5b-6fbf-4d43-994f-0fedb7feaaaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da3c9437-6168-41dd-82af-1822d92c40da",
                    "LayerId": "11db9e00-76c0-4694-b6d8-3d496c328c2e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "11db9e00-76c0-4694-b6d8-3d496c328c2e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0408bc6-f74e-4b80-b4fc-4f3a4690fb5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}
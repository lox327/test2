{
    "id": "e22d555d-7ce8-4cf5-a32f-bc93fe493a6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "attackDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05e060c7-0ec0-4e07-bb32-924ee6c92842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22d555d-7ce8-4cf5-a32f-bc93fe493a6d",
            "compositeImage": {
                "id": "4fc127fe-4a18-408a-85de-c1a2f0b741d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05e060c7-0ec0-4e07-bb32-924ee6c92842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3555330a-5f1c-48a5-a3cb-e4376c1a2baa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05e060c7-0ec0-4e07-bb32-924ee6c92842",
                    "LayerId": "a2e59b18-0b66-4ad6-b9a9-b4e396116e46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "a2e59b18-0b66-4ad6-b9a9-b4e396116e46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e22d555d-7ce8-4cf5-a32f-bc93fe493a6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 21
}
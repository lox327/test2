{
    "id": "8d6126cb-662e-4d49-ac96-53e2ed8d0788",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightnew",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0714e4b-8a79-4772-a200-57d75a77b277",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d6126cb-662e-4d49-ac96-53e2ed8d0788",
            "compositeImage": {
                "id": "23138284-b1f7-4282-a3e3-13060e1df346",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0714e4b-8a79-4772-a200-57d75a77b277",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bf3f874-cdda-4501-b799-890178623afd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0714e4b-8a79-4772-a200-57d75a77b277",
                    "LayerId": "b68ee940-dde1-435c-9a90-51e7f9a7e393"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b68ee940-dde1-435c-9a90-51e7f9a7e393",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d6126cb-662e-4d49-ac96-53e2ed8d0788",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}
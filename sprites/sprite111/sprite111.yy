{
    "id": "5c7fc475-1154-4575-9556-31103e40ae2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite111",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed255022-60e2-4e42-8852-e4bf5b1b5b12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c7fc475-1154-4575-9556-31103e40ae2f",
            "compositeImage": {
                "id": "9d27f409-b746-4d74-9d40-f608551cd25f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed255022-60e2-4e42-8852-e4bf5b1b5b12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e833acf-0c60-436e-8dad-1b52713f9bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed255022-60e2-4e42-8852-e4bf5b1b5b12",
                    "LayerId": "d90e5f3f-1534-45e3-a577-d9373080f545"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d90e5f3f-1534-45e3-a577-d9373080f545",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c7fc475-1154-4575-9556-31103e40ae2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
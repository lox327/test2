{
    "id": "47a85932-477c-4240-a5c2-18dfe921034d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bgDesc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c52dbbe9-a059-4e57-966c-86ab02647ed7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47a85932-477c-4240-a5c2-18dfe921034d",
            "compositeImage": {
                "id": "eaa3d89e-827c-4579-a449-0e38589d4168",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c52dbbe9-a059-4e57-966c-86ab02647ed7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba3e541-49e9-4cbe-81d0-af3550290afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c52dbbe9-a059-4e57-966c-86ab02647ed7",
                    "LayerId": "4d9db023-1d08-4c94-8126-e5e567b9d7fa"
                },
                {
                    "id": "bbbc08d7-b83f-4e6f-aa7a-4376ecea9412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c52dbbe9-a059-4e57-966c-86ab02647ed7",
                    "LayerId": "9454f55f-b82e-4093-9756-efcff5da89ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9454f55f-b82e-4093-9756-efcff5da89ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47a85932-477c-4240-a5c2-18dfe921034d",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4d9db023-1d08-4c94-8126-e5e567b9d7fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47a85932-477c-4240-a5c2-18dfe921034d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 65,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
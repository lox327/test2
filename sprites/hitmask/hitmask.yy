{
    "id": "a2eacce3-bd44-4633-a287-2517270aa7f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hitmask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2fb8992-4ff3-454a-845e-d3d81f620807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2eacce3-bd44-4633-a287-2517270aa7f2",
            "compositeImage": {
                "id": "5a0ac605-49a3-4af1-b666-9716c4b1d9dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2fb8992-4ff3-454a-845e-d3d81f620807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f374a261-a71b-40dc-845d-bf0107c2f764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2fb8992-4ff3-454a-845e-d3d81f620807",
                    "LayerId": "155dff7d-9dac-4a2c-b0d9-9199d6e99281"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "155dff7d-9dac-4a2c-b0d9-9199d6e99281",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2eacce3-bd44-4633-a287-2517270aa7f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 21
}
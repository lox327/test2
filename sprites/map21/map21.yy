{
    "id": "ba702e8b-63db-4222-97de-d7db1bf51251",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "map21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 536,
    "bbox_left": 0,
    "bbox_right": 955,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f3c6434-a161-4368-b850-f118f3bb72cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba702e8b-63db-4222-97de-d7db1bf51251",
            "compositeImage": {
                "id": "999e57a5-c676-448b-9c34-6e46ab0506b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f3c6434-a161-4368-b850-f118f3bb72cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7889f6b9-3e1e-49b8-ad17-3276181a068c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f3c6434-a161-4368-b850-f118f3bb72cc",
                    "LayerId": "c40013a2-d357-4d20-bd57-6cfd733d52d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 537,
    "layers": [
        {
            "id": "c40013a2-d357-4d20-bd57-6cfd733d52d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba702e8b-63db-4222-97de-d7db1bf51251",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 956,
    "xorig": 0,
    "yorig": 0
}
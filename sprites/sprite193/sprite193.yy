{
    "id": "97b387e5-7c11-4a5c-9d6a-fc88fb0302ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite193",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "407235d6-1a7b-4995-bdae-0502847498b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97b387e5-7c11-4a5c-9d6a-fc88fb0302ed",
            "compositeImage": {
                "id": "c2350420-cb2f-43e3-81ec-3006dbde6d0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "407235d6-1a7b-4995-bdae-0502847498b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15368bc-579c-4ee1-b5a5-b1958b4611a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "407235d6-1a7b-4995-bdae-0502847498b3",
                    "LayerId": "8410952c-6e17-40b3-a9e9-848937aa4c02"
                }
            ]
        },
        {
            "id": "d1846c09-7093-4bca-b6a8-84b070e54f4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97b387e5-7c11-4a5c-9d6a-fc88fb0302ed",
            "compositeImage": {
                "id": "589a48da-5a36-4fc9-924c-d42b9f916977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1846c09-7093-4bca-b6a8-84b070e54f4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50484d2d-1ec5-4aae-ae9e-9b9649b06561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1846c09-7093-4bca-b6a8-84b070e54f4f",
                    "LayerId": "8410952c-6e17-40b3-a9e9-848937aa4c02"
                }
            ]
        },
        {
            "id": "cf193a0c-7d9d-4fdc-bfb4-7d26a3a83fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97b387e5-7c11-4a5c-9d6a-fc88fb0302ed",
            "compositeImage": {
                "id": "bbaa1941-c392-42fb-8bf2-2e383622bbbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf193a0c-7d9d-4fdc-bfb4-7d26a3a83fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e69aaac-55f2-41b7-b308-773d8f9ecfb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf193a0c-7d9d-4fdc-bfb4-7d26a3a83fc7",
                    "LayerId": "8410952c-6e17-40b3-a9e9-848937aa4c02"
                }
            ]
        },
        {
            "id": "dc4b9e20-278e-4c27-99ff-183d1b4a4157",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97b387e5-7c11-4a5c-9d6a-fc88fb0302ed",
            "compositeImage": {
                "id": "110a7979-1d5f-48b5-8099-390b8acdfcb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4b9e20-278e-4c27-99ff-183d1b4a4157",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfab92a3-4f44-49c8-984d-e783a01b2309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4b9e20-278e-4c27-99ff-183d1b4a4157",
                    "LayerId": "8410952c-6e17-40b3-a9e9-848937aa4c02"
                }
            ]
        },
        {
            "id": "195c3b9a-24bd-423e-8a8a-2ea393bbb89e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97b387e5-7c11-4a5c-9d6a-fc88fb0302ed",
            "compositeImage": {
                "id": "d64457a1-c6ad-4b30-9c56-1debe8b7c219",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "195c3b9a-24bd-423e-8a8a-2ea393bbb89e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "550b1595-914c-4a5e-bbb6-030607fc0414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195c3b9a-24bd-423e-8a8a-2ea393bbb89e",
                    "LayerId": "8410952c-6e17-40b3-a9e9-848937aa4c02"
                }
            ]
        },
        {
            "id": "2c3a228a-bd0b-4d5b-8048-e24a2dc0acce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97b387e5-7c11-4a5c-9d6a-fc88fb0302ed",
            "compositeImage": {
                "id": "e02e7179-2630-4010-abcd-378535a699e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c3a228a-bd0b-4d5b-8048-e24a2dc0acce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4bf3c4a-68dc-43e8-9ca5-088a2948123f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c3a228a-bd0b-4d5b-8048-e24a2dc0acce",
                    "LayerId": "8410952c-6e17-40b3-a9e9-848937aa4c02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 93,
    "layers": [
        {
            "id": "8410952c-6e17-40b3-a9e9-848937aa4c02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97b387e5-7c11-4a5c-9d6a-fc88fb0302ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 44,
    "xorig": 0,
    "yorig": 0
}
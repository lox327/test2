{
    "id": "c4f3278f-c6ee-45a1-9f9f-e36ee0abfbb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inventory_items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 0,
    "bbox_right": 510,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8abac79-c3c0-41f3-98c0-8c49a1b85f48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4f3278f-c6ee-45a1-9f9f-e36ee0abfbb0",
            "compositeImage": {
                "id": "d76e7292-0bb3-4051-b1b3-7c2220b704a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8abac79-c3c0-41f3-98c0-8c49a1b85f48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb7f2d14-4940-403c-9757-3d84c77355ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8abac79-c3c0-41f3-98c0-8c49a1b85f48",
                    "LayerId": "15873295-d0f3-4120-a551-db85ba09b088"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 256,
    "layers": [
        {
            "id": "15873295-d0f3-4120-a551-db85ba09b088",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4f3278f-c6ee-45a1-9f9f-e36ee0abfbb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}
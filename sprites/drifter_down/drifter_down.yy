{
    "id": "1489fb99-fba3-4508-a737-b002fb50ceb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "drifter_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 22,
    "bbox_right": 37,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "79179a72-a21d-405e-bbf6-25016697caaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "compositeImage": {
                "id": "f5526da4-7beb-4634-af69-79adcf49a8a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79179a72-a21d-405e-bbf6-25016697caaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d33a2a5-563b-4012-91cf-0bd838c526bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79179a72-a21d-405e-bbf6-25016697caaf",
                    "LayerId": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4"
                }
            ]
        },
        {
            "id": "28ee67ab-e578-4deb-b19a-f49573a5a4fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "compositeImage": {
                "id": "e7329768-070d-4de5-8c8b-522ed22c91f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ee67ab-e578-4deb-b19a-f49573a5a4fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a629c1e1-9e00-4866-8af4-784bc0510157",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ee67ab-e578-4deb-b19a-f49573a5a4fe",
                    "LayerId": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4"
                }
            ]
        },
        {
            "id": "80a52459-cf48-40e6-a306-8642a0ae6379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "compositeImage": {
                "id": "e8f15140-6cc9-468f-b586-38ed1e48513d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80a52459-cf48-40e6-a306-8642a0ae6379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d48cb2e-a1fc-496c-a070-19be96f9cc38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80a52459-cf48-40e6-a306-8642a0ae6379",
                    "LayerId": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4"
                }
            ]
        },
        {
            "id": "cb32fc4e-3b6d-4001-9b67-5a5e29f589a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "compositeImage": {
                "id": "440a15fc-646e-46e4-bfb9-e0a7f896a491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb32fc4e-3b6d-4001-9b67-5a5e29f589a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0039c1d4-b330-4670-8714-2ab73d194de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb32fc4e-3b6d-4001-9b67-5a5e29f589a8",
                    "LayerId": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4"
                }
            ]
        },
        {
            "id": "7cc9117c-aac2-4649-b078-88dc8add2153",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "compositeImage": {
                "id": "417c596d-e16f-4dc3-b78d-8079a819cc2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cc9117c-aac2-4649-b078-88dc8add2153",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8b76e67-a045-4640-9cb5-5aac24291d6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cc9117c-aac2-4649-b078-88dc8add2153",
                    "LayerId": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4"
                }
            ]
        },
        {
            "id": "20391949-ae75-4b34-848f-e1896dbc99a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "compositeImage": {
                "id": "33a77e4c-f04d-44b1-81e4-4946e1e35f9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20391949-ae75-4b34-848f-e1896dbc99a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00eeca1a-1917-4ea2-8c28-470af68074f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20391949-ae75-4b34-848f-e1896dbc99a0",
                    "LayerId": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4"
                }
            ]
        },
        {
            "id": "fab7e4fc-4e18-419d-8f4d-f4722749ee4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "compositeImage": {
                "id": "11186a91-0335-4371-aaea-7777e80d51b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab7e4fc-4e18-419d-8f4d-f4722749ee4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5faa177-8b9b-4b44-8e54-6ee4a885cce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab7e4fc-4e18-419d-8f4d-f4722749ee4f",
                    "LayerId": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4"
                }
            ]
        },
        {
            "id": "8bf603e7-9b93-403c-a1ed-b1a690850741",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "compositeImage": {
                "id": "3aad8c56-13ff-436e-9bbb-47303e8b04fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bf603e7-9b93-403c-a1ed-b1a690850741",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab514a91-f68f-4dbc-a0cb-12c63f187d80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bf603e7-9b93-403c-a1ed-b1a690850741",
                    "LayerId": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "9ed4b9df-256b-469f-b8d1-8134bdf48bb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1489fb99-fba3-4508-a737-b002fb50ceb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
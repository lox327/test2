{
    "id": "36249efa-75e0-46a5-a0d6-0a9f27c207ef",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 251,
    "bbox_left": 1,
    "bbox_right": 248,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8074e443-a08a-4d42-9ccc-1ee61da2f191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36249efa-75e0-46a5-a0d6-0a9f27c207ef",
            "compositeImage": {
                "id": "bf93db28-0e8a-4693-b4fd-a1f0866a0868",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8074e443-a08a-4d42-9ccc-1ee61da2f191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "423a8ddc-af32-446d-ba27-a2d9aa67ced2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8074e443-a08a-4d42-9ccc-1ee61da2f191",
                    "LayerId": "6e8ceb91-2208-4943-afe1-9d6ae356659c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "6e8ceb91-2208-4943-afe1-9d6ae356659c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36249efa-75e0-46a5-a0d6-0a9f27c207ef",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 60,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}
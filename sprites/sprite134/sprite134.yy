{
    "id": "ed0f10f3-c7bf-4c66-bc29-b79d08f227b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite134",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 283,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2413af34-a06c-4a22-8ae0-e08e8be76b85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed0f10f3-c7bf-4c66-bc29-b79d08f227b0",
            "compositeImage": {
                "id": "9962c0cf-7930-4182-ae21-08f8b2e8b6c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2413af34-a06c-4a22-8ae0-e08e8be76b85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bee2100f-7dbb-4202-9684-e60de112ca67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2413af34-a06c-4a22-8ae0-e08e8be76b85",
                    "LayerId": "86141f79-98fd-498d-8ad5-717ed4209681"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "86141f79-98fd-498d-8ad5-717ed4209681",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed0f10f3-c7bf-4c66-bc29-b79d08f227b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 286,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "c82dc4f5-456f-4d02-ba3f-277844ec38f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bc2bef7-2927-4a4c-95eb-c16df4cd1f35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82dc4f5-456f-4d02-ba3f-277844ec38f8",
            "compositeImage": {
                "id": "4bb92ba7-47e5-42c8-a47e-4b741f68c82a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bc2bef7-2927-4a4c-95eb-c16df4cd1f35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f199a29-872f-4912-897f-8ef6e2231957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc2bef7-2927-4a4c-95eb-c16df4cd1f35",
                    "LayerId": "37333037-a087-4e15-a042-d11387c00779"
                }
            ]
        },
        {
            "id": "63e019cd-4862-4ef6-a30d-a439bf9a1092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82dc4f5-456f-4d02-ba3f-277844ec38f8",
            "compositeImage": {
                "id": "4e8ebfea-91d2-42dd-bdf0-1b2740c6b42d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63e019cd-4862-4ef6-a30d-a439bf9a1092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14d64bfc-05d8-4c4b-b5c9-a49dda29f04e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63e019cd-4862-4ef6-a30d-a439bf9a1092",
                    "LayerId": "37333037-a087-4e15-a042-d11387c00779"
                }
            ]
        },
        {
            "id": "4973e198-1985-4174-885e-2a9413c2e166",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82dc4f5-456f-4d02-ba3f-277844ec38f8",
            "compositeImage": {
                "id": "bf29b0f5-0b15-4327-a173-7dac51e84ea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4973e198-1985-4174-885e-2a9413c2e166",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "846d123a-94d8-4faa-bb69-c85d237e2b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4973e198-1985-4174-885e-2a9413c2e166",
                    "LayerId": "37333037-a087-4e15-a042-d11387c00779"
                }
            ]
        },
        {
            "id": "43548cfa-122c-4d02-aebe-f14c99e13c39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82dc4f5-456f-4d02-ba3f-277844ec38f8",
            "compositeImage": {
                "id": "16873040-7787-4084-83f7-13ad7cc1deb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43548cfa-122c-4d02-aebe-f14c99e13c39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "690355b8-7604-4d91-b2cd-c65e765d5043",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43548cfa-122c-4d02-aebe-f14c99e13c39",
                    "LayerId": "37333037-a087-4e15-a042-d11387c00779"
                }
            ]
        },
        {
            "id": "ec5f90c7-3c7c-479e-a6cb-24cd159686c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82dc4f5-456f-4d02-ba3f-277844ec38f8",
            "compositeImage": {
                "id": "2472a717-429d-4abb-9c33-40b6389bcc9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec5f90c7-3c7c-479e-a6cb-24cd159686c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d323dc44-33da-4b29-9aa0-7d42e27404c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec5f90c7-3c7c-479e-a6cb-24cd159686c5",
                    "LayerId": "37333037-a087-4e15-a042-d11387c00779"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "37333037-a087-4e15-a042-d11387c00779",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c82dc4f5-456f-4d02-ba3f-277844ec38f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
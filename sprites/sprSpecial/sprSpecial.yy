{
    "id": "c12bef92-45a3-47f1-9c1a-fa3ab008e45b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSpecial",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e68475b2-0356-447c-80f8-8ded58a825b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c12bef92-45a3-47f1-9c1a-fa3ab008e45b",
            "compositeImage": {
                "id": "e7199779-a533-40b8-8377-af252d394f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e68475b2-0356-447c-80f8-8ded58a825b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39aa71ef-e550-4e3d-a4e6-57de1a77d2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e68475b2-0356-447c-80f8-8ded58a825b2",
                    "LayerId": "869c0e48-aa22-478b-b292-3e2a9b0c4ad5"
                }
            ]
        },
        {
            "id": "6d3f95e0-e1bf-41c5-a8ef-d788dbe1191e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c12bef92-45a3-47f1-9c1a-fa3ab008e45b",
            "compositeImage": {
                "id": "621642f3-2e64-4628-a3cd-9235b3871d20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d3f95e0-e1bf-41c5-a8ef-d788dbe1191e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "983ced5c-ad10-426a-b609-ff66b40c04bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d3f95e0-e1bf-41c5-a8ef-d788dbe1191e",
                    "LayerId": "869c0e48-aa22-478b-b292-3e2a9b0c4ad5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "869c0e48-aa22-478b-b292-3e2a9b0c4ad5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c12bef92-45a3-47f1-9c1a-fa3ab008e45b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}
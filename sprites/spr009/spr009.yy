{
    "id": "9290a2ab-1bd4-4742-ad5f-d9b8de2e55ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr009",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 0,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e993c16d-8a24-4b9e-a2a5-3149c44bc82d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9290a2ab-1bd4-4742-ad5f-d9b8de2e55ec",
            "compositeImage": {
                "id": "fd31f5b7-19a4-4ab1-8fcd-131e3516d22c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e993c16d-8a24-4b9e-a2a5-3149c44bc82d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d9fdc9f-8590-4a24-ae4f-ffcf039f476c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e993c16d-8a24-4b9e-a2a5-3149c44bc82d",
                    "LayerId": "6cb9db74-e664-4ad9-ab5f-34a0f5d6a464"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 38,
    "layers": [
        {
            "id": "6cb9db74-e664-4ad9-ab5f-34a0f5d6a464",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9290a2ab-1bd4-4742-ad5f-d9b8de2e55ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 11,
    "xorig": 0,
    "yorig": 0
}
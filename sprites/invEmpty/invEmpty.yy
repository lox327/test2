{
    "id": "c270b2cf-a70c-494d-92bc-50d49e864861",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "invEmpty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "efb45635-209f-4d1f-a088-11acf6a8ddac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c270b2cf-a70c-494d-92bc-50d49e864861",
            "compositeImage": {
                "id": "1b10d06d-5b40-48f5-8772-af88ae379771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efb45635-209f-4d1f-a088-11acf6a8ddac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d42dcb4-84b7-4716-a27b-5edff808d3a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efb45635-209f-4d1f-a088-11acf6a8ddac",
                    "LayerId": "00331894-3f77-461e-8eb7-3fcca6c3fbcc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "00331894-3f77-461e-8eb7-3fcca6c3fbcc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c270b2cf-a70c-494d-92bc-50d49e864861",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
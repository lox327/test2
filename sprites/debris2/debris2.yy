{
    "id": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "debris2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c69f9874-6aec-4c08-a51c-718453ea6b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "fd38e55c-5868-41c1-a556-b204b80cbc25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c69f9874-6aec-4c08-a51c-718453ea6b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c5234da-4331-41f1-b2ea-4b4764c2ea8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c69f9874-6aec-4c08-a51c-718453ea6b3e",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "945e15b5-1275-4a1d-b079-7fc36b973320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "94801180-22bf-43b3-b60e-0ec0d7e24e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "945e15b5-1275-4a1d-b079-7fc36b973320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67236b4c-62c8-41d7-aa49-ac4949744b72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "945e15b5-1275-4a1d-b079-7fc36b973320",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "f0ad0373-1640-43cc-8977-89f64c4b3c0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "419e995a-3475-47f7-b540-47873be818c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0ad0373-1640-43cc-8977-89f64c4b3c0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40ac6523-37e0-47a5-a9bc-2e9b50b2a216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0ad0373-1640-43cc-8977-89f64c4b3c0b",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "58fa3a84-e824-45dc-80ee-359018ff6554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "36887721-abb2-4457-8f79-60abcc8a58e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58fa3a84-e824-45dc-80ee-359018ff6554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b2710c4-693e-4a5c-a0c9-5ff8d1d47fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58fa3a84-e824-45dc-80ee-359018ff6554",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "8a65134d-7422-4f7f-a047-f7a51d1fa9a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "868e8571-be62-4dd9-a9e5-ec1c15e5ef18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a65134d-7422-4f7f-a047-f7a51d1fa9a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a189306-236f-4611-b5b9-0e7b226c9059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a65134d-7422-4f7f-a047-f7a51d1fa9a0",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "05d35e6f-04fa-436c-bbff-d83e2519248b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "3a8ab30c-d0e2-4097-9584-88cbb3a035a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d35e6f-04fa-436c-bbff-d83e2519248b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d412a815-90b2-4465-9778-f61405980d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d35e6f-04fa-436c-bbff-d83e2519248b",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "1b4ea1c1-ec66-4756-9b75-c3942d5d8578",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "2c82b8d7-95cc-4c73-abd2-50806acc3ee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b4ea1c1-ec66-4756-9b75-c3942d5d8578",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e207121c-79a0-44a3-b9c8-f9d022e9b4c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b4ea1c1-ec66-4756-9b75-c3942d5d8578",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "cb399e7b-4c70-4cf5-8fa9-b10ef30f2e88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "9d8485d2-ee88-4909-94ca-59b772c622b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb399e7b-4c70-4cf5-8fa9-b10ef30f2e88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b99d0726-10e9-4a95-9693-8cebee64f7e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb399e7b-4c70-4cf5-8fa9-b10ef30f2e88",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "c2fefc69-d4fb-42ea-b247-481e345c82b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "a54eba9f-ca02-46aa-a0d9-3bfce4b13cc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2fefc69-d4fb-42ea-b247-481e345c82b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fb337de-9dd1-4a0a-99c2-12ffe42de850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2fefc69-d4fb-42ea-b247-481e345c82b9",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "1e1f79f8-2caa-4560-ab8a-7104621f41e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "2f634b2e-0754-4027-b5ab-77a439da5fab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e1f79f8-2caa-4560-ab8a-7104621f41e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0b6033f-ecae-4134-83e2-f90816e860e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e1f79f8-2caa-4560-ab8a-7104621f41e6",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        },
        {
            "id": "ec842144-0349-44e3-aca1-c1ac6817b21b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "compositeImage": {
                "id": "3c2d7916-31f2-4580-be7e-cd2a0f012547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec842144-0349-44e3-aca1-c1ac6817b21b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ce71fad-75d1-4717-aa23-1fa40f59a843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec842144-0349-44e3-aca1-c1ac6817b21b",
                    "LayerId": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fa2888c8-2bc3-49c6-bbe9-047c69034ce3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be5a06c2-aca8-46d2-9435-7ae35bb849d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 67,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
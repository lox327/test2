{
    "id": "54ff6868-0a9f-44a6-9373-0b3fcf412c9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "footmask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7b8fa21-d493-4a95-b5bf-ae4913aec740",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54ff6868-0a9f-44a6-9373-0b3fcf412c9b",
            "compositeImage": {
                "id": "c868f45a-2328-40ee-bcba-5089c24b57b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7b8fa21-d493-4a95-b5bf-ae4913aec740",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "498f1ebd-4f86-40d3-a29a-bf1ceb7e3f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7b8fa21-d493-4a95-b5bf-ae4913aec740",
                    "LayerId": "88155e0c-a790-4e74-a288-a6547a13af5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "88155e0c-a790-4e74-a288-a6547a13af5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54ff6868-0a9f-44a6-9373-0b3fcf412c9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 7
}
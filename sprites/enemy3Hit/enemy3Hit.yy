{
    "id": "775ec107-b479-4644-b8d7-292f08b503a2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy3Hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6eceb9c5-fb29-4ca6-883d-9a0023ad886d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "775ec107-b479-4644-b8d7-292f08b503a2",
            "compositeImage": {
                "id": "9ffd5a3c-aa24-4d3d-b534-478e2da54d12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eceb9c5-fb29-4ca6-883d-9a0023ad886d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee18324f-327c-4579-9db5-325ab660d121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eceb9c5-fb29-4ca6-883d-9a0023ad886d",
                    "LayerId": "ce33fda4-58bd-44a1-95ba-988c67f8e9ae"
                }
            ]
        },
        {
            "id": "eec59744-c5bc-47b1-8ac9-cb305a115644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "775ec107-b479-4644-b8d7-292f08b503a2",
            "compositeImage": {
                "id": "869fac5c-2f6b-4297-810a-730f69419bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eec59744-c5bc-47b1-8ac9-cb305a115644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42a44f95-5da1-4fad-84ed-2d2d3aa2becd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eec59744-c5bc-47b1-8ac9-cb305a115644",
                    "LayerId": "ce33fda4-58bd-44a1-95ba-988c67f8e9ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ce33fda4-58bd-44a1-95ba-988c67f8e9ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "775ec107-b479-4644-b8d7-292f08b503a2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
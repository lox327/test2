{
    "id": "1974e787-8155-4a90-9b10-2f18d5280f2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 5,
    "bbox_right": 42,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6334e65-2919-4515-895b-f06873e00555",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "compositeImage": {
                "id": "ad637768-d710-41bf-82fe-c84299228ec2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6334e65-2919-4515-895b-f06873e00555",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d26c5a22-ce12-4d05-88b2-205f8c861ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6334e65-2919-4515-895b-f06873e00555",
                    "LayerId": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8"
                }
            ]
        },
        {
            "id": "00874d5b-73db-4bc8-b4c3-5c2cf8a0ee1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "compositeImage": {
                "id": "dfad2614-992b-4a65-a10b-5c3901272de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00874d5b-73db-4bc8-b4c3-5c2cf8a0ee1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bc2e79e-437a-4a50-9a59-bf32de2c108f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00874d5b-73db-4bc8-b4c3-5c2cf8a0ee1f",
                    "LayerId": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8"
                }
            ]
        },
        {
            "id": "1f11ed32-bb16-408e-aaf0-6f045650ec09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "compositeImage": {
                "id": "f40d2b1b-afd8-4b5c-a07d-f17cdce82053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f11ed32-bb16-408e-aaf0-6f045650ec09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cae3896-63e4-4c4c-8e33-09b2b45e0e16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f11ed32-bb16-408e-aaf0-6f045650ec09",
                    "LayerId": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8"
                }
            ]
        },
        {
            "id": "71631021-3bca-4729-80f9-a560a946bf7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "compositeImage": {
                "id": "8c04ed09-5541-4204-b389-f644bf71abf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71631021-3bca-4729-80f9-a560a946bf7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "005544b7-9f11-412b-aefd-f12b26479141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71631021-3bca-4729-80f9-a560a946bf7f",
                    "LayerId": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8"
                }
            ]
        },
        {
            "id": "4baff3d1-6e78-470d-898b-88f13485de6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "compositeImage": {
                "id": "2b8c3736-c5e5-4830-b506-6b35a6f3dceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4baff3d1-6e78-470d-898b-88f13485de6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fa9179c-bbed-4cab-ae79-05cd9340246f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4baff3d1-6e78-470d-898b-88f13485de6d",
                    "LayerId": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8"
                }
            ]
        },
        {
            "id": "d88221c9-0877-46bb-9625-ffdb49e6149d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "compositeImage": {
                "id": "1634ae9b-f98f-4ffb-97a5-f8fc99a132ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88221c9-0877-46bb-9625-ffdb49e6149d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d95fb23a-3045-46cc-9c63-1b9b710ba8cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88221c9-0877-46bb-9625-ffdb49e6149d",
                    "LayerId": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8"
                }
            ]
        },
        {
            "id": "b5fcf400-871f-4015-9166-b7229b061e31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "compositeImage": {
                "id": "fd85ba6f-96be-4658-96ab-3c35148d412d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5fcf400-871f-4015-9166-b7229b061e31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07256298-07f6-4ca5-a567-a6d1366ac308",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5fcf400-871f-4015-9166-b7229b061e31",
                    "LayerId": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8"
                }
            ]
        },
        {
            "id": "07ddf9f2-42ea-4209-b6bf-ce3a6b78954f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "compositeImage": {
                "id": "2938d38d-0598-4496-84ca-9f6b73ff9109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07ddf9f2-42ea-4209-b6bf-ce3a6b78954f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5539cbd8-a7a5-4d47-a30a-575b0fd22a3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07ddf9f2-42ea-4209-b6bf-ce3a6b78954f",
                    "LayerId": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "ad62a06c-23c9-41f1-9be5-718f0c9efeb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1974e787-8155-4a90-9b10-2f18d5280f2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "da1de812-dc3d-4f68-949e-3cc0cf246f27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarRunner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 9,
    "bbox_right": 47,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59b14c1a-3c32-4b91-9d2c-8181c794f8f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da1de812-dc3d-4f68-949e-3cc0cf246f27",
            "compositeImage": {
                "id": "bdb3d61f-ad85-4095-b20f-22a121059fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b14c1a-3c32-4b91-9d2c-8181c794f8f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe6ddf2-03e2-4f66-98f3-2c519a14dc66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b14c1a-3c32-4b91-9d2c-8181c794f8f1",
                    "LayerId": "5361173c-7a96-405c-88cf-2029f96ab50d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "5361173c-7a96-405c-88cf-2029f96ab50d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da1de812-dc3d-4f68-949e-3cc0cf246f27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "58c37abc-569b-40cc-a6e2-cda6cbf9ac3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr003",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1512a5c-b777-4c76-8042-0e2dc986c95f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58c37abc-569b-40cc-a6e2-cda6cbf9ac3b",
            "compositeImage": {
                "id": "3cf1a649-9c76-4813-8f55-b448a5e6f12b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1512a5c-b777-4c76-8042-0e2dc986c95f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8294eff9-081c-4b25-8abe-0e683dddf707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1512a5c-b777-4c76-8042-0e2dc986c95f",
                    "LayerId": "a8a7a135-84c1-485b-bc82-75bb1413f577"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a8a7a135-84c1-485b-bc82-75bb1413f577",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58c37abc-569b-40cc-a6e2-cda6cbf9ac3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
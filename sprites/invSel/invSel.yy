{
    "id": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "invSel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ca37176-a5da-4af8-84d3-a1564c29787d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "2bac3c2b-353f-44dd-bf90-708316ad2b02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ca37176-a5da-4af8-84d3-a1564c29787d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a0dab62-51a7-4bfc-b405-b8edbf01d82b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ca37176-a5da-4af8-84d3-a1564c29787d",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "7d5a8d5a-8e6d-418e-866a-9f08044f5158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "9cba6857-143b-4f12-8cb1-97bcbebf14e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d5a8d5a-8e6d-418e-866a-9f08044f5158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9137f06-e928-4a32-b7fe-bd990c233874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d5a8d5a-8e6d-418e-866a-9f08044f5158",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "7ae37308-118a-4403-ae0f-9943a1769c65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "be53f832-527b-4a4f-988d-7f3850d32b43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae37308-118a-4403-ae0f-9943a1769c65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dde8aa4-ff30-48cc-b12b-7ab480cb5f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae37308-118a-4403-ae0f-9943a1769c65",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "68496603-5750-4a01-900c-a0307ef4f9e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "012902be-5511-450d-9034-4141a866bd43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68496603-5750-4a01-900c-a0307ef4f9e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d1ef9e7-5010-4a8a-b796-73d26106a141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68496603-5750-4a01-900c-a0307ef4f9e7",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "366ec443-329f-41ef-af68-ef3dfe7dff71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "8bb64c1f-a6b0-4699-897a-9dd976e12d0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "366ec443-329f-41ef-af68-ef3dfe7dff71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ef8124-f789-484d-97cb-48118cbf4503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "366ec443-329f-41ef-af68-ef3dfe7dff71",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "ebf92923-f664-4c66-ae41-11df1513c90f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "22f7dbf4-d119-48c1-8a26-a028e439816b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf92923-f664-4c66-ae41-11df1513c90f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d3dda4-c021-4efd-a5c4-ef2eaac5c78f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf92923-f664-4c66-ae41-11df1513c90f",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "7efa6804-be8e-4807-9b77-3e9ed3582e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "ea2d1d9c-ae76-45c9-9dd3-80db111f7aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7efa6804-be8e-4807-9b77-3e9ed3582e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "260ebe7e-a3c0-4d8e-9275-9b93eb5d6bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7efa6804-be8e-4807-9b77-3e9ed3582e65",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "59a68a42-0e46-4053-a0e2-717daec339f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "03e1a16d-278e-4ad1-9144-98701c7a21e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a68a42-0e46-4053-a0e2-717daec339f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4bfafcb-5bd4-4999-87e8-231652750205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a68a42-0e46-4053-a0e2-717daec339f3",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "8343d78f-b176-4abb-9b10-151be38f7f1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "cc9af1bd-ce2a-45f0-8b1b-f6ca8578df59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8343d78f-b176-4abb-9b10-151be38f7f1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a73dee6-cf3b-4cc4-87c4-630c2fd29cf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8343d78f-b176-4abb-9b10-151be38f7f1e",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        },
        {
            "id": "f23c1cb1-31d8-4b5b-b996-1fe55513144f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "compositeImage": {
                "id": "4f6410ad-d363-417a-89d6-7060fa63eb4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f23c1cb1-31d8-4b5b-b996-1fe55513144f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe8bd3c-f277-4cdb-b1a2-a79b6d3f1a59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f23c1cb1-31d8-4b5b-b996-1fe55513144f",
                    "LayerId": "be1aca11-20cb-4564-821d-6ebcf2fb4416"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "be1aca11-20cb-4564-821d-6ebcf2fb4416",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5374bb1a-5c7d-4d6d-ab54-890c2d59ad6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
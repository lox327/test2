{
    "id": "4e1d424d-bf7d-4f2a-9c80-513e6feb750e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy3Dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "615ea890-fd37-4234-aa22-7d68b4619e82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e1d424d-bf7d-4f2a-9c80-513e6feb750e",
            "compositeImage": {
                "id": "0eb32300-aee0-481e-a232-0dd45bf23afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "615ea890-fd37-4234-aa22-7d68b4619e82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e2333dc-df1a-4e27-96d3-0b28a5ab2dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "615ea890-fd37-4234-aa22-7d68b4619e82",
                    "LayerId": "1221c3d9-c045-4888-b360-8a0eb14cb4a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1221c3d9-c045-4888-b360-8a0eb14cb4a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e1d424d-bf7d-4f2a-9c80-513e6feb750e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
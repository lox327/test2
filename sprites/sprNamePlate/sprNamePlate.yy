{
    "id": "3a585faf-f11a-4da2-9786-b00c020e975b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprNamePlate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44c63230-7464-44d8-ae77-552db81e91e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a585faf-f11a-4da2-9786-b00c020e975b",
            "compositeImage": {
                "id": "01e2ad91-a502-47ff-b5ed-777581f8c650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44c63230-7464-44d8-ae77-552db81e91e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cb016e2-6fa6-498a-a766-560fed6e228d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44c63230-7464-44d8-ae77-552db81e91e9",
                    "LayerId": "98bae14d-e0ee-4b35-87e1-3fdf895d05a9"
                }
            ]
        },
        {
            "id": "8e746596-0a74-4f8d-8ac5-ddc28090a1e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a585faf-f11a-4da2-9786-b00c020e975b",
            "compositeImage": {
                "id": "5d69403d-1368-489c-8464-bbf04cbae8e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e746596-0a74-4f8d-8ac5-ddc28090a1e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b8c8deb-d3fc-4d28-a9e3-43fb98fa35a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e746596-0a74-4f8d-8ac5-ddc28090a1e1",
                    "LayerId": "98bae14d-e0ee-4b35-87e1-3fdf895d05a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "98bae14d-e0ee-4b35-87e1-3fdf895d05a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a585faf-f11a-4da2-9786-b00c020e975b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "f76c95aa-8c9b-4c87-9943-91e445a10410",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "block1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fac401d1-7f21-40df-ad59-ce071e3a5653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f76c95aa-8c9b-4c87-9943-91e445a10410",
            "compositeImage": {
                "id": "271defce-3962-4b58-840f-98a45f3e1903",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fac401d1-7f21-40df-ad59-ce071e3a5653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae43ff35-f7f2-4ba0-882b-529d98faeaa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fac401d1-7f21-40df-ad59-ce071e3a5653",
                    "LayerId": "7d00b9ff-4a36-4e34-b7b9-ee30db9864de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7d00b9ff-4a36-4e34-b7b9-ee30db9864de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f76c95aa-8c9b-4c87-9943-91e445a10410",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
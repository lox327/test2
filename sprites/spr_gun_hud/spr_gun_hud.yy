{
    "id": "c1d4a56b-5437-45fa-ac5e-349bbcf669b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gun_hud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a8c64b8-54fe-4226-a7c6-abc622aae906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1d4a56b-5437-45fa-ac5e-349bbcf669b7",
            "compositeImage": {
                "id": "ec6a45dd-17f7-4a00-81a0-a6fe094155dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a8c64b8-54fe-4226-a7c6-abc622aae906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4936d66-ca58-4528-8fb2-e6b9233a0973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a8c64b8-54fe-4226-a7c6-abc622aae906",
                    "LayerId": "a0bc9cee-64af-477f-922f-fd3762c6d479"
                }
            ]
        },
        {
            "id": "3639839c-16d4-429e-a655-1dda3b960ef1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1d4a56b-5437-45fa-ac5e-349bbcf669b7",
            "compositeImage": {
                "id": "ac84807b-ee5c-49d5-ab91-236202976837",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3639839c-16d4-429e-a655-1dda3b960ef1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a49525e-2f0f-4f0e-bb16-bcae58536def",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3639839c-16d4-429e-a655-1dda3b960ef1",
                    "LayerId": "a0bc9cee-64af-477f-922f-fd3762c6d479"
                }
            ]
        },
        {
            "id": "c29bf648-418a-42f0-89a6-d6f44a7b9334",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1d4a56b-5437-45fa-ac5e-349bbcf669b7",
            "compositeImage": {
                "id": "f8f40182-ffc5-44ab-9e82-279147f1c388",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c29bf648-418a-42f0-89a6-d6f44a7b9334",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d360ff3-7ee8-4f3a-a1fc-9c449d30ce46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c29bf648-418a-42f0-89a6-d6f44a7b9334",
                    "LayerId": "a0bc9cee-64af-477f-922f-fd3762c6d479"
                }
            ]
        },
        {
            "id": "a4b94ffb-ed4d-4712-9f14-b8ad23f2f280",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1d4a56b-5437-45fa-ac5e-349bbcf669b7",
            "compositeImage": {
                "id": "b1958083-2f01-4012-be8f-bc517dc89cfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b94ffb-ed4d-4712-9f14-b8ad23f2f280",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "942c91fc-d0ce-4f2d-bfa3-e403f2da6ba4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b94ffb-ed4d-4712-9f14-b8ad23f2f280",
                    "LayerId": "a0bc9cee-64af-477f-922f-fd3762c6d479"
                }
            ]
        },
        {
            "id": "f9987840-a30e-48ea-a268-174ac7d1e03b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1d4a56b-5437-45fa-ac5e-349bbcf669b7",
            "compositeImage": {
                "id": "a810ba6b-6518-489a-b1ca-62c5155be5a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9987840-a30e-48ea-a268-174ac7d1e03b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f854f8d8-915b-4de4-890e-2691676b43ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9987840-a30e-48ea-a268-174ac7d1e03b",
                    "LayerId": "a0bc9cee-64af-477f-922f-fd3762c6d479"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a0bc9cee-64af-477f-922f-fd3762c6d479",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1d4a56b-5437-45fa-ac5e-349bbcf669b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "805fe6b8-01d7-4ce4-a68c-0dc72b734d94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite149",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "660d0740-32c9-41e1-8844-f3f8136c26c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "805fe6b8-01d7-4ce4-a68c-0dc72b734d94",
            "compositeImage": {
                "id": "85273538-1648-4baf-8106-3bed46698271",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "660d0740-32c9-41e1-8844-f3f8136c26c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e11f2eb6-7da6-4040-b7da-c2911db0a67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "660d0740-32c9-41e1-8844-f3f8136c26c8",
                    "LayerId": "848a845b-7c14-49ff-99c8-587e3a1985d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "848a845b-7c14-49ff-99c8-587e3a1985d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "805fe6b8-01d7-4ce4-a68c-0dc72b734d94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "c784e50c-cb84-4a66-8388-3f447159b204",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "items1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "668a3286-f16f-4557-bc65-bfd6dde3b729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c784e50c-cb84-4a66-8388-3f447159b204",
            "compositeImage": {
                "id": "5292453d-ee23-4cbf-a200-af6fb39d88ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668a3286-f16f-4557-bc65-bfd6dde3b729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150e6faa-4652-4ee0-b9f6-26c1afecd976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668a3286-f16f-4557-bc65-bfd6dde3b729",
                    "LayerId": "83fee930-2fd1-4cf2-904d-395dfbe0c468"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "83fee930-2fd1-4cf2-904d-395dfbe0c468",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c784e50c-cb84-4a66-8388-3f447159b204",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}
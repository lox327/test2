{
    "id": "7f76e7b1-d384-4f60-b46b-403fae67af34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "terminal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82c7d92c-e2b5-4194-8d15-2fc6a586f09e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f76e7b1-d384-4f60-b46b-403fae67af34",
            "compositeImage": {
                "id": "7201d6fa-8ba7-4521-9a30-62e5f0e59ef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82c7d92c-e2b5-4194-8d15-2fc6a586f09e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34b38508-1a06-48a0-8ada-ea97942b5cc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82c7d92c-e2b5-4194-8d15-2fc6a586f09e",
                    "LayerId": "f5fc91e3-4811-4d90-b0bd-b8a1ae2bf58f"
                }
            ]
        },
        {
            "id": "9918f16d-4813-46f6-be68-46a7216d301d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f76e7b1-d384-4f60-b46b-403fae67af34",
            "compositeImage": {
                "id": "9be0a052-1f88-4606-aed2-62ae0a5d6fa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9918f16d-4813-46f6-be68-46a7216d301d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "091b346c-9b3c-49d1-b415-25908f40adb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9918f16d-4813-46f6-be68-46a7216d301d",
                    "LayerId": "f5fc91e3-4811-4d90-b0bd-b8a1ae2bf58f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f5fc91e3-4811-4d90-b0bd-b8a1ae2bf58f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f76e7b1-d384-4f60-b46b-403fae67af34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
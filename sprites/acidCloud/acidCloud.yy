{
    "id": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "acidCloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2989253-6e9d-4aba-8398-9d5cc2471cc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "a55032e4-bedb-40e3-abc6-4f8434c99ec9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2989253-6e9d-4aba-8398-9d5cc2471cc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3a7294-b191-40e4-91b0-4b65721225c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2989253-6e9d-4aba-8398-9d5cc2471cc9",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        },
        {
            "id": "0593c721-c750-4032-91ff-cdc20bea70b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "324a4594-607b-4b2c-8b0b-779b2094c028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0593c721-c750-4032-91ff-cdc20bea70b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1291823-6591-46d8-9797-7740bdce5c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0593c721-c750-4032-91ff-cdc20bea70b2",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        },
        {
            "id": "3a2cd7d3-e930-4535-8e98-2bcc1e55d835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "09f49e92-dddb-4729-9557-82430a86d8f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2cd7d3-e930-4535-8e98-2bcc1e55d835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3a99d1-fe18-4276-b3a6-e71b41b78c38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2cd7d3-e930-4535-8e98-2bcc1e55d835",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        },
        {
            "id": "caa65d8e-11aa-42db-872a-c6bd7f280ec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "1ef24ce9-3044-41cd-a5ef-85a1f5a0a4a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caa65d8e-11aa-42db-872a-c6bd7f280ec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a691e55-8a9c-4db8-a47e-a6a724753ba1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caa65d8e-11aa-42db-872a-c6bd7f280ec7",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        },
        {
            "id": "6b03d0e6-0f57-4336-88bf-a1c3fb662815",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "d65e510f-4856-4e0e-b991-dfeafc19cc23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b03d0e6-0f57-4336-88bf-a1c3fb662815",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8fc3c59-0c89-48d7-8148-63803d40046a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b03d0e6-0f57-4336-88bf-a1c3fb662815",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        },
        {
            "id": "7938fd24-3cb3-4ca4-a48a-e6a47b9be058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "99938fab-4f83-49b6-9573-4f166f60ea70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7938fd24-3cb3-4ca4-a48a-e6a47b9be058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55a3bc3f-f110-4c37-9a12-e2f66907fced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7938fd24-3cb3-4ca4-a48a-e6a47b9be058",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        },
        {
            "id": "0b6476d5-6c10-4f51-88c3-aa170f8a25af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "7495da15-dbcd-4759-adb1-565f71cec61e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b6476d5-6c10-4f51-88c3-aa170f8a25af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c88acc3-7e50-4a4f-83a9-a99a0cb0951e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b6476d5-6c10-4f51-88c3-aa170f8a25af",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        },
        {
            "id": "d4cd764e-cd5e-45c7-a8ae-f45ee1d69be6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "dca16350-140b-4487-8867-72265e7bfa6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4cd764e-cd5e-45c7-a8ae-f45ee1d69be6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a4eea6-5673-4156-bb02-22a60fba0ea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4cd764e-cd5e-45c7-a8ae-f45ee1d69be6",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        },
        {
            "id": "5d551848-42f5-4a50-bff3-ff7b705fc07b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "compositeImage": {
                "id": "1d9aab3c-6875-46f6-b793-9f261f988748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d551848-42f5-4a50-bff3-ff7b705fc07b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9842e89-39a4-4057-8e55-5ea606e2c75e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d551848-42f5-4a50-bff3-ff7b705fc07b",
                    "LayerId": "740f2149-0792-4ff5-aacc-de9f4dbf9034"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "740f2149-0792-4ff5-aacc-de9f4dbf9034",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff3cc5bf-463f-4df3-bf4f-2a360044b84e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 66,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
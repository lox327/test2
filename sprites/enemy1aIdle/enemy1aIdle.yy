{
    "id": "1ac1902e-a32a-468e-a760-1c9fe2326a0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e6b7ced-df40-4019-84ec-465d5c8fe9dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ac1902e-a32a-468e-a760-1c9fe2326a0c",
            "compositeImage": {
                "id": "d04159b1-2375-49dd-b1dd-88437463a311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e6b7ced-df40-4019-84ec-465d5c8fe9dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ffe23d8-cd33-48e4-b74f-bac169766bf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e6b7ced-df40-4019-84ec-465d5c8fe9dd",
                    "LayerId": "7394e6e0-0480-4bef-b484-fb554032db9d"
                }
            ]
        },
        {
            "id": "c063e731-e35e-484f-a23b-f2cbab429b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ac1902e-a32a-468e-a760-1c9fe2326a0c",
            "compositeImage": {
                "id": "fde82780-67cc-4fc5-b853-efcf33025fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c063e731-e35e-484f-a23b-f2cbab429b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4b8259-658a-4acf-9a53-b651413bd01d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c063e731-e35e-484f-a23b-f2cbab429b6e",
                    "LayerId": "7394e6e0-0480-4bef-b484-fb554032db9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7394e6e0-0480-4bef-b484-fb554032db9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ac1902e-a32a-468e-a760-1c9fe2326a0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
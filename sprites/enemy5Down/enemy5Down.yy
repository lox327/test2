{
    "id": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 11,
    "bbox_right": 49,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5673d0f2-6ecb-4787-be67-8585f5e3a073",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "compositeImage": {
                "id": "34ca9236-7c75-4b65-b4dc-4c9eba8b659f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5673d0f2-6ecb-4787-be67-8585f5e3a073",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68f23dfc-868a-4dbc-b330-bcad90047801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5673d0f2-6ecb-4787-be67-8585f5e3a073",
                    "LayerId": "85cf333a-cea2-49a1-8497-d8fdd2f6c383"
                }
            ]
        },
        {
            "id": "b7d5e77a-3eae-4b50-aac0-28fa1a2173e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "compositeImage": {
                "id": "5890af64-53c7-448c-b7a4-1a1256f1e23a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d5e77a-3eae-4b50-aac0-28fa1a2173e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12955deb-e1e3-42eb-bba2-5c57e83f1d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d5e77a-3eae-4b50-aac0-28fa1a2173e1",
                    "LayerId": "85cf333a-cea2-49a1-8497-d8fdd2f6c383"
                }
            ]
        },
        {
            "id": "be66183f-5ea8-43fa-8ac3-08e4dbaa8814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "compositeImage": {
                "id": "407fefe0-3a9b-417d-88a2-5d5f5a933f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be66183f-5ea8-43fa-8ac3-08e4dbaa8814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a74e4f1a-26fb-4576-a025-cc5b33b94e63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be66183f-5ea8-43fa-8ac3-08e4dbaa8814",
                    "LayerId": "85cf333a-cea2-49a1-8497-d8fdd2f6c383"
                }
            ]
        },
        {
            "id": "e3eee79a-db77-4d81-a8ca-1fbda348298e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "compositeImage": {
                "id": "6cc4c8a2-1ce8-44d5-aa60-0c9f8ae45586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3eee79a-db77-4d81-a8ca-1fbda348298e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2148a53f-e735-4654-8987-604f4879f997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3eee79a-db77-4d81-a8ca-1fbda348298e",
                    "LayerId": "85cf333a-cea2-49a1-8497-d8fdd2f6c383"
                }
            ]
        },
        {
            "id": "e606e84e-72a3-4ea0-9f8c-ec6bf34ce331",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "compositeImage": {
                "id": "8d250961-197f-4ebb-8455-10c256c8a11c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e606e84e-72a3-4ea0-9f8c-ec6bf34ce331",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82cbe0c4-a70a-4afe-a576-4996b8bd26d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e606e84e-72a3-4ea0-9f8c-ec6bf34ce331",
                    "LayerId": "85cf333a-cea2-49a1-8497-d8fdd2f6c383"
                }
            ]
        },
        {
            "id": "cc8d61dd-f909-4fc3-9f1d-484ea92f3a99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "compositeImage": {
                "id": "857e7f9a-6b7e-48e4-bc5a-0d40f9f07fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc8d61dd-f909-4fc3-9f1d-484ea92f3a99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc5de588-2945-48d7-8ff0-2e654a2830f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc8d61dd-f909-4fc3-9f1d-484ea92f3a99",
                    "LayerId": "85cf333a-cea2-49a1-8497-d8fdd2f6c383"
                }
            ]
        },
        {
            "id": "dbe4361c-ac6e-4194-865e-2cf2c5d88a36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "compositeImage": {
                "id": "7f9524ee-1a04-490e-857d-7f31073f662b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe4361c-ac6e-4194-865e-2cf2c5d88a36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76b57e74-48fe-4c4b-a29b-5fc2ef856483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe4361c-ac6e-4194-865e-2cf2c5d88a36",
                    "LayerId": "85cf333a-cea2-49a1-8497-d8fdd2f6c383"
                }
            ]
        },
        {
            "id": "db8ddefe-2a7d-406d-a302-01b85ea37c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "compositeImage": {
                "id": "a3097446-4b97-409b-8be1-b69cdfb2632a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8ddefe-2a7d-406d-a302-01b85ea37c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8934cd19-ee5a-48ca-964f-1beed89a69b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8ddefe-2a7d-406d-a302-01b85ea37c5a",
                    "LayerId": "85cf333a-cea2-49a1-8497-d8fdd2f6c383"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "85cf333a-cea2-49a1-8497-d8fdd2f6c383",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7ab815a-09cd-4f33-95dc-c363204d54e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
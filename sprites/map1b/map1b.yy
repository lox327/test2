{
    "id": "c984a25d-089b-4d82-8b1b-1cfcac5d8dc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "map1b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 536,
    "bbox_left": 0,
    "bbox_right": 955,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ab6f104-11f7-43dd-bbc5-e9854889a287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c984a25d-089b-4d82-8b1b-1cfcac5d8dc7",
            "compositeImage": {
                "id": "22ff75e1-ab7e-41b3-b8de-5edf6e0cacb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab6f104-11f7-43dd-bbc5-e9854889a287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6731c2cc-1ec0-412f-9760-fa9fc2857496",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab6f104-11f7-43dd-bbc5-e9854889a287",
                    "LayerId": "681e238e-332a-46eb-8d34-8d8f56cfb6eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 537,
    "layers": [
        {
            "id": "681e238e-332a-46eb-8d34-8d8f56cfb6eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c984a25d-089b-4d82-8b1b-1cfcac5d8dc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 956,
    "xorig": 0,
    "yorig": 0
}
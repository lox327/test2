{
    "id": "0545320c-bc68-4581-9811-119f034af2b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerBash_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b10d84e2-c346-472d-ac54-17c19cbb40bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0545320c-bc68-4581-9811-119f034af2b9",
            "compositeImage": {
                "id": "8aacebdd-70bf-49e2-ad1c-d6590ff33598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10d84e2-c346-472d-ac54-17c19cbb40bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f75778f3-1ca3-440f-900f-e5f9ff282deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10d84e2-c346-472d-ac54-17c19cbb40bb",
                    "LayerId": "17e5434a-cedf-4f92-bdbd-0db06be1f5c0"
                }
            ]
        },
        {
            "id": "04802626-9fda-46da-a275-50d5752aabba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0545320c-bc68-4581-9811-119f034af2b9",
            "compositeImage": {
                "id": "d19bfe29-232d-4231-a5c3-c6fba8c9a995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04802626-9fda-46da-a275-50d5752aabba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a03032f-e6c3-47bc-abcb-7ad5f934ce1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04802626-9fda-46da-a275-50d5752aabba",
                    "LayerId": "17e5434a-cedf-4f92-bdbd-0db06be1f5c0"
                }
            ]
        },
        {
            "id": "f9f0c02d-2d5f-4d92-adf0-839d336cf3fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0545320c-bc68-4581-9811-119f034af2b9",
            "compositeImage": {
                "id": "6031e537-b230-44b8-b0f4-06989afc9f2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9f0c02d-2d5f-4d92-adf0-839d336cf3fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13d03eba-1214-4245-8c06-d5eea3d24fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f0c02d-2d5f-4d92-adf0-839d336cf3fb",
                    "LayerId": "17e5434a-cedf-4f92-bdbd-0db06be1f5c0"
                }
            ]
        },
        {
            "id": "c51f1423-4a22-4bcd-801a-bcd856980362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0545320c-bc68-4581-9811-119f034af2b9",
            "compositeImage": {
                "id": "938cf8bb-ac60-418e-bbff-72f79ed51ffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c51f1423-4a22-4bcd-801a-bcd856980362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93ded4cd-b058-4296-9b4a-4bed0d20e779",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c51f1423-4a22-4bcd-801a-bcd856980362",
                    "LayerId": "17e5434a-cedf-4f92-bdbd-0db06be1f5c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "17e5434a-cedf-4f92-bdbd-0db06be1f5c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0545320c-bc68-4581-9811-119f034af2b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
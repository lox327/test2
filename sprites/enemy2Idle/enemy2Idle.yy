{
    "id": "bcc25971-a68a-444d-b48c-df592d752a3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 10,
    "bbox_right": 50,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0cbeaa9-40d4-4c51-8b03-9744deeb5ace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "68f2a5d5-4fdb-4b61-99c4-55e0399218b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0cbeaa9-40d4-4c51-8b03-9744deeb5ace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcc2e0fe-f3d4-4b38-879e-08e7715b7760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0cbeaa9-40d4-4c51-8b03-9744deeb5ace",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "d991680a-9ab8-4ab0-9999-895460ff0085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "fdd2d33d-cb13-47fb-a943-3de1c32d1975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d991680a-9ab8-4ab0-9999-895460ff0085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4d01050-1a91-4bc2-9a10-1740b19367e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d991680a-9ab8-4ab0-9999-895460ff0085",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "aaa4439c-37ce-47bf-aea7-62a47c793002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "5eda5e2b-7d9b-4405-ad20-abc40b2eb4c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaa4439c-37ce-47bf-aea7-62a47c793002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "382af130-9a18-48ab-8bb8-8e1564ba19f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaa4439c-37ce-47bf-aea7-62a47c793002",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "485540da-3013-461a-8317-42d2ad8f9bf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "f01efbf7-e596-4474-a36a-f056e738d3a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "485540da-3013-461a-8317-42d2ad8f9bf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bc86985-8827-41e2-99b1-9136d4d9cfff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "485540da-3013-461a-8317-42d2ad8f9bf6",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "d42a49cc-1daa-4f65-9bdb-a98638a17afb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "bfc359d9-91b3-43cf-9c64-9c49831d0ab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d42a49cc-1daa-4f65-9bdb-a98638a17afb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa55decf-bff6-450d-87c0-121639d9e2de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d42a49cc-1daa-4f65-9bdb-a98638a17afb",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "7da278d1-446d-4dc7-8a47-f900fd6ab040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "2fac6e29-ef04-4447-80c4-1b7450c268a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da278d1-446d-4dc7-8a47-f900fd6ab040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f9dea50-3119-4457-8b65-fba349ad8ef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da278d1-446d-4dc7-8a47-f900fd6ab040",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "8adf6a72-4e83-4890-9ed3-8ce028a70233",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "6a96ff72-470f-47dd-9103-d18754544218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8adf6a72-4e83-4890-9ed3-8ce028a70233",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36aa3d63-50d7-40de-8f32-e902d4e52692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8adf6a72-4e83-4890-9ed3-8ce028a70233",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "43c8df34-e4ff-4dc8-b728-91b2f1a73af6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "5161ebd0-519d-4fee-8dac-5d6e8566b371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43c8df34-e4ff-4dc8-b728-91b2f1a73af6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b584a57a-62cc-48f0-86a8-7cd5cc10c9aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43c8df34-e4ff-4dc8-b728-91b2f1a73af6",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "50e6c82f-9d08-481d-a353-cdfb6c86d9ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "cfd127d5-4eee-4afd-870d-72513bab4ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50e6c82f-9d08-481d-a353-cdfb6c86d9ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38764fc0-3c02-4a12-b71b-007d7bc87a80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50e6c82f-9d08-481d-a353-cdfb6c86d9ba",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "79b30f77-dfa7-408a-bca4-41576b6361f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "16b8639e-923e-4c07-8a02-81f9156e50a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79b30f77-dfa7-408a-bca4-41576b6361f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e467c0d-1838-4541-928f-9f521c58524f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79b30f77-dfa7-408a-bca4-41576b6361f8",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "3b864ded-0e34-439c-9ae5-51ae735047d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "9a27a1e1-f0a2-4230-8ba9-be0e23ca3852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b864ded-0e34-439c-9ae5-51ae735047d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2f8b2b-287f-461c-9c9c-9dbe716fec54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b864ded-0e34-439c-9ae5-51ae735047d8",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        },
        {
            "id": "739e2458-18f0-40a1-a8a8-d7189cb15be8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "compositeImage": {
                "id": "4bac083e-0549-49ae-a9d6-207b14f4fbe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "739e2458-18f0-40a1-a8a8-d7189cb15be8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48afc178-82d6-461d-acab-8e917ea1ed82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "739e2458-18f0-40a1-a8a8-d7189cb15be8",
                    "LayerId": "d7e912dd-ace1-43c1-932a-380b8271dc97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "d7e912dd-ace1-43c1-932a-380b8271dc97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcc25971-a68a-444d-b48c-df592d752a3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
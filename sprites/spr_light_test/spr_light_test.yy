{
    "id": "abf73f4e-759c-4c34-9e8f-27619dc9e9f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light_test",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "895fbfdc-be6a-4fd4-b8c0-4b91af9a3918",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abf73f4e-759c-4c34-9e8f-27619dc9e9f1",
            "compositeImage": {
                "id": "3773795d-4ee6-4b89-a472-8ed14a791467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895fbfdc-be6a-4fd4-b8c0-4b91af9a3918",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eed20997-b069-48b1-8e6f-2fc858dd2f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895fbfdc-be6a-4fd4-b8c0-4b91af9a3918",
                    "LayerId": "393d28fd-ed13-42a1-a472-dc05aecea533"
                },
                {
                    "id": "35f2f5bb-bd04-467b-9590-88b23f2ddf28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895fbfdc-be6a-4fd4-b8c0-4b91af9a3918",
                    "LayerId": "8b063d87-d1fb-427f-b763-3ad55b75d828"
                },
                {
                    "id": "011b1746-d842-4575-ad8c-cd35a54facf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895fbfdc-be6a-4fd4-b8c0-4b91af9a3918",
                    "LayerId": "bbbbea06-d872-48a3-b4c3-2fa63c48cc1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "393d28fd-ed13-42a1-a472-dc05aecea533",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abf73f4e-759c-4c34-9e8f-27619dc9e9f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 14,
            "visible": true
        },
        {
            "id": "8b063d87-d1fb-427f-b763-3ad55b75d828",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abf73f4e-759c-4c34-9e8f-27619dc9e9f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 35,
            "visible": true
        },
        {
            "id": "bbbbea06-d872-48a3-b4c3-2fa63c48cc1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abf73f4e-759c-4c34-9e8f-27619dc9e9f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 256,
    "yorig": 256
}
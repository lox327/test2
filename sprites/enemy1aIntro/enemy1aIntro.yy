{
    "id": "980edd4c-24a9-4333-9959-0062d92d417f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aIntro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "105ebf82-9c51-48f2-b343-4e605acebf64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "980edd4c-24a9-4333-9959-0062d92d417f",
            "compositeImage": {
                "id": "ff7c7c67-fc38-4360-97d3-846ae33ee10f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "105ebf82-9c51-48f2-b343-4e605acebf64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "711fe7be-425d-47ef-9898-be2d87de86fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "105ebf82-9c51-48f2-b343-4e605acebf64",
                    "LayerId": "3c26207b-e0f6-459f-b938-1b5446d915e0"
                }
            ]
        },
        {
            "id": "c5a3e0b5-0fa5-45cd-bd80-e66e24ac0fab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "980edd4c-24a9-4333-9959-0062d92d417f",
            "compositeImage": {
                "id": "64d1531f-a935-417c-9f5a-32fbe521f001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5a3e0b5-0fa5-45cd-bd80-e66e24ac0fab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e8213ce-1ade-49c5-8745-3caf03c45f79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5a3e0b5-0fa5-45cd-bd80-e66e24ac0fab",
                    "LayerId": "3c26207b-e0f6-459f-b938-1b5446d915e0"
                }
            ]
        },
        {
            "id": "ddebc17f-bb6d-423b-a8c8-ff774980de08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "980edd4c-24a9-4333-9959-0062d92d417f",
            "compositeImage": {
                "id": "f4b16523-a44f-4bcb-8cb8-5addd4646801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddebc17f-bb6d-423b-a8c8-ff774980de08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd060c58-13f2-4d35-973f-bb1c4c5a3992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddebc17f-bb6d-423b-a8c8-ff774980de08",
                    "LayerId": "3c26207b-e0f6-459f-b938-1b5446d915e0"
                }
            ]
        },
        {
            "id": "2144b53e-845c-4213-aa81-134eaa3758f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "980edd4c-24a9-4333-9959-0062d92d417f",
            "compositeImage": {
                "id": "7420c680-5a0c-49e0-b560-25b29d31a6d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2144b53e-845c-4213-aa81-134eaa3758f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23ad644-ad65-488c-8506-2f00f7a3ac3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2144b53e-845c-4213-aa81-134eaa3758f8",
                    "LayerId": "3c26207b-e0f6-459f-b938-1b5446d915e0"
                }
            ]
        },
        {
            "id": "f3275316-e9a8-4192-91fe-e4408ebfab24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "980edd4c-24a9-4333-9959-0062d92d417f",
            "compositeImage": {
                "id": "f8b6b7e5-4a4e-4faf-b64c-1fac7a5e7b35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3275316-e9a8-4192-91fe-e4408ebfab24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf8fc88-6047-4fbe-9a74-2380ba294373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3275316-e9a8-4192-91fe-e4408ebfab24",
                    "LayerId": "3c26207b-e0f6-459f-b938-1b5446d915e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3c26207b-e0f6-459f-b938-1b5446d915e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "980edd4c-24a9-4333-9959-0062d92d417f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "751b521e-6b8c-4e84-8e05-9bc20f2342fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerBash_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6daea30d-74c2-4118-b1e0-12794616c96b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751b521e-6b8c-4e84-8e05-9bc20f2342fc",
            "compositeImage": {
                "id": "cf77bfab-9df9-4c38-a012-ae998913338a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6daea30d-74c2-4118-b1e0-12794616c96b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40a75eac-a013-4dc9-819f-9ce12b52096f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6daea30d-74c2-4118-b1e0-12794616c96b",
                    "LayerId": "41abd6d8-7a48-40de-b6e2-3bca16274e26"
                }
            ]
        },
        {
            "id": "06086b31-79c3-4506-9072-cedcdeaec0a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751b521e-6b8c-4e84-8e05-9bc20f2342fc",
            "compositeImage": {
                "id": "d8f547cb-ac48-4d6c-bdf9-048746030e0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06086b31-79c3-4506-9072-cedcdeaec0a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82e10a66-7340-4eaa-8472-62bda369e3eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06086b31-79c3-4506-9072-cedcdeaec0a9",
                    "LayerId": "41abd6d8-7a48-40de-b6e2-3bca16274e26"
                }
            ]
        },
        {
            "id": "9d2542f7-092d-41e4-9871-4cdbb4b806dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751b521e-6b8c-4e84-8e05-9bc20f2342fc",
            "compositeImage": {
                "id": "6a476017-a1c0-4e47-a2cb-f23f3bec075b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d2542f7-092d-41e4-9871-4cdbb4b806dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1eba456-74bf-48a3-946b-9bcad6ecc745",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d2542f7-092d-41e4-9871-4cdbb4b806dd",
                    "LayerId": "41abd6d8-7a48-40de-b6e2-3bca16274e26"
                }
            ]
        },
        {
            "id": "bd9f2eaf-0428-4570-a5f2-d664f3ce3412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "751b521e-6b8c-4e84-8e05-9bc20f2342fc",
            "compositeImage": {
                "id": "14c8afbc-4b34-4552-a04d-fe8b102f9acb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd9f2eaf-0428-4570-a5f2-d664f3ce3412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74235202-a413-42be-b4cc-58a62b0b1891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd9f2eaf-0428-4570-a5f2-d664f3ce3412",
                    "LayerId": "41abd6d8-7a48-40de-b6e2-3bca16274e26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "41abd6d8-7a48-40de-b6e2-3bca16274e26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "751b521e-6b8c-4e84-8e05-9bc20f2342fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
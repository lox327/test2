{
    "id": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy4Up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be8eb156-0091-4ad7-999c-7cf654e5ab17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "compositeImage": {
                "id": "a82407e3-fa35-427a-bf23-481f4c8a20a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8eb156-0091-4ad7-999c-7cf654e5ab17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f5963a2-b1a7-4eda-97f5-d87982f7b4b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8eb156-0091-4ad7-999c-7cf654e5ab17",
                    "LayerId": "f19173f0-82d6-453e-9378-9d6d8aadc34a"
                }
            ]
        },
        {
            "id": "66b4526b-b54d-44e6-935b-d1ed2b2565cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "compositeImage": {
                "id": "33c9a306-654f-4a56-96de-92cba0880247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66b4526b-b54d-44e6-935b-d1ed2b2565cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4aa1d92-9597-4ab2-adff-dfac84cce01e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66b4526b-b54d-44e6-935b-d1ed2b2565cc",
                    "LayerId": "f19173f0-82d6-453e-9378-9d6d8aadc34a"
                }
            ]
        },
        {
            "id": "0b464fe3-0b22-44ef-92fa-28b3e7244505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "compositeImage": {
                "id": "608cceca-f9d3-4ff3-822f-0031e0addc5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b464fe3-0b22-44ef-92fa-28b3e7244505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1686b3d9-e76d-40d4-8e67-855b44d2bdef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b464fe3-0b22-44ef-92fa-28b3e7244505",
                    "LayerId": "f19173f0-82d6-453e-9378-9d6d8aadc34a"
                }
            ]
        },
        {
            "id": "fd2d8d7e-9c2a-4d7c-a347-dec319aa9515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "compositeImage": {
                "id": "2950f4fa-7f4a-4456-a16d-75b832d10ff2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2d8d7e-9c2a-4d7c-a347-dec319aa9515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dd4561c-f29f-452d-8d4f-34f9763e14a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2d8d7e-9c2a-4d7c-a347-dec319aa9515",
                    "LayerId": "f19173f0-82d6-453e-9378-9d6d8aadc34a"
                }
            ]
        },
        {
            "id": "cd4a0bf9-732b-4e5f-bb50-5834c89aab14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "compositeImage": {
                "id": "c108af90-01b0-4241-a543-f93ba3c5df0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd4a0bf9-732b-4e5f-bb50-5834c89aab14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d2704cd-b6c6-4436-ace9-7c0f3651c349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd4a0bf9-732b-4e5f-bb50-5834c89aab14",
                    "LayerId": "f19173f0-82d6-453e-9378-9d6d8aadc34a"
                }
            ]
        },
        {
            "id": "d6887d80-2b18-42d2-bf32-ea6818f18ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "compositeImage": {
                "id": "85c10cb9-91fe-4058-a4aa-c21391311f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6887d80-2b18-42d2-bf32-ea6818f18ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15e616f7-5017-481a-b927-d391e7cca06a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6887d80-2b18-42d2-bf32-ea6818f18ae9",
                    "LayerId": "f19173f0-82d6-453e-9378-9d6d8aadc34a"
                }
            ]
        },
        {
            "id": "3485ab4b-9b22-4cd7-b6eb-283b8fc456cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "compositeImage": {
                "id": "cab77b9c-0f82-4b76-99b2-f2c8ab0fe53a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3485ab4b-9b22-4cd7-b6eb-283b8fc456cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a23ea186-42ca-4346-a20e-00ef663e6eef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3485ab4b-9b22-4cd7-b6eb-283b8fc456cf",
                    "LayerId": "f19173f0-82d6-453e-9378-9d6d8aadc34a"
                }
            ]
        },
        {
            "id": "8e5e5104-3e82-4761-99b3-c60e8475dbdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "compositeImage": {
                "id": "421ab0a4-b0e9-441c-bd61-843fe357d18f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5e5104-3e82-4761-99b3-c60e8475dbdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dab7db7-b6f0-4059-a375-b2cd1a369d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5e5104-3e82-4761-99b3-c60e8475dbdb",
                    "LayerId": "f19173f0-82d6-453e-9378-9d6d8aadc34a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "f19173f0-82d6-453e-9378-9d6d8aadc34a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26ff6790-c5b7-43a5-a9d2-141a8988a1b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
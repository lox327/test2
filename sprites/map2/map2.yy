{
    "id": "ba702e8b-63db-4222-97de-d7db1bf51251",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "map2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1405,
    "bbox_left": 0,
    "bbox_right": 2499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9fdcdd8-9955-489b-88ba-b5d13ecb6874",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba702e8b-63db-4222-97de-d7db1bf51251",
            "compositeImage": {
                "id": "646dd327-22a2-4e49-a812-8847e7a9a523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9fdcdd8-9955-489b-88ba-b5d13ecb6874",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9ada21e-8885-46a5-895b-b80056bd3c40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9fdcdd8-9955-489b-88ba-b5d13ecb6874",
                    "LayerId": "f8a94ff2-1e8f-4d9d-9f5a-5f84c0cf079e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1406,
    "layers": [
        {
            "id": "f8a94ff2-1e8f-4d9d-9f5a-5f84c0cf079e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba702e8b-63db-4222-97de-d7db1bf51251",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2500,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "a579f000-94d7-44a3-86be-297afd3612a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite98",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 196,
    "bbox_left": 168,
    "bbox_right": 360,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9dba6621-3915-4936-8495-6f60dde695ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a579f000-94d7-44a3-86be-297afd3612a5",
            "compositeImage": {
                "id": "9c0d6c5f-f903-4843-a2ca-df48461aaecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dba6621-3915-4936-8495-6f60dde695ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7803adcf-03fe-4fc3-ad31-2fcdce6d007f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dba6621-3915-4936-8495-6f60dde695ab",
                    "LayerId": "c5a53729-fc53-42a9-a81a-912c8b89e7a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "c5a53729-fc53-42a9-a81a-912c8b89e7a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a579f000-94d7-44a3-86be-297afd3612a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
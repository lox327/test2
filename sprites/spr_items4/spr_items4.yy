{
    "id": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e76bb96-4ca7-4243-adcb-5d27dff637d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "compositeImage": {
                "id": "dfb01307-87ca-4c66-bcb4-f6e522f0a63f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e76bb96-4ca7-4243-adcb-5d27dff637d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a700cd61-4ff3-4821-b387-a9e1f8de1bd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e76bb96-4ca7-4243-adcb-5d27dff637d9",
                    "LayerId": "fb8565db-5122-4cd7-97a0-d1b5cefdb213"
                }
            ]
        },
        {
            "id": "710b366d-1048-4e11-8be1-c1aef5e3145e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "compositeImage": {
                "id": "cd048db1-2c05-464a-a9cf-677cbbe5c4e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "710b366d-1048-4e11-8be1-c1aef5e3145e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9560ffb-974d-45a0-b795-813b597749f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "710b366d-1048-4e11-8be1-c1aef5e3145e",
                    "LayerId": "fb8565db-5122-4cd7-97a0-d1b5cefdb213"
                }
            ]
        },
        {
            "id": "3618fdf1-39c1-4896-8ca2-aef4c355763e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "compositeImage": {
                "id": "8c96265c-53d8-4d6e-9bb4-f64a59ac855c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3618fdf1-39c1-4896-8ca2-aef4c355763e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d35dadf6-a5e4-4690-a471-4351d91f3cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3618fdf1-39c1-4896-8ca2-aef4c355763e",
                    "LayerId": "fb8565db-5122-4cd7-97a0-d1b5cefdb213"
                }
            ]
        },
        {
            "id": "fbdb2172-90d3-4552-8953-c8bb3a9f2ea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "compositeImage": {
                "id": "33bb9c4a-bbb6-4b64-8d0a-18f7a0dcebca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbdb2172-90d3-4552-8953-c8bb3a9f2ea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96e93d85-bca6-47b3-a56e-e4abe6482276",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbdb2172-90d3-4552-8953-c8bb3a9f2ea0",
                    "LayerId": "fb8565db-5122-4cd7-97a0-d1b5cefdb213"
                }
            ]
        },
        {
            "id": "b3615c6e-0b4a-4db7-baf1-21e70889dc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "compositeImage": {
                "id": "9d0ef219-0185-4c10-96ba-93e3b3aa20eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3615c6e-0b4a-4db7-baf1-21e70889dc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef891ca8-fda3-4636-85fc-1da2af508c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3615c6e-0b4a-4db7-baf1-21e70889dc07",
                    "LayerId": "fb8565db-5122-4cd7-97a0-d1b5cefdb213"
                }
            ]
        },
        {
            "id": "26dea49e-7e12-414a-9c6c-9a60d4da1116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "compositeImage": {
                "id": "8c1c607b-76c5-4cf1-af14-3f8a94756ea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26dea49e-7e12-414a-9c6c-9a60d4da1116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "277b2b77-d8c2-4297-b4cb-dd9e2eef5c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26dea49e-7e12-414a-9c6c-9a60d4da1116",
                    "LayerId": "fb8565db-5122-4cd7-97a0-d1b5cefdb213"
                }
            ]
        },
        {
            "id": "cf38377b-17d1-4643-9d2a-29b82ce1b128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "compositeImage": {
                "id": "348a9325-9fa9-4041-a899-f1a60aa6f524",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf38377b-17d1-4643-9d2a-29b82ce1b128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f49d890-d65c-4063-8716-60c3df25ece2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf38377b-17d1-4643-9d2a-29b82ce1b128",
                    "LayerId": "fb8565db-5122-4cd7-97a0-d1b5cefdb213"
                }
            ]
        },
        {
            "id": "8be20f7a-645a-49a3-ac11-4cb56f767f84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "compositeImage": {
                "id": "f82a9190-6c1b-4685-a880-e19761d67f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8be20f7a-645a-49a3-ac11-4cb56f767f84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07280b19-e56c-4589-b510-50684ff9694d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8be20f7a-645a-49a3-ac11-4cb56f767f84",
                    "LayerId": "fb8565db-5122-4cd7-97a0-d1b5cefdb213"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fb8565db-5122-4cd7-97a0-d1b5cefdb213",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40fdf3de-ccf4-4f42-b323-a6e619736b2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
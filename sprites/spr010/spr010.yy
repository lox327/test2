{
    "id": "f4f19498-a4d2-45e1-95bc-4ec1022b0f3b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr010",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25370a2d-ab74-46e1-8f57-8efcd499d315",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4f19498-a4d2-45e1-95bc-4ec1022b0f3b",
            "compositeImage": {
                "id": "c8005ac1-0016-447b-a65b-4fbb8ebc556d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25370a2d-ab74-46e1-8f57-8efcd499d315",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f34872a-071a-4d9a-9296-5f55a12d1afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25370a2d-ab74-46e1-8f57-8efcd499d315",
                    "LayerId": "a6332a20-658b-4f4e-bed6-402c7c265b7b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "a6332a20-658b-4f4e-bed6-402c7c265b7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4f19498-a4d2-45e1-95bc-4ec1022b0f3b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "50849815-c04c-49fa-9bbd-5615fbbb0ebd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "artifact",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03322b31-42a2-45cf-8c56-0dd4b6ef384f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50849815-c04c-49fa-9bbd-5615fbbb0ebd",
            "compositeImage": {
                "id": "ac999316-7453-4175-94a7-9bb4f473c76e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03322b31-42a2-45cf-8c56-0dd4b6ef384f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef8e12c2-0c9f-4bc2-a4ff-80695443bec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03322b31-42a2-45cf-8c56-0dd4b6ef384f",
                    "LayerId": "b178bc90-e7bf-4d1d-b7ff-64bfed698920"
                }
            ]
        },
        {
            "id": "2f16ed2a-5266-457f-9242-140c6e5e6888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50849815-c04c-49fa-9bbd-5615fbbb0ebd",
            "compositeImage": {
                "id": "5d3d0b49-4fbf-4364-9adf-fa935032bf74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f16ed2a-5266-457f-9242-140c6e5e6888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fe32383-580e-4919-9b79-e2ef0f3658e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f16ed2a-5266-457f-9242-140c6e5e6888",
                    "LayerId": "b178bc90-e7bf-4d1d-b7ff-64bfed698920"
                }
            ]
        },
        {
            "id": "6e124925-5c41-4d08-8cff-2bb29f1d96a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50849815-c04c-49fa-9bbd-5615fbbb0ebd",
            "compositeImage": {
                "id": "28db7133-6255-430a-8c8b-d1a7c8b087a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e124925-5c41-4d08-8cff-2bb29f1d96a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4428f41-18de-446e-9a02-f4fd266fb44c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e124925-5c41-4d08-8cff-2bb29f1d96a4",
                    "LayerId": "b178bc90-e7bf-4d1d-b7ff-64bfed698920"
                }
            ]
        },
        {
            "id": "742a0180-0fb6-46ad-a89b-fc80952570d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50849815-c04c-49fa-9bbd-5615fbbb0ebd",
            "compositeImage": {
                "id": "32ad2b9f-69d7-44ff-9ea8-cc0dce56e984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742a0180-0fb6-46ad-a89b-fc80952570d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9409af11-f1de-4395-8ff3-01263ba24a6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742a0180-0fb6-46ad-a89b-fc80952570d0",
                    "LayerId": "b178bc90-e7bf-4d1d-b7ff-64bfed698920"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b178bc90-e7bf-4d1d-b7ff-64bfed698920",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50849815-c04c-49fa-9bbd-5615fbbb0ebd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
{
    "id": "b97a78de-e82c-4149-815b-d9ced56e13b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34ecb75d-dfb9-4a6a-82fa-48e18044099a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b97a78de-e82c-4149-815b-d9ced56e13b7",
            "compositeImage": {
                "id": "a288551b-f76a-4790-abda-433b098887b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ecb75d-dfb9-4a6a-82fa-48e18044099a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14114bd4-ad74-468f-b24b-9bd98f4d176d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ecb75d-dfb9-4a6a-82fa-48e18044099a",
                    "LayerId": "e7a16267-3040-4a2e-b451-6fa09a6f385e"
                },
                {
                    "id": "08b633d6-68f0-4615-a46c-bb0d479b2782",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ecb75d-dfb9-4a6a-82fa-48e18044099a",
                    "LayerId": "031a8ce8-6576-4ba5-b340-ee9317ffdd90"
                },
                {
                    "id": "afb83f91-cc8e-454c-9998-93fd0dc46398",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ecb75d-dfb9-4a6a-82fa-48e18044099a",
                    "LayerId": "ce226f9f-b2ce-46c4-a758-ff4e0492f1f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "e7a16267-3040-4a2e-b451-6fa09a6f385e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b97a78de-e82c-4149-815b-d9ced56e13b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 14,
            "visible": true
        },
        {
            "id": "031a8ce8-6576-4ba5-b340-ee9317ffdd90",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b97a78de-e82c-4149-815b-d9ced56e13b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 35,
            "visible": true
        },
        {
            "id": "ce226f9f-b2ce-46c4-a758-ff4e0492f1f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b97a78de-e82c-4149-815b-d9ced56e13b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}
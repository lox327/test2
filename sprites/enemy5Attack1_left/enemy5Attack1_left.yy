{
    "id": "765b8832-6ed2-4b0f-a2a6-a6d31e056f0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Attack1_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d7cd13d-b76f-424d-a54f-440b786cb56f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "765b8832-6ed2-4b0f-a2a6-a6d31e056f0c",
            "compositeImage": {
                "id": "1884e88f-a13b-426f-9c75-0626b67c1f4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7cd13d-b76f-424d-a54f-440b786cb56f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59c17d0e-687a-4b2d-bcb9-ab888485e497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7cd13d-b76f-424d-a54f-440b786cb56f",
                    "LayerId": "9cdbf452-0aec-4fb0-9fa1-b24549b014bf"
                }
            ]
        },
        {
            "id": "26b0d42d-efdb-45f5-8f9d-aedd28449299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "765b8832-6ed2-4b0f-a2a6-a6d31e056f0c",
            "compositeImage": {
                "id": "b37b7ee0-f71e-489b-9182-4fe410fa0c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26b0d42d-efdb-45f5-8f9d-aedd28449299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f91e7ba-fcfe-4718-a04c-3162ec792c66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26b0d42d-efdb-45f5-8f9d-aedd28449299",
                    "LayerId": "9cdbf452-0aec-4fb0-9fa1-b24549b014bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "9cdbf452-0aec-4fb0-9fa1-b24549b014bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "765b8832-6ed2-4b0f-a2a6-a6d31e056f0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "3d6e8576-2a0e-43aa-8bab-ada99cae9b55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4c2e421-9801-4d1e-b5cf-68b976299c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d6e8576-2a0e-43aa-8bab-ada99cae9b55",
            "compositeImage": {
                "id": "f7b11a51-6f25-40e8-9313-36087770ea8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4c2e421-9801-4d1e-b5cf-68b976299c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22ed06a5-f16d-4b4f-9e0c-a7a29a8bed25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4c2e421-9801-4d1e-b5cf-68b976299c5c",
                    "LayerId": "73ce2b7c-fb02-4002-ac07-aff6ae0a6edf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "73ce2b7c-fb02-4002-ac07-aff6ae0a6edf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d6e8576-2a0e-43aa-8bab-ada99cae9b55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "1015778a-0d53-4e50-9a97-4ab5c3b68e92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite136",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89acae44-eb4d-488a-887e-ed8bceb4d47a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1015778a-0d53-4e50-9a97-4ab5c3b68e92",
            "compositeImage": {
                "id": "8e5dbc55-f157-489b-bb35-62beffaf50ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89acae44-eb4d-488a-887e-ed8bceb4d47a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db503dcd-6b14-455b-b9bd-619b7c468095",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89acae44-eb4d-488a-887e-ed8bceb4d47a",
                    "LayerId": "5a298060-8066-4fd5-bb0a-f17a69a5b810"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "5a298060-8066-4fd5-bb0a-f17a69a5b810",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1015778a-0d53-4e50-9a97-4ab5c3b68e92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
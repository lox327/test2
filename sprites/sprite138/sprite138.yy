{
    "id": "d7a498d0-4d1b-4cbb-acf5-6fca8b092f35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite138",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 0,
    "bbox_right": 583,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c385588c-102b-4730-9daf-b3389e81b06d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7a498d0-4d1b-4cbb-acf5-6fca8b092f35",
            "compositeImage": {
                "id": "2cdf543f-e8dc-47be-89d3-98054646e984",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c385588c-102b-4730-9daf-b3389e81b06d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3335bed5-dddd-45bf-9598-c6db42e53248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c385588c-102b-4730-9daf-b3389e81b06d",
                    "LayerId": "6c14ab89-f9f1-4d07-a744-0aee147c53ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "6c14ab89-f9f1-4d07-a744-0aee147c53ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7a498d0-4d1b-4cbb-acf5-6fca8b092f35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 584,
    "xorig": 0,
    "yorig": 0
}
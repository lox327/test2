{
    "id": "f3b693ec-27d8-4019-adca-7f447ce3e8fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bullet4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b728164b-98fe-41e8-8716-6985491bc946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b693ec-27d8-4019-adca-7f447ce3e8fe",
            "compositeImage": {
                "id": "e1e037dc-c927-4e37-b9d2-33c70856cfd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b728164b-98fe-41e8-8716-6985491bc946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2394e6e-28ce-40de-9caa-8afb429c8450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b728164b-98fe-41e8-8716-6985491bc946",
                    "LayerId": "e9504278-e05a-4c14-b366-7019b51b909b"
                }
            ]
        },
        {
            "id": "45306d55-3674-436e-9ce4-f2ed0231c7cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3b693ec-27d8-4019-adca-7f447ce3e8fe",
            "compositeImage": {
                "id": "dd96d7dc-d132-4232-a330-c864451bd1c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45306d55-3674-436e-9ce4-f2ed0231c7cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec5f7cfb-ba4d-47fd-bf5a-a749c033f42d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45306d55-3674-436e-9ce4-f2ed0231c7cf",
                    "LayerId": "e9504278-e05a-4c14-b366-7019b51b909b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e9504278-e05a-4c14-b366-7019b51b909b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3b693ec-27d8-4019-adca-7f447ce3e8fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
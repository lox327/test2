{
    "id": "3516e0d6-22a7-4e51-885b-d53d1d1933f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lbl_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 18,
    "bbox_right": 44,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "676e11ee-0fe3-4759-96da-6cd4384a06b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3516e0d6-22a7-4e51-885b-d53d1d1933f1",
            "compositeImage": {
                "id": "13f501fd-7f89-4edb-9254-2f039fdfd9c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676e11ee-0fe3-4759-96da-6cd4384a06b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc1f7102-539b-48a9-aab1-f36faa22a904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676e11ee-0fe3-4759-96da-6cd4384a06b0",
                    "LayerId": "d60ee294-8200-4e1a-aac8-862e0a5beb01"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d60ee294-8200-4e1a-aac8-862e0a5beb01",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3516e0d6-22a7-4e51-885b-d53d1d1933f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
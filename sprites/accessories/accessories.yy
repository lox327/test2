{
    "id": "ae383281-6a9d-45f3-bb8f-6b39581e234b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "accessories",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 258,
    "bbox_left": 2,
    "bbox_right": 473,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82d3d068-4f7f-4744-a114-3e7134216439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae383281-6a9d-45f3-bb8f-6b39581e234b",
            "compositeImage": {
                "id": "44f3345d-72b2-4641-912e-e253e51616c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d3d068-4f7f-4744-a114-3e7134216439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "578f12c0-be81-4c41-83fc-4dd616bb230c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d3d068-4f7f-4744-a114-3e7134216439",
                    "LayerId": "58324809-1c6e-4562-8ed3-6a3e8f19a54e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "58324809-1c6e-4562-8ed3-6a3e8f19a54e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae383281-6a9d-45f3-bb8f-6b39581e234b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
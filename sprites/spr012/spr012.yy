{
    "id": "f15c753d-47b2-4e5b-b7ba-0550a2b90355",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr012",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5304062-6eb6-40cc-82a7-ffe61f89873e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f15c753d-47b2-4e5b-b7ba-0550a2b90355",
            "compositeImage": {
                "id": "770b3076-30fa-41ee-aaf2-4ab3d03e5387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5304062-6eb6-40cc-82a7-ffe61f89873e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a6b8d36-d4eb-43f4-9bec-49eb21a26f74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5304062-6eb6-40cc-82a7-ffe61f89873e",
                    "LayerId": "615157bb-cd8a-4c24-942e-cf00f9aa8e41"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "615157bb-cd8a-4c24-942e-cf00f9aa8e41",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f15c753d-47b2-4e5b-b7ba-0550a2b90355",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}
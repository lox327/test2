{
    "id": "38d8d713-a0ff-4424-ac60-9d03322f4db2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightman",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 3,
    "bbox_right": 99,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ee43978-dc46-49ae-8fc7-c61160bcfe37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38d8d713-a0ff-4424-ac60-9d03322f4db2",
            "compositeImage": {
                "id": "26f54db7-d9b6-4703-bba8-6975bf00aa1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee43978-dc46-49ae-8fc7-c61160bcfe37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "981f2085-1b76-41e1-9d29-7e4703bd90b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee43978-dc46-49ae-8fc7-c61160bcfe37",
                    "LayerId": "e33fbbe4-5266-407c-b20b-3d101c491ae0"
                }
            ]
        },
        {
            "id": "f44f224f-b3b5-4c92-97ac-8c585e178cb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38d8d713-a0ff-4424-ac60-9d03322f4db2",
            "compositeImage": {
                "id": "e33e0010-2ee3-4ab9-8fc9-2f9aac63295d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f44f224f-b3b5-4c92-97ac-8c585e178cb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed7a4f39-7020-43ba-96ff-a696fc6cbe99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f44f224f-b3b5-4c92-97ac-8c585e178cb6",
                    "LayerId": "e33fbbe4-5266-407c-b20b-3d101c491ae0"
                }
            ]
        },
        {
            "id": "d8b83971-2885-44c8-aeb7-dda4c4b6620f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38d8d713-a0ff-4424-ac60-9d03322f4db2",
            "compositeImage": {
                "id": "c02bfbe8-2a9f-49bb-9c08-d46282544840",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b83971-2885-44c8-aeb7-dda4c4b6620f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ac03bc4-ec20-418d-b032-a12d803fd505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b83971-2885-44c8-aeb7-dda4c4b6620f",
                    "LayerId": "e33fbbe4-5266-407c-b20b-3d101c491ae0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "e33fbbe4-5266-407c-b20b-3d101c491ae0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38d8d713-a0ff-4424-ac60-9d03322f4db2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 103,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "658c1115-15fb-4411-9e80-20da052e990d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 27,
    "bbox_right": 45,
    "bbox_top": 43,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d94ba67-d21f-45d7-a4dc-c8025f7e3ac5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "aad5a59f-30ab-42eb-986a-d4155d9df74a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d94ba67-d21f-45d7-a4dc-c8025f7e3ac5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cddaced-45bf-4aac-9a03-5014f759cc11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d94ba67-d21f-45d7-a4dc-c8025f7e3ac5",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "39f91516-9a1f-4f8a-86ad-60bd489aea47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "7b65b09a-b81d-4898-a173-c76b5d35d817",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39f91516-9a1f-4f8a-86ad-60bd489aea47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1ea460f-5cba-4aad-8b46-13a27253a0d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39f91516-9a1f-4f8a-86ad-60bd489aea47",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "eb7ed6e6-cdc9-4598-a835-1215fd4e3400",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "86f5e57d-23b5-4e6c-9583-0da3fd5d69b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb7ed6e6-cdc9-4598-a835-1215fd4e3400",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4058e62c-abcd-4a83-bced-c909e74aaa7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb7ed6e6-cdc9-4598-a835-1215fd4e3400",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "a376072a-108d-475c-a4e6-749b7ac999da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "426b0597-bb1c-44fc-b442-d3f9652c441c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a376072a-108d-475c-a4e6-749b7ac999da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "beeafb6c-ef77-4f5f-83a8-923ca02110f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a376072a-108d-475c-a4e6-749b7ac999da",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "a7d17bc2-157e-4858-aeb9-1a8ff173b9e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "48473ed3-6af5-49f6-95b8-79610b5db228",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7d17bc2-157e-4858-aeb9-1a8ff173b9e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3720a149-b4f7-44bb-82ca-84c0a622e4dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d17bc2-157e-4858-aeb9-1a8ff173b9e4",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "d867698a-22eb-4175-8730-d2e574cfd721",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "cace005a-cd5c-487d-a9c0-aa6eeaa49a19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d867698a-22eb-4175-8730-d2e574cfd721",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba005b7d-8882-4052-a108-ad8f1cc3f124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d867698a-22eb-4175-8730-d2e574cfd721",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "0f78406c-e6cf-496a-82e4-30d2f16505bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "b1e46c88-f98d-465b-bfad-b8cee84a07b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f78406c-e6cf-496a-82e4-30d2f16505bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca73dbd4-d3f5-4098-b48a-e9ea66872183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f78406c-e6cf-496a-82e4-30d2f16505bc",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "0c194441-0954-49d4-a526-9b39948c157a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "412ee48b-3d5f-4f98-97ae-46c80c56a48c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c194441-0954-49d4-a526-9b39948c157a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39cda605-47c7-45e9-b257-03e8713b74df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c194441-0954-49d4-a526-9b39948c157a",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "e32d54a7-1b80-45c7-8c00-8030966059ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "98468b44-1e67-4725-9632-04ea07c2dc02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e32d54a7-1b80-45c7-8c00-8030966059ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f01c325b-690a-4b36-91b8-1b422dc696dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e32d54a7-1b80-45c7-8c00-8030966059ce",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "065c6f27-508a-48a8-8f98-4073c800f27e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "85d4e667-f477-4230-91f8-3b17eac0a3e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "065c6f27-508a-48a8-8f98-4073c800f27e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3e97f60-6008-429c-afb4-5e0145bf08d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "065c6f27-508a-48a8-8f98-4073c800f27e",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "02680818-eebb-4dee-8011-6cade5147caa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "d482b561-2e91-4e6a-a3f8-660c18aa346d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02680818-eebb-4dee-8011-6cade5147caa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88f4464-a7d2-400d-8478-1cbd075604e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02680818-eebb-4dee-8011-6cade5147caa",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "aab1ea17-c30e-482c-8cf2-28fbdc33ecaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "72b0c3d8-64a9-4c29-94c3-86d1918148c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aab1ea17-c30e-482c-8cf2-28fbdc33ecaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d57d32d-3d38-4628-bb3f-d53cd6347bc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aab1ea17-c30e-482c-8cf2-28fbdc33ecaa",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "ad52d681-c0b2-4fd2-80dc-e5b8f458ec71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "6fe0de65-b26b-4609-97c7-c9b98bde75dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad52d681-c0b2-4fd2-80dc-e5b8f458ec71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6379033d-c895-4525-82cf-fafbfdfe79c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad52d681-c0b2-4fd2-80dc-e5b8f458ec71",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "882435f2-5f2a-488b-9459-ec8b2c3b9c34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "f75ea630-70d0-4c9d-a03d-cc06445c3b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "882435f2-5f2a-488b-9459-ec8b2c3b9c34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9ef4402-b735-4479-b2e7-a9abbd395478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "882435f2-5f2a-488b-9459-ec8b2c3b9c34",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "f5994dbe-02ef-4375-93aa-1e82c27c5f72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "fa0c0972-45ba-4a08-8332-452ba87e5162",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5994dbe-02ef-4375-93aa-1e82c27c5f72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c02d79e-485a-4d8d-849b-79fe82d3112f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5994dbe-02ef-4375-93aa-1e82c27c5f72",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        },
        {
            "id": "e91ef14c-7202-4eb9-bf26-43ea4823c5b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "compositeImage": {
                "id": "cd0d294e-7de7-4a1b-9808-904f3573faab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e91ef14c-7202-4eb9-bf26-43ea4823c5b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a44a4784-d236-42cb-84ee-1663ccf8c85a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e91ef14c-7202-4eb9-bf26-43ea4823c5b1",
                    "LayerId": "90035236-f074-4e31-933a-14277bf3095c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "90035236-f074-4e31-933a-14277bf3095c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "658c1115-15fb-4411-9e80-20da052e990d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 35
}
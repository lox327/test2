{
    "id": "1abed393-d683-48df-a1b2-4580a2f2cab8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light2_bak",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 251,
    "bbox_left": 2,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e211a3c6-0435-43ba-8444-bd4b6527b7a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1abed393-d683-48df-a1b2-4580a2f2cab8",
            "compositeImage": {
                "id": "de02e76e-8cf6-4baf-ad69-1b62b3a20f69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e211a3c6-0435-43ba-8444-bd4b6527b7a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fbdae99-69db-4d15-be90-aa6f077e3e49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e211a3c6-0435-43ba-8444-bd4b6527b7a6",
                    "LayerId": "245168cf-d67d-4c8d-8219-bb589fa4c332"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "245168cf-d67d-4c8d-8219-bb589fa4c332",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1abed393-d683-48df-a1b2-4580a2f2cab8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}
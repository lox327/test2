{
    "id": "06306863-baf6-4a0e-9f38-130cfa06eecc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarNewt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 4,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "453a5eaa-23a9-42f2-b200-a9794750230e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06306863-baf6-4a0e-9f38-130cfa06eecc",
            "compositeImage": {
                "id": "11a1b917-68cd-43ad-bac7-8cabbb023530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "453a5eaa-23a9-42f2-b200-a9794750230e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "141aec38-2e9d-4dae-8b70-3eccf124e254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "453a5eaa-23a9-42f2-b200-a9794750230e",
                    "LayerId": "2d502572-f5e3-40bc-8279-d0ec684f1893"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "2d502572-f5e3-40bc-8279-d0ec684f1893",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06306863-baf6-4a0e-9f38-130cfa06eecc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
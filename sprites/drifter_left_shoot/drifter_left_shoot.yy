{
    "id": "e32f5f2a-9827-4ca7-9f2c-0b7b5f210c1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "drifter_left_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 20,
    "bbox_right": 39,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11d60cfa-76cb-44f8-9a5b-4481c64239bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e32f5f2a-9827-4ca7-9f2c-0b7b5f210c1a",
            "compositeImage": {
                "id": "c476f4e3-4cb5-4792-87b3-2e896e9daf47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11d60cfa-76cb-44f8-9a5b-4481c64239bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "502d362b-1353-41b6-9c36-cee252f1ed2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11d60cfa-76cb-44f8-9a5b-4481c64239bb",
                    "LayerId": "d75f0b1e-23c8-43d0-bc8f-e39bf71df3ed"
                }
            ]
        },
        {
            "id": "e6ed36aa-01c9-4fd1-adad-6fed89cba619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e32f5f2a-9827-4ca7-9f2c-0b7b5f210c1a",
            "compositeImage": {
                "id": "b85b5bd7-f37c-40a5-bcfe-5cc8a4912173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6ed36aa-01c9-4fd1-adad-6fed89cba619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73a6649b-9a05-473b-bc1a-03f6b6c14deb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6ed36aa-01c9-4fd1-adad-6fed89cba619",
                    "LayerId": "d75f0b1e-23c8-43d0-bc8f-e39bf71df3ed"
                }
            ]
        },
        {
            "id": "35420522-6d3d-4b4c-893d-cdb8de2cd285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e32f5f2a-9827-4ca7-9f2c-0b7b5f210c1a",
            "compositeImage": {
                "id": "386c42b1-bd77-4fb5-8a12-8b4d7692a789",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35420522-6d3d-4b4c-893d-cdb8de2cd285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70907ecd-7f39-43a6-b3ab-1919e945832d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35420522-6d3d-4b4c-893d-cdb8de2cd285",
                    "LayerId": "d75f0b1e-23c8-43d0-bc8f-e39bf71df3ed"
                }
            ]
        },
        {
            "id": "049b3b2a-d1a6-4ab3-bebd-d36e04287a0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e32f5f2a-9827-4ca7-9f2c-0b7b5f210c1a",
            "compositeImage": {
                "id": "40cd66cc-28e7-4fed-8eb8-198dc484e1ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "049b3b2a-d1a6-4ab3-bebd-d36e04287a0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "994a267f-174a-497a-8cdb-278f6209293f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "049b3b2a-d1a6-4ab3-bebd-d36e04287a0a",
                    "LayerId": "d75f0b1e-23c8-43d0-bc8f-e39bf71df3ed"
                }
            ]
        },
        {
            "id": "f6654df9-2d98-4d37-b0ab-d230278aaeb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e32f5f2a-9827-4ca7-9f2c-0b7b5f210c1a",
            "compositeImage": {
                "id": "2d4bcc79-0901-4ce4-8aaf-531a46fee800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6654df9-2d98-4d37-b0ab-d230278aaeb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2255037e-4c72-4326-a763-267e809cbb92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6654df9-2d98-4d37-b0ab-d230278aaeb5",
                    "LayerId": "d75f0b1e-23c8-43d0-bc8f-e39bf71df3ed"
                }
            ]
        },
        {
            "id": "22611b36-bc93-413b-b426-ee4f8acd59fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e32f5f2a-9827-4ca7-9f2c-0b7b5f210c1a",
            "compositeImage": {
                "id": "7799c47d-b7bc-4ec9-a6a2-874a23ad8070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22611b36-bc93-413b-b426-ee4f8acd59fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4f280e4-5851-41ab-865d-89b5f37d703d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22611b36-bc93-413b-b426-ee4f8acd59fa",
                    "LayerId": "d75f0b1e-23c8-43d0-bc8f-e39bf71df3ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "d75f0b1e-23c8-43d0-bc8f-e39bf71df3ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e32f5f2a-9827-4ca7-9f2c-0b7b5f210c1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
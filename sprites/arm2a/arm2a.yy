{
    "id": "a82baa86-5ab9-45ce-9377-cdbb53cedecd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "arm2a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 295,
    "bbox_left": 5,
    "bbox_right": 81,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b8b901fa-0fde-48ee-ab64-5dd57446de76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a82baa86-5ab9-45ce-9377-cdbb53cedecd",
            "compositeImage": {
                "id": "7ce0eda7-52f9-4c1b-8bf9-8bca84e09a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8b901fa-0fde-48ee-ab64-5dd57446de76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62c33a67-38aa-4ed9-be58-db4b40fe1b32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8b901fa-0fde-48ee-ab64-5dd57446de76",
                    "LayerId": "55d5fa8e-02d8-4ba3-9e97-5942352ade82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "55d5fa8e-02d8-4ba3-9e97-5942352ade82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a82baa86-5ab9-45ce-9377-cdbb53cedecd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 85,
    "xorig": 0,
    "yorig": 0
}
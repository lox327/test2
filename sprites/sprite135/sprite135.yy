{
    "id": "0c636bd6-9b49-4ed4-aec3-c9be142beff8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite135",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96e2dde3-022e-477e-ba58-700c4774937a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c636bd6-9b49-4ed4-aec3-c9be142beff8",
            "compositeImage": {
                "id": "3ebba499-3b9e-4162-825e-ae88f2dc5294",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96e2dde3-022e-477e-ba58-700c4774937a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610e4d7a-7f45-40c9-9fbb-5f0deed4c45e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96e2dde3-022e-477e-ba58-700c4774937a",
                    "LayerId": "42ec611b-2b7d-4319-9246-825977a6c766"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "42ec611b-2b7d-4319-9246-825977a6c766",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c636bd6-9b49-4ed4-aec3-c9be142beff8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 0,
    "yorig": 0
}
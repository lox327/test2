{
    "id": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "drifter_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 12,
    "bbox_right": 29,
    "bbox_top": 37,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f698f3a-c202-4987-9843-fdb4dde302b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "compositeImage": {
                "id": "049ca957-418e-4123-ae74-030dffeddea6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f698f3a-c202-4987-9843-fdb4dde302b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa191aba-725a-463e-aa6b-ef7446ae4309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f698f3a-c202-4987-9843-fdb4dde302b4",
                    "LayerId": "4900e622-d0ba-4296-929b-0baee73d07c4"
                }
            ]
        },
        {
            "id": "f244ad41-5bd4-4aa2-95a0-9df012f82b7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "compositeImage": {
                "id": "61802229-51a8-4b4e-9c32-0a2b1abc5d47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f244ad41-5bd4-4aa2-95a0-9df012f82b7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e8f9010-691f-4c52-b12b-80ab4a18451a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f244ad41-5bd4-4aa2-95a0-9df012f82b7e",
                    "LayerId": "4900e622-d0ba-4296-929b-0baee73d07c4"
                }
            ]
        },
        {
            "id": "3e18e3ae-1e9a-4d7f-894e-b2e7cd5453bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "compositeImage": {
                "id": "4ca4edb0-34ee-4c3c-a735-90ce88fb0b62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e18e3ae-1e9a-4d7f-894e-b2e7cd5453bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cd2759e-b8b3-466f-b79c-a5ad36872e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e18e3ae-1e9a-4d7f-894e-b2e7cd5453bf",
                    "LayerId": "4900e622-d0ba-4296-929b-0baee73d07c4"
                }
            ]
        },
        {
            "id": "e333bdcc-60af-49c5-ab22-3d5ef2fb5033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "compositeImage": {
                "id": "35e9b2fb-cf02-4961-8a0f-9b9cd4b1e470",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e333bdcc-60af-49c5-ab22-3d5ef2fb5033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c6f489a-dff3-47f7-9549-267b339a1b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e333bdcc-60af-49c5-ab22-3d5ef2fb5033",
                    "LayerId": "4900e622-d0ba-4296-929b-0baee73d07c4"
                }
            ]
        },
        {
            "id": "91097c8f-390f-4d7e-911b-d854d4a09c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "compositeImage": {
                "id": "e2981f10-f84d-4ef8-83cf-0f4ac8afd09b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91097c8f-390f-4d7e-911b-d854d4a09c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d6de602-d1b4-483d-89b7-78ce2eb09187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91097c8f-390f-4d7e-911b-d854d4a09c7b",
                    "LayerId": "4900e622-d0ba-4296-929b-0baee73d07c4"
                }
            ]
        },
        {
            "id": "63f66ce2-62d7-4019-bdcf-648b9625a9b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "compositeImage": {
                "id": "646bb442-f11d-42e5-8c31-a5fbaef10f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63f66ce2-62d7-4019-bdcf-648b9625a9b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34fb190-4860-4531-a62d-7d3cbdd00894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63f66ce2-62d7-4019-bdcf-648b9625a9b5",
                    "LayerId": "4900e622-d0ba-4296-929b-0baee73d07c4"
                }
            ]
        },
        {
            "id": "bc73319b-9815-422f-8e2e-b191dbbf5431",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "compositeImage": {
                "id": "15d3b8f7-73a1-48a4-adcf-0550c3cdf990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc73319b-9815-422f-8e2e-b191dbbf5431",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05b0e793-575b-4907-9022-5515de88b71c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc73319b-9815-422f-8e2e-b191dbbf5431",
                    "LayerId": "4900e622-d0ba-4296-929b-0baee73d07c4"
                }
            ]
        },
        {
            "id": "034e4d3a-df4d-4031-bdc0-9fc5ec183c5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "compositeImage": {
                "id": "c875a374-254c-4ebd-b73f-f94d2c8e80c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "034e4d3a-df4d-4031-bdc0-9fc5ec183c5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff296de7-aa22-4072-87b0-1869634a4fff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "034e4d3a-df4d-4031-bdc0-9fc5ec183c5e",
                    "LayerId": "4900e622-d0ba-4296-929b-0baee73d07c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "4900e622-d0ba-4296-929b-0baee73d07c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa1a6502-6b15-4b9b-a1b2-6935b83d13df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 30
}
{
    "id": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 5,
    "bbox_right": 42,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db315aa2-49df-4e3a-b157-269c07f2d1b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "compositeImage": {
                "id": "c82d4304-42c0-48dd-9915-0689b91906c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db315aa2-49df-4e3a-b157-269c07f2d1b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601e26f4-524c-4896-9c52-3111bd5ace1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db315aa2-49df-4e3a-b157-269c07f2d1b1",
                    "LayerId": "4502372d-6773-4784-bb4a-9b701e4f95f8"
                }
            ]
        },
        {
            "id": "82cd5d7a-0fd8-4769-a230-370e3a5a0d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "compositeImage": {
                "id": "4f588496-4288-43bc-bbab-9995fc4ccfa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82cd5d7a-0fd8-4769-a230-370e3a5a0d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "472e550f-259e-421b-a9d3-9156193797eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82cd5d7a-0fd8-4769-a230-370e3a5a0d12",
                    "LayerId": "4502372d-6773-4784-bb4a-9b701e4f95f8"
                }
            ]
        },
        {
            "id": "4bcb0922-1412-480d-a5ca-875891fca426",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "compositeImage": {
                "id": "3ae2e7c9-fac7-4a34-bd43-9ea8b2f541f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bcb0922-1412-480d-a5ca-875891fca426",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1225f825-799d-45a2-82d2-6e31a4e63525",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bcb0922-1412-480d-a5ca-875891fca426",
                    "LayerId": "4502372d-6773-4784-bb4a-9b701e4f95f8"
                }
            ]
        },
        {
            "id": "9701ded0-e36d-487b-aa73-f3d9b44f578b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "compositeImage": {
                "id": "0e263d69-e226-40c6-9d78-a14c27b6deb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9701ded0-e36d-487b-aa73-f3d9b44f578b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bb2ba51-9dfd-4c88-889f-f0392a94ff26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9701ded0-e36d-487b-aa73-f3d9b44f578b",
                    "LayerId": "4502372d-6773-4784-bb4a-9b701e4f95f8"
                }
            ]
        },
        {
            "id": "92bba0d4-c557-4fb8-ab82-15e25d6ba90c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "compositeImage": {
                "id": "e84b9b2c-d68e-46c1-9acd-22bf391b916e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92bba0d4-c557-4fb8-ab82-15e25d6ba90c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29cbc4cc-302e-476f-8a0c-ad24e5db3285",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92bba0d4-c557-4fb8-ab82-15e25d6ba90c",
                    "LayerId": "4502372d-6773-4784-bb4a-9b701e4f95f8"
                }
            ]
        },
        {
            "id": "042bd0dc-7d2e-496e-887d-1e0813376a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "compositeImage": {
                "id": "c3812449-c4a4-4bcd-9416-154f2263992a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "042bd0dc-7d2e-496e-887d-1e0813376a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a7671f9-d540-4804-a614-bfd125708495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "042bd0dc-7d2e-496e-887d-1e0813376a06",
                    "LayerId": "4502372d-6773-4784-bb4a-9b701e4f95f8"
                }
            ]
        },
        {
            "id": "47f6de51-81e1-4918-b388-78f5a291a983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "compositeImage": {
                "id": "1c0604aa-18bf-4f0e-ad48-80a6906892ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47f6de51-81e1-4918-b388-78f5a291a983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55914026-afa3-4c52-8fd1-5f4e6ed8cd83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47f6de51-81e1-4918-b388-78f5a291a983",
                    "LayerId": "4502372d-6773-4784-bb4a-9b701e4f95f8"
                }
            ]
        },
        {
            "id": "ec6d7415-a42d-4994-8145-2c8c12988fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "compositeImage": {
                "id": "7e71ac0e-a13d-495a-ac05-77b86a6d2196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec6d7415-a42d-4994-8145-2c8c12988fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3574f9b-abd9-4b45-bcb2-9c75a1f4b1a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec6d7415-a42d-4994-8145-2c8c12988fa6",
                    "LayerId": "4502372d-6773-4784-bb4a-9b701e4f95f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "4502372d-6773-4784-bb4a-9b701e4f95f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "057f0cba-4f7c-48e3-b80c-688fb3bbf029",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "1aece566-4901-46ce-b2f9-76703eda03e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "random1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 16,
    "bbox_right": 47,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e3a56c0-83d3-4c6f-8651-803a2655946f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aece566-4901-46ce-b2f9-76703eda03e0",
            "compositeImage": {
                "id": "de4f38cf-c33b-420a-b356-733e478bec5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e3a56c0-83d3-4c6f-8651-803a2655946f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ce49f19-0cb5-417d-9f9f-5590202bc7a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e3a56c0-83d3-4c6f-8651-803a2655946f",
                    "LayerId": "abaeadb3-861a-4f22-bf6d-514c4bfd68cb"
                }
            ]
        },
        {
            "id": "141f7063-c007-40f6-bdc7-b8e75e9cbf24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aece566-4901-46ce-b2f9-76703eda03e0",
            "compositeImage": {
                "id": "756d1b42-9bb7-4d31-9641-2e62367e2bab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "141f7063-c007-40f6-bdc7-b8e75e9cbf24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23dbf6c-1177-41f9-973a-ffecaa68f71a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "141f7063-c007-40f6-bdc7-b8e75e9cbf24",
                    "LayerId": "abaeadb3-861a-4f22-bf6d-514c4bfd68cb"
                }
            ]
        },
        {
            "id": "fa07df8f-1fab-4ff2-99ae-df4a6f2dda7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1aece566-4901-46ce-b2f9-76703eda03e0",
            "compositeImage": {
                "id": "47dbf25a-57ca-45d5-81d7-5ec3bc57e311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa07df8f-1fab-4ff2-99ae-df4a6f2dda7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d42135a-b54c-4391-9f26-089fd599f91a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa07df8f-1fab-4ff2-99ae-df4a6f2dda7f",
                    "LayerId": "abaeadb3-861a-4f22-bf6d-514c4bfd68cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "abaeadb3-861a-4f22-bf6d-514c4bfd68cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1aece566-4901-46ce-b2f9-76703eda03e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
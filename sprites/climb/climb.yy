{
    "id": "36bf373c-5420-4e9c-8cae-93cb1d62afae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "climb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fbb3dd57-b7bd-4818-b366-91f88f3aa91e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36bf373c-5420-4e9c-8cae-93cb1d62afae",
            "compositeImage": {
                "id": "e1691678-8773-4556-a74c-0aad1649f1b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb3dd57-b7bd-4818-b366-91f88f3aa91e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30b7dcbb-9fb2-4659-9f98-90a080b0e2fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb3dd57-b7bd-4818-b366-91f88f3aa91e",
                    "LayerId": "4122c690-f292-4cbc-a297-33377a40e786"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4122c690-f292-4cbc-a297-33377a40e786",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36bf373c-5420-4e9c-8cae-93cb1d62afae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "1bbb598a-16fe-4620-9030-619fbd584867",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_ui",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f464c5a-fc18-4be6-987d-8439837319f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bbb598a-16fe-4620-9030-619fbd584867",
            "compositeImage": {
                "id": "018aff9d-5224-43c6-9681-cd423f1ade9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f464c5a-fc18-4be6-987d-8439837319f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f8cf758-0356-4317-a3fc-09bcbfcada9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f464c5a-fc18-4be6-987d-8439837319f6",
                    "LayerId": "d5e7f9b0-373f-412d-baad-c05aded267dc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "d5e7f9b0-373f-412d-baad-c05aded267dc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bbb598a-16fe-4620-9030-619fbd584867",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 416,
    "xorig": 208,
    "yorig": 125
}
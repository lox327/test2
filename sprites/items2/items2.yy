{
    "id": "ee01fac3-7fb1-441a-9403-8bea9fc63f53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "items2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "027237b2-5866-450a-82ce-0e5520dd2fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee01fac3-7fb1-441a-9403-8bea9fc63f53",
            "compositeImage": {
                "id": "18262415-6073-42bf-b85d-ace65fb4c091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "027237b2-5866-450a-82ce-0e5520dd2fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1913febf-9cd6-4f72-8361-3427c913dfcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "027237b2-5866-450a-82ce-0e5520dd2fe1",
                    "LayerId": "67923978-335f-44d9-b4d4-1350fe3689b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "67923978-335f-44d9-b4d4-1350fe3689b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee01fac3-7fb1-441a-9403-8bea9fc63f53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}
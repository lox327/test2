{
    "id": "45498a85-78ac-43f3-b8e4-9d366a1ddab3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 49,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "830b0c45-cf24-4119-bc69-00315f935466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45498a85-78ac-43f3-b8e4-9d366a1ddab3",
            "compositeImage": {
                "id": "b289c8cb-8294-4838-b6d3-4e9dec9e7a75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "830b0c45-cf24-4119-bc69-00315f935466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "600ad577-3c1f-47cf-bffe-e841ad723cdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "830b0c45-cf24-4119-bc69-00315f935466",
                    "LayerId": "cb984db3-2887-46ca-9f44-569ea7a64579"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "cb984db3-2887-46ca-9f44-569ea7a64579",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45498a85-78ac-43f3-b8e4-9d366a1ddab3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
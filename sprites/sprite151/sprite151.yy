{
    "id": "b9307213-b2db-44db-ba34-440135533e90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite151",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 3,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "daf68fcd-bb25-4aae-a3ad-3cb2b7889ec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9307213-b2db-44db-ba34-440135533e90",
            "compositeImage": {
                "id": "243b79ae-4fa5-46ae-906b-d1c415f8ec34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daf68fcd-bb25-4aae-a3ad-3cb2b7889ec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa194b56-4405-4afa-b7f3-93111c1992ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daf68fcd-bb25-4aae-a3ad-3cb2b7889ec0",
                    "LayerId": "c926f670-b75a-4336-9791-8b6905f84afb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "c926f670-b75a-4336-9791-8b6905f84afb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9307213-b2db-44db-ba34-440135533e90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
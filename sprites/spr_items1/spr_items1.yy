{
    "id": "1ddb0180-bb6c-47f7-863a-2b3f734f33be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_items1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a072d051-f433-4b9b-af74-b2012286709f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ddb0180-bb6c-47f7-863a-2b3f734f33be",
            "compositeImage": {
                "id": "7755805b-9fb5-4313-8bd9-ed8aad91db87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a072d051-f433-4b9b-af74-b2012286709f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c032903-fef7-411e-8e97-8498eb0ffc35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a072d051-f433-4b9b-af74-b2012286709f",
                    "LayerId": "65d110cc-c652-4e26-ac2d-31041351249c"
                }
            ]
        },
        {
            "id": "abe50134-6e08-4574-8854-7ebe567c9e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ddb0180-bb6c-47f7-863a-2b3f734f33be",
            "compositeImage": {
                "id": "e28ea166-a0ca-4676-9fe5-88903f110ff8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe50134-6e08-4574-8854-7ebe567c9e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fadb38e-4c32-4455-8f92-59a5ac93ec5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe50134-6e08-4574-8854-7ebe567c9e27",
                    "LayerId": "65d110cc-c652-4e26-ac2d-31041351249c"
                }
            ]
        },
        {
            "id": "e2593bfc-2f36-45fb-a961-c9f889808047",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ddb0180-bb6c-47f7-863a-2b3f734f33be",
            "compositeImage": {
                "id": "a5722b27-7335-4b4b-b16c-eaee9f9fe59c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2593bfc-2f36-45fb-a961-c9f889808047",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fd3f502-5aab-4818-b86c-f70832885aa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2593bfc-2f36-45fb-a961-c9f889808047",
                    "LayerId": "65d110cc-c652-4e26-ac2d-31041351249c"
                }
            ]
        },
        {
            "id": "602df694-d7ef-4595-8c11-0e46f325cd3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ddb0180-bb6c-47f7-863a-2b3f734f33be",
            "compositeImage": {
                "id": "317f2d33-f888-41b0-9eb2-2d6deccb270c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "602df694-d7ef-4595-8c11-0e46f325cd3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eb5a385-1c13-44d6-a748-7442da866810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "602df694-d7ef-4595-8c11-0e46f325cd3b",
                    "LayerId": "65d110cc-c652-4e26-ac2d-31041351249c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "65d110cc-c652-4e26-ac2d-31041351249c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ddb0180-bb6c-47f7-863a-2b3f734f33be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
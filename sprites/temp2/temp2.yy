{
    "id": "0888566b-d285-4e0f-b858-f24175eca415",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "temp2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 823,
    "bbox_left": 0,
    "bbox_right": 909,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5a2fd08-55e2-4a8e-88d7-52a78ab689d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0888566b-d285-4e0f-b858-f24175eca415",
            "compositeImage": {
                "id": "ab0780a4-7755-4a42-8149-61598ec38a5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5a2fd08-55e2-4a8e-88d7-52a78ab689d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "356b5a50-96b5-4c20-b62b-33ff7d055eed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5a2fd08-55e2-4a8e-88d7-52a78ab689d6",
                    "LayerId": "86b3d51c-81a3-4769-9086-24e9121ffbe6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 824,
    "layers": [
        {
            "id": "86b3d51c-81a3-4769-9086-24e9121ffbe6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0888566b-d285-4e0f-b858-f24175eca415",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 910,
    "xorig": 909,
    "yorig": 823
}
{
    "id": "db53c7cd-fb6a-408e-ad4a-b030be0b2e54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerBash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f730e847-2e2a-4655-a951-2d1afe5d2c8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db53c7cd-fb6a-408e-ad4a-b030be0b2e54",
            "compositeImage": {
                "id": "2e0d1477-797b-4bdd-a1f8-7355ea161ec5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f730e847-2e2a-4655-a951-2d1afe5d2c8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcf3aa73-37bc-4a65-9908-58afa1d71fc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f730e847-2e2a-4655-a951-2d1afe5d2c8b",
                    "LayerId": "6b4a2f64-1ed5-4312-b123-5f2220fac1f4"
                }
            ]
        },
        {
            "id": "1f5cd352-4e25-4c2b-bb36-f671c1334e35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db53c7cd-fb6a-408e-ad4a-b030be0b2e54",
            "compositeImage": {
                "id": "d518cb3a-9215-47ac-b4ea-72bf5baa6242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f5cd352-4e25-4c2b-bb36-f671c1334e35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b23d273e-f3b3-4661-a05b-191d24619ada",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f5cd352-4e25-4c2b-bb36-f671c1334e35",
                    "LayerId": "6b4a2f64-1ed5-4312-b123-5f2220fac1f4"
                }
            ]
        },
        {
            "id": "26a60e46-d09a-4bd8-aaea-3ef4f337dd59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db53c7cd-fb6a-408e-ad4a-b030be0b2e54",
            "compositeImage": {
                "id": "d8533812-f533-4138-b4e0-27f5c0e35f8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a60e46-d09a-4bd8-aaea-3ef4f337dd59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d91f93df-80d0-4da1-a63c-ba0abfc88d99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a60e46-d09a-4bd8-aaea-3ef4f337dd59",
                    "LayerId": "6b4a2f64-1ed5-4312-b123-5f2220fac1f4"
                }
            ]
        },
        {
            "id": "840133f8-7a41-45cb-89d3-ccb62f5a0452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db53c7cd-fb6a-408e-ad4a-b030be0b2e54",
            "compositeImage": {
                "id": "748493a6-2e78-414e-897e-b6ef53cab867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "840133f8-7a41-45cb-89d3-ccb62f5a0452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b7852b-7ad5-4ab4-979a-4035aa24b72b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "840133f8-7a41-45cb-89d3-ccb62f5a0452",
                    "LayerId": "6b4a2f64-1ed5-4312-b123-5f2220fac1f4"
                }
            ]
        },
        {
            "id": "92989efc-d1eb-4709-ad15-90b5891eb14a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db53c7cd-fb6a-408e-ad4a-b030be0b2e54",
            "compositeImage": {
                "id": "2d7c9ca3-98a0-4ffd-8145-45143543b364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92989efc-d1eb-4709-ad15-90b5891eb14a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fba73216-fe46-4c1b-a1dc-b9668d83fe6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92989efc-d1eb-4709-ad15-90b5891eb14a",
                    "LayerId": "6b4a2f64-1ed5-4312-b123-5f2220fac1f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "6b4a2f64-1ed5-4312-b123-5f2220fac1f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db53c7cd-fb6a-408e-ad4a-b030be0b2e54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 21
}
{
    "id": "63b61e02-be2a-4f55-b039-bba19df8b235",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "attackLeft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "772352ac-cd96-433c-a9b8-d597d842d622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b61e02-be2a-4f55-b039-bba19df8b235",
            "compositeImage": {
                "id": "65169ae3-6bc4-4353-9a07-bfbb50f3dc46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "772352ac-cd96-433c-a9b8-d597d842d622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ab1606f-3318-4167-8a65-0f851c98c050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "772352ac-cd96-433c-a9b8-d597d842d622",
                    "LayerId": "f8ff9f70-a764-40eb-b3e6-521af9e8411f"
                }
            ]
        },
        {
            "id": "358166ec-61ef-450c-b2a2-27dea7a7c29f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b61e02-be2a-4f55-b039-bba19df8b235",
            "compositeImage": {
                "id": "1f6f7612-93e9-4b67-abd4-9f0b8a6cd807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "358166ec-61ef-450c-b2a2-27dea7a7c29f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d32cdf03-627c-4674-aab1-d0dfece5b752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "358166ec-61ef-450c-b2a2-27dea7a7c29f",
                    "LayerId": "f8ff9f70-a764-40eb-b3e6-521af9e8411f"
                }
            ]
        },
        {
            "id": "2cea9118-6b12-46b8-9b3f-1499f6820f81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b61e02-be2a-4f55-b039-bba19df8b235",
            "compositeImage": {
                "id": "104c0123-2e3d-409d-b593-6efeec014e14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cea9118-6b12-46b8-9b3f-1499f6820f81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6457c625-8456-44f4-8a32-33bccffed31c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cea9118-6b12-46b8-9b3f-1499f6820f81",
                    "LayerId": "f8ff9f70-a764-40eb-b3e6-521af9e8411f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "f8ff9f70-a764-40eb-b3e6-521af9e8411f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63b61e02-be2a-4f55-b039-bba19df8b235",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 20
}
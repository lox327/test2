{
    "id": "9064c949-0928-4b73-a6db-a1fca817f470",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84ffed12-3c2e-405b-8ec1-74e9f33c9ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9064c949-0928-4b73-a6db-a1fca817f470",
            "compositeImage": {
                "id": "36dbd270-bb57-4481-b5d6-7f00ba6a83f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84ffed12-3c2e-405b-8ec1-74e9f33c9ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c254dadd-84f2-4e2d-a4d1-69303bc62e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84ffed12-3c2e-405b-8ec1-74e9f33c9ffd",
                    "LayerId": "a4b986dd-0108-4adf-b71b-0c35cd01d9b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a4b986dd-0108-4adf-b71b-0c35cd01d9b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9064c949-0928-4b73-a6db-a1fca817f470",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
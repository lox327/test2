{
    "id": "c75df6f6-fe0b-4eed-bc89-fe3ffb95d6d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 20,
    "bbox_right": 101,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa47516d-eb64-4d88-bee0-ddd576dc5d17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c75df6f6-fe0b-4eed-bc89-fe3ffb95d6d8",
            "compositeImage": {
                "id": "34fe6aa8-8069-4638-8404-c51b06b59284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa47516d-eb64-4d88-bee0-ddd576dc5d17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18bbff7f-4b48-488e-becf-67dd6b6e439e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa47516d-eb64-4d88-bee0-ddd576dc5d17",
                    "LayerId": "4abf4100-c287-4797-b312-72fb9d505307"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4abf4100-c287-4797-b312-72fb9d505307",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c75df6f6-fe0b-4eed-bc89-fe3ffb95d6d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
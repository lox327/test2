{
    "id": "9075330d-81ed-4751-99fd-af13c31b3077",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBounty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a47f1ba1-7ad5-47b2-beea-8e893aa18031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "339f4d1b-e3ae-4fd9-9845-8028fc47a5a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a47f1ba1-7ad5-47b2-beea-8e893aa18031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2faa6c4-d983-407e-b6fb-80b2308b4ad8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a47f1ba1-7ad5-47b2-beea-8e893aa18031",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "934085a2-b268-4b43-bfe8-172934c118bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "0a6e96c8-ff5b-44d4-b216-567efa9878df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "934085a2-b268-4b43-bfe8-172934c118bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca8a9ba5-1b9d-491f-ac72-f34b716f08f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "934085a2-b268-4b43-bfe8-172934c118bf",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "fd35c5b2-472c-4688-b56d-90b7038d9762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "5bb1a24e-ba98-489e-9360-8e798e169173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd35c5b2-472c-4688-b56d-90b7038d9762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da47628b-8578-4574-ad0f-8eb8e47c475d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd35c5b2-472c-4688-b56d-90b7038d9762",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "37a8de11-6f4d-4032-9c33-4c157a09b7b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "d9b9ce20-f9c8-4076-beb4-be8c4cfb46c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37a8de11-6f4d-4032-9c33-4c157a09b7b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a86a8e2f-e34b-4cce-a54b-55db65e225ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37a8de11-6f4d-4032-9c33-4c157a09b7b4",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "64759b01-bb18-471b-81e4-38e470e03000",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "9440a3f0-68e8-40ad-ad43-5637ac2eece0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64759b01-bb18-471b-81e4-38e470e03000",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0873290f-7ae0-4815-972e-80d501fd0e33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64759b01-bb18-471b-81e4-38e470e03000",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "4478db8d-1d9b-4837-a607-d25608ba2dee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "c9de104f-c4f2-45a5-8495-0065865aa53d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4478db8d-1d9b-4837-a607-d25608ba2dee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e836aaf0-8086-4115-9e02-e24a6d9c113e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4478db8d-1d9b-4837-a607-d25608ba2dee",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "b8758f51-2db8-4c24-be2f-dba2ed634fd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "25ebbca6-ea1a-4cbf-b045-70269545c053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8758f51-2db8-4c24-be2f-dba2ed634fd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e0917de-99e6-45e0-adab-6f5d569f28c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8758f51-2db8-4c24-be2f-dba2ed634fd5",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "93143602-9643-41ca-aafc-3ed7fefd6b0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "88599753-7758-40f9-aee0-bc477c81f0bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93143602-9643-41ca-aafc-3ed7fefd6b0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc94935a-d049-4d79-8be6-bb42dd4832a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93143602-9643-41ca-aafc-3ed7fefd6b0e",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "0fee8a86-04c3-4a22-95cf-41f78c87217e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "3ee99dc1-8b6f-415a-96ae-14dacb80e8f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fee8a86-04c3-4a22-95cf-41f78c87217e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15350cd-2390-46b9-8f6c-ef6478f8fbd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fee8a86-04c3-4a22-95cf-41f78c87217e",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "90bc1336-2b8c-4e46-b681-ea7cd663a104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "c72108a6-c84e-4763-b642-6d151707391d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90bc1336-2b8c-4e46-b681-ea7cd663a104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aff15a1-12c8-46ca-8028-4014e009e5e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90bc1336-2b8c-4e46-b681-ea7cd663a104",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "3e3062eb-a2b7-4163-8011-433e123ce267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "47c8a35b-d015-4840-9fb5-45b0d8f6e84c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e3062eb-a2b7-4163-8011-433e123ce267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78644433-9e94-4c8f-a5e7-defec726b4d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e3062eb-a2b7-4163-8011-433e123ce267",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        },
        {
            "id": "80abe403-bd8f-4040-80bb-dfb87adcd73e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "compositeImage": {
                "id": "00d32561-e7cd-47f5-bc8f-e7da8e737048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80abe403-bd8f-4040-80bb-dfb87adcd73e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db19cc63-0e83-4c6f-b253-d2f570391571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80abe403-bd8f-4040-80bb-dfb87adcd73e",
                    "LayerId": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "ade714df-6ed5-4e70-a6b4-4635f8cdb06e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9075330d-81ed-4751-99fd-af13c31b3077",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 0
}
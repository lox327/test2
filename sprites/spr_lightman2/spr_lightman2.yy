{
    "id": "3682f5e7-fe77-4da9-b98b-4543fb7f2e0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightman2",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f9974ea-5461-4f1f-beb9-059823a6139f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3682f5e7-fe77-4da9-b98b-4543fb7f2e0d",
            "compositeImage": {
                "id": "cf1cddd0-1e86-4d87-bc62-0fab6646695d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f9974ea-5461-4f1f-beb9-059823a6139f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a3cd833-d947-4b29-83e6-0f04d2ed029e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f9974ea-5461-4f1f-beb9-059823a6139f",
                    "LayerId": "89e732f7-1635-419b-9f13-1a947c5d368e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "89e732f7-1635-419b-9f13-1a947c5d368e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3682f5e7-fe77-4da9-b98b-4543fb7f2e0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 48
}
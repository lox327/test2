{
    "id": "be5a6efe-a336-4d99-b7e2-70b99f22d463",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grenade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c166645-a359-4756-85f6-9eba66243e42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a6efe-a336-4d99-b7e2-70b99f22d463",
            "compositeImage": {
                "id": "a9b17290-d207-44e7-b9e6-cb8b2b044d70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c166645-a359-4756-85f6-9eba66243e42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ca164ee-2b92-41d8-a302-39ec33699b17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c166645-a359-4756-85f6-9eba66243e42",
                    "LayerId": "dd71599d-99c3-468d-8891-59c3adc9789e"
                }
            ]
        },
        {
            "id": "252f20c4-76cc-475d-9372-4fe0d9136eb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a6efe-a336-4d99-b7e2-70b99f22d463",
            "compositeImage": {
                "id": "5117238f-8261-403a-95e3-121b31e86073",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "252f20c4-76cc-475d-9372-4fe0d9136eb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "365f9dc5-91b0-417a-b4c2-9bb6a4d267b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "252f20c4-76cc-475d-9372-4fe0d9136eb7",
                    "LayerId": "dd71599d-99c3-468d-8891-59c3adc9789e"
                }
            ]
        },
        {
            "id": "0149f59c-b042-49e2-9b41-61d58197d5d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be5a6efe-a336-4d99-b7e2-70b99f22d463",
            "compositeImage": {
                "id": "59ee2cf5-bcf2-483c-9729-fb5548a2512c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0149f59c-b042-49e2-9b41-61d58197d5d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1405279f-88da-4ab8-a99a-c38898a540c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0149f59c-b042-49e2-9b41-61d58197d5d4",
                    "LayerId": "dd71599d-99c3-468d-8891-59c3adc9789e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dd71599d-99c3-468d-8891-59c3adc9789e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be5a6efe-a336-4d99-b7e2-70b99f22d463",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
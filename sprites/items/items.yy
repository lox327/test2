{
    "id": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "items",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 96,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1e09b37-b06d-4aba-ac05-15530d21c118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "0db8d5fe-56a7-4c66-ad00-19e8ce5cf38d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1e09b37-b06d-4aba-ac05-15530d21c118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "110715d5-23cd-4a22-b50a-58a75001379d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1e09b37-b06d-4aba-ac05-15530d21c118",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "30076998-b8c3-4f81-ac35-aa55df663fb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "b95c6519-12aa-41a2-87ab-fb191b6fe8d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30076998-b8c3-4f81-ac35-aa55df663fb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac42120e-49bc-44ee-b839-b0ede30ace1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30076998-b8c3-4f81-ac35-aa55df663fb9",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "bb9f6c2e-2105-479b-ac90-063525bf1e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "bf9cb24a-24b1-4046-a7ef-d936628ca92f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb9f6c2e-2105-479b-ac90-063525bf1e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ec11ce-cb19-4b90-873f-10fd1693b8fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb9f6c2e-2105-479b-ac90-063525bf1e5a",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "07d52cde-89e7-41ca-95f7-0dcec8d41c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "8629a62b-4004-4b1f-9d8a-8b143840286e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07d52cde-89e7-41ca-95f7-0dcec8d41c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658dfe01-a69d-41bc-928b-9639f66cff5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07d52cde-89e7-41ca-95f7-0dcec8d41c9b",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "30ac03bf-f350-4f6e-840c-1de255a259b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "223248ef-c1e4-48b4-b714-df87a9493c02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30ac03bf-f350-4f6e-840c-1de255a259b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c5f19c-58ad-4278-8d1c-ba6523d30d6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30ac03bf-f350-4f6e-840c-1de255a259b4",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "8cd7e1ae-5a34-4fd4-9489-506c586bb230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "c1b00371-04a5-44e0-9849-13c8e87ead22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd7e1ae-5a34-4fd4-9489-506c586bb230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c657246-f53e-4ae6-ad5e-152a4c370fd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd7e1ae-5a34-4fd4-9489-506c586bb230",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "1e7fac55-0cc7-44b1-97f9-66fce53c547b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "bd06e233-2193-48ff-bdcf-1cb20bcccba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e7fac55-0cc7-44b1-97f9-66fce53c547b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b66797eb-fbfb-423c-94a0-b1154bd9857d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e7fac55-0cc7-44b1-97f9-66fce53c547b",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "5c1934cb-6a2a-44b8-be0f-a942c4c84939",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "522681aa-0584-494a-b4c2-c4124fe900a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c1934cb-6a2a-44b8-be0f-a942c4c84939",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91e9369d-f122-447d-99ba-f6754d0d1d61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c1934cb-6a2a-44b8-be0f-a942c4c84939",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "66078483-b9bc-4688-9aae-5253516e516e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "7c0f0a31-39c8-4f00-84ff-4e747b87e629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66078483-b9bc-4688-9aae-5253516e516e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f4b2971-74a9-4edd-b074-2fcb86a9d155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66078483-b9bc-4688-9aae-5253516e516e",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "86b6052e-9fc6-489f-8a7c-b995487a2b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "6d94c8f0-17cc-4933-ad9e-66705aaa8552",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86b6052e-9fc6-489f-8a7c-b995487a2b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a71d53-228d-4b34-981f-cc0016d3888f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86b6052e-9fc6-489f-8a7c-b995487a2b15",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "8c8e318d-5a77-45a6-8092-3be7a0a5e5d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "9dfa587b-25aa-4328-b753-3fa9bc6b052a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c8e318d-5a77-45a6-8092-3be7a0a5e5d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc44139-b3a7-4463-a8ef-c26b2c472aea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c8e318d-5a77-45a6-8092-3be7a0a5e5d8",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        },
        {
            "id": "878e5590-5a91-493c-b5d8-555bc648a4f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "compositeImage": {
                "id": "070432d4-0c2f-4603-a594-e76c83185bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "878e5590-5a91-493c-b5d8-555bc648a4f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04d1d2fd-d3aa-455d-822d-8f36608b7eb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "878e5590-5a91-493c-b5d8-555bc648a4f0",
                    "LayerId": "fd793ac5-35e3-42b9-87b2-ac73e2388816"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "fd793ac5-35e3-42b9-87b2-ac73e2388816",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4c5d8bf-0ef9-449c-94a9-36ffc6b25dcb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 225,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "c5e59d20-1a49-4562-864f-d9e49b39222d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "guns",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb7ad8eb-bfed-45cc-a13d-c3e8d985a0da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "5b8c3d7b-9ec9-404f-9bc9-ff0ad8e36dfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb7ad8eb-bfed-45cc-a13d-c3e8d985a0da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5e7fa7-a51e-49bc-b7fb-6631525de5c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb7ad8eb-bfed-45cc-a13d-c3e8d985a0da",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "8747435a-09de-44bb-8169-2bf4340824e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "9094e211-09b3-4a02-8ea3-1fe8367f3402",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8747435a-09de-44bb-8169-2bf4340824e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe841038-b866-465f-a75c-47174f62e69e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8747435a-09de-44bb-8169-2bf4340824e1",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "43ef35b7-216c-4c58-8bec-0ad1c196c26a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "cad86dba-2cf7-408e-9524-2b1bc849444e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ef35b7-216c-4c58-8bec-0ad1c196c26a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a18cf015-7964-4764-a16b-31b779785c58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ef35b7-216c-4c58-8bec-0ad1c196c26a",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "201453e3-376d-46fa-bfd4-f632b4072fdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "2aee4032-72d0-4e27-ab52-ab68d179cb07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "201453e3-376d-46fa-bfd4-f632b4072fdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74e2c031-3c40-4c39-9821-8a438a476442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "201453e3-376d-46fa-bfd4-f632b4072fdf",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "85ad8154-a479-4d4b-912f-3d5f76f41433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "3a4c267c-b6f5-4ad3-b579-fddbdcfd708a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85ad8154-a479-4d4b-912f-3d5f76f41433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861a5ed6-8017-4a97-88e9-0eaed57ade6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85ad8154-a479-4d4b-912f-3d5f76f41433",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "4b0e265c-9e5a-4585-bbe5-e7c064274be2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "dd551d04-040e-4b6d-8b70-37202edbcf81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b0e265c-9e5a-4585-bbe5-e7c064274be2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "373e3f12-1af6-4a45-820d-17ccd3227690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b0e265c-9e5a-4585-bbe5-e7c064274be2",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "9fb34a46-5a6d-4580-93a0-13f2b3828416",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "51344dab-81d4-4353-b493-8711498a9207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb34a46-5a6d-4580-93a0-13f2b3828416",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f8094f8-50d5-43a9-b6dc-7eb40b9e1b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb34a46-5a6d-4580-93a0-13f2b3828416",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "ca7069e9-b0c8-447b-877d-c2d27a2608fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "77dd4e4d-83a8-49d7-98ab-1709944a56e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca7069e9-b0c8-447b-877d-c2d27a2608fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57fecdfe-1c88-4c7d-8c02-bc0b5cf58b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca7069e9-b0c8-447b-877d-c2d27a2608fe",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "19178a02-1f51-46a6-8cb4-67684e1d832a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "64a12f05-e9ba-4413-b38c-98f4b9317ffb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19178a02-1f51-46a6-8cb4-67684e1d832a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b14e752-b708-4684-9596-adb88988de69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19178a02-1f51-46a6-8cb4-67684e1d832a",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "2d524753-a511-4d3f-93aa-b68dc0d5b283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "e5928176-ba44-463c-941b-168f4b227e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d524753-a511-4d3f-93aa-b68dc0d5b283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ef90ea1-ff38-4d90-8695-fa98e4e51d06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d524753-a511-4d3f-93aa-b68dc0d5b283",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "f45bbd59-78f7-4ef5-bf50-09ed9aeb2313",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "c1f18e48-fb72-495c-b00f-e2402670ea63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f45bbd59-78f7-4ef5-bf50-09ed9aeb2313",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8f54de3-bdfa-4d0f-83b9-3cebc6c9f346",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f45bbd59-78f7-4ef5-bf50-09ed9aeb2313",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        },
        {
            "id": "6d81fdaa-3108-4a04-821b-6b95ee6ca7b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "compositeImage": {
                "id": "6f794bc8-f9d9-4353-abd2-d25ad6e4e7cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d81fdaa-3108-4a04-821b-6b95ee6ca7b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b477aaa-bb06-40c6-bfec-eb2bf4f471d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d81fdaa-3108-4a04-821b-6b95ee6ca7b8",
                    "LayerId": "021ae48b-39ae-4e61-ab6d-5c7ee334558d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "021ae48b-39ae-4e61-ab6d-5c7ee334558d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5e59d20-1a49-4562-864f-d9e49b39222d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
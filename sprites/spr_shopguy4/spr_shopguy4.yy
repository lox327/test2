{
    "id": "68c47d7e-3234-424e-8eb2-6ea0653a8a90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shopguy4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aefb4d63-4391-42be-9814-a3bc81fc9201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "68c47d7e-3234-424e-8eb2-6ea0653a8a90",
            "compositeImage": {
                "id": "3e5e1672-ea72-43da-ac18-efac5674fc4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aefb4d63-4391-42be-9814-a3bc81fc9201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b904dd4-f662-417e-99ab-eec95d308aa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aefb4d63-4391-42be-9814-a3bc81fc9201",
                    "LayerId": "6bc5eab1-27bb-4e14-aaaa-04b5b39e26b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 188,
    "layers": [
        {
            "id": "6bc5eab1-27bb-4e14-aaaa-04b5b39e26b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "68c47d7e-3234-424e-8eb2-6ea0653a8a90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 94
}
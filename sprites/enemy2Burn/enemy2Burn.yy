{
    "id": "a8233498-65ea-4fd5-a175-389a269f08ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Burn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d37fb08-fb3c-4c6d-9db0-847d4dd41964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8233498-65ea-4fd5-a175-389a269f08ea",
            "compositeImage": {
                "id": "03330ea9-c171-4aba-af72-b1e48a921579",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d37fb08-fb3c-4c6d-9db0-847d4dd41964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f1e2b24-cfe1-4b98-bbaa-296a5efa3cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d37fb08-fb3c-4c6d-9db0-847d4dd41964",
                    "LayerId": "b804817d-f6b9-4479-85ca-ebc2324f0d49"
                }
            ]
        },
        {
            "id": "453d4954-1434-427f-a1ac-bc7d8ee7f8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8233498-65ea-4fd5-a175-389a269f08ea",
            "compositeImage": {
                "id": "c4761598-9730-4f0e-b29e-f728ee4ffcf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "453d4954-1434-427f-a1ac-bc7d8ee7f8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aac7d2e-62c6-4d77-8695-d5fba037a964",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "453d4954-1434-427f-a1ac-bc7d8ee7f8e4",
                    "LayerId": "b804817d-f6b9-4479-85ca-ebc2324f0d49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b804817d-f6b9-4479-85ca-ebc2324f0d49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8233498-65ea-4fd5-a175-389a269f08ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
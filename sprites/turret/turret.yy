{
    "id": "6b271a52-57fd-45d1-9dda-609f8f4f91da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turret",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac5d9c24-9885-4b0e-a60c-228f99fcc64c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b271a52-57fd-45d1-9dda-609f8f4f91da",
            "compositeImage": {
                "id": "b0dbbf2c-496e-448d-a657-19ced8e5b8ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac5d9c24-9885-4b0e-a60c-228f99fcc64c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601538be-82ad-4e0b-8779-ff6870ec114b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac5d9c24-9885-4b0e-a60c-228f99fcc64c",
                    "LayerId": "69c2760f-86ef-449d-a1ed-372a1a30c04d"
                }
            ]
        },
        {
            "id": "7053a978-ff94-4990-9633-02071515e29b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b271a52-57fd-45d1-9dda-609f8f4f91da",
            "compositeImage": {
                "id": "360b7e5b-3139-4ff1-a8ea-ea24a286c891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7053a978-ff94-4990-9633-02071515e29b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "263ce959-a5e8-44f3-97c4-be3a32f35dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7053a978-ff94-4990-9633-02071515e29b",
                    "LayerId": "69c2760f-86ef-449d-a1ed-372a1a30c04d"
                }
            ]
        },
        {
            "id": "cdd789fd-cb5f-40b7-ae35-5a6b75f89c89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b271a52-57fd-45d1-9dda-609f8f4f91da",
            "compositeImage": {
                "id": "9aa14335-1f57-40b9-b47f-104f637c0fb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdd789fd-cb5f-40b7-ae35-5a6b75f89c89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb2223a8-8dc0-4a3b-99da-b90a3f5f4ef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdd789fd-cb5f-40b7-ae35-5a6b75f89c89",
                    "LayerId": "69c2760f-86ef-449d-a1ed-372a1a30c04d"
                }
            ]
        },
        {
            "id": "473e9ff0-a646-478d-98a8-ba5110c3131d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b271a52-57fd-45d1-9dda-609f8f4f91da",
            "compositeImage": {
                "id": "25b0f656-f026-4c75-8cad-ca06b1cf22ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "473e9ff0-a646-478d-98a8-ba5110c3131d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d09b11db-3d94-412e-a33a-67dcd745cc63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "473e9ff0-a646-478d-98a8-ba5110c3131d",
                    "LayerId": "69c2760f-86ef-449d-a1ed-372a1a30c04d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "69c2760f-86ef-449d-a1ed-372a1a30c04d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b271a52-57fd-45d1-9dda-609f8f4f91da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 32
}
{
    "id": "ce96756d-f6c8-4114-88e7-9cf8020a71eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite99",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 236,
    "bbox_left": 147,
    "bbox_right": 403,
    "bbox_top": 36,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87b0c883-74f4-474b-ab88-6e35048c7e62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce96756d-f6c8-4114-88e7-9cf8020a71eb",
            "compositeImage": {
                "id": "847e9e30-7cc3-4dc1-8c0b-7706ea22cb21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b0c883-74f4-474b-ab88-6e35048c7e62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb23902-663d-4770-abe5-e07682716047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b0c883-74f4-474b-ab88-6e35048c7e62",
                    "LayerId": "ed966dd6-776a-48f0-aa9d-49f256800639"
                }
            ]
        }
    ],
    "gridX": 24,
    "gridY": 24,
    "height": 270,
    "layers": [
        {
            "id": "ed966dd6-776a-48f0-aa9d-49f256800639",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce96756d-f6c8-4114-88e7-9cf8020a71eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
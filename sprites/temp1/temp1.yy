{
    "id": "5479fa57-7291-41d0-8050-ba9894976f1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "temp1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 7,
    "bbox_right": 50,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9812347-0341-44ee-994e-f1283ec9797e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5479fa57-7291-41d0-8050-ba9894976f1a",
            "compositeImage": {
                "id": "3b728c97-9b1c-4078-b43c-68c7a4b567e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9812347-0341-44ee-994e-f1283ec9797e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77c76701-a70d-4c2b-b47e-a247e11d36f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9812347-0341-44ee-994e-f1283ec9797e",
                    "LayerId": "21e635e4-5130-40a6-a04b-fa2dee4c2319"
                }
            ]
        },
        {
            "id": "346ae4e6-b65e-4bcd-ad35-6b617e53a966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5479fa57-7291-41d0-8050-ba9894976f1a",
            "compositeImage": {
                "id": "6f2ed551-b328-4eb9-ba84-3fc4b624f539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "346ae4e6-b65e-4bcd-ad35-6b617e53a966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "561ce936-6cfe-48fa-99c8-f4604dedc4e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "346ae4e6-b65e-4bcd-ad35-6b617e53a966",
                    "LayerId": "21e635e4-5130-40a6-a04b-fa2dee4c2319"
                }
            ]
        },
        {
            "id": "0760b19b-3baa-4ee3-a660-1b513d63b88d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5479fa57-7291-41d0-8050-ba9894976f1a",
            "compositeImage": {
                "id": "c512cb7d-00fb-4ee4-9114-b9ff24ee0064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0760b19b-3baa-4ee3-a660-1b513d63b88d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ff201a6-45a0-4a14-acab-b4f5d71a8647",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0760b19b-3baa-4ee3-a660-1b513d63b88d",
                    "LayerId": "21e635e4-5130-40a6-a04b-fa2dee4c2319"
                }
            ]
        },
        {
            "id": "05d1c615-57b0-42b1-8fed-06d74c36db8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5479fa57-7291-41d0-8050-ba9894976f1a",
            "compositeImage": {
                "id": "27975c26-78c6-4a6f-bc5c-8e0d80ff00d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d1c615-57b0-42b1-8fed-06d74c36db8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd3599ea-1a59-4868-9cae-9a929f53f586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d1c615-57b0-42b1-8fed-06d74c36db8e",
                    "LayerId": "21e635e4-5130-40a6-a04b-fa2dee4c2319"
                }
            ]
        },
        {
            "id": "ae816d3e-f39e-4b89-8bbe-a293385a9d34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5479fa57-7291-41d0-8050-ba9894976f1a",
            "compositeImage": {
                "id": "ae053367-d2d1-4c43-b7e2-88f79245f1cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae816d3e-f39e-4b89-8bbe-a293385a9d34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a0b8405-c20e-49e4-a672-82ba296a1c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae816d3e-f39e-4b89-8bbe-a293385a9d34",
                    "LayerId": "21e635e4-5130-40a6-a04b-fa2dee4c2319"
                }
            ]
        },
        {
            "id": "8aac7323-c494-45b2-941d-012892ac1267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5479fa57-7291-41d0-8050-ba9894976f1a",
            "compositeImage": {
                "id": "e7354d1a-d60b-4131-bec7-7056c631b91e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aac7323-c494-45b2-941d-012892ac1267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d07dec1-42da-48a5-a2ad-a26d647365e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aac7323-c494-45b2-941d-012892ac1267",
                    "LayerId": "21e635e4-5130-40a6-a04b-fa2dee4c2319"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "21e635e4-5130-40a6-a04b-fa2dee4c2319",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5479fa57-7291-41d0-8050-ba9894976f1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "0101776a-1aa3-43db-a02b-758dc4946d1b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12c0a996-a529-4ccd-8208-2844fa2e28b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
            "compositeImage": {
                "id": "924ad3a8-686a-4945-8067-33c2207100ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c0a996-a529-4ccd-8208-2844fa2e28b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27c16ab6-950e-48aa-918f-f816ae6ae546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c0a996-a529-4ccd-8208-2844fa2e28b4",
                    "LayerId": "8225b094-7d3e-4ab5-9f8a-17350717c7c2"
                }
            ]
        },
        {
            "id": "68458cb6-f2f2-4ee6-8689-57d7ad8fca29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
            "compositeImage": {
                "id": "b8d8186d-1efa-451b-9b68-e23be07d0348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68458cb6-f2f2-4ee6-8689-57d7ad8fca29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2613b911-ec19-4a62-b057-fc4e9c34fbdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68458cb6-f2f2-4ee6-8689-57d7ad8fca29",
                    "LayerId": "8225b094-7d3e-4ab5-9f8a-17350717c7c2"
                }
            ]
        },
        {
            "id": "7f51721b-e94d-49e3-b599-dd12125585bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
            "compositeImage": {
                "id": "b1ed6572-f360-4360-95b7-738b42c15a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f51721b-e94d-49e3-b599-dd12125585bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a476cde-761c-47d2-9707-30b144a37bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f51721b-e94d-49e3-b599-dd12125585bc",
                    "LayerId": "8225b094-7d3e-4ab5-9f8a-17350717c7c2"
                }
            ]
        },
        {
            "id": "9b390cf3-29e6-4763-8d01-c2909293a2de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
            "compositeImage": {
                "id": "cfd0ca31-85db-4ce0-a241-ac2ed7380a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b390cf3-29e6-4763-8d01-c2909293a2de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ce5028c-90fe-461d-a568-1a015c7cb23b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b390cf3-29e6-4763-8d01-c2909293a2de",
                    "LayerId": "8225b094-7d3e-4ab5-9f8a-17350717c7c2"
                }
            ]
        },
        {
            "id": "80eeb8da-f8c0-47ce-aec4-3b9af29077f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
            "compositeImage": {
                "id": "91a4d5c6-bd5c-48d6-9bd1-46c8f01d0a18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80eeb8da-f8c0-47ce-aec4-3b9af29077f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09b4a4ba-735d-42c5-bc6e-4377c5b9ee9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80eeb8da-f8c0-47ce-aec4-3b9af29077f5",
                    "LayerId": "8225b094-7d3e-4ab5-9f8a-17350717c7c2"
                }
            ]
        },
        {
            "id": "d342afd8-8015-4130-9766-23039c5f2309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
            "compositeImage": {
                "id": "46e33357-a007-4407-9657-4a7d355b9081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d342afd8-8015-4130-9766-23039c5f2309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b94b2150-dd1b-475f-9d87-ae824f1aa729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d342afd8-8015-4130-9766-23039c5f2309",
                    "LayerId": "8225b094-7d3e-4ab5-9f8a-17350717c7c2"
                }
            ]
        },
        {
            "id": "f1656953-bf2f-4330-a3e0-02176a81ce93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
            "compositeImage": {
                "id": "711c72cd-f12d-49fb-9ae8-eb7cdf442d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1656953-bf2f-4330-a3e0-02176a81ce93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d3a4c3b-0cb1-4424-9b2f-62dd34f25df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1656953-bf2f-4330-a3e0-02176a81ce93",
                    "LayerId": "8225b094-7d3e-4ab5-9f8a-17350717c7c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "8225b094-7d3e-4ab5-9f8a-17350717c7c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0101776a-1aa3-43db-a02b-758dc4946d1b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 21
}
{
    "id": "ae572284-537f-473c-9acd-8701a1da0ba8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "drifter_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 1,
    "bbox_right": 35,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d848b1a9-bff4-425c-8c43-13e89155bca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "compositeImage": {
                "id": "a5d7afd7-934b-4c88-92d4-2c2a5e6fa597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d848b1a9-bff4-425c-8c43-13e89155bca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d624765-06cd-4be5-b966-33051ffb62fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d848b1a9-bff4-425c-8c43-13e89155bca8",
                    "LayerId": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509"
                }
            ]
        },
        {
            "id": "212fd87c-21b1-42b4-8c72-b268d8aa892e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "compositeImage": {
                "id": "689142b3-c5f5-4f92-8eda-c4facfe2c855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "212fd87c-21b1-42b4-8c72-b268d8aa892e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b72e7e0-c4c1-418a-9136-ccb0ad6b3ff1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "212fd87c-21b1-42b4-8c72-b268d8aa892e",
                    "LayerId": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509"
                }
            ]
        },
        {
            "id": "7681b4ef-2d50-4909-96c9-7442147f815e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "compositeImage": {
                "id": "d8f14372-3282-4fcb-bb9a-bcef0b6bd833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7681b4ef-2d50-4909-96c9-7442147f815e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d938eeb-1eff-493f-9f6d-5db08699aeee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7681b4ef-2d50-4909-96c9-7442147f815e",
                    "LayerId": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509"
                }
            ]
        },
        {
            "id": "54bff5de-6d7a-45c2-9b9f-71f27f705d3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "compositeImage": {
                "id": "19e385f1-e784-43a0-b0e7-2863e497ae43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54bff5de-6d7a-45c2-9b9f-71f27f705d3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3abe6ba-99fa-46a4-823b-41803975ae99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54bff5de-6d7a-45c2-9b9f-71f27f705d3e",
                    "LayerId": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509"
                }
            ]
        },
        {
            "id": "8db3fda9-e393-4929-b59c-292c3bb978e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "compositeImage": {
                "id": "e3fb8b14-72dc-4d54-8b2a-5a7f5c6049d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8db3fda9-e393-4929-b59c-292c3bb978e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5084ec42-47f6-420e-84ab-08af248f8406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8db3fda9-e393-4929-b59c-292c3bb978e1",
                    "LayerId": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509"
                }
            ]
        },
        {
            "id": "629bab18-8289-47de-be78-e92844014321",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "compositeImage": {
                "id": "c0bea5fa-a5e9-47c8-bc65-121358d53912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "629bab18-8289-47de-be78-e92844014321",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dceaaf89-d9a6-466f-930b-4b80e958698b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "629bab18-8289-47de-be78-e92844014321",
                    "LayerId": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509"
                }
            ]
        },
        {
            "id": "de13fec6-6a54-43e7-903b-416f16df0d65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "compositeImage": {
                "id": "6378d61e-6a23-498b-afcc-857d79c2c2cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de13fec6-6a54-43e7-903b-416f16df0d65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35cfcec7-23fb-489f-9694-9ca811bbb889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de13fec6-6a54-43e7-903b-416f16df0d65",
                    "LayerId": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509"
                }
            ]
        },
        {
            "id": "145f9486-7967-43b0-843e-0ed33a9e3f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "compositeImage": {
                "id": "64f9a414-7a94-452a-a859-77c0545b9a44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "145f9486-7967-43b0-843e-0ed33a9e3f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08bba5e8-2db2-49bc-9898-5061b81addaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "145f9486-7967-43b0-843e-0ed33a9e3f32",
                    "LayerId": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "9c39d7fc-a4b9-4a62-a3c1-123c4cb59509",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae572284-537f-473c-9acd-8701a1da0ba8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 30
}
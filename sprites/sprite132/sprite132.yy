{
    "id": "06cf25bc-deb6-488b-b75f-d5c1c4d69f2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite132",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 0,
    "bbox_right": 68,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2791e8e9-d0f8-4ae8-821a-bea1ca0ece9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06cf25bc-deb6-488b-b75f-d5c1c4d69f2e",
            "compositeImage": {
                "id": "a1e934e1-ff1a-4904-b7a4-3a62eb266c3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2791e8e9-d0f8-4ae8-821a-bea1ca0ece9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07cebc07-5529-4c7e-83f5-6f35e58596a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2791e8e9-d0f8-4ae8-821a-bea1ca0ece9c",
                    "LayerId": "9ef91791-fd51-48b8-9c54-cd311cea51e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 59,
    "layers": [
        {
            "id": "9ef91791-fd51-48b8-9c54-cd311cea51e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06cf25bc-deb6-488b-b75f-d5c1c4d69f2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "39a260ba-8f86-432c-ae97-6fdd2d947a28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Attack1_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 12,
    "bbox_right": 48,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c4b03cb6-eb8e-4d89-a7db-012b7e3781d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39a260ba-8f86-432c-ae97-6fdd2d947a28",
            "compositeImage": {
                "id": "958ae05f-370f-4f1e-bd20-6cef5b0bb1e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b03cb6-eb8e-4d89-a7db-012b7e3781d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d74cb9a-5e16-4777-b0e2-8c089e3ede1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b03cb6-eb8e-4d89-a7db-012b7e3781d8",
                    "LayerId": "34f70678-0b9d-4b3b-9f45-3309b1d7a73d"
                }
            ]
        },
        {
            "id": "113082b4-8173-4668-b4b5-b7d671417acb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39a260ba-8f86-432c-ae97-6fdd2d947a28",
            "compositeImage": {
                "id": "d3cf69c8-2e63-4044-a78b-b4b886f54b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "113082b4-8173-4668-b4b5-b7d671417acb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce83f4ea-5c3e-48f9-b601-6832f862d16d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "113082b4-8173-4668-b4b5-b7d671417acb",
                    "LayerId": "34f70678-0b9d-4b3b-9f45-3309b1d7a73d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "34f70678-0b9d-4b3b-9f45-3309b1d7a73d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39a260ba-8f86-432c-ae97-6fdd2d947a28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
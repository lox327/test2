{
    "id": "77644426-933d-4ed5-9670-137bbfb4b943",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bounty1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd0a6efb-ca67-4f6c-84b9-51c0df8e8fdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77644426-933d-4ed5-9670-137bbfb4b943",
            "compositeImage": {
                "id": "50bf825f-d184-48db-bc34-9dc0971cc9bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd0a6efb-ca67-4f6c-84b9-51c0df8e8fdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf97c235-3677-41bf-9984-0bec66829511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd0a6efb-ca67-4f6c-84b9-51c0df8e8fdc",
                    "LayerId": "1c60d702-2491-4bf1-95aa-7c3459d3e450"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "1c60d702-2491-4bf1-95aa-7c3459d3e450",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77644426-933d-4ed5-9670-137bbfb4b943",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
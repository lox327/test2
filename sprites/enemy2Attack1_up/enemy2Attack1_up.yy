{
    "id": "e9c08190-e2ca-43e4-8925-cf82cd5375cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Attack1_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc5bcd6e-d39a-4eec-944e-7245b235f859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9c08190-e2ca-43e4-8925-cf82cd5375cc",
            "compositeImage": {
                "id": "c704f71c-b482-49e6-9b33-860b4915226b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc5bcd6e-d39a-4eec-944e-7245b235f859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b83cbe-584d-4cf5-b92d-4c8495a89bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc5bcd6e-d39a-4eec-944e-7245b235f859",
                    "LayerId": "5f95ceaf-93fd-4b41-b1c5-76c811313e92"
                }
            ]
        },
        {
            "id": "7a97d4a9-a2fb-411b-b501-6bdd519badfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9c08190-e2ca-43e4-8925-cf82cd5375cc",
            "compositeImage": {
                "id": "5ffebca9-6ff7-479a-b0f3-4e6c6fca6662",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a97d4a9-a2fb-411b-b501-6bdd519badfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d0ac1db-2a43-4607-9f7e-0cd0941fa7cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a97d4a9-a2fb-411b-b501-6bdd519badfd",
                    "LayerId": "5f95ceaf-93fd-4b41-b1c5-76c811313e92"
                }
            ]
        },
        {
            "id": "c60e99d9-12ca-4618-a1f1-26ea696bee15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9c08190-e2ca-43e4-8925-cf82cd5375cc",
            "compositeImage": {
                "id": "739ffd8c-35ce-4add-bf9b-7a93d79f9922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c60e99d9-12ca-4618-a1f1-26ea696bee15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77bfc79-e7ed-40af-8aa9-2561b7fa6208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c60e99d9-12ca-4618-a1f1-26ea696bee15",
                    "LayerId": "5f95ceaf-93fd-4b41-b1c5-76c811313e92"
                }
            ]
        },
        {
            "id": "392bc929-e626-4d48-8b31-74351516473a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e9c08190-e2ca-43e4-8925-cf82cd5375cc",
            "compositeImage": {
                "id": "d609415f-03f0-49b2-8398-8887384525d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "392bc929-e626-4d48-8b31-74351516473a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd9b8b6a-d831-4884-8dae-4c2aabdc1cdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "392bc929-e626-4d48-8b31-74351516473a",
                    "LayerId": "5f95ceaf-93fd-4b41-b1c5-76c811313e92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "5f95ceaf-93fd-4b41-b1c5-76c811313e92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e9c08190-e2ca-43e4-8925-cf82cd5375cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
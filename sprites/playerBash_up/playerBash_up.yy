{
    "id": "5c080e32-803c-4c17-b28c-da3dae14deb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerBash_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c857c44e-d3cc-4e14-af21-1f0d057abb88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c080e32-803c-4c17-b28c-da3dae14deb9",
            "compositeImage": {
                "id": "cc10e5bd-0728-4e9b-9268-21450895d75c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c857c44e-d3cc-4e14-af21-1f0d057abb88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25b39605-d8ee-444f-ae88-23636c6de696",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c857c44e-d3cc-4e14-af21-1f0d057abb88",
                    "LayerId": "0170077b-5212-4db4-ba5d-79cab7cebf23"
                }
            ]
        },
        {
            "id": "f15b2dbe-9e45-4eb6-8df1-892940d5630a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c080e32-803c-4c17-b28c-da3dae14deb9",
            "compositeImage": {
                "id": "055eedf1-879b-412a-bf26-d0e483dcdb0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f15b2dbe-9e45-4eb6-8df1-892940d5630a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5deae066-1080-4110-9661-f11d2309142b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f15b2dbe-9e45-4eb6-8df1-892940d5630a",
                    "LayerId": "0170077b-5212-4db4-ba5d-79cab7cebf23"
                }
            ]
        },
        {
            "id": "1702539d-231b-41bc-a47f-57773e0d4f23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c080e32-803c-4c17-b28c-da3dae14deb9",
            "compositeImage": {
                "id": "18995e7c-f83e-4847-981a-1890436ea540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1702539d-231b-41bc-a47f-57773e0d4f23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d7463b3-a3ba-4c55-8fc7-3544c9c7450e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1702539d-231b-41bc-a47f-57773e0d4f23",
                    "LayerId": "0170077b-5212-4db4-ba5d-79cab7cebf23"
                }
            ]
        },
        {
            "id": "9a4af98d-eebe-4825-877c-e955e6039ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c080e32-803c-4c17-b28c-da3dae14deb9",
            "compositeImage": {
                "id": "948d3619-a86a-4232-a3b2-14888ad61612",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a4af98d-eebe-4825-877c-e955e6039ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bcbbeda-c94f-4ee0-b424-684abd271f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a4af98d-eebe-4825-877c-e955e6039ba3",
                    "LayerId": "0170077b-5212-4db4-ba5d-79cab7cebf23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "0170077b-5212-4db4-ba5d-79cab7cebf23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c080e32-803c-4c17-b28c-da3dae14deb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
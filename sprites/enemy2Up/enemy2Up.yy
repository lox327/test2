{
    "id": "27320bbf-7f93-4644-aa7d-098d735ece2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7ca160e-3e97-496e-ab35-aaa7710183dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "compositeImage": {
                "id": "926cfa20-796e-4e82-ad94-b9d96a6db689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7ca160e-3e97-496e-ab35-aaa7710183dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164e0dc7-0017-4b0b-8191-abd46e3a5872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7ca160e-3e97-496e-ab35-aaa7710183dd",
                    "LayerId": "722b8b21-4f5d-4e62-b421-f52434d7b2d6"
                }
            ]
        },
        {
            "id": "524eb60a-7b53-4fb6-bee5-f606b2addd1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "compositeImage": {
                "id": "dd502e48-9a49-4e25-a57b-cd09c5b32fdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "524eb60a-7b53-4fb6-bee5-f606b2addd1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c53d9bef-3569-48b6-a97d-512ed80a0ce8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "524eb60a-7b53-4fb6-bee5-f606b2addd1b",
                    "LayerId": "722b8b21-4f5d-4e62-b421-f52434d7b2d6"
                }
            ]
        },
        {
            "id": "9307ad76-36b0-4da5-abc1-db71291cacd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "compositeImage": {
                "id": "a903816a-d528-48df-8ff6-7f4b4aade496",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9307ad76-36b0-4da5-abc1-db71291cacd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f71c37de-b7d5-4bcc-9f20-663366281b90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9307ad76-36b0-4da5-abc1-db71291cacd5",
                    "LayerId": "722b8b21-4f5d-4e62-b421-f52434d7b2d6"
                }
            ]
        },
        {
            "id": "7725a4d2-2d88-4817-b0d3-69ef640ec500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "compositeImage": {
                "id": "0b602078-aa47-4cc6-bdb0-3233691878ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7725a4d2-2d88-4817-b0d3-69ef640ec500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3637b15c-9634-4cd1-b4c6-9d1cc7087f90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7725a4d2-2d88-4817-b0d3-69ef640ec500",
                    "LayerId": "722b8b21-4f5d-4e62-b421-f52434d7b2d6"
                }
            ]
        },
        {
            "id": "12e35457-ddb7-4ade-b006-bff9f8980249",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "compositeImage": {
                "id": "ccfe5186-5319-403f-8fae-ab497a263023",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12e35457-ddb7-4ade-b006-bff9f8980249",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4467d37a-8667-4cae-80a7-b7876ad4c07d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12e35457-ddb7-4ade-b006-bff9f8980249",
                    "LayerId": "722b8b21-4f5d-4e62-b421-f52434d7b2d6"
                }
            ]
        },
        {
            "id": "bc216ce1-2e6f-499f-a5b8-57d6f623179f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "compositeImage": {
                "id": "fc87266e-33fa-40ac-8b58-18e393bda8f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc216ce1-2e6f-499f-a5b8-57d6f623179f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35f999c5-1e25-4fe2-9290-87f30367cad1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc216ce1-2e6f-499f-a5b8-57d6f623179f",
                    "LayerId": "722b8b21-4f5d-4e62-b421-f52434d7b2d6"
                }
            ]
        },
        {
            "id": "4e9e519b-656e-4359-9f85-6d423850133e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "compositeImage": {
                "id": "0604126a-a5a1-40d9-aa89-a1628af87004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e9e519b-656e-4359-9f85-6d423850133e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cbb0767-b0c3-4ab1-a69b-c6c415129fbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e9e519b-656e-4359-9f85-6d423850133e",
                    "LayerId": "722b8b21-4f5d-4e62-b421-f52434d7b2d6"
                }
            ]
        },
        {
            "id": "11ab0c5e-a882-453b-9a5a-d82f3caf8bbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "compositeImage": {
                "id": "c2926a0f-eb12-4822-96e3-3157b623aa85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11ab0c5e-a882-453b-9a5a-d82f3caf8bbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51512d01-2746-4869-8901-80460c6b5a04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11ab0c5e-a882-453b-9a5a-d82f3caf8bbd",
                    "LayerId": "722b8b21-4f5d-4e62-b421-f52434d7b2d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "722b8b21-4f5d-4e62-b421-f52434d7b2d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27320bbf-7f93-4644-aa7d-098d735ece2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
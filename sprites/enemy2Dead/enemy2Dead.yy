{
    "id": "a57131b3-839b-409e-80b8-b91fb7335e72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23880628-0189-485d-b174-a0b486f3fd0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a57131b3-839b-409e-80b8-b91fb7335e72",
            "compositeImage": {
                "id": "dbaa6c4f-10de-40de-98f9-74b46305250e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23880628-0189-485d-b174-a0b486f3fd0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fa3d6f6-cf0e-4fba-9a58-57fce572e047",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23880628-0189-485d-b174-a0b486f3fd0b",
                    "LayerId": "7f76ce14-a9f2-4fcc-9703-6ce5b0c5941b"
                }
            ]
        },
        {
            "id": "72e642ba-df7e-41a9-96f0-6cc438a6b027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a57131b3-839b-409e-80b8-b91fb7335e72",
            "compositeImage": {
                "id": "3f8b71fb-4409-4ee0-8c89-cd12b9c0a518",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72e642ba-df7e-41a9-96f0-6cc438a6b027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc27e309-7c9d-495b-871e-ef5f451e2f1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72e642ba-df7e-41a9-96f0-6cc438a6b027",
                    "LayerId": "7f76ce14-a9f2-4fcc-9703-6ce5b0c5941b"
                }
            ]
        },
        {
            "id": "0f9b3b58-96d9-4fa1-9a1b-8f6bb0eb3b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a57131b3-839b-409e-80b8-b91fb7335e72",
            "compositeImage": {
                "id": "7694cac1-ec91-41e6-aa4e-5e16e22b5cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f9b3b58-96d9-4fa1-9a1b-8f6bb0eb3b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc338610-c855-49f0-882a-fc2d9768cf9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f9b3b58-96d9-4fa1-9a1b-8f6bb0eb3b90",
                    "LayerId": "7f76ce14-a9f2-4fcc-9703-6ce5b0c5941b"
                }
            ]
        },
        {
            "id": "d47aaf64-02f9-43b3-9640-cd4586c9093e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a57131b3-839b-409e-80b8-b91fb7335e72",
            "compositeImage": {
                "id": "29da91b1-bd03-49b1-b4f0-65e5ebd30d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d47aaf64-02f9-43b3-9640-cd4586c9093e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac32ec4e-2366-4066-a945-ea9bda06d9f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d47aaf64-02f9-43b3-9640-cd4586c9093e",
                    "LayerId": "7f76ce14-a9f2-4fcc-9703-6ce5b0c5941b"
                }
            ]
        },
        {
            "id": "51674fb2-e0b2-4b2e-bdb1-dbac88c97ce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a57131b3-839b-409e-80b8-b91fb7335e72",
            "compositeImage": {
                "id": "cd804947-15f5-4be8-ad02-7e33fd7f1360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51674fb2-e0b2-4b2e-bdb1-dbac88c97ce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5d6f604-4be8-43e9-ab11-e577b681458b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51674fb2-e0b2-4b2e-bdb1-dbac88c97ce2",
                    "LayerId": "7f76ce14-a9f2-4fcc-9703-6ce5b0c5941b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "7f76ce14-a9f2-4fcc-9703-6ce5b0c5941b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a57131b3-839b-409e-80b8-b91fb7335e72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
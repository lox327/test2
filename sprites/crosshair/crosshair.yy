{
    "id": "92b605d0-ed9d-4dc4-a581-8614a17fccdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "crosshair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2c19253-5bf0-451b-8014-a9852c03f0dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92b605d0-ed9d-4dc4-a581-8614a17fccdd",
            "compositeImage": {
                "id": "b47eb6ae-bb9c-468b-9538-8425d38ec10b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2c19253-5bf0-451b-8014-a9852c03f0dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c545fc94-5693-4d1c-be4e-2e5a3513218b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2c19253-5bf0-451b-8014-a9852c03f0dc",
                    "LayerId": "ed375df2-71c1-443f-8e35-ff400f2d01a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ed375df2-71c1-443f-8e35-ff400f2d01a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92b605d0-ed9d-4dc4-a581-8614a17fccdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
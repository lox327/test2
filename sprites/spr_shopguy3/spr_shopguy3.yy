{
    "id": "d6057468-ffb9-4e27-83db-a7bca88e8358",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shopguy3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78640368-2390-472b-8e3b-77a81c97c687",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6057468-ffb9-4e27-83db-a7bca88e8358",
            "compositeImage": {
                "id": "0d804803-245b-4d73-94fb-aa6836566ed3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78640368-2390-472b-8e3b-77a81c97c687",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cd696ad-ba75-4cb1-81dc-a01335473c54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78640368-2390-472b-8e3b-77a81c97c687",
                    "LayerId": "0f914a1e-b89b-4e29-b20b-eaa80b220145"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 188,
    "layers": [
        {
            "id": "0f914a1e-b89b-4e29-b20b-eaa80b220145",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6057468-ffb9-4e27-83db-a7bca88e8358",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 94
}
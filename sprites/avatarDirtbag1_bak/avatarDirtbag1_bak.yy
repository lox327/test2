{
    "id": "429976e0-47be-4965-a0e4-7893bb1e5dbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarDirtbag1_bak",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 7,
    "bbox_right": 46,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc7b8ffc-6748-4bd2-9983-8ece31e7d138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "429976e0-47be-4965-a0e4-7893bb1e5dbc",
            "compositeImage": {
                "id": "3a05ee7b-bc74-47e5-a3e8-67ffc1d5f185",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc7b8ffc-6748-4bd2-9983-8ece31e7d138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c29e3e4-102a-4d35-9a2a-6d1b610bd08c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc7b8ffc-6748-4bd2-9983-8ece31e7d138",
                    "LayerId": "d03065f3-496e-4fb3-b30e-c30286267145"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "d03065f3-496e-4fb3-b30e-c30286267145",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "429976e0-47be-4965-a0e4-7893bb1e5dbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
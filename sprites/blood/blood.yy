{
    "id": "59af6108-6d4a-425d-a47d-3418388af745",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "blood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d41e7201-810e-4b46-b310-ffeeee533148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59af6108-6d4a-425d-a47d-3418388af745",
            "compositeImage": {
                "id": "3cf7601f-fd98-4ff4-809b-a24ff99c4a04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d41e7201-810e-4b46-b310-ffeeee533148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c41b9a28-a5b7-4601-8700-9ec68f5dba93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d41e7201-810e-4b46-b310-ffeeee533148",
                    "LayerId": "b289607a-007f-489f-b1ee-0bbc0de0840b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "b289607a-007f-489f-b1ee-0bbc0de0840b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59af6108-6d4a-425d-a47d-3418388af745",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}
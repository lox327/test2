{
    "id": "300d7ab4-7416-481f-8594-c25d1ce07a38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "map2a1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 536,
    "bbox_left": 0,
    "bbox_right": 955,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27965b1b-29f8-47d0-bc94-6447c52c4d37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "300d7ab4-7416-481f-8594-c25d1ce07a38",
            "compositeImage": {
                "id": "e648f60d-0bc5-4bef-94e0-9fe6ae26ee4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27965b1b-29f8-47d0-bc94-6447c52c4d37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "540cf51a-2ff1-40e6-930b-485b2ec87400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27965b1b-29f8-47d0-bc94-6447c52c4d37",
                    "LayerId": "333d7192-c491-4a66-98d6-e267a035b834"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 537,
    "layers": [
        {
            "id": "333d7192-c491-4a66-98d6-e267a035b834",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "300d7ab4-7416-481f-8594-c25d1ce07a38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 956,
    "xorig": 0,
    "yorig": 0
}
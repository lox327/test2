{
    "id": "aa587602-be58-4159-bf34-a6c2c42a76fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy4Right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 17,
    "bbox_right": 54,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43709bc3-1127-4008-8b96-67cd18f6ce2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "compositeImage": {
                "id": "78fd3e8e-21cc-487f-9828-7f12d946cd36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43709bc3-1127-4008-8b96-67cd18f6ce2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64be6f29-7ea7-47b9-af21-cfa229b61f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43709bc3-1127-4008-8b96-67cd18f6ce2e",
                    "LayerId": "3bb5f968-b846-4486-8a64-0fe666a1074c"
                }
            ]
        },
        {
            "id": "08434814-eaea-4662-834c-6bb2be697df1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "compositeImage": {
                "id": "f0dd1d18-5f4e-406a-ade3-0da4b24985ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08434814-eaea-4662-834c-6bb2be697df1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8e1d403-4d24-47d0-b407-2b144faee9cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08434814-eaea-4662-834c-6bb2be697df1",
                    "LayerId": "3bb5f968-b846-4486-8a64-0fe666a1074c"
                }
            ]
        },
        {
            "id": "9964a6e0-c0d4-43e1-a3d5-379ea2a8bf38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "compositeImage": {
                "id": "40cab255-4dee-425e-94bb-28fde9ef3975",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9964a6e0-c0d4-43e1-a3d5-379ea2a8bf38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a350af1-6640-4629-b999-735aba3b344a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9964a6e0-c0d4-43e1-a3d5-379ea2a8bf38",
                    "LayerId": "3bb5f968-b846-4486-8a64-0fe666a1074c"
                }
            ]
        },
        {
            "id": "81f49048-a04a-4a2a-8d6f-cbae7cff2eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "compositeImage": {
                "id": "5de74db9-a6be-4843-82d2-dceea9e23af0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f49048-a04a-4a2a-8d6f-cbae7cff2eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92d33e8c-04e6-40a0-be47-c40ab93a3a93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f49048-a04a-4a2a-8d6f-cbae7cff2eae",
                    "LayerId": "3bb5f968-b846-4486-8a64-0fe666a1074c"
                }
            ]
        },
        {
            "id": "b989410f-57ea-4f74-85be-a34d99fb0839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "compositeImage": {
                "id": "d47ab453-babc-47d2-aa1f-2bd26dee2019",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b989410f-57ea-4f74-85be-a34d99fb0839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe0b282f-d5a6-42f5-9d83-17ba267b383e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b989410f-57ea-4f74-85be-a34d99fb0839",
                    "LayerId": "3bb5f968-b846-4486-8a64-0fe666a1074c"
                }
            ]
        },
        {
            "id": "d9f32464-f7b0-4882-982a-78ef87821343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "compositeImage": {
                "id": "f14fc232-2cd1-4b4e-b4c8-a158904bebb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9f32464-f7b0-4882-982a-78ef87821343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c37bf01d-db4e-4551-b821-e72cbf2af683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9f32464-f7b0-4882-982a-78ef87821343",
                    "LayerId": "3bb5f968-b846-4486-8a64-0fe666a1074c"
                }
            ]
        },
        {
            "id": "42f7bfbb-a2d9-4c71-8741-ed5855bb6e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "compositeImage": {
                "id": "f2370bba-bec3-4496-ae0e-ab34c6c198f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f7bfbb-a2d9-4c71-8741-ed5855bb6e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bd9cbc-adf7-49bc-b111-f81f3494f384",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f7bfbb-a2d9-4c71-8741-ed5855bb6e7f",
                    "LayerId": "3bb5f968-b846-4486-8a64-0fe666a1074c"
                }
            ]
        },
        {
            "id": "1fae86ba-107e-44a1-8fcc-46ac817d7d72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "compositeImage": {
                "id": "dbe87938-858d-449e-9056-9eb7dbc933f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fae86ba-107e-44a1-8fcc-46ac817d7d72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40bc714e-4227-44c0-9fe7-15596d9eb37d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fae86ba-107e-44a1-8fcc-46ac817d7d72",
                    "LayerId": "3bb5f968-b846-4486-8a64-0fe666a1074c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "3bb5f968-b846-4486-8a64-0fe666a1074c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa587602-be58-4159-bf34-a6c2c42a76fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "7ad4ee05-bbee-472b-9eec-63de9bbdfe4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lbl_tech1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 13,
    "bbox_right": 51,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84a329b5-6dae-482a-ac33-83e5b2aa7f1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ad4ee05-bbee-472b-9eec-63de9bbdfe4c",
            "compositeImage": {
                "id": "9ec66d20-02af-4c40-89fd-5303ef6085e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84a329b5-6dae-482a-ac33-83e5b2aa7f1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51be95ee-7803-48de-aa12-5236648d1626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84a329b5-6dae-482a-ac33-83e5b2aa7f1a",
                    "LayerId": "fe33a040-4cda-4d8e-8f70-12f9c8b93219"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fe33a040-4cda-4d8e-8f70-12f9c8b93219",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ad4ee05-bbee-472b-9eec-63de9bbdfe4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
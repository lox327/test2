{
    "id": "6c7ccd21-a08c-4132-bffe-365810fddaa1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "char2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 274,
    "bbox_left": 0,
    "bbox_right": 124,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6ae37e2-bd6a-4c2f-9607-b388f89769bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c7ccd21-a08c-4132-bffe-365810fddaa1",
            "compositeImage": {
                "id": "cc5c894c-72eb-4e4a-abbe-e7ba1ccb5674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6ae37e2-bd6a-4c2f-9607-b388f89769bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a1df86-4eae-4656-8d20-92aea5fe3b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6ae37e2-bd6a-4c2f-9607-b388f89769bb",
                    "LayerId": "4f2ed816-cd17-417a-bf07-c4a86dc19085"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 275,
    "layers": [
        {
            "id": "4f2ed816-cd17-417a-bf07-c4a86dc19085",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c7ccd21-a08c-4132-bffe-365810fddaa1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 125,
    "xorig": 62,
    "yorig": 137
}
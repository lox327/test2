{
    "id": "bb881bfd-f57f-42c5-864d-395ea05e5258",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Attack1_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 11,
    "bbox_right": 49,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa0bf835-e24e-4b4b-80c1-079fd74a88b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb881bfd-f57f-42c5-864d-395ea05e5258",
            "compositeImage": {
                "id": "4df09a91-1b84-4ccf-ae27-169bca2e7197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0bf835-e24e-4b4b-80c1-079fd74a88b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e74853a-9688-40f2-ad79-91e7bc9f8d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0bf835-e24e-4b4b-80c1-079fd74a88b6",
                    "LayerId": "ec7e1dbd-4528-40f9-8bb9-d30d2dd8e984"
                }
            ]
        },
        {
            "id": "fd0f8897-3e78-4462-a3b1-05665d750f83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb881bfd-f57f-42c5-864d-395ea05e5258",
            "compositeImage": {
                "id": "9321892d-41f6-4e9e-a3b5-2f3d2273b15d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd0f8897-3e78-4462-a3b1-05665d750f83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f06359a-b131-45d6-bffd-c8452a6e54a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd0f8897-3e78-4462-a3b1-05665d750f83",
                    "LayerId": "ec7e1dbd-4528-40f9-8bb9-d30d2dd8e984"
                }
            ]
        },
        {
            "id": "4428d7ce-66b9-4382-abcd-300eaac8e2aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb881bfd-f57f-42c5-864d-395ea05e5258",
            "compositeImage": {
                "id": "2529e726-f3ad-4c3e-8aad-a9b6c81380d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4428d7ce-66b9-4382-abcd-300eaac8e2aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99996d7e-158e-4d7b-be8d-036bc58ce090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4428d7ce-66b9-4382-abcd-300eaac8e2aa",
                    "LayerId": "ec7e1dbd-4528-40f9-8bb9-d30d2dd8e984"
                }
            ]
        },
        {
            "id": "ffef36c3-ca32-4a36-af24-f90887619b07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb881bfd-f57f-42c5-864d-395ea05e5258",
            "compositeImage": {
                "id": "0034b138-f0e2-429e-b0a3-79c83708cd6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffef36c3-ca32-4a36-af24-f90887619b07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78e0cac7-3054-426f-9388-0b5644b3ac87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffef36c3-ca32-4a36-af24-f90887619b07",
                    "LayerId": "ec7e1dbd-4528-40f9-8bb9-d30d2dd8e984"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "ec7e1dbd-4528-40f9-8bb9-d30d2dd8e984",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb881bfd-f57f-42c5-864d-395ea05e5258",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
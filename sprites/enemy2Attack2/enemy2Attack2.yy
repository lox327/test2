{
    "id": "2fd7df56-1335-481d-be11-53addb2e479d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Attack2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b1c6726-f955-4a91-a80b-f2de71ec7e0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd7df56-1335-481d-be11-53addb2e479d",
            "compositeImage": {
                "id": "0dd225e1-4094-40d2-82b4-debfcaf5b231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b1c6726-f955-4a91-a80b-f2de71ec7e0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19acc426-2f6a-48f2-a037-c911c9c4b708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b1c6726-f955-4a91-a80b-f2de71ec7e0c",
                    "LayerId": "e0be5d5b-45d3-4dde-b2b9-0a826c9b33e4"
                }
            ]
        },
        {
            "id": "9a76b95c-5399-4bb6-b5de-8acd6c3649c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd7df56-1335-481d-be11-53addb2e479d",
            "compositeImage": {
                "id": "6f312351-b087-4885-b263-0929e3ea8b12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a76b95c-5399-4bb6-b5de-8acd6c3649c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199e72e2-60e1-46bd-97e5-325e5285ef18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a76b95c-5399-4bb6-b5de-8acd6c3649c3",
                    "LayerId": "e0be5d5b-45d3-4dde-b2b9-0a826c9b33e4"
                }
            ]
        },
        {
            "id": "00fa6a96-9093-4db3-a0f4-cc5199c3b64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd7df56-1335-481d-be11-53addb2e479d",
            "compositeImage": {
                "id": "a112021b-9b0a-47a5-a8d2-76e672c07cc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00fa6a96-9093-4db3-a0f4-cc5199c3b64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f293e0ed-fc3b-4b03-a92b-e077071b890a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00fa6a96-9093-4db3-a0f4-cc5199c3b64b",
                    "LayerId": "e0be5d5b-45d3-4dde-b2b9-0a826c9b33e4"
                }
            ]
        },
        {
            "id": "68521a06-24d5-4a13-8402-001196e8e6bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd7df56-1335-481d-be11-53addb2e479d",
            "compositeImage": {
                "id": "4e5f0f74-849b-4c7b-a009-2f3964ed2376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68521a06-24d5-4a13-8402-001196e8e6bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b38a345-4ade-4fc1-9938-98d3df2ac3b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68521a06-24d5-4a13-8402-001196e8e6bc",
                    "LayerId": "e0be5d5b-45d3-4dde-b2b9-0a826c9b33e4"
                }
            ]
        },
        {
            "id": "e336bb60-4953-416b-9e7f-07ac9e5f114b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd7df56-1335-481d-be11-53addb2e479d",
            "compositeImage": {
                "id": "3a2a40b1-97e0-46fc-a5f9-ae641802862d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e336bb60-4953-416b-9e7f-07ac9e5f114b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4b1bcc-bd30-4ebe-880d-e2fddead5ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e336bb60-4953-416b-9e7f-07ac9e5f114b",
                    "LayerId": "e0be5d5b-45d3-4dde-b2b9-0a826c9b33e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e0be5d5b-45d3-4dde-b2b9-0a826c9b33e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fd7df56-1335-481d-be11-53addb2e479d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
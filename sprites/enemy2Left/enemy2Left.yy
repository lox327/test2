{
    "id": "4193d649-3916-4901-949a-85315b8b537b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 17,
    "bbox_right": 54,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b1d0aa57-6c97-4d57-94de-67ccd29c5310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "compositeImage": {
                "id": "ba952621-6671-4369-8513-1b09bbefb76a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1d0aa57-6c97-4d57-94de-67ccd29c5310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fb30175-655d-4930-ad7e-8cffb6057c51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1d0aa57-6c97-4d57-94de-67ccd29c5310",
                    "LayerId": "0732268a-0d25-4dc5-9e1c-84ff34707c1e"
                }
            ]
        },
        {
            "id": "a648cfb8-d22f-4866-b00d-c596f0826d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "compositeImage": {
                "id": "aed51260-2889-44f1-97a8-8a0ba1664fa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a648cfb8-d22f-4866-b00d-c596f0826d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "943fc554-9a8b-4d66-9e5a-9657bd943971",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a648cfb8-d22f-4866-b00d-c596f0826d3b",
                    "LayerId": "0732268a-0d25-4dc5-9e1c-84ff34707c1e"
                }
            ]
        },
        {
            "id": "7818b9c5-daaa-46c4-9e4d-da9ca48735be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "compositeImage": {
                "id": "b0e3f36f-d646-4a65-b2a5-0f40acb2420e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7818b9c5-daaa-46c4-9e4d-da9ca48735be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa30d452-437e-4b1e-b1bf-3b61707d4dcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7818b9c5-daaa-46c4-9e4d-da9ca48735be",
                    "LayerId": "0732268a-0d25-4dc5-9e1c-84ff34707c1e"
                }
            ]
        },
        {
            "id": "164b68fe-9359-4a95-ae0b-cff7ca05a239",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "compositeImage": {
                "id": "ff316b84-47e7-4fd1-93fd-036e9bb474d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164b68fe-9359-4a95-ae0b-cff7ca05a239",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8943a46f-bcf2-46b5-a426-60908d29d4ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164b68fe-9359-4a95-ae0b-cff7ca05a239",
                    "LayerId": "0732268a-0d25-4dc5-9e1c-84ff34707c1e"
                }
            ]
        },
        {
            "id": "bcced7e2-0e63-4eee-9b1a-fd27fca55e2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "compositeImage": {
                "id": "9ba3fe96-e4c7-4fc2-9e21-dbf4180072cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcced7e2-0e63-4eee-9b1a-fd27fca55e2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9eb498b-b286-4883-8eb7-02a514f2d74e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcced7e2-0e63-4eee-9b1a-fd27fca55e2a",
                    "LayerId": "0732268a-0d25-4dc5-9e1c-84ff34707c1e"
                }
            ]
        },
        {
            "id": "473031ea-6a7a-49bb-af6d-e1dff0f84615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "compositeImage": {
                "id": "272a77ad-6255-47d3-80f6-67a36ea0b677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "473031ea-6a7a-49bb-af6d-e1dff0f84615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaca2179-6751-4a5b-8392-4b4ad819fed1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "473031ea-6a7a-49bb-af6d-e1dff0f84615",
                    "LayerId": "0732268a-0d25-4dc5-9e1c-84ff34707c1e"
                }
            ]
        },
        {
            "id": "39c69aca-00ac-42cb-b134-ebd2997d3e0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "compositeImage": {
                "id": "d5d8d97c-8718-4894-8801-cad62598e01c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39c69aca-00ac-42cb-b134-ebd2997d3e0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d11072e-38f9-4309-85c7-d8a410508f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39c69aca-00ac-42cb-b134-ebd2997d3e0f",
                    "LayerId": "0732268a-0d25-4dc5-9e1c-84ff34707c1e"
                }
            ]
        },
        {
            "id": "ce6c0051-9963-4e8e-b4ec-c6ee7dc58acc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "compositeImage": {
                "id": "c0efbebf-2459-43ad-b415-d06abda62d7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce6c0051-9963-4e8e-b4ec-c6ee7dc58acc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c32e7f7a-bf28-454b-bdd4-5894fce84aff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce6c0051-9963-4e8e-b4ec-c6ee7dc58acc",
                    "LayerId": "0732268a-0d25-4dc5-9e1c-84ff34707c1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "0732268a-0d25-4dc5-9e1c-84ff34707c1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4193d649-3916-4901-949a-85315b8b537b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
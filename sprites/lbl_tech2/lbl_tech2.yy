{
    "id": "2a66e9a2-fc21-47f5-8b89-c16fbb978c8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lbl_tech2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 11,
    "bbox_right": 51,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b375c23a-6db3-4250-92a1-a15842f8412e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a66e9a2-fc21-47f5-8b89-c16fbb978c8b",
            "compositeImage": {
                "id": "d3598a0e-a64a-4e9f-a1bf-ba51ebb20191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b375c23a-6db3-4250-92a1-a15842f8412e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e75b2bff-17cf-4fdf-8af3-c987eed99314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b375c23a-6db3-4250-92a1-a15842f8412e",
                    "LayerId": "92d6f327-9d4e-4bb9-9098-efa03de47e1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "92d6f327-9d4e-4bb9-9098-efa03de47e1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a66e9a2-fc21-47f5-8b89-c16fbb978c8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
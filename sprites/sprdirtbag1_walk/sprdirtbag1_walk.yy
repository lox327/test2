{
    "id": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDirtbag1_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 459,
    "bbox_left": 0,
    "bbox_right": 259,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a071cf0b-e83d-4e19-93e1-723764a51b18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "compositeImage": {
                "id": "bebc7dd3-30d4-4356-b0f5-b39f48fdb5b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a071cf0b-e83d-4e19-93e1-723764a51b18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d5c1a51-c77e-4579-8b68-ba33c27c5ad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a071cf0b-e83d-4e19-93e1-723764a51b18",
                    "LayerId": "c70d5c77-badb-46dc-830a-addfb237a3f9"
                }
            ]
        },
        {
            "id": "042b22e1-7b11-4ed9-95bf-0aa68377b4c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "compositeImage": {
                "id": "8b4969ef-add6-44a1-a020-e86dbe85aaa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "042b22e1-7b11-4ed9-95bf-0aa68377b4c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d82d5b8-8535-4e1d-ab57-37021e4cfdec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "042b22e1-7b11-4ed9-95bf-0aa68377b4c7",
                    "LayerId": "c70d5c77-badb-46dc-830a-addfb237a3f9"
                }
            ]
        },
        {
            "id": "c7f36372-51c4-4d7b-a019-b72b8380c536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "compositeImage": {
                "id": "45063938-750b-4068-9994-3deeb023fdaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7f36372-51c4-4d7b-a019-b72b8380c536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9728b04b-8795-44b9-a7e6-3b731c5fe425",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7f36372-51c4-4d7b-a019-b72b8380c536",
                    "LayerId": "c70d5c77-badb-46dc-830a-addfb237a3f9"
                }
            ]
        },
        {
            "id": "062f39d0-7a0d-4f82-8fe1-57f0da9e55ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "compositeImage": {
                "id": "3afb5bf5-84be-496f-94bb-59dca3a70dab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "062f39d0-7a0d-4f82-8fe1-57f0da9e55ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52c3ca08-f2e3-45f7-8836-f8a1f09952c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "062f39d0-7a0d-4f82-8fe1-57f0da9e55ee",
                    "LayerId": "c70d5c77-badb-46dc-830a-addfb237a3f9"
                }
            ]
        },
        {
            "id": "977c6413-e786-4067-97d4-194622e445a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "compositeImage": {
                "id": "15dfe6cc-121e-42f3-abcf-886863144812",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "977c6413-e786-4067-97d4-194622e445a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d32789f-e4ca-4fb5-9d1c-d1a054419656",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "977c6413-e786-4067-97d4-194622e445a8",
                    "LayerId": "c70d5c77-badb-46dc-830a-addfb237a3f9"
                }
            ]
        },
        {
            "id": "776947b4-b496-4e70-817f-ed31aa3ac412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "compositeImage": {
                "id": "614ae783-ce96-4bea-ab9e-dbde6a67a2db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "776947b4-b496-4e70-817f-ed31aa3ac412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "323af57e-4789-45c7-8734-f78574b77ebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "776947b4-b496-4e70-817f-ed31aa3ac412",
                    "LayerId": "c70d5c77-badb-46dc-830a-addfb237a3f9"
                }
            ]
        },
        {
            "id": "582cdf9d-855d-4f25-bf9a-63dc7722c9ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "compositeImage": {
                "id": "a95edd31-4072-406f-94ad-7cc4175c9371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "582cdf9d-855d-4f25-bf9a-63dc7722c9ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d712cb36-d1c1-46ea-890c-8ff4dc55a915",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "582cdf9d-855d-4f25-bf9a-63dc7722c9ad",
                    "LayerId": "c70d5c77-badb-46dc-830a-addfb237a3f9"
                }
            ]
        },
        {
            "id": "c882c2de-6041-46d1-8f4b-a9af3ccf9642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "compositeImage": {
                "id": "2ff9b31d-216c-4c3d-9c0b-56fddecd6d07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c882c2de-6041-46d1-8f4b-a9af3ccf9642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d699036e-497a-402c-b642-d3e6dfd7876c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c882c2de-6041-46d1-8f4b-a9af3ccf9642",
                    "LayerId": "c70d5c77-badb-46dc-830a-addfb237a3f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 460,
    "layers": [
        {
            "id": "c70d5c77-badb-46dc-830a-addfb237a3f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a9c0e84-f7a1-492b-aa3a-cbf2fb9d91b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 260,
    "xorig": 373,
    "yorig": -1214
}
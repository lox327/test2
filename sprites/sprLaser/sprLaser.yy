{
    "id": "23d7be55-5fa1-49fa-8e8f-7942448bcb6c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprLaser",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4217369d-853c-488c-bf43-cf8fff2299e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d7be55-5fa1-49fa-8e8f-7942448bcb6c",
            "compositeImage": {
                "id": "5c1eac3b-c5d0-4c3d-a536-224bcd00b36e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4217369d-853c-488c-bf43-cf8fff2299e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39dcfad3-57a4-45d6-b578-a81f025bc868",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4217369d-853c-488c-bf43-cf8fff2299e8",
                    "LayerId": "fee5133d-7d95-4c59-bee6-c9cdfd966763"
                },
                {
                    "id": "a87495ef-30d0-4758-9382-38a4d5b6e76a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4217369d-853c-488c-bf43-cf8fff2299e8",
                    "LayerId": "946b203c-1901-471d-af7d-aa31413dd45f"
                }
            ]
        },
        {
            "id": "1d9d09aa-012c-4d50-aabf-40ad490f4078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d7be55-5fa1-49fa-8e8f-7942448bcb6c",
            "compositeImage": {
                "id": "787ede22-3f44-459c-81cb-7c7726010617",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d9d09aa-012c-4d50-aabf-40ad490f4078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e070db-197c-4290-858f-6493f91bab70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d9d09aa-012c-4d50-aabf-40ad490f4078",
                    "LayerId": "946b203c-1901-471d-af7d-aa31413dd45f"
                },
                {
                    "id": "2040bf80-7302-4c86-b853-68b274cb301e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d9d09aa-012c-4d50-aabf-40ad490f4078",
                    "LayerId": "fee5133d-7d95-4c59-bee6-c9cdfd966763"
                }
            ]
        },
        {
            "id": "b7f60f02-6a58-4922-82d6-c0c0beb184b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23d7be55-5fa1-49fa-8e8f-7942448bcb6c",
            "compositeImage": {
                "id": "06df5382-9099-4e38-ae68-f637c6094a47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7f60f02-6a58-4922-82d6-c0c0beb184b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff2b7916-cf0d-4f0c-961d-c11aed5b1cfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f60f02-6a58-4922-82d6-c0c0beb184b9",
                    "LayerId": "946b203c-1901-471d-af7d-aa31413dd45f"
                },
                {
                    "id": "55a01bef-3d82-4e5f-b90c-eb0fe3de7266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7f60f02-6a58-4922-82d6-c0c0beb184b9",
                    "LayerId": "fee5133d-7d95-4c59-bee6-c9cdfd966763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "946b203c-1901-471d-af7d-aa31413dd45f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23d7be55-5fa1-49fa-8e8f-7942448bcb6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fee5133d-7d95-4c59-bee6-c9cdfd966763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23d7be55-5fa1-49fa-8e8f-7942448bcb6c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "f25e51df-99f8-4976-959e-c788ac909b79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gun4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57e1d1f3-d3b4-419d-aa27-f33f1286f23d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f25e51df-99f8-4976-959e-c788ac909b79",
            "compositeImage": {
                "id": "b2d16426-4201-4020-a320-f898458d1143",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57e1d1f3-d3b4-419d-aa27-f33f1286f23d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dbc1903-fca6-42be-a822-cc840887358c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57e1d1f3-d3b4-419d-aa27-f33f1286f23d",
                    "LayerId": "029675ec-bd14-4bb8-9611-bc09482a9a80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "029675ec-bd14-4bb8-9611-bc09482a9a80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f25e51df-99f8-4976-959e-c788ac909b79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}
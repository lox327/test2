{
    "id": "cb7975b8-037a-4025-8ce6-068f30ea939e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarXiu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "237ba903-e63b-4041-82b1-9fc34c03b5df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb7975b8-037a-4025-8ce6-068f30ea939e",
            "compositeImage": {
                "id": "46050516-a032-44d5-bbbb-10e1537b298c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "237ba903-e63b-4041-82b1-9fc34c03b5df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f1f6524-6198-4e7e-afac-23b8dceb2791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "237ba903-e63b-4041-82b1-9fc34c03b5df",
                    "LayerId": "921bf9e0-7c79-45c9-809d-5971e5dc4e09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "921bf9e0-7c79-45c9-809d-5971e5dc4e09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb7975b8-037a-4025-8ce6-068f30ea939e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
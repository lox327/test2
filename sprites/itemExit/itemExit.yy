{
    "id": "e399d58f-9add-412f-a080-fdea66996e3f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "itemExit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7fcc980a-ebc1-47a2-8af1-ede02b725fcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e399d58f-9add-412f-a080-fdea66996e3f",
            "compositeImage": {
                "id": "d484edec-588e-43ee-86f4-e8573adeeef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fcc980a-ebc1-47a2-8af1-ede02b725fcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a701e2ec-3b68-4f4c-960d-3ac4c75576a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fcc980a-ebc1-47a2-8af1-ede02b725fcb",
                    "LayerId": "62f7ff50-9efb-4521-a574-0d8837080a1d"
                }
            ]
        },
        {
            "id": "699487c9-1993-4b00-a5e8-3cf3bb4a724e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e399d58f-9add-412f-a080-fdea66996e3f",
            "compositeImage": {
                "id": "5c5711a3-beb6-4956-91c6-cdf3e57b6a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "699487c9-1993-4b00-a5e8-3cf3bb4a724e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7e0fe56-842d-4e0a-b8ad-6f24ca292b3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "699487c9-1993-4b00-a5e8-3cf3bb4a724e",
                    "LayerId": "62f7ff50-9efb-4521-a574-0d8837080a1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "62f7ff50-9efb-4521-a574-0d8837080a1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e399d58f-9add-412f-a080-fdea66996e3f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
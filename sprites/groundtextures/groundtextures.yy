{
    "id": "ac05ced3-818b-4473-851f-c8d7de9a7093",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "groundtextures",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 217,
    "bbox_left": 166,
    "bbox_right": 373,
    "bbox_top": 19,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fab2e300-aa85-4e14-9a39-08f4d8fd22c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac05ced3-818b-4473-851f-c8d7de9a7093",
            "compositeImage": {
                "id": "dc510999-8185-4eef-9521-b72e94c95748",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab2e300-aa85-4e14-9a39-08f4d8fd22c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc108af-425a-4767-a363-c8afa2ace183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab2e300-aa85-4e14-9a39-08f4d8fd22c2",
                    "LayerId": "d47c3c70-802f-4545-b7f7-eb8a04e577f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "d47c3c70-802f-4545-b7f7-eb8a04e577f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac05ced3-818b-4473-851f-c8d7de9a7093",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
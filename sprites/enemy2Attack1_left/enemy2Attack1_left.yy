{
    "id": "5d0fa202-de61-4cb9-8bee-4d725ce989e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Attack1_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 14,
    "bbox_right": 59,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33c86103-6f92-4850-b0da-5f6b9c5fe1db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d0fa202-de61-4cb9-8bee-4d725ce989e6",
            "compositeImage": {
                "id": "58f4f081-5d32-4f50-a831-3c3135464f4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c86103-6f92-4850-b0da-5f6b9c5fe1db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8aebee7-041a-4d0a-bba1-d45a44db9b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c86103-6f92-4850-b0da-5f6b9c5fe1db",
                    "LayerId": "17d51dd2-71d7-442f-a041-aa16efada241"
                }
            ]
        },
        {
            "id": "8e014488-bdd5-4828-a168-7521eab82cc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d0fa202-de61-4cb9-8bee-4d725ce989e6",
            "compositeImage": {
                "id": "5aa4d0ed-b28b-4aa1-bd43-3fab88733d17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e014488-bdd5-4828-a168-7521eab82cc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdfb044f-b828-4aab-bdae-312888bf42bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e014488-bdd5-4828-a168-7521eab82cc0",
                    "LayerId": "17d51dd2-71d7-442f-a041-aa16efada241"
                }
            ]
        },
        {
            "id": "96f6db93-134a-44a1-9296-9f3f8fdb3c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d0fa202-de61-4cb9-8bee-4d725ce989e6",
            "compositeImage": {
                "id": "27f88034-ad06-4215-a43e-6ee9d803cc00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96f6db93-134a-44a1-9296-9f3f8fdb3c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a631495-3ff0-4739-a703-c5328f99aa15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96f6db93-134a-44a1-9296-9f3f8fdb3c4e",
                    "LayerId": "17d51dd2-71d7-442f-a041-aa16efada241"
                }
            ]
        },
        {
            "id": "77775135-6d67-4d46-8d0f-d873466556ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d0fa202-de61-4cb9-8bee-4d725ce989e6",
            "compositeImage": {
                "id": "e8b5ad39-2af2-4832-8d06-1efca921edb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77775135-6d67-4d46-8d0f-d873466556ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eadea35d-fab4-4dda-8292-a45ef39eebd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77775135-6d67-4d46-8d0f-d873466556ba",
                    "LayerId": "17d51dd2-71d7-442f-a041-aa16efada241"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "17d51dd2-71d7-442f-a041-aa16efada241",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d0fa202-de61-4cb9-8bee-4d725ce989e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
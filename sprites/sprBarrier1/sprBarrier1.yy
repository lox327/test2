{
    "id": "1507d081-5453-4856-9d86-eca5e93205f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBarrier1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26c682ca-23f6-44b3-85ea-bd2ca7604032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1507d081-5453-4856-9d86-eca5e93205f3",
            "compositeImage": {
                "id": "e516fa56-e906-41f5-8a67-7fc4545dff01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c682ca-23f6-44b3-85ea-bd2ca7604032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5be9ce2d-0e45-493f-bb83-93acf55d3335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c682ca-23f6-44b3-85ea-bd2ca7604032",
                    "LayerId": "106c19fc-3996-4ade-9ba2-2348ebf3c684"
                }
            ]
        },
        {
            "id": "8c90e594-24e2-4ce1-af87-61d85463de21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1507d081-5453-4856-9d86-eca5e93205f3",
            "compositeImage": {
                "id": "89ca838c-4b4e-43ca-8c68-793370f9f8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c90e594-24e2-4ce1-af87-61d85463de21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abcd6fd4-c0e9-446a-8775-68a27c1433f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c90e594-24e2-4ce1-af87-61d85463de21",
                    "LayerId": "106c19fc-3996-4ade-9ba2-2348ebf3c684"
                }
            ]
        },
        {
            "id": "604a7723-8089-41f4-87e8-dfe004466cd7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1507d081-5453-4856-9d86-eca5e93205f3",
            "compositeImage": {
                "id": "83e92238-8085-4f18-9ba2-40f8dfb84e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "604a7723-8089-41f4-87e8-dfe004466cd7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa2ba34d-f73e-41d5-9d89-8386862639e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "604a7723-8089-41f4-87e8-dfe004466cd7",
                    "LayerId": "106c19fc-3996-4ade-9ba2-2348ebf3c684"
                }
            ]
        },
        {
            "id": "2b912b48-7eb4-4e2b-9a53-a60e22210381",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1507d081-5453-4856-9d86-eca5e93205f3",
            "compositeImage": {
                "id": "28adfe6a-9a29-4bfe-9425-4107763f5ab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b912b48-7eb4-4e2b-9a53-a60e22210381",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e65d3856-f693-48c4-b3e0-1d85dba3a8c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b912b48-7eb4-4e2b-9a53-a60e22210381",
                    "LayerId": "106c19fc-3996-4ade-9ba2-2348ebf3c684"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "106c19fc-3996-4ade-9ba2-2348ebf3c684",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1507d081-5453-4856-9d86-eca5e93205f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
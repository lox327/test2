{
    "id": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite115",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 449,
    "bbox_left": 0,
    "bbox_right": 149,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ee218cf-d0cb-4498-8dbd-f5fb71d89156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "compositeImage": {
                "id": "44b1be82-a255-4c56-b5d4-3c5bd2ab4cfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ee218cf-d0cb-4498-8dbd-f5fb71d89156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff121698-36fc-446c-a56d-e3b083a39e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ee218cf-d0cb-4498-8dbd-f5fb71d89156",
                    "LayerId": "6068e458-d720-4276-8287-358274884b26"
                }
            ]
        },
        {
            "id": "f4b78565-8da7-423c-8659-aff99a64219a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "compositeImage": {
                "id": "ad4b9687-1ba5-439c-af25-5b7187de05ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4b78565-8da7-423c-8659-aff99a64219a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb92f1a3-4ee1-4c0a-837d-aa2983c5d636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4b78565-8da7-423c-8659-aff99a64219a",
                    "LayerId": "6068e458-d720-4276-8287-358274884b26"
                }
            ]
        },
        {
            "id": "5848e897-2dc3-43d1-a9a7-1a4226d2a888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "compositeImage": {
                "id": "7e9e6e08-ea8d-4a80-97ef-d465e53df49a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5848e897-2dc3-43d1-a9a7-1a4226d2a888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8428fb8e-ab6b-4d00-a502-e335216e98d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5848e897-2dc3-43d1-a9a7-1a4226d2a888",
                    "LayerId": "6068e458-d720-4276-8287-358274884b26"
                }
            ]
        },
        {
            "id": "54aba7a1-a6f3-4777-a72c-01cd718535dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "compositeImage": {
                "id": "4c0d8319-a566-4809-9ded-0e3d2d5b8181",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54aba7a1-a6f3-4777-a72c-01cd718535dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cc12f0a-c961-45bd-8cbc-578c300c3680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54aba7a1-a6f3-4777-a72c-01cd718535dd",
                    "LayerId": "6068e458-d720-4276-8287-358274884b26"
                }
            ]
        },
        {
            "id": "dbfca0fb-acf1-4140-b12c-757449f1add8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "compositeImage": {
                "id": "fd77401e-00f6-4854-ac99-18806f52e799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbfca0fb-acf1-4140-b12c-757449f1add8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b648ff2a-f891-489a-8a40-0026f3c29a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbfca0fb-acf1-4140-b12c-757449f1add8",
                    "LayerId": "6068e458-d720-4276-8287-358274884b26"
                }
            ]
        },
        {
            "id": "6c5abc46-d7b9-4a89-a53d-9a3ff284d4db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "compositeImage": {
                "id": "5c529c99-bb98-40c5-8199-7a6bda09cfe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c5abc46-d7b9-4a89-a53d-9a3ff284d4db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2578db5b-55f3-4657-adca-ccb6da28d9b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c5abc46-d7b9-4a89-a53d-9a3ff284d4db",
                    "LayerId": "6068e458-d720-4276-8287-358274884b26"
                }
            ]
        },
        {
            "id": "2b00f73f-ea69-489b-8406-664034878713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "compositeImage": {
                "id": "04770042-1cde-4ebc-863c-65e9e7a7d041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b00f73f-ea69-489b-8406-664034878713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b30d5012-3c3e-4dd9-8474-be54264e07eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b00f73f-ea69-489b-8406-664034878713",
                    "LayerId": "6068e458-d720-4276-8287-358274884b26"
                }
            ]
        },
        {
            "id": "cb721472-f5a0-490a-ac57-df62c88aac3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "compositeImage": {
                "id": "e0897c7d-7fb7-4c59-9439-91d2923b0a08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb721472-f5a0-490a-ac57-df62c88aac3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f636bbf3-0740-4b9b-ae33-d2a7cd8efe43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb721472-f5a0-490a-ac57-df62c88aac3d",
                    "LayerId": "6068e458-d720-4276-8287-358274884b26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 450,
    "layers": [
        {
            "id": "6068e458-d720-4276-8287-358274884b26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c5634da-f1bd-4b01-9e30-f6b2260929c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}
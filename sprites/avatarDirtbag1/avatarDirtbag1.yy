{
    "id": "53245572-cd1b-4502-a538-c4e132e40c1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarDirtbag1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 133,
    "bbox_left": 0,
    "bbox_right": 99,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0300046f-0864-4da3-ba2a-a9459730319e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53245572-cd1b-4502-a538-c4e132e40c1a",
            "compositeImage": {
                "id": "6bf12bf6-0c5c-4d74-a372-6691db4a2828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0300046f-0864-4da3-ba2a-a9459730319e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bda88a3-f68c-4740-91e0-77789436af19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0300046f-0864-4da3-ba2a-a9459730319e",
                    "LayerId": "82db8352-2353-4744-a8b9-c883b46e099a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 134,
    "layers": [
        {
            "id": "82db8352-2353-4744-a8b9-c883b46e099a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53245572-cd1b-4502-a538-c4e132e40c1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}
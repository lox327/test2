{
    "id": "444dbebe-0855-4992-9b04-4cf5c83d81d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hp1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e547d09-0ab1-472e-9c50-aeeb411840c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "444dbebe-0855-4992-9b04-4cf5c83d81d9",
            "compositeImage": {
                "id": "33f9923e-c74f-47da-8e72-87bd8909f00a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e547d09-0ab1-472e-9c50-aeeb411840c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56af10ed-0f90-48c0-9905-df01f1791f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e547d09-0ab1-472e-9c50-aeeb411840c4",
                    "LayerId": "b022e3e1-0b47-401c-9dfb-b190593e5183"
                }
            ]
        },
        {
            "id": "078279a8-d46e-4db1-aa5d-d1144bebb98d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "444dbebe-0855-4992-9b04-4cf5c83d81d9",
            "compositeImage": {
                "id": "37afc883-42d2-4236-9af0-7be45d034aca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "078279a8-d46e-4db1-aa5d-d1144bebb98d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aed1a21-7514-47e3-9a1b-2cb4a4bf33e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "078279a8-d46e-4db1-aa5d-d1144bebb98d",
                    "LayerId": "b022e3e1-0b47-401c-9dfb-b190593e5183"
                }
            ]
        },
        {
            "id": "7e9037dd-d01c-42cb-81ff-cd60f94e0c6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "444dbebe-0855-4992-9b04-4cf5c83d81d9",
            "compositeImage": {
                "id": "ca08eaa0-8cf9-43ae-a374-6330561b67ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e9037dd-d01c-42cb-81ff-cd60f94e0c6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87abf0f3-730f-4263-a827-48888441e9cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e9037dd-d01c-42cb-81ff-cd60f94e0c6b",
                    "LayerId": "b022e3e1-0b47-401c-9dfb-b190593e5183"
                }
            ]
        },
        {
            "id": "5dc05093-ae83-40c6-890f-3048a9e0d500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "444dbebe-0855-4992-9b04-4cf5c83d81d9",
            "compositeImage": {
                "id": "094f4673-53e7-44c8-969a-7a813b498771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dc05093-ae83-40c6-890f-3048a9e0d500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f78f400-f7a3-41ab-9673-d2b970bdb380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dc05093-ae83-40c6-890f-3048a9e0d500",
                    "LayerId": "b022e3e1-0b47-401c-9dfb-b190593e5183"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b022e3e1-0b47-401c-9dfb-b190593e5183",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "444dbebe-0855-4992-9b04-4cf5c83d81d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
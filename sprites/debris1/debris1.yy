{
    "id": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "debris1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "228ae549-90ac-44a7-b987-3895b805423e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "9d1fe780-bfb1-41ff-b4e1-3e114d80632e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "228ae549-90ac-44a7-b987-3895b805423e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d9059ed-3ace-4cbc-88cf-4b9847eb4621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "228ae549-90ac-44a7-b987-3895b805423e",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "16cc4fb7-58cb-4998-9e60-8be9cbf70ac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "9787f266-57fe-4813-a74e-ea000ab7bd24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16cc4fb7-58cb-4998-9e60-8be9cbf70ac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c39f66-ba22-4e41-944a-50015c6cf6f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16cc4fb7-58cb-4998-9e60-8be9cbf70ac2",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "3b21a121-ddc2-4328-bfdb-26d8e150ec82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "3b8934ac-6126-4f3e-b9e8-53edfba2023f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b21a121-ddc2-4328-bfdb-26d8e150ec82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a791f1a-26e6-4fd8-84b0-aca76ba95e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b21a121-ddc2-4328-bfdb-26d8e150ec82",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "c3935313-3735-4fce-be4c-8db103e4d30d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "6c197e6c-5167-40e5-8f61-3f1767c56056",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3935313-3735-4fce-be4c-8db103e4d30d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3b8693a-41b9-4a19-9ad3-c6dccbb536ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3935313-3735-4fce-be4c-8db103e4d30d",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "2557b805-9d3d-4c13-bb96-8c1b691c78b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "6ee5af78-ad46-491e-8b03-a7ce87329cef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2557b805-9d3d-4c13-bb96-8c1b691c78b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b470d3-73de-4963-985b-ad9eb2d6ff07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2557b805-9d3d-4c13-bb96-8c1b691c78b3",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "badb0b83-a11c-4194-8d14-7979808305af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "6cc6546a-edc8-431f-bdd4-d5e74ea764b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "badb0b83-a11c-4194-8d14-7979808305af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe33ef5b-e522-473d-b736-a24014d691c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "badb0b83-a11c-4194-8d14-7979808305af",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "6f1e204d-a8c1-4cab-be40-767d405dcca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "bdbebd67-09b7-4247-b7eb-8fef5dd44340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f1e204d-a8c1-4cab-be40-767d405dcca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f77cb39-cc56-4f94-8d7b-b9198427c4c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f1e204d-a8c1-4cab-be40-767d405dcca8",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "83222ec9-8e46-4d10-8cc5-d51adada3923",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "230b9f0b-9828-405e-8efb-c60a129f79b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83222ec9-8e46-4d10-8cc5-d51adada3923",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8832fa78-f95b-4af9-90bc-8799270e7bcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83222ec9-8e46-4d10-8cc5-d51adada3923",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "b62e4ad0-9fdc-4250-8616-690be757a5a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "19833427-f8e1-416a-ad08-0155c2faf74f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b62e4ad0-9fdc-4250-8616-690be757a5a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "236b4ae5-d5cb-4292-b1f7-79d727fc9a57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b62e4ad0-9fdc-4250-8616-690be757a5a0",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "ffd5564d-0f46-42b6-95d0-8b9c1069d1d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "1b20b74a-7a51-46e6-82f5-8636b751fe1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffd5564d-0f46-42b6-95d0-8b9c1069d1d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04791486-f425-4573-8293-b88981d2e39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffd5564d-0f46-42b6-95d0-8b9c1069d1d5",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        },
        {
            "id": "21396a26-eb64-4ff3-948f-9d3efd15bf45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "compositeImage": {
                "id": "d4492bd0-1ee2-4a4d-8092-21fa99563708",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21396a26-eb64-4ff3-948f-9d3efd15bf45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "685e449a-912d-485b-a4b2-69adab2f0026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21396a26-eb64-4ff3-948f-9d3efd15bf45",
                    "LayerId": "32e3dc5f-a96f-4e47-aed5-1a72452b579a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "32e3dc5f-a96f-4e47-aed5-1a72452b579a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0aeadb6-1c8f-4791-8f07-0085e3ff88e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 67,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "d9c92b6c-f707-4d1a-927c-aae572a28e6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarPike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7af95b00-95ca-450e-bd54-d07b4ba8d22e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9c92b6c-f707-4d1a-927c-aae572a28e6f",
            "compositeImage": {
                "id": "d5e4eed3-614a-4c4f-9eb7-a6b3a0f76e20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7af95b00-95ca-450e-bd54-d07b4ba8d22e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13d823bf-f3bc-4cd0-82c9-65613b773ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7af95b00-95ca-450e-bd54-d07b4ba8d22e",
                    "LayerId": "d0a3b82d-7f77-4aab-b034-51c683d1f0ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "d0a3b82d-7f77-4aab-b034-51c683d1f0ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9c92b6c-f707-4d1a-927c-aae572a28e6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
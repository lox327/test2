{
    "id": "01321ca5-05a6-4a77-8d13-14b6f711dc30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wallMini",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38b42bfe-1065-48a9-9976-4654a3a88f53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01321ca5-05a6-4a77-8d13-14b6f711dc30",
            "compositeImage": {
                "id": "3ba5baf8-d8cf-4ac1-9da3-4ebe49d56b0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38b42bfe-1065-48a9-9976-4654a3a88f53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19247961-33d4-4af5-9b5a-196316a13cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38b42bfe-1065-48a9-9976-4654a3a88f53",
                    "LayerId": "8539a256-85c1-4713-b112-d547d0036f0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8539a256-85c1-4713-b112-d547d0036f0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01321ca5-05a6-4a77-8d13-14b6f711dc30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
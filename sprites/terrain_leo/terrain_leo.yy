{
    "id": "305b3a55-6bfd-4f5b-9d14-9310288764ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "terrain_leo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1194,
    "bbox_left": 223,
    "bbox_right": 1882,
    "bbox_top": 86,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2e137f0-01b9-4c78-8a19-33e2a3236a18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "305b3a55-6bfd-4f5b-9d14-9310288764ed",
            "compositeImage": {
                "id": "e337a37d-5d92-4aa5-8d42-e761008a2c95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2e137f0-01b9-4c78-8a19-33e2a3236a18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90403398-3462-46b0-8e8b-6ee4b24d816f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2e137f0-01b9-4c78-8a19-33e2a3236a18",
                    "LayerId": "164cd9eb-8d68-44dc-b72c-30686a8bebd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1250,
    "layers": [
        {
            "id": "164cd9eb-8d68-44dc-b72c-30686a8bebd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "305b3a55-6bfd-4f5b-9d14-9310288764ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}
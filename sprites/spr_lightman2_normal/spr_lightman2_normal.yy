{
    "id": "7c75f3ec-a9b4-4ca7-a1bc-ce75f1166da4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightman2_normal",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3555217e-81f4-4237-abf9-c5c34a8f2db0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c75f3ec-a9b4-4ca7-a1bc-ce75f1166da4",
            "compositeImage": {
                "id": "d8568a13-c42a-4183-8b86-e785d43a38f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3555217e-81f4-4237-abf9-c5c34a8f2db0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4006350-6f45-49d9-974f-e0fb25546900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3555217e-81f4-4237-abf9-c5c34a8f2db0",
                    "LayerId": "922b5617-ca49-48cb-ba9d-442825d16dac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "922b5617-ca49-48cb-ba9d-442825d16dac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c75f3ec-a9b4-4ca7-a1bc-ce75f1166da4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 48
}
{
    "id": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 11,
    "bbox_right": 49,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d09c847e-4688-41e2-8c0e-6aede39dbb72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "compositeImage": {
                "id": "3e7ca270-61af-428e-a85b-2c76d9fd8c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d09c847e-4688-41e2-8c0e-6aede39dbb72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "844377d7-6fad-434f-98da-6e543a40987e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d09c847e-4688-41e2-8c0e-6aede39dbb72",
                    "LayerId": "dca5f3c5-e969-46f9-b602-74d79f0381c5"
                }
            ]
        },
        {
            "id": "6f927d31-dcaa-49af-acd2-922273916179",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "compositeImage": {
                "id": "2c0973ac-9b80-4890-a0e0-fe29b03dec1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f927d31-dcaa-49af-acd2-922273916179",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7e1c830-6438-412a-9e1b-ec6e186bb4bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f927d31-dcaa-49af-acd2-922273916179",
                    "LayerId": "dca5f3c5-e969-46f9-b602-74d79f0381c5"
                }
            ]
        },
        {
            "id": "7b8ec965-751c-4602-96e4-d315e70eec6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "compositeImage": {
                "id": "7546c726-eba2-4aba-8dc2-791734766bc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b8ec965-751c-4602-96e4-d315e70eec6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83358abe-2810-4719-a86e-03a1c8a7ce7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b8ec965-751c-4602-96e4-d315e70eec6a",
                    "LayerId": "dca5f3c5-e969-46f9-b602-74d79f0381c5"
                }
            ]
        },
        {
            "id": "7fe7440c-5c9e-4bf4-82ff-d3d00b48f3b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "compositeImage": {
                "id": "e328c053-9684-4f36-9959-e96310302e04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fe7440c-5c9e-4bf4-82ff-d3d00b48f3b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d652fc0-9c09-479f-a8dd-6eadc97a70e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fe7440c-5c9e-4bf4-82ff-d3d00b48f3b1",
                    "LayerId": "dca5f3c5-e969-46f9-b602-74d79f0381c5"
                }
            ]
        },
        {
            "id": "c8fc14c7-ab7f-4736-928d-09995c62efd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "compositeImage": {
                "id": "8d7a229e-2144-4ef5-969e-bb3476f737d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8fc14c7-ab7f-4736-928d-09995c62efd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f792dd23-38e3-4d02-8cc7-9bb6e5ec08f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8fc14c7-ab7f-4736-928d-09995c62efd1",
                    "LayerId": "dca5f3c5-e969-46f9-b602-74d79f0381c5"
                }
            ]
        },
        {
            "id": "2fd027fe-acb5-43a5-ae0f-4d6465721e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "compositeImage": {
                "id": "4e363b5b-4278-48c8-a910-687de5298d5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd027fe-acb5-43a5-ae0f-4d6465721e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2b7f898-611b-4d17-9aa9-6362a5c1b428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd027fe-acb5-43a5-ae0f-4d6465721e44",
                    "LayerId": "dca5f3c5-e969-46f9-b602-74d79f0381c5"
                }
            ]
        },
        {
            "id": "4b297ccc-076a-40a3-8cde-f301cefff5f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "compositeImage": {
                "id": "1b1f1334-3a20-41ec-ab00-37c80afeb67e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b297ccc-076a-40a3-8cde-f301cefff5f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0cc42e5-3c73-4821-862c-eebdbbf1e159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b297ccc-076a-40a3-8cde-f301cefff5f2",
                    "LayerId": "dca5f3c5-e969-46f9-b602-74d79f0381c5"
                }
            ]
        },
        {
            "id": "6248caa6-706b-458f-90e9-1e3a7b7bc3d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "compositeImage": {
                "id": "626d4d58-ae2c-4aca-b330-dd60ff125fe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6248caa6-706b-458f-90e9-1e3a7b7bc3d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f512fec4-b1f2-44c9-93dc-5cc8628a2f42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6248caa6-706b-458f-90e9-1e3a7b7bc3d2",
                    "LayerId": "dca5f3c5-e969-46f9-b602-74d79f0381c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "dca5f3c5-e969-46f9-b602-74d79f0381c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0bb44ef-00d8-4b70-8ebf-3b63da5996e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
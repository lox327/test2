{
    "id": "5d0ce56f-e81b-4b70-8087-416ac52e4766",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarReagon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7009bf0a-d6db-474d-a270-d5cdef32b3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d0ce56f-e81b-4b70-8087-416ac52e4766",
            "compositeImage": {
                "id": "3b346787-0549-4d51-b4f0-df9e3b22720d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7009bf0a-d6db-474d-a270-d5cdef32b3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1beb430f-801f-43d6-9e78-019c4f60ddc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7009bf0a-d6db-474d-a270-d5cdef32b3db",
                    "LayerId": "172fddfb-ca5f-487a-b7d3-3ffb50958577"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "172fddfb-ca5f-487a-b7d3-3ffb50958577",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d0ce56f-e81b-4b70-8087-416ac52e4766",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "3b8fa866-d47c-4878-be2d-98bdb7d57aed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05ed8a14-a0a7-4988-853b-c3d3b21e7cc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b8fa866-d47c-4878-be2d-98bdb7d57aed",
            "compositeImage": {
                "id": "6d24aead-3fd6-4a37-9702-1dfc21cb924e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05ed8a14-a0a7-4988-853b-c3d3b21e7cc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76796aff-c345-4caa-b1eb-d35c53813389",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05ed8a14-a0a7-4988-853b-c3d3b21e7cc9",
                    "LayerId": "ccff3833-197b-459a-b60a-4302dbfee0d7"
                }
            ]
        },
        {
            "id": "89f1fa74-0ac4-46be-b785-6d36e7221c78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b8fa866-d47c-4878-be2d-98bdb7d57aed",
            "compositeImage": {
                "id": "365d6c48-6f04-44bf-b949-f6eacdc95c32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89f1fa74-0ac4-46be-b785-6d36e7221c78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c909b615-2f33-48a9-a402-283e38245b26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89f1fa74-0ac4-46be-b785-6d36e7221c78",
                    "LayerId": "ccff3833-197b-459a-b60a-4302dbfee0d7"
                }
            ]
        },
        {
            "id": "e4dbdb32-2f45-472d-b28e-7314735d5763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b8fa866-d47c-4878-be2d-98bdb7d57aed",
            "compositeImage": {
                "id": "84820c8d-b3e6-4eef-850a-7d46023f2add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4dbdb32-2f45-472d-b28e-7314735d5763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e6e39e-d5a0-460d-821f-0c6d56ff1fe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4dbdb32-2f45-472d-b28e-7314735d5763",
                    "LayerId": "ccff3833-197b-459a-b60a-4302dbfee0d7"
                }
            ]
        },
        {
            "id": "d1c1d047-3fbe-4ec4-a46a-6222c1f6f8e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b8fa866-d47c-4878-be2d-98bdb7d57aed",
            "compositeImage": {
                "id": "eb913615-d92b-4770-9d69-54a83ea67aa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1c1d047-3fbe-4ec4-a46a-6222c1f6f8e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec9048cc-fb5b-4ef7-9c2b-de5a198b0a90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1c1d047-3fbe-4ec4-a46a-6222c1f6f8e4",
                    "LayerId": "ccff3833-197b-459a-b60a-4302dbfee0d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ccff3833-197b-459a-b60a-4302dbfee0d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b8fa866-d47c-4878-be2d-98bdb7d57aed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
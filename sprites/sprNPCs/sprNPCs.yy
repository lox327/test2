{
    "id": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprNPCs",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a4321c5-0504-4001-92c7-d0022973916f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "571551de-4b52-4749-91f4-3201f965eac2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a4321c5-0504-4001-92c7-d0022973916f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f2171ad-549b-4eae-8c4b-3828eec79135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a4321c5-0504-4001-92c7-d0022973916f",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "70e1c992-a653-4bed-b8bf-ec483f16f25e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "14a5d51f-16ba-4cdc-ac44-1b64179a6b46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e1c992-a653-4bed-b8bf-ec483f16f25e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50205c8a-0fea-4f81-bcc4-ef29b7532e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e1c992-a653-4bed-b8bf-ec483f16f25e",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "20c2b732-25f6-4440-ac27-7f2239bd0123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "74bdaa27-717d-4691-b217-d9e62d49cfb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20c2b732-25f6-4440-ac27-7f2239bd0123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ac18864-590e-419b-9835-3b35365cf061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20c2b732-25f6-4440-ac27-7f2239bd0123",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "aa936b37-f770-4f1a-940e-8b7761649bcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "fcdca7e0-9456-45a1-857a-e1f69c088b47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa936b37-f770-4f1a-940e-8b7761649bcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "232d1727-791c-492d-bba6-9e059dd762f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa936b37-f770-4f1a-940e-8b7761649bcb",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "95a85e3c-28f9-459e-8235-488c9ff3a594",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "08416d63-41a2-4616-9059-ed512a0883eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95a85e3c-28f9-459e-8235-488c9ff3a594",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849c3565-b435-4f97-b640-aea58d2c1c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95a85e3c-28f9-459e-8235-488c9ff3a594",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "cb6fc2d1-5dba-4070-8bf1-279cc01545de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "f250ffe6-d4c3-471a-9354-5b1281beffb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb6fc2d1-5dba-4070-8bf1-279cc01545de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82dadc26-efb3-443e-b328-3116d1c4d843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb6fc2d1-5dba-4070-8bf1-279cc01545de",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "e6341cf1-70c1-4771-9e7b-6bd30b09cfb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "f219b494-7c70-413e-a84c-f0b645068a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6341cf1-70c1-4771-9e7b-6bd30b09cfb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94af307f-4b01-4210-b91c-984b61651d40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6341cf1-70c1-4771-9e7b-6bd30b09cfb0",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "ee2307fe-8928-4174-9015-bd50effd58b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "60b68f83-53cb-4433-99c8-f91d34dcfdad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2307fe-8928-4174-9015-bd50effd58b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9c2248-8648-486f-bcf7-cc8b6b97cc32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2307fe-8928-4174-9015-bd50effd58b3",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "4737c37a-58c9-4ffe-b2c9-cc417d81cba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "f696e105-330c-43ab-be08-b0a71af5b283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4737c37a-58c9-4ffe-b2c9-cc417d81cba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef56bfc7-66a7-4d18-9245-2d38e0f01fbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4737c37a-58c9-4ffe-b2c9-cc417d81cba6",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "9f4f0476-acc0-4dea-aa02-afcf17ae46ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "94e28345-f601-41a8-81f7-286c93fd11bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4f0476-acc0-4dea-aa02-afcf17ae46ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "668a72d9-deb7-45ae-9c90-e5a36c53c586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4f0476-acc0-4dea-aa02-afcf17ae46ed",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "ecfb23d4-2670-4635-9d0d-21d14ea9d39d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "4646da5c-5545-456f-a9de-2895b19d078c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecfb23d4-2670-4635-9d0d-21d14ea9d39d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d09af61-6917-492e-9a42-3b554c6c5d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecfb23d4-2670-4635-9d0d-21d14ea9d39d",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        },
        {
            "id": "42762479-ab4c-4503-8871-79435822965d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "compositeImage": {
                "id": "db821176-eb5e-49ed-a950-a5fd37da7323",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42762479-ab4c-4503-8871-79435822965d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e666092d-c0ea-482c-9ea6-e69ff57d1cca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42762479-ab4c-4503-8871-79435822965d",
                    "LayerId": "455b5b09-638d-4509-b3ad-8637fd2ba654"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 53,
    "layers": [
        {
            "id": "455b5b09-638d-4509-b3ad-8637fd2ba654",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa6dfb36-183c-48bc-9b22-3fed2587064c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "8b67566e-0d33-4da2-8e37-14f1bcc3ff69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "drifter_down_shoot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 23,
    "bbox_right": 39,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "be64703c-1dc3-4ef5-8255-8a63c1864038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b67566e-0d33-4da2-8e37-14f1bcc3ff69",
            "compositeImage": {
                "id": "8f896a48-33cd-4939-bcfe-cc24e00cb6c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be64703c-1dc3-4ef5-8255-8a63c1864038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2db5073a-504b-4685-b305-7b59e67016c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be64703c-1dc3-4ef5-8255-8a63c1864038",
                    "LayerId": "dac42052-5a9b-4ea4-a1bb-a7fb8b3ef549"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "dac42052-5a9b-4ea4-a1bb-a7fb8b3ef549",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b67566e-0d33-4da2-8e37-14f1bcc3ff69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
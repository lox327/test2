{
    "id": "51dee188-6070-4242-a376-6f1956a6d597",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarSilverhand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 4,
    "bbox_right": 50,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c54e9295-2843-42af-b2ae-00b5a4f8460b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51dee188-6070-4242-a376-6f1956a6d597",
            "compositeImage": {
                "id": "079ce32e-1852-4945-9d89-5bbda7048af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c54e9295-2843-42af-b2ae-00b5a4f8460b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88226ae8-409c-422d-b09e-44bca92895a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c54e9295-2843-42af-b2ae-00b5a4f8460b",
                    "LayerId": "c9761b5f-c8be-40a8-940f-f7bd178306db"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "c9761b5f-c8be-40a8-940f-f7bd178306db",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51dee188-6070-4242-a376-6f1956a6d597",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 54,
    "xorig": 0,
    "yorig": 0
}
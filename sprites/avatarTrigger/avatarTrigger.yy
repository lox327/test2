{
    "id": "366b1603-a1dc-43a9-a635-1e43be9f1ac1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarTrigger",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 10,
    "bbox_right": 45,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10550c05-0e8a-4839-8024-43151514c15d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "366b1603-a1dc-43a9-a635-1e43be9f1ac1",
            "compositeImage": {
                "id": "dfb357ce-6258-44cb-a8a4-f42f1279e34d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10550c05-0e8a-4839-8024-43151514c15d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7875830-4711-4626-97d9-11f097ba269c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10550c05-0e8a-4839-8024-43151514c15d",
                    "LayerId": "a2c15d4e-aadf-4ec0-ac9a-845e8a86ea3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "a2c15d4e-aadf-4ec0-ac9a-845e8a86ea3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "366b1603-a1dc-43a9-a635-1e43be9f1ac1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "cc30ecb4-580b-4b23-b7a3-fd88368a0ba1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shop2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2dca468c-33dc-495d-a6d5-a757bbda8bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc30ecb4-580b-4b23-b7a3-fd88368a0ba1",
            "compositeImage": {
                "id": "192fb902-6d57-4200-ad97-3b65226280b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dca468c-33dc-495d-a6d5-a757bbda8bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d9bc322-546f-4018-8a79-3c50e08258d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dca468c-33dc-495d-a6d5-a757bbda8bc9",
                    "LayerId": "a727e23e-446e-4691-b2e4-965c4f96b177"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "a727e23e-446e-4691-b2e4-965c4f96b177",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc30ecb4-580b-4b23-b7a3-fd88368a0ba1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}
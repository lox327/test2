{
    "id": "dac8ad7a-7676-4ad3-9590-8cb07ced7e96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite150",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 3,
    "bbox_right": 51,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d78f3759-9743-4df7-830d-4b2d79b3fbbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dac8ad7a-7676-4ad3-9590-8cb07ced7e96",
            "compositeImage": {
                "id": "f0ac358e-a232-43a8-a8d3-ad441069f9ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d78f3759-9743-4df7-830d-4b2d79b3fbbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7780a38-b694-4edf-af1f-89bee576187c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d78f3759-9743-4df7-830d-4b2d79b3fbbe",
                    "LayerId": "78a7058f-a722-403f-ba07-9a904afce5ca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "78a7058f-a722-403f-ba07-9a904afce5ca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dac8ad7a-7676-4ad3-9590-8cb07ced7e96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
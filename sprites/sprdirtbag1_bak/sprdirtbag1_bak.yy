{
    "id": "5c7fc475-1154-4575-9556-31103e40ae2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDirtbag1_bak",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ce21021-3731-4a4b-a088-5926b878fb7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c7fc475-1154-4575-9556-31103e40ae2f",
            "compositeImage": {
                "id": "531d6073-3852-419d-b8ae-2a02059d7b36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ce21021-3731-4a4b-a088-5926b878fb7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35d8c6d9-b861-49c7-a9db-dbb3f0d7bef3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ce21021-3731-4a4b-a088-5926b878fb7a",
                    "LayerId": "a1a2df9b-e01e-45cc-b333-c704a2e4c071"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "a1a2df9b-e01e-45cc-b333-c704a2e4c071",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c7fc475-1154-4575-9556-31103e40ae2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 0
}
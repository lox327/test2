{
    "id": "e0944e57-27a1-42c2-b377-2fb896465088",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tank_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c9af6ba-1692-43f9-9a2c-051c6973ca91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0944e57-27a1-42c2-b377-2fb896465088",
            "compositeImage": {
                "id": "e2589779-c45f-4f07-9725-bda465337f54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c9af6ba-1692-43f9-9a2c-051c6973ca91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccc72ab3-32a7-435f-be6a-ce3f7d126a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c9af6ba-1692-43f9-9a2c-051c6973ca91",
                    "LayerId": "ace52dab-86cb-4138-8bda-4e8533199e9e"
                }
            ]
        },
        {
            "id": "016973b1-d2f4-4a16-a56f-4ac934124220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0944e57-27a1-42c2-b377-2fb896465088",
            "compositeImage": {
                "id": "0abbaa4b-e22e-40d5-b594-4bc39476a96c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016973b1-d2f4-4a16-a56f-4ac934124220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece2e9b3-d450-474c-b2a8-60afc91f2135",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016973b1-d2f4-4a16-a56f-4ac934124220",
                    "LayerId": "ace52dab-86cb-4138-8bda-4e8533199e9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "ace52dab-86cb-4138-8bda-4e8533199e9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0944e57-27a1-42c2-b377-2fb896465088",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 14
}
{
    "id": "3eb9b7a8-f629-4ced-bc83-21ab255a8981",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c02a88c-cb4b-4494-80bf-2e46a81410aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eb9b7a8-f629-4ced-bc83-21ab255a8981",
            "compositeImage": {
                "id": "34e07cd0-03bf-41f5-b1fe-c21dcd2231ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c02a88c-cb4b-4494-80bf-2e46a81410aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95747b18-04d5-43d0-ab34-0463bdc0ec9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c02a88c-cb4b-4494-80bf-2e46a81410aa",
                    "LayerId": "4db20e95-1750-4b57-8b9e-85b85175cc8d"
                }
            ]
        },
        {
            "id": "116de6c3-f339-4b1c-8502-cfe15d7fd3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3eb9b7a8-f629-4ced-bc83-21ab255a8981",
            "compositeImage": {
                "id": "95ab5d91-ebfe-482d-99e8-cda5f2b9a267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "116de6c3-f339-4b1c-8502-cfe15d7fd3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0215d890-9ee8-4bbf-b738-11e6f9d03920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "116de6c3-f339-4b1c-8502-cfe15d7fd3dc",
                    "LayerId": "4db20e95-1750-4b57-8b9e-85b85175cc8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "4db20e95-1750-4b57-8b9e-85b85175cc8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3eb9b7a8-f629-4ced-bc83-21ab255a8981",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
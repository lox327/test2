{
    "id": "38a55b98-ce3c-45e3-be56-00e45726d0d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerBashBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 13,
    "bbox_right": 34,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bc2a16d-a609-4117-9ab3-56fd43b2ad32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38a55b98-ce3c-45e3-be56-00e45726d0d8",
            "compositeImage": {
                "id": "52f63d70-e463-41be-8f40-a664ddef7c2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bc2a16d-a609-4117-9ab3-56fd43b2ad32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07012173-f3b8-4517-a44a-baedf5438212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bc2a16d-a609-4117-9ab3-56fd43b2ad32",
                    "LayerId": "ab3d0d22-5b02-4bcf-bffe-8f87c462fe76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "ab3d0d22-5b02-4bcf-bffe-8f87c462fe76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38a55b98-ce3c-45e3-be56-00e45726d0d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 0,
    "yorig": 21
}
{
    "id": "e79cc84b-ba3a-47a3-9d77-640c05c6a82e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr002",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3805c59d-c4ae-41e1-a67b-60c186af7591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e79cc84b-ba3a-47a3-9d77-640c05c6a82e",
            "compositeImage": {
                "id": "9b757ea6-4563-42e3-807e-386e8d021b40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3805c59d-c4ae-41e1-a67b-60c186af7591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78aafce4-16b6-4070-936c-bb5418e8df1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3805c59d-c4ae-41e1-a67b-60c186af7591",
                    "LayerId": "e33b24ce-c4d0-4ecf-a8c2-ff99a5ee518d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "e33b24ce-c4d0-4ecf-a8c2-ff99a5ee518d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e79cc84b-ba3a-47a3-9d77-640c05c6a82e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
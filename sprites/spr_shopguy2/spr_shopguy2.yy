{
    "id": "1895ab3e-de03-489b-879e-f1431c33cba5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shopguy2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9f61d440-2381-4d5a-b6f3-df91ed0c1732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1895ab3e-de03-489b-879e-f1431c33cba5",
            "compositeImage": {
                "id": "3d5e394e-d198-4cc0-8d05-8bf3cc0ed1a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f61d440-2381-4d5a-b6f3-df91ed0c1732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "756495d6-5de2-4ba5-9006-ad68876fdbc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f61d440-2381-4d5a-b6f3-df91ed0c1732",
                    "LayerId": "40881ec4-ed3b-4c60-802a-4b3fb718d304"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 188,
    "layers": [
        {
            "id": "40881ec4-ed3b-4c60-802a-4b3fb718d304",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1895ab3e-de03-489b-879e-f1431c33cba5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 94
}
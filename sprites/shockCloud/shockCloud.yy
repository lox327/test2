{
    "id": "8e51a1b4-c4e9-4df9-b5d6-99d1e8967c5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shockCloud",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 10,
    "bbox_right": 115,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e720141b-b321-44fa-8a1b-f25fafd1c8b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e51a1b4-c4e9-4df9-b5d6-99d1e8967c5e",
            "compositeImage": {
                "id": "96d52aa1-279c-45cc-9419-5cb3061edc9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e720141b-b321-44fa-8a1b-f25fafd1c8b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "388fbc9d-645d-4c00-b786-42aa6ac3c33a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e720141b-b321-44fa-8a1b-f25fafd1c8b1",
                    "LayerId": "950d2f3d-f75a-42d1-b7ea-27dba9312e44"
                }
            ]
        },
        {
            "id": "0f8b50fc-3fbb-4031-bf84-18aecd257e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e51a1b4-c4e9-4df9-b5d6-99d1e8967c5e",
            "compositeImage": {
                "id": "378ee91e-3bec-4ae1-8cbd-4b6a408011ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f8b50fc-3fbb-4031-bf84-18aecd257e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "561a7b60-390b-42af-a9c3-4f1323f2a795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f8b50fc-3fbb-4031-bf84-18aecd257e91",
                    "LayerId": "950d2f3d-f75a-42d1-b7ea-27dba9312e44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "950d2f3d-f75a-42d1-b7ea-27dba9312e44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e51a1b4-c4e9-4df9-b5d6-99d1e8967c5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 66,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}
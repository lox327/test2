{
    "id": "32a3f1e2-d85f-4737-82c5-b1742b372c44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite001",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "165d636b-1528-422a-ae1c-5cd50f895128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32a3f1e2-d85f-4737-82c5-b1742b372c44",
            "compositeImage": {
                "id": "cb0978ff-9594-4f5e-9398-0807df2298da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "165d636b-1528-422a-ae1c-5cd50f895128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ac13cb8-0f47-419e-bfa7-48b373b80400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "165d636b-1528-422a-ae1c-5cd50f895128",
                    "LayerId": "c4f73c42-b241-4652-b939-e10c22d94d9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "c4f73c42-b241-4652-b939-e10c22d94d9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32a3f1e2-d85f-4737-82c5-b1742b372c44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}
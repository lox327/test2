{
    "id": "a2dd49f5-c4f9-4b64-bf48-c8859f2786f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lightman_normal",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 101,
    "bbox_left": 4,
    "bbox_right": 98,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8255d364-07df-41f0-a35c-40a701abb39d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2dd49f5-c4f9-4b64-bf48-c8859f2786f5",
            "compositeImage": {
                "id": "2637cbda-9daf-491c-b8c0-27985251c37d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8255d364-07df-41f0-a35c-40a701abb39d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f40442bd-d7ca-475c-8eab-64b874a41959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8255d364-07df-41f0-a35c-40a701abb39d",
                    "LayerId": "7bb1674e-d959-4812-b72a-272842b8c4b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 106,
    "layers": [
        {
            "id": "7bb1674e-d959-4812-b72a-272842b8c4b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2dd49f5-c4f9-4b64-bf48-c8859f2786f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 103,
    "xorig": 0,
    "yorig": 0
}
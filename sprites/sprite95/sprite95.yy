{
    "id": "0d001614-e8fb-4b4d-ab10-22a8baa177df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite95",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 63,
    "bbox_right": 446,
    "bbox_top": 130,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba419854-9045-444f-895c-cd0bf2cc4454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d001614-e8fb-4b4d-ab10-22a8baa177df",
            "compositeImage": {
                "id": "029e4640-021d-42bb-865c-25a8bd8ad1ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba419854-9045-444f-895c-cd0bf2cc4454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b03fcd-5486-4827-9f04-f4985cccb231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba419854-9045-444f-895c-cd0bf2cc4454",
                    "LayerId": "3b6c1cb5-f609-4c63-8578-27d772b338ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "3b6c1cb5-f609-4c63-8578-27d772b338ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d001614-e8fb-4b4d-ab10-22a8baa177df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
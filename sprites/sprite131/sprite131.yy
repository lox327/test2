{
    "id": "7675a9e2-001e-4c00-a234-2e519f2eeaa7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite131",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 0,
    "bbox_right": 68,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fd8b1f3-153d-4494-9a18-05d1ae150c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7675a9e2-001e-4c00-a234-2e519f2eeaa7",
            "compositeImage": {
                "id": "4a378d99-b0f0-4f0e-a773-06c11d0ebc67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd8b1f3-153d-4494-9a18-05d1ae150c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eee668f7-f3b0-40df-ac94-b3f67307d018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd8b1f3-153d-4494-9a18-05d1ae150c97",
                    "LayerId": "78c52512-7601-4920-b57b-b2a64040513c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 59,
    "layers": [
        {
            "id": "78c52512-7601-4920-b57b-b2a64040513c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7675a9e2-001e-4c00-a234-2e519f2eeaa7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 69,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "10c585bf-2db2-49e7-8115-65340dc76da8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprTylerItems2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "095a9d98-7270-48f5-be23-0421b8069646",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10c585bf-2db2-49e7-8115-65340dc76da8",
            "compositeImage": {
                "id": "df2b2bba-600b-426f-ac73-f176820cbafb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "095a9d98-7270-48f5-be23-0421b8069646",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64788335-b948-4732-9d5b-f87420dcec63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "095a9d98-7270-48f5-be23-0421b8069646",
                    "LayerId": "be594710-f9e4-4bd7-adda-a5784af90349"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 47,
    "layers": [
        {
            "id": "be594710-f9e4-4bd7-adda-a5784af90349",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10c585bf-2db2-49e7-8115-65340dc76da8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 0,
    "yorig": 0
}
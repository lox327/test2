{
    "id": "8669a1c8-d9c3-46c5-99e3-bb5fb4033f7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bullet4a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf4cabf0-dec5-4e90-a99e-133d59bddee3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8669a1c8-d9c3-46c5-99e3-bb5fb4033f7c",
            "compositeImage": {
                "id": "6c22fe84-ce02-4699-a1c0-c6c6d0215741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf4cabf0-dec5-4e90-a99e-133d59bddee3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f58c010-fe15-46f1-a3cc-6a279b3c9f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4cabf0-dec5-4e90-a99e-133d59bddee3",
                    "LayerId": "74e10219-936c-4ced-be0a-519f126c83ce"
                }
            ]
        },
        {
            "id": "abf221e1-6755-432b-a143-1f823c304dab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8669a1c8-d9c3-46c5-99e3-bb5fb4033f7c",
            "compositeImage": {
                "id": "120df7d4-6e8c-428f-9aa7-47b206eeb893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abf221e1-6755-432b-a143-1f823c304dab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b8c0a75-5443-4b4b-94b5-e651cd227454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abf221e1-6755-432b-a143-1f823c304dab",
                    "LayerId": "74e10219-936c-4ced-be0a-519f126c83ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "74e10219-936c-4ced-be0a-519f126c83ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8669a1c8-d9c3-46c5-99e3-bb5fb4033f7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}
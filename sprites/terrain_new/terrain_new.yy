{
    "id": "9bdae581-a760-4174-8a0c-478fc7b661d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "terrain_new",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 150,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3efd669d-1dc7-40d2-b6ad-b7d26b06b6d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bdae581-a760-4174-8a0c-478fc7b661d7",
            "compositeImage": {
                "id": "159655ed-dc1a-4adc-831b-16cafc352e59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3efd669d-1dc7-40d2-b6ad-b7d26b06b6d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da999034-71dc-42f5-beb6-c63535f8002c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3efd669d-1dc7-40d2-b6ad-b7d26b06b6d0",
                    "LayerId": "0312d78b-318a-4ea4-ad63-3a7383ccc50d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "0312d78b-318a-4ea4-ad63-3a7383ccc50d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bdae581-a760-4174-8a0c-478fc7b661d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}
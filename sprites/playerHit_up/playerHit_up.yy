{
    "id": "5306c76c-fbf3-4e6e-b610-a718bc7c9ccd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerHit_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66c65b09-099e-4426-87d7-3c6539be9817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5306c76c-fbf3-4e6e-b610-a718bc7c9ccd",
            "compositeImage": {
                "id": "fe0fb75f-0ba6-4079-9d90-394123190bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66c65b09-099e-4426-87d7-3c6539be9817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cfc2203-ba2b-4a6e-96dd-c724ab4f93bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66c65b09-099e-4426-87d7-3c6539be9817",
                    "LayerId": "3bafb914-2de2-4388-ad2b-6857616c9d10"
                }
            ]
        },
        {
            "id": "996de8e4-c815-4861-b682-7af03403d1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5306c76c-fbf3-4e6e-b610-a718bc7c9ccd",
            "compositeImage": {
                "id": "01449ecb-728c-4446-b9a0-3bbcf7eb5d04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "996de8e4-c815-4861-b682-7af03403d1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3274ff96-2b3b-4c93-83c9-d3aa6dadacbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "996de8e4-c815-4861-b682-7af03403d1f9",
                    "LayerId": "3bafb914-2de2-4388-ad2b-6857616c9d10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "3bafb914-2de2-4388-ad2b-6857616c9d10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5306c76c-fbf3-4e6e-b610-a718bc7c9ccd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
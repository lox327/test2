{
    "id": "b3589bfc-c465-463a-b471-b778fb5e0583",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy4Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 10,
    "bbox_right": 50,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89e72abf-c061-4e5d-8201-832006645d61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "compositeImage": {
                "id": "7584f309-bc4c-489e-9144-6b546e4afb7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89e72abf-c061-4e5d-8201-832006645d61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c19d06-f1e6-4ce4-8885-79d099cc940d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89e72abf-c061-4e5d-8201-832006645d61",
                    "LayerId": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9"
                }
            ]
        },
        {
            "id": "4bd0f181-3171-494b-b41c-d18d17b76d09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "compositeImage": {
                "id": "290be57f-e23c-45ff-ae43-5564fe6310cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd0f181-3171-494b-b41c-d18d17b76d09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a93584-8af9-4851-a8fb-7aa2f6ec221c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd0f181-3171-494b-b41c-d18d17b76d09",
                    "LayerId": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9"
                }
            ]
        },
        {
            "id": "459cef58-e88c-47f7-9098-3c7ab8dfd278",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "compositeImage": {
                "id": "8e6621ae-16bf-42bf-bfa1-6abb000ba21d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "459cef58-e88c-47f7-9098-3c7ab8dfd278",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "049cd76c-96a1-409e-aa63-2d256d1a7ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "459cef58-e88c-47f7-9098-3c7ab8dfd278",
                    "LayerId": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9"
                }
            ]
        },
        {
            "id": "6d201184-8244-4500-9049-81ca4aabbbbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "compositeImage": {
                "id": "a921e5ff-3a78-4dfd-8685-cd0aee16ec47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d201184-8244-4500-9049-81ca4aabbbbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9859c87a-5d9c-420c-9572-23f6b13c1be0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d201184-8244-4500-9049-81ca4aabbbbd",
                    "LayerId": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9"
                }
            ]
        },
        {
            "id": "461b2013-e472-42f3-8344-78a11583413a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "compositeImage": {
                "id": "cd02559a-4427-427e-9ac5-ebd72e751031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "461b2013-e472-42f3-8344-78a11583413a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8914020e-2d21-4482-ba05-4568dc8f5c8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "461b2013-e472-42f3-8344-78a11583413a",
                    "LayerId": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9"
                }
            ]
        },
        {
            "id": "e39d910d-bdeb-41cd-b6d5-d955919c86fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "compositeImage": {
                "id": "e4780de8-cc35-4378-9006-52f699db5556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e39d910d-bdeb-41cd-b6d5-d955919c86fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a27e486-d9a4-49d4-bb37-0fda95962021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e39d910d-bdeb-41cd-b6d5-d955919c86fc",
                    "LayerId": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9"
                }
            ]
        },
        {
            "id": "8c65a72e-47e3-406e-bcc7-04e22247e973",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "compositeImage": {
                "id": "848ceba8-d193-47fd-b57e-1a2c3d0ff884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c65a72e-47e3-406e-bcc7-04e22247e973",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84f65596-a3ca-4c89-a900-55ccde90bdd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c65a72e-47e3-406e-bcc7-04e22247e973",
                    "LayerId": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9"
                }
            ]
        },
        {
            "id": "0a03ddf0-c10b-4f89-bb83-245bd1562fca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "compositeImage": {
                "id": "98fcc72d-3eee-4d3b-b173-6f7f64c14bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a03ddf0-c10b-4f89-bb83-245bd1562fca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f74d66c-1588-4372-a5c8-ac1facc41927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a03ddf0-c10b-4f89-bb83-245bd1562fca",
                    "LayerId": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "57e3baab-2952-4fcb-9350-2cb65ad2e0d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3589bfc-c465-463a-b471-b778fb5e0583",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
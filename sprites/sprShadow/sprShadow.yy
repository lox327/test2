{
    "id": "53d7f0a7-7368-4e30-abcc-4804b8410326",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprShadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5acd8590-71f6-4f91-9336-a21ccc0d0783",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53d7f0a7-7368-4e30-abcc-4804b8410326",
            "compositeImage": {
                "id": "3d7eb6f9-2ac8-46e2-9187-912b0f22524e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5acd8590-71f6-4f91-9336-a21ccc0d0783",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b589687-1761-4c65-8452-c308fcda1e3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5acd8590-71f6-4f91-9336-a21ccc0d0783",
                    "LayerId": "21a7c39e-b709-416c-8923-105be04b9dec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "21a7c39e-b709-416c-8923-105be04b9dec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53d7f0a7-7368-4e30-abcc-4804b8410326",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "ac9a5c93-0726-45cd-8f6b-33fd6a68b345",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerHit_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdea10b0-f063-4364-87e1-7a19dbd61fc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac9a5c93-0726-45cd-8f6b-33fd6a68b345",
            "compositeImage": {
                "id": "d6e05fbc-e8aa-4779-a383-599d119f3d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdea10b0-f063-4364-87e1-7a19dbd61fc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd8422e4-a180-4cf6-ad49-6317d66eebc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdea10b0-f063-4364-87e1-7a19dbd61fc9",
                    "LayerId": "0418c3b4-e588-48d6-9c8f-58474d4eb500"
                }
            ]
        },
        {
            "id": "faebf105-0ff4-423f-ad10-b2e45ca7869d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac9a5c93-0726-45cd-8f6b-33fd6a68b345",
            "compositeImage": {
                "id": "973736fd-8acb-492f-bbc5-8f2c1630d7ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faebf105-0ff4-423f-ad10-b2e45ca7869d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a611a2d-a80f-4fa9-bb61-a79d37fa6187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faebf105-0ff4-423f-ad10-b2e45ca7869d",
                    "LayerId": "0418c3b4-e588-48d6-9c8f-58474d4eb500"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "0418c3b4-e588-48d6-9c8f-58474d4eb500",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac9a5c93-0726-45cd-8f6b-33fd6a68b345",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "3fb15be6-0c05-409a-b551-003ee632de38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerBash_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a69117f0-c0d9-44ee-b55d-412b9878440b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fb15be6-0c05-409a-b551-003ee632de38",
            "compositeImage": {
                "id": "c8800512-52dd-4644-a58a-e3c8e3ddd53b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a69117f0-c0d9-44ee-b55d-412b9878440b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca93070-f10d-4db3-818b-04dfc002595e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a69117f0-c0d9-44ee-b55d-412b9878440b",
                    "LayerId": "8d9482cf-3f11-439f-a0f1-1034ad88b04a"
                }
            ]
        },
        {
            "id": "a9ad9db9-43be-47a8-8f32-2b314c66e70d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fb15be6-0c05-409a-b551-003ee632de38",
            "compositeImage": {
                "id": "8e1e5cca-49ac-4c02-99f1-2fb604d85a4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9ad9db9-43be-47a8-8f32-2b314c66e70d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d357b7fc-7dce-43be-b4d1-f5447f774caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9ad9db9-43be-47a8-8f32-2b314c66e70d",
                    "LayerId": "8d9482cf-3f11-439f-a0f1-1034ad88b04a"
                }
            ]
        },
        {
            "id": "d6996709-b628-48bc-b8dd-9d1fe8c00158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fb15be6-0c05-409a-b551-003ee632de38",
            "compositeImage": {
                "id": "5ee2f76d-7b8b-4c52-a1a6-58e3b50f6bde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6996709-b628-48bc-b8dd-9d1fe8c00158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c5a5b51-de2d-4e3b-8a48-eb324c1aff4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6996709-b628-48bc-b8dd-9d1fe8c00158",
                    "LayerId": "8d9482cf-3f11-439f-a0f1-1034ad88b04a"
                }
            ]
        },
        {
            "id": "aa2120a5-3271-40b7-896f-2d05b5421b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fb15be6-0c05-409a-b551-003ee632de38",
            "compositeImage": {
                "id": "d50674aa-3ad9-4a2a-9086-8b19f07d6e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa2120a5-3271-40b7-896f-2d05b5421b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2289262-a9a4-423f-a16d-48ecbcda0ae6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa2120a5-3271-40b7-896f-2d05b5421b2c",
                    "LayerId": "8d9482cf-3f11-439f-a0f1-1034ad88b04a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "8d9482cf-3f11-439f-a0f1-1034ad88b04a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fb15be6-0c05-409a-b551-003ee632de38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 35
}
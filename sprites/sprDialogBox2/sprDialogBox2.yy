{
    "id": "75d1816c-b77e-49e3-81bd-f2da6af99fec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDialogBox2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a19e5c84-533e-4f97-8079-1d4d7f910b6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d1816c-b77e-49e3-81bd-f2da6af99fec",
            "compositeImage": {
                "id": "7e6fdd88-3de3-4631-9555-9c7f987e6c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a19e5c84-533e-4f97-8079-1d4d7f910b6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87e71a9e-592a-49d4-a863-35aefa15cc25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a19e5c84-533e-4f97-8079-1d4d7f910b6f",
                    "LayerId": "fca33de7-c5fe-44f0-9387-11988cedb91e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fca33de7-c5fe-44f0-9387-11988cedb91e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75d1816c-b77e-49e3-81bd-f2da6af99fec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}
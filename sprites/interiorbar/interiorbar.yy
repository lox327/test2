{
    "id": "9cfca21a-1ea1-4192-aab1-068b4c5c2033",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "interiorbar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e36d89ae-edd7-42d2-97a3-61a73e2d488c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cfca21a-1ea1-4192-aab1-068b4c5c2033",
            "compositeImage": {
                "id": "368041d5-f95d-427b-a890-15a334f925dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e36d89ae-edd7-42d2-97a3-61a73e2d488c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "413ad860-c450-4b8b-87f7-691a9bf45e9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e36d89ae-edd7-42d2-97a3-61a73e2d488c",
                    "LayerId": "4228d746-1e46-455c-be0f-4306a50f3a8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "4228d746-1e46-455c-be0f-4306a50f3a8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cfca21a-1ea1-4192-aab1-068b4c5c2033",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}
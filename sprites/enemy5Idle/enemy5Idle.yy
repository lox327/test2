{
    "id": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 10,
    "bbox_right": 50,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a096f55a-f69e-454a-a574-1736e164cb14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "compositeImage": {
                "id": "6526568d-ddce-4d6a-b888-ac46eed2b6d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a096f55a-f69e-454a-a574-1736e164cb14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a26cd4b2-68f3-4a8f-bdfa-d575241beffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a096f55a-f69e-454a-a574-1736e164cb14",
                    "LayerId": "ed967fc7-1417-4094-80c5-cfd82f63cc11"
                }
            ]
        },
        {
            "id": "c32dbfba-6912-479a-853c-91e79bb945d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "compositeImage": {
                "id": "b0306822-d8a1-4726-857c-10eee128b616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c32dbfba-6912-479a-853c-91e79bb945d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd66209-d174-4cc4-b260-bdf4ceffca04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c32dbfba-6912-479a-853c-91e79bb945d3",
                    "LayerId": "ed967fc7-1417-4094-80c5-cfd82f63cc11"
                }
            ]
        },
        {
            "id": "7876036b-2134-446d-9062-6aa375d88ec6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "compositeImage": {
                "id": "a39199c5-a02f-48b5-a8d9-40b66664a9ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7876036b-2134-446d-9062-6aa375d88ec6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "542b7401-6959-4a0b-835e-63ee9d6a0da7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7876036b-2134-446d-9062-6aa375d88ec6",
                    "LayerId": "ed967fc7-1417-4094-80c5-cfd82f63cc11"
                }
            ]
        },
        {
            "id": "ddadc78a-37eb-4cc0-a9c4-266a48900230",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "compositeImage": {
                "id": "95c4645b-5788-4819-af8d-a9882867159e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddadc78a-37eb-4cc0-a9c4-266a48900230",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed1ed6c-1490-4a08-927a-c56a1affd405",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddadc78a-37eb-4cc0-a9c4-266a48900230",
                    "LayerId": "ed967fc7-1417-4094-80c5-cfd82f63cc11"
                }
            ]
        },
        {
            "id": "45045765-27ac-42dc-8a5c-e99e72e1ba6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "compositeImage": {
                "id": "e0c512cd-b291-4ad7-bece-0871ace9d002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45045765-27ac-42dc-8a5c-e99e72e1ba6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "047a5ab0-07b1-41bb-9053-e22d5b4d796e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45045765-27ac-42dc-8a5c-e99e72e1ba6c",
                    "LayerId": "ed967fc7-1417-4094-80c5-cfd82f63cc11"
                }
            ]
        },
        {
            "id": "70228c14-2983-46c4-8322-b450b05ebe9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "compositeImage": {
                "id": "ecfd265a-0356-4ed7-8c0a-594bd009e6d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70228c14-2983-46c4-8322-b450b05ebe9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66818886-546d-4453-a0d6-52b320179eaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70228c14-2983-46c4-8322-b450b05ebe9a",
                    "LayerId": "ed967fc7-1417-4094-80c5-cfd82f63cc11"
                }
            ]
        },
        {
            "id": "f10ba187-b553-496f-b9fa-510d888a791d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "compositeImage": {
                "id": "46078573-8b62-4489-bbe4-f2f23a6938f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f10ba187-b553-496f-b9fa-510d888a791d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4d0831c-bbff-40aa-8a36-922a1e4c2ae4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f10ba187-b553-496f-b9fa-510d888a791d",
                    "LayerId": "ed967fc7-1417-4094-80c5-cfd82f63cc11"
                }
            ]
        },
        {
            "id": "73c1b745-6630-4ea0-ac89-889251099db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "compositeImage": {
                "id": "d3c959ed-16ae-41df-b90a-d8589e9029e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c1b745-6630-4ea0-ac89-889251099db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4961869-74d7-45fd-b844-e290224c858d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c1b745-6630-4ea0-ac89-889251099db2",
                    "LayerId": "ed967fc7-1417-4094-80c5-cfd82f63cc11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "ed967fc7-1417-4094-80c5-cfd82f63cc11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2100b3f-8120-4fd3-806c-882076f2ea0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "6f764011-bfcb-4426-8ccf-339fec1f0415",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lbl_armor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 12,
    "bbox_right": 55,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8838f2f8-5402-403f-b458-57e803940583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f764011-bfcb-4426-8ccf-339fec1f0415",
            "compositeImage": {
                "id": "d7564051-df7a-4aae-8000-68faa2e8b137",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8838f2f8-5402-403f-b458-57e803940583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee488177-5b28-4936-93b6-d4a968222f29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8838f2f8-5402-403f-b458-57e803940583",
                    "LayerId": "617682d2-693f-4a4a-9422-380206164067"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "617682d2-693f-4a4a-9422-380206164067",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f764011-bfcb-4426-8ccf-339fec1f0415",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "3796c70d-1a51-4baa-b37c-81ebcf8f159a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy5Stun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1250beb-6b0f-4455-ae8b-6cce749125d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796c70d-1a51-4baa-b37c-81ebcf8f159a",
            "compositeImage": {
                "id": "68806242-2242-4160-9a4c-e27a66beb3d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1250beb-6b0f-4455-ae8b-6cce749125d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61a00e13-e903-41ec-9a88-39a93770a005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1250beb-6b0f-4455-ae8b-6cce749125d4",
                    "LayerId": "a298cb65-639a-44ec-8888-b5c6aee6e31e"
                }
            ]
        },
        {
            "id": "a7cab653-c524-4d97-bd74-526426d3355f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3796c70d-1a51-4baa-b37c-81ebcf8f159a",
            "compositeImage": {
                "id": "d9bf73b1-85fd-4172-b2ef-075daf2c1985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7cab653-c524-4d97-bd74-526426d3355f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88efbc81-7619-4b75-9e84-ebc74a11f7f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7cab653-c524-4d97-bd74-526426d3355f",
                    "LayerId": "a298cb65-639a-44ec-8888-b5c6aee6e31e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a298cb65-639a-44ec-8888-b5c6aee6e31e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3796c70d-1a51-4baa-b37c-81ebcf8f159a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
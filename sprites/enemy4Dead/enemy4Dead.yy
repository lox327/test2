{
    "id": "c829ba14-a44d-495c-aee0-3d6f9d9e6e98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy4Dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 49,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f00f086-f54f-4c93-a7e0-872dcee27c0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c829ba14-a44d-495c-aee0-3d6f9d9e6e98",
            "compositeImage": {
                "id": "2275ef99-c719-4f21-988f-f6c1afd2e557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f00f086-f54f-4c93-a7e0-872dcee27c0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "229b330d-ed55-48f3-8af5-40ca288b0e67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f00f086-f54f-4c93-a7e0-872dcee27c0e",
                    "LayerId": "07d4bff8-432c-4088-a881-8df21c98fde7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "07d4bff8-432c-4088-a881-8df21c98fde7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c829ba14-a44d-495c-aee0-3d6f9d9e6e98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
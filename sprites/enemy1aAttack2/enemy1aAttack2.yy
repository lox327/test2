{
    "id": "ae7dc0f9-0b2c-406c-a01e-f456517b65e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aAttack2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21f341f7-8aa3-42c7-9193-fbe5ce5de072",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae7dc0f9-0b2c-406c-a01e-f456517b65e6",
            "compositeImage": {
                "id": "678dc17f-799a-4c90-8c50-b1477198bb20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f341f7-8aa3-42c7-9193-fbe5ce5de072",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8efc8f7-38b1-4edb-9638-c972226ffc9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f341f7-8aa3-42c7-9193-fbe5ce5de072",
                    "LayerId": "896aa2ef-1687-4037-a16e-6075f915844a"
                }
            ]
        },
        {
            "id": "c50fd0d9-21ab-4bfb-9481-8cc58916e496",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae7dc0f9-0b2c-406c-a01e-f456517b65e6",
            "compositeImage": {
                "id": "3f605f93-f79b-4031-87d8-6e2c54654588",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c50fd0d9-21ab-4bfb-9481-8cc58916e496",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "708056e9-ef8b-4ddc-8eba-7f7b024efb15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c50fd0d9-21ab-4bfb-9481-8cc58916e496",
                    "LayerId": "896aa2ef-1687-4037-a16e-6075f915844a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "896aa2ef-1687-4037-a16e-6075f915844a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae7dc0f9-0b2c-406c-a01e-f456517b65e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
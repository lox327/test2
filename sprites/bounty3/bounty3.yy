{
    "id": "a96b2879-cea0-474f-b977-14f511991209",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bounty3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00827fc8-c0b9-40b2-97ec-235721ba45af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a96b2879-cea0-474f-b977-14f511991209",
            "compositeImage": {
                "id": "f39fa473-5915-44e3-aa21-4f964e769dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00827fc8-c0b9-40b2-97ec-235721ba45af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db40ea69-afd5-4a57-9adb-353e2621ecf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00827fc8-c0b9-40b2-97ec-235721ba45af",
                    "LayerId": "ca52072a-a905-4f31-add0-452dd5390372"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "ca52072a-a905-4f31-add0-452dd5390372",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a96b2879-cea0-474f-b977-14f511991209",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
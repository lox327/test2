{
    "id": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy4Attack3_charge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a70e11f-4d2b-4cb5-896b-b16342787182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
            "compositeImage": {
                "id": "d7b7a2cc-be1c-420a-bc9a-c2b2a92643b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a70e11f-4d2b-4cb5-896b-b16342787182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51a793f6-9b66-4a41-afa0-989ebb3e9bcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a70e11f-4d2b-4cb5-896b-b16342787182",
                    "LayerId": "8e03d904-51cf-460c-bd54-c5959a1cd8b7"
                }
            ]
        },
        {
            "id": "01bc4221-809f-4da6-8ff3-8e21220ac3f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
            "compositeImage": {
                "id": "a17d90e8-bd6d-4cbd-97d4-ad0c77913156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01bc4221-809f-4da6-8ff3-8e21220ac3f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "552040e6-eede-4a8c-a680-c5e13b565d6a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01bc4221-809f-4da6-8ff3-8e21220ac3f5",
                    "LayerId": "8e03d904-51cf-460c-bd54-c5959a1cd8b7"
                }
            ]
        },
        {
            "id": "6c20cdb5-872e-4b14-afa1-47482963995b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
            "compositeImage": {
                "id": "7e5ca5f7-7136-4adf-a631-bb9a5b1bb1a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c20cdb5-872e-4b14-afa1-47482963995b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce76f832-357f-4958-9d53-72975c18d8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c20cdb5-872e-4b14-afa1-47482963995b",
                    "LayerId": "8e03d904-51cf-460c-bd54-c5959a1cd8b7"
                }
            ]
        },
        {
            "id": "2c958318-a0de-4faf-b7d5-3e9eaf091064",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
            "compositeImage": {
                "id": "6cf4b996-460c-46c3-aa2b-21eb769d7471",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c958318-a0de-4faf-b7d5-3e9eaf091064",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "256c7b72-22e9-4e3a-a7d0-611481ee0433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c958318-a0de-4faf-b7d5-3e9eaf091064",
                    "LayerId": "8e03d904-51cf-460c-bd54-c5959a1cd8b7"
                }
            ]
        },
        {
            "id": "c8723779-55ee-4820-a51f-1fe31695fa74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
            "compositeImage": {
                "id": "1caf0900-4b0b-4ae3-8ae7-bddd414024b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8723779-55ee-4820-a51f-1fe31695fa74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "725a2e1b-0c23-41d0-825a-d9810e42ca0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8723779-55ee-4820-a51f-1fe31695fa74",
                    "LayerId": "8e03d904-51cf-460c-bd54-c5959a1cd8b7"
                }
            ]
        },
        {
            "id": "0e241090-758f-4ea5-b667-1d9cfe9d165d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
            "compositeImage": {
                "id": "ee2da5f8-6100-47bd-86e8-d9c27e14953a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e241090-758f-4ea5-b667-1d9cfe9d165d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "095908f8-297e-4312-90e1-847c4fd06017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e241090-758f-4ea5-b667-1d9cfe9d165d",
                    "LayerId": "8e03d904-51cf-460c-bd54-c5959a1cd8b7"
                }
            ]
        },
        {
            "id": "d8edfcb2-16a5-4d62-9663-0aeaca47a284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
            "compositeImage": {
                "id": "e700c174-5b9f-4d71-a2e1-6ef31df43987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8edfcb2-16a5-4d62-9663-0aeaca47a284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6eb5ee2-33d9-4728-bcd7-aeebdde05661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8edfcb2-16a5-4d62-9663-0aeaca47a284",
                    "LayerId": "8e03d904-51cf-460c-bd54-c5959a1cd8b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8e03d904-51cf-460c-bd54-c5959a1cd8b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d86a058-be27-4a1b-80dc-f900e94ffe0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
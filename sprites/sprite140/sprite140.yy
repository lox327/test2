{
    "id": "d6c4e410-5b59-4d2f-8217-0551a5ba06fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite140",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 0,
    "bbox_right": 261,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2e9572a-7057-479b-ba29-5b577b81b6cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6c4e410-5b59-4d2f-8217-0551a5ba06fd",
            "compositeImage": {
                "id": "ae494e25-6db5-487c-9369-856bd3ced210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e9572a-7057-479b-ba29-5b577b81b6cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e9a307a-638d-4e6c-b0f2-bdd741016dcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e9572a-7057-479b-ba29-5b577b81b6cf",
                    "LayerId": "987da2ea-df0f-45cc-a94c-694e59423583"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "987da2ea-df0f-45cc-a94c-694e59423583",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6c4e410-5b59-4d2f-8217-0551a5ba06fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 263,
    "xorig": 0,
    "yorig": 0
}
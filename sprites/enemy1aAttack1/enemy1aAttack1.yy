{
    "id": "e5c9322f-c7a9-4d2a-9261-25066d65ac5e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aAttack1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc4c2b5a-438a-461a-81f9-95980c0c51a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5c9322f-c7a9-4d2a-9261-25066d65ac5e",
            "compositeImage": {
                "id": "e33a41ea-0c3b-440b-aaf5-5b9416d7c5dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc4c2b5a-438a-461a-81f9-95980c0c51a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02fc27f0-559c-438f-ab14-ae259e75ec6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc4c2b5a-438a-461a-81f9-95980c0c51a2",
                    "LayerId": "fd43e416-a5ca-48cd-8e71-263d13428171"
                }
            ]
        },
        {
            "id": "4c797566-e035-43c4-8d0f-6f92e337d0ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5c9322f-c7a9-4d2a-9261-25066d65ac5e",
            "compositeImage": {
                "id": "6c4af936-9cfb-4cae-ad97-2aeaada04b60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c797566-e035-43c4-8d0f-6f92e337d0ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8abeeb7-5730-4669-82bb-9f21c80c27a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c797566-e035-43c4-8d0f-6f92e337d0ed",
                    "LayerId": "fd43e416-a5ca-48cd-8e71-263d13428171"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fd43e416-a5ca-48cd-8e71-263d13428171",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5c9322f-c7a9-4d2a-9261-25066d65ac5e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
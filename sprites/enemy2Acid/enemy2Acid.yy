{
    "id": "905bc13b-27b7-4021-8beb-331de3844a4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Acid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "057f9ed4-fc86-4f80-aed8-6b24decbd77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "905bc13b-27b7-4021-8beb-331de3844a4d",
            "compositeImage": {
                "id": "3ca8b00d-1b49-4aec-93a8-e456089314b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "057f9ed4-fc86-4f80-aed8-6b24decbd77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a30478ae-5aa9-4382-b144-3bd98111bdd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "057f9ed4-fc86-4f80-aed8-6b24decbd77d",
                    "LayerId": "059a509b-bd0a-4e34-8b18-7df7b403220a"
                }
            ]
        },
        {
            "id": "12c7a16e-8fff-451a-b1cd-6f967d035324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "905bc13b-27b7-4021-8beb-331de3844a4d",
            "compositeImage": {
                "id": "cdcae2a6-2e65-41ac-9b55-24d1e4374cf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c7a16e-8fff-451a-b1cd-6f967d035324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a33311aa-d235-4987-b31a-1cf3ca75011c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c7a16e-8fff-451a-b1cd-6f967d035324",
                    "LayerId": "059a509b-bd0a-4e34-8b18-7df7b403220a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "059a509b-bd0a-4e34-8b18-7df7b403220a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "905bc13b-27b7-4021-8beb-331de3844a4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
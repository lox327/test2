{
    "id": "fcfbb12a-c950-42e7-84d8-84f0a4c2df25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shopguy1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0f1018c-5ee2-48e0-b085-348bbcccd9f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fcfbb12a-c950-42e7-84d8-84f0a4c2df25",
            "compositeImage": {
                "id": "6ac49b3a-35f3-402f-97c2-6699e2fa4242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0f1018c-5ee2-48e0-b085-348bbcccd9f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7ea4be5-6b82-4fd7-9b2b-4fa6a5d121ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0f1018c-5ee2-48e0-b085-348bbcccd9f7",
                    "LayerId": "8c78b643-acd1-45a5-9c56-c77a6ae35c28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 188,
    "layers": [
        {
            "id": "8c78b643-acd1-45a5-9c56-c77a6ae35c28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fcfbb12a-c950-42e7-84d8-84f0a4c2df25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 94
}
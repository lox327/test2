{
    "id": "c94b35fd-89b7-4a48-a5cc-642409162724",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gas1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "446a1492-d51f-4031-b6b8-a4be5eede684",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c94b35fd-89b7-4a48-a5cc-642409162724",
            "compositeImage": {
                "id": "45e6e06c-d3ac-4080-8746-1e8a9060bea1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "446a1492-d51f-4031-b6b8-a4be5eede684",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "866419be-1860-45f6-87b7-f67b2c840b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "446a1492-d51f-4031-b6b8-a4be5eede684",
                    "LayerId": "cf28bf60-9dea-4434-b731-060850cfd0d8"
                }
            ]
        },
        {
            "id": "dae7ed54-617d-4809-9011-692d4d4c383d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c94b35fd-89b7-4a48-a5cc-642409162724",
            "compositeImage": {
                "id": "6b0aa1bb-ceb0-400e-a510-a8433e2b9721",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dae7ed54-617d-4809-9011-692d4d4c383d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95d738e6-2876-470a-b9d5-8d38a81ddcb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dae7ed54-617d-4809-9011-692d4d4c383d",
                    "LayerId": "cf28bf60-9dea-4434-b731-060850cfd0d8"
                }
            ]
        },
        {
            "id": "78b16e72-c6ee-4d48-924b-7e4480fcebf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c94b35fd-89b7-4a48-a5cc-642409162724",
            "compositeImage": {
                "id": "cab13e44-aeba-4361-9717-af4fa050d514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b16e72-c6ee-4d48-924b-7e4480fcebf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "465061fc-0868-4217-9c27-6134acb755e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b16e72-c6ee-4d48-924b-7e4480fcebf0",
                    "LayerId": "cf28bf60-9dea-4434-b731-060850cfd0d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "cf28bf60-9dea-4434-b731-060850cfd0d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c94b35fd-89b7-4a48-a5cc-642409162724",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
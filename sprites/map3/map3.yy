{
    "id": "300d7ab4-7416-481f-8594-c25d1ce07a38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "map3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1405,
    "bbox_left": 0,
    "bbox_right": 2499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b368bac8-2040-41fc-bde4-d6105057d93d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "300d7ab4-7416-481f-8594-c25d1ce07a38",
            "compositeImage": {
                "id": "c2980ca8-aea5-4b97-8e32-322e736bf597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b368bac8-2040-41fc-bde4-d6105057d93d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fad8706-2cea-4a7b-a5a8-7bd4f0c657f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b368bac8-2040-41fc-bde4-d6105057d93d",
                    "LayerId": "91bf4b03-3394-4fe0-a5e7-7875942d4b12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1406,
    "layers": [
        {
            "id": "91bf4b03-3394-4fe0-a5e7-7875942d4b12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "300d7ab4-7416-481f-8594-c25d1ce07a38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2500,
    "xorig": 0,
    "yorig": 0
}
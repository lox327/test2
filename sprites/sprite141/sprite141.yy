{
    "id": "04a2d9ca-a1b7-4311-8102-3169244aab87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite141",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 4,
    "bbox_left": 0,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "117532ca-dcb5-4260-a11e-5f943247495a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04a2d9ca-a1b7-4311-8102-3169244aab87",
            "compositeImage": {
                "id": "79ced55c-795f-4a79-bd54-a59d43f5971e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "117532ca-dcb5-4260-a11e-5f943247495a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86bdf870-de70-43d1-91b9-87fa097b255c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "117532ca-dcb5-4260-a11e-5f943247495a",
                    "LayerId": "be9ef76a-3459-455c-a937-bc023e31d386"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5,
    "layers": [
        {
            "id": "be9ef76a-3459-455c-a937-bc023e31d386",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04a2d9ca-a1b7-4311-8102-3169244aab87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 29,
    "xorig": 0,
    "yorig": 0
}
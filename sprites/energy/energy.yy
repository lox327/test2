{
    "id": "4b28011b-114f-421b-8490-22f5dfd63497",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "energy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 3,
    "bbox_right": 12,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9237d39c-fe0a-4895-8acb-117e01f00019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b28011b-114f-421b-8490-22f5dfd63497",
            "compositeImage": {
                "id": "a7e482ae-40f3-4391-a64f-b0f7e3d36ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9237d39c-fe0a-4895-8acb-117e01f00019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d4e0260-a928-470c-a43f-5707c41b63a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9237d39c-fe0a-4895-8acb-117e01f00019",
                    "LayerId": "25da8f73-2de9-4b9f-922a-c3083f31b110"
                }
            ]
        },
        {
            "id": "4c85a084-b95f-4aa4-9e5f-6d5361f9fe6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b28011b-114f-421b-8490-22f5dfd63497",
            "compositeImage": {
                "id": "0a2a73fb-ed54-4f56-9876-ce15351e6a8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c85a084-b95f-4aa4-9e5f-6d5361f9fe6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64ea2bfa-16d6-419d-bdf4-0c2f3422cb89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c85a084-b95f-4aa4-9e5f-6d5361f9fe6f",
                    "LayerId": "25da8f73-2de9-4b9f-922a-c3083f31b110"
                }
            ]
        },
        {
            "id": "be450984-3cd0-4927-976e-1d7bd6ae68f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b28011b-114f-421b-8490-22f5dfd63497",
            "compositeImage": {
                "id": "2a565620-24b6-487a-b015-39c926ad4bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be450984-3cd0-4927-976e-1d7bd6ae68f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89afa0cc-7c62-4af8-aef3-82ae08d07faf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be450984-3cd0-4927-976e-1d7bd6ae68f3",
                    "LayerId": "25da8f73-2de9-4b9f-922a-c3083f31b110"
                }
            ]
        },
        {
            "id": "fe05bd04-0f22-4353-8882-67ae4f771a70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b28011b-114f-421b-8490-22f5dfd63497",
            "compositeImage": {
                "id": "d59a205a-0f3b-4cfd-921f-55667d3db556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe05bd04-0f22-4353-8882-67ae4f771a70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbfb55cb-cc5e-40f4-a9b8-9e3a39f5988f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe05bd04-0f22-4353-8882-67ae4f771a70",
                    "LayerId": "25da8f73-2de9-4b9f-922a-c3083f31b110"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "25da8f73-2de9-4b9f-922a-c3083f31b110",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b28011b-114f-421b-8490-22f5dfd63497",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}
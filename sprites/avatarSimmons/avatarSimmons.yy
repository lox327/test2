{
    "id": "a416f98e-00df-48e4-8721-e202e83325a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "avatarSimmons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8479d1d-a58d-441c-89bc-7837ee8b6f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a416f98e-00df-48e4-8721-e202e83325a0",
            "compositeImage": {
                "id": "7a1e44b8-45b1-4aa9-975b-fb7bfaad1cf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8479d1d-a58d-441c-89bc-7837ee8b6f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e411f86a-9409-440a-8e48-2b974dbeddb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8479d1d-a58d-441c-89bc-7837ee8b6f5f",
                    "LayerId": "ca22e163-bffb-4239-ab44-8aef44d4fd82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "ca22e163-bffb-4239-ab44-8aef44d4fd82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a416f98e-00df-48e4-8721-e202e83325a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 56,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "2657cb5c-1dee-4fab-b5ce-2a98e9a04781",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy4Attack3_charge2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "311d27b8-c84c-49b1-b1ad-45d4c45f3895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2657cb5c-1dee-4fab-b5ce-2a98e9a04781",
            "compositeImage": {
                "id": "be6735e2-ab6b-4dc7-9ae8-b2ee457228cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "311d27b8-c84c-49b1-b1ad-45d4c45f3895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a7c9250-33d8-467c-aea6-59f39b8e4c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "311d27b8-c84c-49b1-b1ad-45d4c45f3895",
                    "LayerId": "9202c4fe-39d1-4aa8-b44f-ab895baa384e"
                }
            ]
        },
        {
            "id": "cfbc2fb1-32d4-4f84-839f-27a3c3bf6c58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2657cb5c-1dee-4fab-b5ce-2a98e9a04781",
            "compositeImage": {
                "id": "754bde2a-cba0-4039-9c1e-1c5f470eb66e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfbc2fb1-32d4-4f84-839f-27a3c3bf6c58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06c337db-efc5-44df-93f1-af2a5811bd84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfbc2fb1-32d4-4f84-839f-27a3c3bf6c58",
                    "LayerId": "9202c4fe-39d1-4aa8-b44f-ab895baa384e"
                }
            ]
        },
        {
            "id": "dbdec77e-563b-4154-acfb-79c7fe5c367f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2657cb5c-1dee-4fab-b5ce-2a98e9a04781",
            "compositeImage": {
                "id": "41446c07-5406-4750-b71a-07f1e500a84a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbdec77e-563b-4154-acfb-79c7fe5c367f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00e0d741-588f-4725-bb38-cc861835d9c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbdec77e-563b-4154-acfb-79c7fe5c367f",
                    "LayerId": "9202c4fe-39d1-4aa8-b44f-ab895baa384e"
                }
            ]
        },
        {
            "id": "e7a03d17-ec3a-409c-b610-9efa7a561fa0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2657cb5c-1dee-4fab-b5ce-2a98e9a04781",
            "compositeImage": {
                "id": "9b642bf7-236e-4d0c-b1a7-fa9c4a0beb0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7a03d17-ec3a-409c-b610-9efa7a561fa0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf55d2eb-5274-47bd-b9af-52d8e16484cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7a03d17-ec3a-409c-b610-9efa7a561fa0",
                    "LayerId": "9202c4fe-39d1-4aa8-b44f-ab895baa384e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9202c4fe-39d1-4aa8-b44f-ab895baa384e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2657cb5c-1dee-4fab-b5ce-2a98e9a04781",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
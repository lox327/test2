{
    "id": "1d9f3746-45c7-42ec-afe5-7bc7604df280",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "emptySlot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1e1d07f-bf90-4dc4-907b-dded8e251966",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d9f3746-45c7-42ec-afe5-7bc7604df280",
            "compositeImage": {
                "id": "1c4936a0-4095-4406-93c8-5323a475bbd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e1d07f-bf90-4dc4-907b-dded8e251966",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af6aa3e5-e06d-4074-b526-422a164fa7f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e1d07f-bf90-4dc4-907b-dded8e251966",
                    "LayerId": "2185fa48-aa73-4aa0-b022-a95513e42e4d"
                },
                {
                    "id": "67fbf257-5ccf-4eb7-8a84-afca59f754f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e1d07f-bf90-4dc4-907b-dded8e251966",
                    "LayerId": "79822733-6c86-461b-ab83-603f07bc141f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2185fa48-aa73-4aa0-b022-a95513e42e4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d9f3746-45c7-42ec-afe5-7bc7604df280",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 60,
            "visible": true
        },
        {
            "id": "79822733-6c86-461b-ab83-603f07bc141f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d9f3746-45c7-42ec-afe5-7bc7604df280",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
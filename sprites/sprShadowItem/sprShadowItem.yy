{
    "id": "652b51d2-a9d8-4446-ac75-9abfaf35e34c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprShadowItem",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a42d3c7-595f-44fd-aa91-0a7e39c8a475",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "652b51d2-a9d8-4446-ac75-9abfaf35e34c",
            "compositeImage": {
                "id": "014fc85e-b3f1-4662-8697-52cb033a5dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a42d3c7-595f-44fd-aa91-0a7e39c8a475",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "671b1bfb-bb1a-469d-8fc4-fda0979a7e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a42d3c7-595f-44fd-aa91-0a7e39c8a475",
                    "LayerId": "02511771-dda2-40c8-9385-9f7cb2dfd40c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "02511771-dda2-40c8-9385-9f7cb2dfd40c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "652b51d2-a9d8-4446-ac75-9abfaf35e34c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 0
}
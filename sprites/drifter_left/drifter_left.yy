{
    "id": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "drifter_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca446035-fe6f-4559-bb47-be5b09340ae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "compositeImage": {
                "id": "f3f9423a-e068-4a9a-9c32-d76890aec8d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca446035-fe6f-4559-bb47-be5b09340ae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f70960a4-5ceb-4ab1-a70d-8209e9d21fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca446035-fe6f-4559-bb47-be5b09340ae3",
                    "LayerId": "18360c38-887a-4b03-8a85-27c7a0490b48"
                }
            ]
        },
        {
            "id": "5cca4090-e310-46bc-976b-64682e2c48a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "compositeImage": {
                "id": "e305d42e-56c7-4628-9e1a-e9a7740752ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cca4090-e310-46bc-976b-64682e2c48a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876bfc77-32ec-4973-a14a-86ca4542e6fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cca4090-e310-46bc-976b-64682e2c48a9",
                    "LayerId": "18360c38-887a-4b03-8a85-27c7a0490b48"
                }
            ]
        },
        {
            "id": "9790a2b7-b7f8-463b-a442-865a5f0c0be2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "compositeImage": {
                "id": "fe4bcca5-4c1c-4db1-a8de-76a1f950f230",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9790a2b7-b7f8-463b-a442-865a5f0c0be2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fde66697-f0aa-4267-8ab6-b77100123a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9790a2b7-b7f8-463b-a442-865a5f0c0be2",
                    "LayerId": "18360c38-887a-4b03-8a85-27c7a0490b48"
                }
            ]
        },
        {
            "id": "877c3305-f8a7-44e7-9735-c56b80251009",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "compositeImage": {
                "id": "11c46eee-317d-4d8c-8794-cf46d1fa5753",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "877c3305-f8a7-44e7-9735-c56b80251009",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0d715da-d738-448f-9e5c-edfdc5e2e253",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "877c3305-f8a7-44e7-9735-c56b80251009",
                    "LayerId": "18360c38-887a-4b03-8a85-27c7a0490b48"
                }
            ]
        },
        {
            "id": "c351d5f3-f995-454c-82fa-8f3d29d77dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "compositeImage": {
                "id": "ecfa8f2b-026a-4b36-9958-ef7d2249a384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c351d5f3-f995-454c-82fa-8f3d29d77dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fe06221-341e-4c65-89bf-8c59e973f201",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c351d5f3-f995-454c-82fa-8f3d29d77dc0",
                    "LayerId": "18360c38-887a-4b03-8a85-27c7a0490b48"
                }
            ]
        },
        {
            "id": "dfefe386-76ee-4375-8172-50f755d474da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "compositeImage": {
                "id": "a4a5d5c0-9015-4479-ac1b-19b41d3990da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfefe386-76ee-4375-8172-50f755d474da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8bfe12e-8ee3-4436-bbed-ff33ad1ccb79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfefe386-76ee-4375-8172-50f755d474da",
                    "LayerId": "18360c38-887a-4b03-8a85-27c7a0490b48"
                }
            ]
        },
        {
            "id": "8e5e8335-c843-4411-83a0-9955bc95338d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "compositeImage": {
                "id": "6f30aa76-49a4-420d-938d-0625d8861853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e5e8335-c843-4411-83a0-9955bc95338d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d63ed35f-f083-446d-b479-c3b3c1025aa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e5e8335-c843-4411-83a0-9955bc95338d",
                    "LayerId": "18360c38-887a-4b03-8a85-27c7a0490b48"
                }
            ]
        },
        {
            "id": "01e54517-2489-47aa-8cf0-a87f4236eb01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "compositeImage": {
                "id": "b1ce20cb-d3cb-469b-80bd-991d85c0a070",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01e54517-2489-47aa-8cf0-a87f4236eb01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bdc397-08ca-418c-9cba-d46f0ecfdbf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01e54517-2489-47aa-8cf0-a87f4236eb01",
                    "LayerId": "18360c38-887a-4b03-8a85-27c7a0490b48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "18360c38-887a-4b03-8a85-27c7a0490b48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b83a238-b63f-4eeb-aee5-7a902d2996ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 30
}
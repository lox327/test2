{
    "id": "8bca3e27-b8b8-42d8-b841-4e9338543d59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "emptyBounty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6572082c-d8b6-4a82-b4d6-48beefb2df84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bca3e27-b8b8-42d8-b841-4e9338543d59",
            "compositeImage": {
                "id": "89886509-4321-4d7f-8686-95dabcd1529f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6572082c-d8b6-4a82-b4d6-48beefb2df84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be425a22-a4b2-46e0-9627-7f6855488ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6572082c-d8b6-4a82-b4d6-48beefb2df84",
                    "LayerId": "d4a4688a-d6bb-4867-aa4e-42cd07c80acc"
                },
                {
                    "id": "3ef46674-6d9e-4b02-9534-ab5b01cb1e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6572082c-d8b6-4a82-b4d6-48beefb2df84",
                    "LayerId": "8eb633f7-49b3-4ac4-b070-9e462174b475"
                }
            ]
        },
        {
            "id": "509474bc-c0fa-424e-b7d9-e0686c4b09f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bca3e27-b8b8-42d8-b841-4e9338543d59",
            "compositeImage": {
                "id": "488bd78f-fcbb-4149-93db-1f6b589cacc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "509474bc-c0fa-424e-b7d9-e0686c4b09f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c787d8dd-19d6-4b21-8ffe-ccb7ff8e6a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "509474bc-c0fa-424e-b7d9-e0686c4b09f8",
                    "LayerId": "d4a4688a-d6bb-4867-aa4e-42cd07c80acc"
                },
                {
                    "id": "00db174d-2574-4cfc-a010-19567b58ac71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "509474bc-c0fa-424e-b7d9-e0686c4b09f8",
                    "LayerId": "8eb633f7-49b3-4ac4-b070-9e462174b475"
                }
            ]
        },
        {
            "id": "07cfcce9-b094-40b7-b628-4db68d49ad23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bca3e27-b8b8-42d8-b841-4e9338543d59",
            "compositeImage": {
                "id": "60d2bc92-472b-4c89-9c61-0439a4bc81f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07cfcce9-b094-40b7-b628-4db68d49ad23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a3ba3b4-cc37-4261-8471-c7e28589402f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07cfcce9-b094-40b7-b628-4db68d49ad23",
                    "LayerId": "d4a4688a-d6bb-4867-aa4e-42cd07c80acc"
                },
                {
                    "id": "2584a2f8-82b1-4b85-ad7f-a2fc9c3796d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07cfcce9-b094-40b7-b628-4db68d49ad23",
                    "LayerId": "8eb633f7-49b3-4ac4-b070-9e462174b475"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "8eb633f7-49b3-4ac4-b070-9e462174b475",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bca3e27-b8b8-42d8-b841-4e9338543d59",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d4a4688a-d6bb-4867-aa4e-42cd07c80acc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bca3e27-b8b8-42d8-b841-4e9338543d59",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2 (2)",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "2128272a-68b7-4f43-9de5-1f19a22106a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy4Down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 11,
    "bbox_right": 49,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d531492f-7c95-4323-8c30-2fe9a588b0af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "compositeImage": {
                "id": "1608722f-f70c-4759-b0fe-fde8676ac9d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d531492f-7c95-4323-8c30-2fe9a588b0af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0869565c-2978-4c3c-bdb8-65decebe18f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d531492f-7c95-4323-8c30-2fe9a588b0af",
                    "LayerId": "73962594-bfe0-4f84-84a5-59cf4fafb8a9"
                }
            ]
        },
        {
            "id": "e85cb507-267f-4ec9-83cd-cf71e3f8b78c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "compositeImage": {
                "id": "14203909-35f0-4663-89cf-e3589355d898",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e85cb507-267f-4ec9-83cd-cf71e3f8b78c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4ba5ef-b1f2-41e5-b890-dfca47033797",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e85cb507-267f-4ec9-83cd-cf71e3f8b78c",
                    "LayerId": "73962594-bfe0-4f84-84a5-59cf4fafb8a9"
                }
            ]
        },
        {
            "id": "c92040eb-02c3-4196-810c-36dd23965328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "compositeImage": {
                "id": "bef4719a-85f9-467a-ba1a-ed0b0ae63816",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c92040eb-02c3-4196-810c-36dd23965328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41b92914-99b1-4594-ae2a-15f5414437c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c92040eb-02c3-4196-810c-36dd23965328",
                    "LayerId": "73962594-bfe0-4f84-84a5-59cf4fafb8a9"
                }
            ]
        },
        {
            "id": "75f68fa5-b9e8-4338-a3e0-362782327e18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "compositeImage": {
                "id": "769be436-02fa-4020-8346-1db106cfdf7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75f68fa5-b9e8-4338-a3e0-362782327e18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f49cdb-5098-423c-91a1-604e25cc09b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75f68fa5-b9e8-4338-a3e0-362782327e18",
                    "LayerId": "73962594-bfe0-4f84-84a5-59cf4fafb8a9"
                }
            ]
        },
        {
            "id": "9f5702a6-6cef-4b5e-94a1-1d9339c3121e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "compositeImage": {
                "id": "96a8864e-ca80-4657-af27-b1c85a43b4f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f5702a6-6cef-4b5e-94a1-1d9339c3121e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea2f7d2-a0ce-4c93-a893-27fdee1eef05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f5702a6-6cef-4b5e-94a1-1d9339c3121e",
                    "LayerId": "73962594-bfe0-4f84-84a5-59cf4fafb8a9"
                }
            ]
        },
        {
            "id": "589d2973-d572-4f32-b29a-173b4904d8c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "compositeImage": {
                "id": "fbf5f002-46a7-43d1-9b96-56807ad0e570",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "589d2973-d572-4f32-b29a-173b4904d8c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c8ab4f-9dba-436b-9cd9-1510d78a815a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "589d2973-d572-4f32-b29a-173b4904d8c1",
                    "LayerId": "73962594-bfe0-4f84-84a5-59cf4fafb8a9"
                }
            ]
        },
        {
            "id": "44b2ce3a-23ab-4262-9bbe-42cd29de8ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "compositeImage": {
                "id": "4215873d-410a-4448-bf53-245bdfd066a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b2ce3a-23ab-4262-9bbe-42cd29de8ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9db296e8-1f16-4bf6-8ae5-f02df44be26c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b2ce3a-23ab-4262-9bbe-42cd29de8ca7",
                    "LayerId": "73962594-bfe0-4f84-84a5-59cf4fafb8a9"
                }
            ]
        },
        {
            "id": "97f81352-a655-4619-8a04-b4e35ad10c65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "compositeImage": {
                "id": "28baeef3-63de-4bcb-be2e-4565441bef32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97f81352-a655-4619-8a04-b4e35ad10c65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de08799-4939-4454-aebe-400cc0e8312b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97f81352-a655-4619-8a04-b4e35ad10c65",
                    "LayerId": "73962594-bfe0-4f84-84a5-59cf4fafb8a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "73962594-bfe0-4f84-84a5-59cf4fafb8a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2128272a-68b7-4f43-9de5-1f19a22106a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "daaf660d-4f56-454c-bc17-da0bf2f59fee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d6183ae-02bf-4dbc-af7c-01f18776bd42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaf660d-4f56-454c-bc17-da0bf2f59fee",
            "compositeImage": {
                "id": "1b384adc-0109-43da-acd7-f22d22761db7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d6183ae-02bf-4dbc-af7c-01f18776bd42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61bbb1b3-6502-4af1-8af5-7a4c32d8073e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d6183ae-02bf-4dbc-af7c-01f18776bd42",
                    "LayerId": "595be254-1463-4682-8437-fcd3cf24729e"
                }
            ]
        },
        {
            "id": "6ff9c89d-1e4b-4085-92eb-8bee125f72fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "daaf660d-4f56-454c-bc17-da0bf2f59fee",
            "compositeImage": {
                "id": "8b51de5b-38f3-4ad1-851c-99360e5d098d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ff9c89d-1e4b-4085-92eb-8bee125f72fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df23aa49-6545-4e79-a155-451c4b3b6881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ff9c89d-1e4b-4085-92eb-8bee125f72fb",
                    "LayerId": "595be254-1463-4682-8437-fcd3cf24729e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "595be254-1463-4682-8437-fcd3cf24729e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "daaf660d-4f56-454c-bc17-da0bf2f59fee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}
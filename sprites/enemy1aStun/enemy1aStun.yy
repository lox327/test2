{
    "id": "d9f30402-3b9c-4cc3-81ea-1c04635968b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aStun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1083d15-ffad-496d-bc26-6c0cea687dc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f30402-3b9c-4cc3-81ea-1c04635968b7",
            "compositeImage": {
                "id": "01902da1-794b-44dd-92e2-9ac0e727c34c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1083d15-ffad-496d-bc26-6c0cea687dc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a177d02e-7081-4edf-84d7-56826724e295",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1083d15-ffad-496d-bc26-6c0cea687dc4",
                    "LayerId": "8ffe848c-1e7d-418d-8e5b-a3739f76dc17"
                }
            ]
        },
        {
            "id": "63b52bd5-d58d-469d-b4d2-644c03847369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9f30402-3b9c-4cc3-81ea-1c04635968b7",
            "compositeImage": {
                "id": "3467cabd-7f19-40cd-afac-9f683ca87174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b52bd5-d58d-469d-b4d2-644c03847369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e515068-5071-4b92-90c9-5a8220d8d09d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b52bd5-d58d-469d-b4d2-644c03847369",
                    "LayerId": "8ffe848c-1e7d-418d-8e5b-a3739f76dc17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8ffe848c-1e7d-418d-8e5b-a3739f76dc17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9f30402-3b9c-4cc3-81ea-1c04635968b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
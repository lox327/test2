{
    "id": "4ea7e8c1-bd62-4b9c-8a9b-9926e0930586",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shop1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4739d57-d9ab-4f16-96dd-c8b9df678e39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ea7e8c1-bd62-4b9c-8a9b-9926e0930586",
            "compositeImage": {
                "id": "df708e5f-d0bb-4bc0-8138-d1abb2ccaf06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4739d57-d9ab-4f16-96dd-c8b9df678e39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "403f94d0-4ed0-460d-9338-45eded02ed25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4739d57-d9ab-4f16-96dd-c8b9df678e39",
                    "LayerId": "b4f830c9-22a2-444c-8593-61734acc7d95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "b4f830c9-22a2-444c-8593-61734acc7d95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ea7e8c1-bd62-4b9c-8a9b-9926e0930586",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "310b232d-dede-47c2-87a7-6655c509cdcf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerHit_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "656fda45-b4fd-4f43-af6d-76950725a51b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "310b232d-dede-47c2-87a7-6655c509cdcf",
            "compositeImage": {
                "id": "01f60823-9482-4ca1-9191-d1782dea31df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "656fda45-b4fd-4f43-af6d-76950725a51b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea0cac7a-7098-4006-8ef3-a777e2a1d1c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "656fda45-b4fd-4f43-af6d-76950725a51b",
                    "LayerId": "68a0f9c8-d798-45e7-ada1-1f7d0ffb21c3"
                }
            ]
        },
        {
            "id": "4918e3a3-30b3-485a-a164-bb16f848aff6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "310b232d-dede-47c2-87a7-6655c509cdcf",
            "compositeImage": {
                "id": "be47995f-1400-44bc-8c50-ac7022704866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4918e3a3-30b3-485a-a164-bb16f848aff6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39465986-4f35-4113-941a-11faea07a560",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4918e3a3-30b3-485a-a164-bb16f848aff6",
                    "LayerId": "68a0f9c8-d798-45e7-ada1-1f7d0ffb21c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "68a0f9c8-d798-45e7-ada1-1f7d0ffb21c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "310b232d-dede-47c2-87a7-6655c509cdcf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "3e49e247-b6c0-412d-85bb-de8fcb9c021b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bountyAvatar2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 16,
    "bbox_right": 63,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "745e91b8-7a86-4c03-97ba-867cf18559c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e49e247-b6c0-412d-85bb-de8fcb9c021b",
            "compositeImage": {
                "id": "32a3214d-273b-4150-9a82-ee8e49c08468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "745e91b8-7a86-4c03-97ba-867cf18559c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82ab4c15-7c51-40bb-878f-59922fec80a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "745e91b8-7a86-4c03-97ba-867cf18559c8",
                    "LayerId": "633eb01d-b115-43bb-b0a7-3fc695266280"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 212,
    "layers": [
        {
            "id": "633eb01d-b115-43bb-b0a7-3fc695266280",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e49e247-b6c0-412d-85bb-de8fcb9c021b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}
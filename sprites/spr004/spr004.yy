{
    "id": "8bbe148f-4fbe-4082-87fa-f1b4e50cea25",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr004",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b604620-0371-4885-a53a-912a2ce6fd54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bbe148f-4fbe-4082-87fa-f1b4e50cea25",
            "compositeImage": {
                "id": "a7e381bc-e739-4452-bff3-8574500f0392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b604620-0371-4885-a53a-912a2ce6fd54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a173858e-0087-4fc0-ba3c-4a454bad9869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b604620-0371-4885-a53a-912a2ce6fd54",
                    "LayerId": "c18eedbc-6eb5-45a2-9e0e-306c11615474"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "c18eedbc-6eb5-45a2-9e0e-306c11615474",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bbe148f-4fbe-4082-87fa-f1b4e50cea25",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 0,
    "yorig": 0
}
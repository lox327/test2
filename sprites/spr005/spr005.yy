{
    "id": "f5058f77-7b09-4e7d-b4ff-b1bc89eb92bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr005",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b431a39b-8b2c-40fe-8aea-a889f95bc362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5058f77-7b09-4e7d-b4ff-b1bc89eb92bf",
            "compositeImage": {
                "id": "7e3898b9-d63c-461f-bc08-0c5ea63bbcea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b431a39b-8b2c-40fe-8aea-a889f95bc362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca8d0a6-e9be-4b19-98dd-a78bb6cd78a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b431a39b-8b2c-40fe-8aea-a889f95bc362",
                    "LayerId": "13c8e131-2126-49a9-aa5f-0910c8d21ba8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "13c8e131-2126-49a9-aa5f-0910c8d21ba8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5058f77-7b09-4e7d-b4ff-b1bc89eb92bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 14,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "2adfddec-bd8a-42a8-8e19-9694b9b45455",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "map1a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1405,
    "bbox_left": 0,
    "bbox_right": 2499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c928c8d4-c028-4ed9-811a-803472f3ffaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2adfddec-bd8a-42a8-8e19-9694b9b45455",
            "compositeImage": {
                "id": "181a01a3-7c9e-41ca-bef0-89f2bff952f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c928c8d4-c028-4ed9-811a-803472f3ffaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6da615c8-9cfd-45d8-9f52-d36e87e59ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c928c8d4-c028-4ed9-811a-803472f3ffaa",
                    "LayerId": "ceb87bea-f92e-469c-af0b-58084af3afd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1406,
    "layers": [
        {
            "id": "ceb87bea-f92e-469c-af0b-58084af3afd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2adfddec-bd8a-42a8-8e19-9694b9b45455",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2500,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "debrisLarge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cd15409-d3dd-440b-ac02-4819e5bca94d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "984d34c1-e531-4aa7-aff1-eaadd1d88d8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd15409-d3dd-440b-ac02-4819e5bca94d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2b0130c-11b1-4ef7-9743-e2e11e74c180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd15409-d3dd-440b-ac02-4819e5bca94d",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "586d9254-9e0d-427b-8fb4-65100cbd2823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "1e4f82f9-4f8e-4d1b-b78b-a8de6c0eff76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "586d9254-9e0d-427b-8fb4-65100cbd2823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "000bebbe-f0d3-45b3-83d6-2aece493ea3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "586d9254-9e0d-427b-8fb4-65100cbd2823",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "caff9053-4162-4f27-8e1f-7f9434c853df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "f8548d5b-a854-4f19-a802-8567fc5a10eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caff9053-4162-4f27-8e1f-7f9434c853df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77c9d731-5844-485d-984e-c53fbcfd1a79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caff9053-4162-4f27-8e1f-7f9434c853df",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "95d814b1-f670-43f3-9ced-af63bd540177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "929b40bd-45ca-4f97-8fb2-c45dba124895",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95d814b1-f670-43f3-9ced-af63bd540177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0248819d-acd2-4ae3-b54e-b19ead573698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95d814b1-f670-43f3-9ced-af63bd540177",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "fe2cf49d-ae13-45b9-8878-d62f5736e102",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "d7943206-b4da-405e-9ca4-19b7ee317ced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe2cf49d-ae13-45b9-8878-d62f5736e102",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a102a2-75b9-47b9-8a9d-5b66c71e90af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe2cf49d-ae13-45b9-8878-d62f5736e102",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "c2bf60e5-3fd2-4ca7-9b6a-f1bb18549f94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "335910ab-2830-49d1-832c-7733849daf71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2bf60e5-3fd2-4ca7-9b6a-f1bb18549f94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c6d4e2e-06e9-49d1-b874-262b7d799a76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2bf60e5-3fd2-4ca7-9b6a-f1bb18549f94",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "be733343-97b3-4a14-ac1e-b2356674f10f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "5615fc18-f528-466f-b110-0c3a7ff02ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be733343-97b3-4a14-ac1e-b2356674f10f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fe85424-e1ed-4392-a469-27e1782e56db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be733343-97b3-4a14-ac1e-b2356674f10f",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "cea17e43-718c-480b-9afe-379908b34b2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "704e0d59-867c-46df-9dee-906e6e240038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cea17e43-718c-480b-9afe-379908b34b2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abd66813-2ffa-4cbf-b1df-79dafb6394fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cea17e43-718c-480b-9afe-379908b34b2b",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "d942801e-fb2b-4216-a951-e2f8ba14ecdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "bbbea99b-28bc-4dcb-a3f5-3f6f40dd1818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d942801e-fb2b-4216-a951-e2f8ba14ecdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd721bf5-b625-4255-96b4-76cde8dfb432",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d942801e-fb2b-4216-a951-e2f8ba14ecdf",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "e9b2e77a-1619-4a32-9ed4-78a1902fce7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "68053d87-dd2c-47c3-a3ab-15b20d73fa67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b2e77a-1619-4a32-9ed4-78a1902fce7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106279b6-ecd7-4617-864a-923e221c71c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b2e77a-1619-4a32-9ed4-78a1902fce7f",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        },
        {
            "id": "6e71a734-1ad2-438e-8f4e-c3fb483b99a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "compositeImage": {
                "id": "61ef2b41-eb9f-4b62-a9a2-5a12ba9e1833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e71a734-1ad2-438e-8f4e-c3fb483b99a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60e8c91f-bc6b-4f2a-8ec5-6e01f24afb84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e71a734-1ad2-438e-8f4e-c3fb483b99a5",
                    "LayerId": "247e3efc-ba8c-4ed7-82ad-177c8711875d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "247e3efc-ba8c-4ed7-82ad-177c8711875d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d273b818-ec19-4546-ac8b-b1335fd3d76a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 67,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 7,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
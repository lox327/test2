{
    "id": "4f19fbfa-967a-4c1d-8293-97fe21a08531",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerBashBox2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66221ef1-80cc-4fe8-b9cb-34edd7919540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f19fbfa-967a-4c1d-8293-97fe21a08531",
            "compositeImage": {
                "id": "55551735-8fb6-4fa5-9319-5235346cf03c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66221ef1-80cc-4fe8-b9cb-34edd7919540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8332656-ab27-460a-aec6-7c5c56d82252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66221ef1-80cc-4fe8-b9cb-34edd7919540",
                    "LayerId": "d97d9214-acd7-487c-9dd1-4290d0066e7b"
                },
                {
                    "id": "b7913d1d-ae12-4726-b3cf-8de0880a0379",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66221ef1-80cc-4fe8-b9cb-34edd7919540",
                    "LayerId": "f792abb7-ebdb-4087-a327-595fc85784e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "f792abb7-ebdb-4087-a327-595fc85784e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f19fbfa-967a-4c1d-8293-97fe21a08531",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "d97d9214-acd7-487c-9dd1-4290d0066e7b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f19fbfa-967a-4c1d-8293-97fe21a08531",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}
{
    "id": "75879bf0-79d0-4f1f-a314-e7a1d2f8a3a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tank3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ba38f78-148b-4cde-9c59-2847d2b9cfc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75879bf0-79d0-4f1f-a314-e7a1d2f8a3a9",
            "compositeImage": {
                "id": "8262da4d-6b10-4230-919c-8b5f1f571724",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba38f78-148b-4cde-9c59-2847d2b9cfc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0e17361-6c18-4ce3-9eff-28d6a536f485",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba38f78-148b-4cde-9c59-2847d2b9cfc9",
                    "LayerId": "e6123454-45d5-42a0-8524-2263e8ffe52c"
                }
            ]
        },
        {
            "id": "4393f5d0-cacb-402d-8990-fd2fd5cfc35b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75879bf0-79d0-4f1f-a314-e7a1d2f8a3a9",
            "compositeImage": {
                "id": "53d7d09b-2469-4880-bffa-7cc001ce930f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4393f5d0-cacb-402d-8990-fd2fd5cfc35b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44574b86-1b8d-469e-ace2-aa9c4208d03e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4393f5d0-cacb-402d-8990-fd2fd5cfc35b",
                    "LayerId": "e6123454-45d5-42a0-8524-2263e8ffe52c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 28,
    "layers": [
        {
            "id": "e6123454-45d5-42a0-8524-2263e8ffe52c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75879bf0-79d0-4f1f-a314-e7a1d2f8a3a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 14
}
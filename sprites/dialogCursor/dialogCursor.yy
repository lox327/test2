{
    "id": "2ca8a73e-bfd6-4722-ba92-0df40583a175",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dialogCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 13,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "971143a4-fc66-4710-ae9e-4cb48bbbe870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ca8a73e-bfd6-4722-ba92-0df40583a175",
            "compositeImage": {
                "id": "0d5970bc-6025-4b24-88bc-53b86dab0795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "971143a4-fc66-4710-ae9e-4cb48bbbe870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e35e44a4-6e40-41b4-988c-3947224672fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "971143a4-fc66-4710-ae9e-4cb48bbbe870",
                    "LayerId": "df222f6d-e493-455c-af14-243f128e514d"
                }
            ]
        },
        {
            "id": "f6e813c1-8eb8-4bdb-bc27-8ab7c5845f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ca8a73e-bfd6-4722-ba92-0df40583a175",
            "compositeImage": {
                "id": "d90d32a2-9d82-4a0b-811e-58c8ee13323f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e813c1-8eb8-4bdb-bc27-8ab7c5845f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ab5348f-c862-417b-a1b7-968751adc91b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e813c1-8eb8-4bdb-bc27-8ab7c5845f5b",
                    "LayerId": "df222f6d-e493-455c-af14-243f128e514d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "df222f6d-e493-455c-af14-243f128e514d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ca8a73e-bfd6-4722-ba92-0df40583a175",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
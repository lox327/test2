{
    "id": "a2e83ef2-cef6-4704-afe5-ad5350d70f83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "infobox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3569ae77-6a79-444e-a8bf-37738f3bd870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2e83ef2-cef6-4704-afe5-ad5350d70f83",
            "compositeImage": {
                "id": "f92e0351-7063-4dda-9ae3-16b07d09a6fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3569ae77-6a79-444e-a8bf-37738f3bd870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cf09559-956e-45cb-8a6a-d100f8fe9e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3569ae77-6a79-444e-a8bf-37738f3bd870",
                    "LayerId": "e7d2776f-0be5-4b7d-bbeb-d334e306267f"
                }
            ]
        },
        {
            "id": "0b06d0b4-a1e4-41ef-8eb7-dccd2cbc64ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a2e83ef2-cef6-4704-afe5-ad5350d70f83",
            "compositeImage": {
                "id": "0436d015-8d3c-4782-af5b-e5bbc6d4e694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b06d0b4-a1e4-41ef-8eb7-dccd2cbc64ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "589d4571-69d0-48d2-86d0-c00a0d44ff09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b06d0b4-a1e4-41ef-8eb7-dccd2cbc64ab",
                    "LayerId": "e7d2776f-0be5-4b7d-bbeb-d334e306267f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e7d2776f-0be5-4b7d-bbeb-d334e306267f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a2e83ef2-cef6-4704-afe5-ad5350d70f83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}
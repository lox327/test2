{
    "id": "ea7d58f8-8e7a-478f-b7b2-7ee57d4cf367",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shop3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 399,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "955428d1-f62e-442f-9e99-f3f94933e45f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea7d58f8-8e7a-478f-b7b2-7ee57d4cf367",
            "compositeImage": {
                "id": "5d2a63be-7ad3-4fb0-a58b-f23d7c161283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955428d1-f62e-442f-9e99-f3f94933e45f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e527ce5-ac3b-4e42-b6da-11fffb78e643",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955428d1-f62e-442f-9e99-f3f94933e45f",
                    "LayerId": "fabd71bb-841e-4ed2-bed3-cf89cb8074d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 400,
    "layers": [
        {
            "id": "fabd71bb-841e-4ed2-bed3-cf89cb8074d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea7d58f8-8e7a-478f-b7b2-7ee57d4cf367",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}
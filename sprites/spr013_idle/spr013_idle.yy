{
    "id": "1815d796-783c-4a0c-8114-733d481cc656",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr013_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 0,
    "bbox_right": 211,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5baeff6e-9672-4af9-a9b7-bbb83b9fa6ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1815d796-783c-4a0c-8114-733d481cc656",
            "compositeImage": {
                "id": "0842c449-d188-4e93-b56c-3e708912236f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5baeff6e-9672-4af9-a9b7-bbb83b9fa6ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98d5f14d-b493-490b-a58c-90a7fdd5607f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5baeff6e-9672-4af9-a9b7-bbb83b9fa6ef",
                    "LayerId": "47d8c75b-bde3-4314-b06e-ff2593fad705"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "47d8c75b-bde3-4314-b06e-ff2593fad705",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1815d796-783c-4a0c-8114-733d481cc656",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 219,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "500a14ad-03cd-4d1a-9791-945b0e58e522",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy2Hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "607e329b-c66e-4404-a105-aa508c9db7c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "500a14ad-03cd-4d1a-9791-945b0e58e522",
            "compositeImage": {
                "id": "c86d859b-91e1-47bf-9dc9-bce5da5e84bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "607e329b-c66e-4404-a105-aa508c9db7c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a247e35-e591-4fb4-9399-acbc7064e00c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "607e329b-c66e-4404-a105-aa508c9db7c1",
                    "LayerId": "a9bed719-5b79-4f9f-9995-0bdbbec076a8"
                }
            ]
        },
        {
            "id": "c10c8c7c-a390-4e4b-b362-c702a1ac479a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "500a14ad-03cd-4d1a-9791-945b0e58e522",
            "compositeImage": {
                "id": "6dccd8b0-28ee-4135-b386-b6064ceb1f55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c10c8c7c-a390-4e4b-b362-c702a1ac479a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d114a17-1961-4d64-ab8f-e83f2d8cbffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c10c8c7c-a390-4e4b-b362-c702a1ac479a",
                    "LayerId": "a9bed719-5b79-4f9f-9995-0bdbbec076a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a9bed719-5b79-4f9f-9995-0bdbbec076a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "500a14ad-03cd-4d1a-9791-945b0e58e522",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
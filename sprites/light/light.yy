{
    "id": "8b889c55-37c9-4bb1-b423-8331efc1c285",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "light",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 32,
    "bbox_right": 131,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a584e40-3f6b-40a9-9364-9ef1fde05b32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b889c55-37c9-4bb1-b423-8331efc1c285",
            "compositeImage": {
                "id": "311226c1-0895-46d2-bae0-68af83967655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a584e40-3f6b-40a9-9364-9ef1fde05b32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1c30bd0-8c5e-497c-830d-45604a91ba35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a584e40-3f6b-40a9-9364-9ef1fde05b32",
                    "LayerId": "ab78579f-0718-4995-b2f1-07f3a7411398"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ab78579f-0718-4995-b2f1-07f3a7411398",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b889c55-37c9-4bb1-b423-8331efc1c285",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 20,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}
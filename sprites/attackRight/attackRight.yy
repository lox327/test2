{
    "id": "60d5109c-5422-4b65-a659-44031c2b0052",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "attackRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd7c156d-09f5-4e49-a196-b9b74b410bd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d5109c-5422-4b65-a659-44031c2b0052",
            "compositeImage": {
                "id": "94837ca3-d1ee-41bb-82f2-e1b80ddb7006",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd7c156d-09f5-4e49-a196-b9b74b410bd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3adacdc-3b99-4dac-b397-f655b857235f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd7c156d-09f5-4e49-a196-b9b74b410bd1",
                    "LayerId": "9d09e16f-b93f-48e0-b1ec-0ba27bc64c89"
                }
            ]
        },
        {
            "id": "fc1ff0d4-ee5c-4283-9cff-d5b4999b3044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d5109c-5422-4b65-a659-44031c2b0052",
            "compositeImage": {
                "id": "0ed14b83-c2ee-417c-9698-4b2c106ae2b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc1ff0d4-ee5c-4283-9cff-d5b4999b3044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a79a0e71-3d27-4228-a7a1-c481dac962e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc1ff0d4-ee5c-4283-9cff-d5b4999b3044",
                    "LayerId": "9d09e16f-b93f-48e0-b1ec-0ba27bc64c89"
                }
            ]
        },
        {
            "id": "c365d46a-9b7e-4e6a-8cc5-3a03d4ba261c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60d5109c-5422-4b65-a659-44031c2b0052",
            "compositeImage": {
                "id": "4009fd45-9979-441b-9bcf-e32274c29bbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c365d46a-9b7e-4e6a-8cc5-3a03d4ba261c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f44d9fc-229d-43cf-81f4-b23ee795269b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c365d46a-9b7e-4e6a-8cc5-3a03d4ba261c",
                    "LayerId": "9d09e16f-b93f-48e0-b1ec-0ba27bc64c89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "9d09e16f-b93f-48e0-b1ec-0ba27bc64c89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60d5109c-5422-4b65-a659-44031c2b0052",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 20
}
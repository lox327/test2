{
    "id": "47e9c5b7-8cd5-420a-9869-e45b9c980ed5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bountyAvatar3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 12,
    "bbox_right": 79,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d532d38-d2af-4a64-b772-a6e45905a8d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47e9c5b7-8cd5-420a-9869-e45b9c980ed5",
            "compositeImage": {
                "id": "f1824047-98f7-494a-80fb-eb4640f83f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d532d38-d2af-4a64-b772-a6e45905a8d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c85b6da2-09c9-4544-97fe-7f97db4a490e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d532d38-d2af-4a64-b772-a6e45905a8d4",
                    "LayerId": "4498f20d-7e22-43d9-b357-07fe258240f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 212,
    "layers": [
        {
            "id": "4498f20d-7e22-43d9-b357-07fe258240f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47e9c5b7-8cd5-420a-9869-e45b9c980ed5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 0,
    "yorig": 0
}
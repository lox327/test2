{
    "id": "83fbb90f-b831-432a-9361-5ca2ec230f1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "invCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62414579-bb8c-4d3d-b6bf-df38ab9f574b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83fbb90f-b831-432a-9361-5ca2ec230f1c",
            "compositeImage": {
                "id": "8a11be07-7672-41c9-a072-6e5a80b3b966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62414579-bb8c-4d3d-b6bf-df38ab9f574b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c254cb49-7817-4e6c-b03a-6f30b6e4aca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62414579-bb8c-4d3d-b6bf-df38ab9f574b",
                    "LayerId": "0c3a9471-5490-4810-8f75-48972729cf05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0c3a9471-5490-4810-8f75-48972729cf05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83fbb90f-b831-432a-9361-5ca2ec230f1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "beb1e2c1-1d1d-412e-a376-76154bec88f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c0f5f9b-b9ca-40af-a2cb-184809020efa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb1e2c1-1d1d-412e-a376-76154bec88f8",
            "compositeImage": {
                "id": "743fe13d-62cb-40e8-9055-835c4717551e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c0f5f9b-b9ca-40af-a2cb-184809020efa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7242a885-c65d-495c-a0e2-9004b37b892f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c0f5f9b-b9ca-40af-a2cb-184809020efa",
                    "LayerId": "b745580f-5f0e-49fd-bf1c-80f37bd8d675"
                }
            ]
        },
        {
            "id": "7e0a0c73-1b8a-4065-8707-f0bdffeed8c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beb1e2c1-1d1d-412e-a376-76154bec88f8",
            "compositeImage": {
                "id": "b7782a80-1b7a-42cb-b86e-5d30e3b729c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e0a0c73-1b8a-4065-8707-f0bdffeed8c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e60ca30-ee92-44e8-9746-50739e1911db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e0a0c73-1b8a-4065-8707-f0bdffeed8c5",
                    "LayerId": "b745580f-5f0e-49fd-bf1c-80f37bd8d675"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b745580f-5f0e-49fd-bf1c-80f37bd8d675",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beb1e2c1-1d1d-412e-a376-76154bec88f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
{
    "id": "2d3e9339-05db-4b67-8ef8-b335fc982097",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mod_empty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ac33141-1181-4bd1-ad6b-8a002f194cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d3e9339-05db-4b67-8ef8-b335fc982097",
            "compositeImage": {
                "id": "7b15057f-b02d-49d6-8531-7f33fcae048f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ac33141-1181-4bd1-ad6b-8a002f194cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c4271ed-9b39-4337-933a-e17f79904a51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ac33141-1181-4bd1-ad6b-8a002f194cbe",
                    "LayerId": "b865a901-3dc9-4cea-b4bc-dc11adcf1d61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b865a901-3dc9-4cea-b4bc-dc11adcf1d61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d3e9339-05db-4b67-8ef8-b335fc982097",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}
{
    "id": "b23f1b5a-1abb-4af6-8e6d-2492db6ac2c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "jpbuildings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 200,
    "bbox_left": 53,
    "bbox_right": 474,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c9abddc-b782-45e8-8376-0a90717e920d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b23f1b5a-1abb-4af6-8e6d-2492db6ac2c5",
            "compositeImage": {
                "id": "13e9d56d-8268-486c-a14f-ec89aaf60c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c9abddc-b782-45e8-8376-0a90717e920d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da7d95b-3198-4c58-89c1-fe950b2c646c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c9abddc-b782-45e8-8376-0a90717e920d",
                    "LayerId": "0107a961-60f0-4c99-9a82-c6c5d7499970"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 270,
    "layers": [
        {
            "id": "0107a961-60f0-4c99-9a82-c6c5d7499970",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b23f1b5a-1abb-4af6-8e6d-2492db6ac2c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 0,
    "yorig": 0
}
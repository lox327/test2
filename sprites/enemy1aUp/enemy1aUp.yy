{
    "id": "6880144e-f5d8-4880-a910-d9178401dd0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "enemy1aUp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 22,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e61033cb-be77-4c81-9480-b16652676b7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6880144e-f5d8-4880-a910-d9178401dd0e",
            "compositeImage": {
                "id": "cd5f512a-26b2-45f5-96d7-b04688ca4e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e61033cb-be77-4c81-9480-b16652676b7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c822574-130a-4364-94c7-6d09f2d12403",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e61033cb-be77-4c81-9480-b16652676b7f",
                    "LayerId": "308153f4-2792-48d0-950d-2d199921127e"
                }
            ]
        },
        {
            "id": "a085cd72-255e-4986-b049-83eb6a3058d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6880144e-f5d8-4880-a910-d9178401dd0e",
            "compositeImage": {
                "id": "b6be8a7a-2c3a-4f7e-9f6d-b9c76412f125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a085cd72-255e-4986-b049-83eb6a3058d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89cf037-4bc6-4d4a-8baa-9ac255dd86cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a085cd72-255e-4986-b049-83eb6a3058d2",
                    "LayerId": "308153f4-2792-48d0-950d-2d199921127e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "308153f4-2792-48d0-950d-2d199921127e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6880144e-f5d8-4880-a910-d9178401dd0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}
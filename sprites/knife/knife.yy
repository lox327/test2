{
    "id": "cc5b3b54-15ed-4fba-8248-d6277c559cc0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "knife",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42f2b795-460e-4aaf-926f-6971f3f506e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc5b3b54-15ed-4fba-8248-d6277c559cc0",
            "compositeImage": {
                "id": "7b3395e3-aa61-4a1b-8fce-10336fe5ff86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42f2b795-460e-4aaf-926f-6971f3f506e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "210e77e6-4179-4cba-ad55-e31343f5b4e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42f2b795-460e-4aaf-926f-6971f3f506e3",
                    "LayerId": "14fd8e82-6021-4eda-9d25-08904d06123d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "14fd8e82-6021-4eda-9d25-08904d06123d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc5b3b54-15ed-4fba-8248-d6277c559cc0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}
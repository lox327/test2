{
    "id": "71e4a3ca-568e-4f1b-977e-1372dfa85de2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "playerDown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "feb9fe71-24b5-46c5-ba00-0b1ea15c9d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e4a3ca-568e-4f1b-977e-1372dfa85de2",
            "compositeImage": {
                "id": "7b1b3b62-6288-4055-ac12-cc8620e57c56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feb9fe71-24b5-46c5-ba00-0b1ea15c9d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f16fc3-728c-4ac4-ad80-c2dde49838ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feb9fe71-24b5-46c5-ba00-0b1ea15c9d83",
                    "LayerId": "734f94d3-065a-4f54-b841-658a5142fa5d"
                }
            ]
        },
        {
            "id": "899ea6be-f9a8-421f-b892-e954c66f0d3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e4a3ca-568e-4f1b-977e-1372dfa85de2",
            "compositeImage": {
                "id": "cce64c3a-0633-4836-872c-ac031707367d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "899ea6be-f9a8-421f-b892-e954c66f0d3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89d44255-7383-4ec6-95a3-42b4f3405d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "899ea6be-f9a8-421f-b892-e954c66f0d3b",
                    "LayerId": "734f94d3-065a-4f54-b841-658a5142fa5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "734f94d3-065a-4f54-b841-658a5142fa5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71e4a3ca-568e-4f1b-977e-1372dfa85de2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 21
}
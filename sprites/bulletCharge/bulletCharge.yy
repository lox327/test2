{
    "id": "397c745e-e86e-4f30-ae97-b93fc40f5c87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bulletCharge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d274f7a-0204-481e-87ca-01aecd219c4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397c745e-e86e-4f30-ae97-b93fc40f5c87",
            "compositeImage": {
                "id": "75a9b0b0-295f-40ca-b509-33c6d2a9df73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d274f7a-0204-481e-87ca-01aecd219c4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "719498c2-3f57-4f1d-b63c-67e44af392eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d274f7a-0204-481e-87ca-01aecd219c4a",
                    "LayerId": "64d4d344-5a3c-40f2-a4b0-d4a3012b8417"
                }
            ]
        },
        {
            "id": "76660aef-d96c-460c-b208-75a8a84ce1c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "397c745e-e86e-4f30-ae97-b93fc40f5c87",
            "compositeImage": {
                "id": "1a5f4c07-cf09-47dc-890f-7ee6969f5b9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76660aef-d96c-460c-b208-75a8a84ce1c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "624db007-f235-453b-a92b-91f70e9a63d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76660aef-d96c-460c-b208-75a8a84ce1c6",
                    "LayerId": "64d4d344-5a3c-40f2-a4b0-d4a3012b8417"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "64d4d344-5a3c-40f2-a4b0-d4a3012b8417",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "397c745e-e86e-4f30-ae97-b93fc40f5c87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 9
}
{
    "id": "ed0b73a7-3ffb-40bf-95d6-6ec8e4ceca10",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_GUI",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arimo",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d595e6d0-269b-4187-9f96-5e9ae748e1f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 85,
                "y": 72
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b1b92be1-b3c5-477b-b2a3-a2dbee985f27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 6,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b36c8832-77c5-4bcf-a3bf-f1bfcc06a1bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 52,
                "y": 72
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "653320c2-f769-4376-8640-ab39e2107dc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a8c5f17d-0890-4a09-88c8-b38162e789a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 44
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4673340a-0816-4ea7-addd-b26d6265e737",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9236d498-0d2d-4c09-bc6e-31c0d015102a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 102,
                "y": 16
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "65e3190b-caa8-4d87-8b84-a357520812f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 123,
                "y": 72
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "a752e401-1012-4878-98ef-fc5e1dbac706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 34,
                "y": 72
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1efc5088-0c3c-43d7-aa44-679710d2c62a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 75,
                "y": 72
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "13fe7663-a8e5-4f5b-9b17-beb99cc5177d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 114,
                "y": 58
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c97eecc1-f825-45e0-bfd7-ef2594721b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "41eaa6e5-ba28-4d9c-9571-0f5272d79a8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 90,
                "y": 72
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6782246c-deca-49ad-a29b-e5325b5493b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 16,
                "y": 72
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "dd7b904b-e4f8-4bde-91a0-a67955eff71a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 18,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d56a1ae4-1a97-4d27-b153-322fd231a463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 28,
                "y": 72
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1bc52f06-f81f-476f-9f1c-91b1bbe09db4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f7608183-9ae7-4037-809f-eb73170040aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 58
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "020a9ebc-65f5-4e9c-8646-ff083d119130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 58
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c91fc45d-d715-479d-a4f4-eb15fe534f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 58
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "eb6de275-2b18-4321-9ce4-8ef1ce454843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 58
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "06cf5338-577d-4f4f-acac-346d503bb6f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 58
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "86ab1303-46c6-498e-94e0-67792ad340a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 58
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d74e3767-c8ad-45bd-8ec2-70dff98f2803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 58
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "75d6ae4e-4141-477f-a5b8-f5e8a7d1bbde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9166cd5c-041c-4512-87e4-e0bd424b984d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 58
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "fdb102ac-5147-4832-ab98-9a4b9e736732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8200c739-6d16-4419-87c5-c1f73b549044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 100,
                "y": 72
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "758ca933-26ef-40a0-be35-0a03f6e560aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 58
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e8f0d706-d69e-477b-ba57-e8656d649ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 58
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "47427f08-f5cf-4fe3-a4b7-e0fbf0850759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 58
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b58fa5b1-924c-4524-8bd2-18bbf9444e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 58
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e5e4cf2f-fa4b-44a0-b28f-326340cff218",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f211ad67-8f4a-447b-a68b-93326879248c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 72,
                "y": 16
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "928eab04-12df-4d40-b674-a31dfbf90eb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 112,
                "y": 16
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "44c7806d-3355-43a6-a636-cf77019295d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 16
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "305dfa58-9f47-460c-972f-617de9438ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ed2ac366-ac0a-481d-ba62-a1a7440deeaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 30
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c14f3550-6b14-4440-a85a-26083e85f38e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 30
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0cc095ec-9e0d-416b-b149-59c9d02bdf3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 16
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1f805256-8631-46c3-8ab2-6638cb4a9c54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 16
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "a1c30ac4-ea50-4bad-80f4-c749e8460e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 14,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "077b1fdc-9a9b-4986-825b-d813e4669d28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 9,
                "y": 72
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b4d10433-eba1-46c0-80d9-ed6bb681a2d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 62,
                "y": 16
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "68951a5c-137f-41fa-bd1a-ac09b18f8c2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d915d861-3d00-4876-bb5d-04fa79002cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e4e3c1df-ccc6-432e-b59a-476be7db0d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 82,
                "y": 16
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "142741a4-16bf-4abd-bd14-66c616c6d6bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e2ee0dd5-6d29-435e-9695-8b3069731f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 30
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "7e155827-546e-4fa5-a90d-611883f1c42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5a8cd667-ad4f-440c-91c7-6a3996a9f09e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 52,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "af30ca6e-7991-4a9d-a825-b472253a5e92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 30
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d3bcbcfd-ffd6-44b1-b148-3b67da49e938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 30
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "cedb40bd-b895-4a68-96eb-a7673c7b69af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "284cd057-cc62-44d2-bae8-d1ed978f7122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2540eb06-d258-482d-bcb1-c78975158336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "eda28b4e-aeb0-4f56-a750-c33cd90be320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bbdd32b8-ebde-4ba1-9c04-ad7c95d23640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 22,
                "y": 16
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d922872a-8c96-4c44-889b-b87ee74ffc02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 30
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "42037b7b-ba83-4ea1-b68e-9a762b62a6ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 110,
                "y": 72
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "219f0b70-86f9-4a70-9fdd-35820c94e155",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 22,
                "y": 72
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9b6661b8-bdb5-43ac-8039-b284204489b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 80,
                "y": 72
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "70651d93-b2d7-4c85-9839-ddc035f22d40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 98,
                "y": 30
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4c1217b0-1543-4fea-906d-8d9caaf8cd04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 92,
                "y": 16
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "00835907-ba4f-4f4e-acc7-75fde1e90c60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 105,
                "y": 72
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b1d27628-c79f-443e-8c5c-bd293bccd2ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 65,
                "y": 30
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ec017517-3550-476e-9599-4791dabf6078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "91474489-56f9-4645-aaea-d6e8bea369c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 34,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "026d88ba-9ab2-4e56-adb3-44a6f890d1ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 30
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "635fd3b2-7938-42fb-91f9-da4e13831edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "72177129-44e1-4481-8f50-060fe18b3842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 40,
                "y": 72
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9f55d9ab-4eb2-466f-be1b-1b72b0ce9f64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 44
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b738777f-22ed-47a8-a672-0a27708630b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "af2840f7-9095-4bb0-86f0-9b4705fe0a31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 119,
                "y": 72
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "60fe475c-4ccc-4ef7-b5a8-4bb5a72d9a76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 95,
                "y": 72
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "105b714b-f123-4fd5-abac-d3e4b7ac99b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fbc4ddbb-9c04-4711-b05f-02100a2098ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 115,
                "y": 72
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "45444eed-4391-4b81-927c-505c64b8f6a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7c60efd1-2a40-4cf8-9542-c95b2a79a55b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0e3a1776-f846-404d-8fd2-31c8f20c803a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5660a5be-808a-4bc4-8818-f80d2ba7c934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "34ef3a4c-214e-4975-baa2-29bc534d9b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 44
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bd3314c5-5fe9-48da-ac69-20414e5e7d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 46,
                "y": 72
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8b73dda0-da6f-47e0-b805-17828a1a4cef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cfaf3495-33ca-41fe-b33f-4341b94c356b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 70,
                "y": 72
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bbd15eb3-d494-46ab-b4f8-5947d0a606a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a667b058-ccde-467d-a2b9-af835c9d0865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "919027c5-ad40-4014-ada4-848f08076ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": -1,
                "shift": 8,
                "w": 9,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4e85ef1d-15b3-447a-8f7b-24b45f531285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6e349629-e49a-441a-9baf-bfb9a224f72f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 30
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "50e1cb60-12be-4fca-a24f-39eb8ee4a194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "562c0e2b-d143-4be4-baa1-2a9e13f2d855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 58,
                "y": 72
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "75da7af1-49ab-40f4-aa01-da24ebeea9fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 10,
                "y": 86
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c979d1b1-821a-40b6-bd2a-400ced741363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 64,
                "y": 72
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5d6c9057-bec2-4fd0-b8f6-c7b20e30efce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 44
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "02630b3f-6a3d-4e6f-b324-acf6381244e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "20c59496-c18c-478b-ad7d-c1f592867f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "ef1bb744-1ffc-47ce-bded-8c1a8ecf9a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "a9fb357f-90d3-4e9b-90a8-3c51242fadde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "998ab393-c876-4c4c-80b6-b5e48e482e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "53100256-bece-4f59-aee3-abf2cfd1f020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "6f8f0ed7-c5e1-46a3-a520-34762b861eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "002be0c2-f1a8-4b3e-8fa6-d313607acf25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "d90f0d2d-0fd6-4b68-a1f4-6eca1821ef99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "b003aa73-2d0d-4c8f-b4f8-0a6d693740de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "7e0b51c1-be22-447a-ae88-a84cf254785b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "b369cece-20f8-43bb-881c-cc91a314ec16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "bdd6854d-4806-4614-914e-87d9e700acef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "500d8152-3f5c-4706-94e9-00e0922af4a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "96c64778-624c-4360-ac94-93275af30c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "538fc384-6628-4696-a24d-71bded7a20d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
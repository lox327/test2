{
    "id": "f415a95e-b1d7-4f67-a43e-96ab5c6283e8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_small_bold",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arimo",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "28587baa-35f5-4575-8de1-87cd8ca7f8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 26,
                "y": 57
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5180bb9e-7a7d-4c61-9d4b-080bb8d0308d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 122,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "68f246c8-d95c-4d42-879a-6e46cda75316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 16,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cd0db026-90c1-4a0c-8a86-6e5758ac0cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f0b3ac87-0c41-43b4-b8f5-4e34ca6b1b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 65,
                "y": 35
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ac7d7d62-84f5-4b1f-b420-5c1c7dfafc45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7161a473-4f8c-4ac8-9204-f5c97c8a324a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 13
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "99155b5b-47b2-497a-a771-a1f0b80dbcc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 10,
                "y": 57
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "daf96cfd-3b14-45b7-ac5b-7c6b7e622556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 55,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "adeb547b-8c93-4ec1-bd89-2c6759edb51a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 40,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5bc0d6d9-e89c-4269-8dab-232e755f234b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 28,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "da7d1b0b-de9e-47e7-9b50-3c5b22e3291f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 35
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6e91866c-5035-44e6-a077-d2442f34591e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 57
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "eec11f5c-8a0b-4abd-b47b-244ea84ced9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 70,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fc78e9f5-4f30-4919-a31c-b1d23d10a71c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 18,
                "y": 57
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7d7c4ab9-00d8-4e8a-a17f-e186de5c246f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 90,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "44bddc06-904a-4c8f-8e47-ba71637fda33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 79,
                "y": 35
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cae9df8c-ad58-4eba-8412-e758ff894f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 9,
                "y": 35
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "42e98a65-026a-4ea9-8d34-1b86d4c2641e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 16,
                "y": 35
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "966e1cf8-6d69-4d15-b0a7-85590428baa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 35
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7cfa77ea-f626-416d-abd1-b9043a609dc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 30,
                "y": 35
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "83de47b5-1d7f-4162-b973-94e6b61ac161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 35
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c4a0b323-e62c-44e4-88f8-ac8a41191a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 44,
                "y": 35
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f980ceec-f0a5-4e36-bc39-23321acf6e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 51,
                "y": 35
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "12b4a34b-61ab-4b48-8236-768a02d9638c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 9,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "06e20738-6144-443c-98d5-27b3d367c27f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 86,
                "y": 35
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c8984735-c5f2-494b-95ef-409a0c7746c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 22,
                "y": 57
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "46991bd5-ceb4-41ec-b004-b6e06a9cfdec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 118,
                "y": 46
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "12156f6d-f6e5-414a-9614-16ff520983b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 35
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "47ffafed-6aaa-4217-aac2-dea80d00740b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 35
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d3dc91a1-835e-4b3d-9052-38cf9ac6779f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 35
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "9513c55b-26e1-4342-bc5a-852427081d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 35
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a740cf4a-49a6-4cbf-8ac5-645ae3ae7254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "449ce2d0-c1c9-496d-b9e1-54c7a4164db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "edbc89f0-b802-49f6-8181-760aa744606e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1ef3f7d3-2c4d-4a84-995d-c9c5cc3aafef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 13
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "57b046ef-8d8d-4c53-90da-d0f04104d506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2b82d2d2-d132-4e34-a76a-abdddc3b3648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ad669f4f-c5a0-406d-a7eb-7067e1b4c89b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d22044db-350b-4617-b5fc-9bacc4198a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 13
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "41184f50-a771-4bd6-a011-46c6d4c8a68f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 13
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "67908ae8-103b-4b75-851d-807c8e32c6a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 114,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d9f973cd-8853-4314-8993-e6c4a0398ab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 34,
                "y": 46
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "49df16c3-de6b-4bb5-aa60-213a83884a4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 42,
                "y": 13
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "67a02827-6a31-4e6a-9e51-43db581cf6e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 35
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "737638c1-885f-4ebd-9057-11983510f4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7673f9c1-6a35-467c-aa5a-e32f6635ace9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 90,
                "y": 13
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fb7173a9-d77c-4fa1-abc6-d8542e599777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 13
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0968754c-719f-4d17-a1b6-64f4c3bc0f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 34,
                "y": 13
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "fa194837-d4f2-4e40-8c66-3e30869c2981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 26,
                "y": 13
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "30a7648a-d1f5-49b8-89f4-6c79adf36f87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "b9da9c78-8d5c-4085-8e45-d45aa4a65542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f2e0570e-8aa9-48d8-9818-9c702b23d624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 24
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "491702d0-29f0-4896-8136-64aad06530e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "27e96343-8d09-4d5e-9b53-c849ea0aa011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f9824d48-8428-4a95-a5a0-bb92618bb3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0c31824e-2ec9-4c48-9e1b-42fc8bd5eeb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3e5dc67d-001e-4e31-a050-3510249b631f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 58,
                "y": 13
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6a6adffa-d455-4568-85ff-bf61958f6d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 24
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8cd9ffe5-dbd1-45e6-bbd2-ff27a814eaa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 105,
                "y": 46
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "1f391e53-82e5-4638-be86-5a837084d9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 80,
                "y": 46
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "491cbf3c-39bb-4404-9b52-45f4efe95cf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 100,
                "y": 46
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "74cb2781-d819-472b-b2fa-6ffd6089b25a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 107,
                "y": 24
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "106e2c7b-54ea-4b26-bdbf-33e9c3dbdc55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 66,
                "y": 13
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e205f5bf-3ce4-4323-ab32-cf8304dc655d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 95,
                "y": 46
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "78866987-1c85-4674-9563-f2df5fe46779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 86,
                "y": 24
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f569e909-809a-4d4a-a5fb-14ede155e42a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 24
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a5dfe1ed-e73b-4bec-a588-b8a04fe8c691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "21bb3ffd-5a0d-447a-9abb-9805ce8cdb49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 65,
                "y": 24
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f3e4d4bd-e0a5-4432-98b5-9c5a5de90c0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 58,
                "y": 24
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "f7eadeb2-3dce-497e-b6b5-80e07e0ec754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 75,
                "y": 46
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0cd0a0c1-26fb-4996-b843-bae4e0eaa9d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7486d999-6b4b-4429-9f54-96e238809ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "187d5254-5898-4501-82a0-bb356bd56d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 14,
                "y": 57
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b813f7c6-4cb6-4f9d-863c-c17f63c40c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 85,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "35de4cd7-61d0-492a-9adc-086b6d9b6077",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "77be6fc5-c5e2-4899-b028-3838aa617d60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 6,
                "y": 57
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "eece5f78-24ac-44ec-b3bf-e358bbba3fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e1ec4e3d-a741-4b21-a1ac-fb993edd8c0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c9d67343-ba6e-4527-aa85-ad632d850ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9463ecd6-6274-4ff6-9272-10efe47eeb5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 16,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0d6770be-1c2a-44ce-ac27-12e5566f5977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 9,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "dd108fd5-f72d-4829-a836-80975313735e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 65,
                "y": 46
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0c2ecd0c-3d5d-4bc1-b8f2-675d45f3114e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "71118a55-dca2-4a48-b339-88ba0351ed83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 50,
                "y": 46
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7f7a6dbe-073d-4010-9120-b911e7878735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 119,
                "y": 13
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a6bc9b2d-3c00-4933-a687-cfd3fe668d2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 112,
                "y": 13
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1870e5a0-e3b1-478a-b369-69727e7b7270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1e3ada3d-e0d3-48a9-9252-28cb27ab13ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 105,
                "y": 13
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a965dfac-2252-44b7-987c-e90de7eef42f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 98,
                "y": 13
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2d3144ec-5c8b-4136-9d50-d986e9e0a011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 22,
                "y": 46
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f561a3a2-6e1b-40d4-9107-bdc8b6f46abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 60,
                "y": 46
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8db4f0c3-85be-4ca1-9075-1911123cf0fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 110,
                "y": 46
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "370a8d06-1c7b-4e6c-8ce5-70a0cf0a4d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 45,
                "y": 46
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3917bd54-b91f-4cbc-96cc-a64f3c9e40b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 114,
                "y": 24
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
{
    "id": "8fd228bb-2f60-4664-9456-67846c878997",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_small_reg",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arimo",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7b3856e2-c9e3-4daf-983d-a2d259ca8df7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 10,
                "y": 57
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b54ca9e6-e8c2-4c0f-89c4-350105d3aa61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 109,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "16bf2814-b739-46d5-a901-b6f322697630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 14,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "55f78474-52a5-4f59-b5fc-993c28feea8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 65,
                "y": 24
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1fac5e79-f795-43d6-95cf-d3417903bfd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 51,
                "y": 24
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2acccfad-115d-4c03-9cf9-296b7c7c34f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "71db8422-cb0e-49b9-b9c6-373f5f6a40e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 34,
                "y": 13
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0daf9160-74df-4c4c-b239-128a670e33e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 113,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "368434d8-5e99-466d-8597-905b68655669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 79,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "921178f0-f6ea-43d4-9075-fd17a54d6ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 74,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c9c36ab0-0af2-4af9-8c1a-e49d4196c689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 39,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5f955926-e57e-4c9e-af8d-46db0a92dbfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "b930c969-45ed-4bc4-9a06-5fb91d1c3345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 93,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8d37cfd3-e38c-48a5-80b8-4cf194c0f177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 69,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9d52bcd6-69d1-4939-af03-4cf3b3c70836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "61dc1bda-1a3f-42f7-87b3-6edbf941e3e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 54,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9b7deb53-5396-4e0e-9401-a7aba26659c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d1fe969b-1e39-415a-a785-b7cc65ccff18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 79,
                "y": 24
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "22de563c-bd7c-47d9-9913-f7cda896b2c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 86,
                "y": 24
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5b747127-de15-4b83-9086-b9e00e69301d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 93,
                "y": 24
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "89078a1d-88b5-4d57-9a2f-784470c4de49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 100,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "35d0430e-f1fa-4856-812a-02738fdf32a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 107,
                "y": 24
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "0970002c-57fb-4e67-9419-e1aa41731f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 114,
                "y": 24
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c9742310-b685-4375-90e3-950159ad5a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 9,
                "y": 35
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9956ffcf-2218-4373-8013-3e278d799119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 16,
                "y": 35
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "28534a1f-9542-4a86-9c96-2e9b143081d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 35
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "086f23fd-e5cf-4d32-babe-c84eb0a32a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 117,
                "y": 46
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8c526e7f-62da-42b7-b74b-ef7886c1781e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 2,
                "y": 57
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "faa376da-af78-4860-b821-f3e8133ca692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 35
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3871d94f-dfe8-4e76-8736-1cb04022680e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 35
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "92928a8a-73d0-4e5d-bb69-78314e4bd4ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 35
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d939c3fc-7531-46ee-85ac-6a05e522fe25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 44,
                "y": 24
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d415f9ad-4079-4b26-a2c7-40c38f8e8841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3b064113-22aa-46c9-a3b0-4555d5028173",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 2,
                "y": 13
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4445163f-f461-4b64-bd57-ab74e006092d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 24
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3414f24b-140d-4b51-8ec1-b52bf6285c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 13
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dc7561a5-677a-47c1-a13d-b8e411889236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d80df081-c7d6-4312-bbbd-9e562166277a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d4189084-be70-4776-ae56-c553ef63d475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 115,
                "y": 13
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "91c45410-deb4-40a7-86df-cf0b46028132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c965ce1c-bb8d-4fb3-bb3b-315101a02dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fde66213-66d3-4149-a3af-57051f8b9ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 121,
                "y": 46
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ca006cee-178a-4df1-8163-2c8bb5c3475e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 117,
                "y": 35
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5d235260-9ed0-4b60-8393-a4324646654c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c9e937d3-f3ae-4c43-b507-281c8b9684c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 30,
                "y": 24
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a5ccd03c-8f82-4ca5-9bd2-91727906d22c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "397e9b2a-fb15-4570-a8cd-20b527c9e166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "822ffac2-c0e3-4a73-8622-8d7e9c118d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7faf0342-a3a5-4b1b-8f88-cec3a2d76fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 66,
                "y": 13
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b7e58000-d17a-4196-8901-dc4ca1490235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c78fdb38-ce6d-4cd8-921f-7d59c9f4929e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "76378eb9-0485-436f-a596-a7a42188080e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 87,
                "y": 13
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d228b8b2-19b7-4225-9b12-bdd1bee30582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 108,
                "y": 13
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9df73d43-c670-42df-928e-c014359a9e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 9,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 13
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4a1bbd26-6e11-405c-8d21-d224d0ecb2c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 10,
                "y": 13
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3d9d9c94-eb38-43f7-9a20-9cfc67ce0028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 9,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "0245e809-ca5a-4dd3-b154-968adc9549c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 42,
                "y": 13
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "e2b64a41-ceb1-455e-b644-fd0ca41af2fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 26,
                "y": 13
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "18d73724-d1d5-41e7-9d58-bbde10761b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 23,
                "y": 24
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7c09fa33-9a13-4d62-81c9-9b4f7df24106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 59,
                "y": 46
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "82a8f76c-8d0e-4347-8d37-31f1fb5949a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 64,
                "y": 46
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "127fb755-679f-4a47-8352-c05919d86051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 89,
                "y": 46
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "197cfc8c-c531-4843-b06d-3e574a06a9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 69,
                "y": 35
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fce6d1c9-59ef-4646-9b43-969bd0202025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 9,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 50,
                "y": 13
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "28dd626d-6094-438c-ab30-09f28249d777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 84,
                "y": 46
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "577be766-accf-4631-8351-3e64eee9a738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 73,
                "y": 13
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "13615d12-8d83-4bfe-b294-7be9ede459f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 80,
                "y": 13
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "384c4b08-54c2-4d34-af5a-764d69cf8d7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 57,
                "y": 35
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f8b2e474-1081-4731-bce0-7130c79d352d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 51,
                "y": 35
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8dcb26be-7728-438e-8641-37890b8a90ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 94,
                "y": 13
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a3b8aab2-dfa7-4a0f-a503-3267bf68fc3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 44,
                "y": 46
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "d58b5f3f-d076-4e0c-be54-331fd7c828f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 105,
                "y": 35
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "85cbe9bb-6735-45c5-b9cf-1d83adbfd2ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 8,
                "y": 46
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5f548fdc-e26c-4d48-beed-a308c3dde981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 97,
                "y": 46
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f7c81286-60e8-49e5-8f5b-28098df3d1e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 9,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 29,
                "y": 46
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cdf07ca8-6d39-4512-a792-60921b0b1a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 101,
                "y": 13
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "404a887a-7943-4ae0-8e45-7898fd1dcd10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 105,
                "y": 46
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "88a10e24-007e-4eee-b14e-dc982ba2f44b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 9,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "20e9b491-7130-4692-9d57-b1e11be60596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 63,
                "y": 35
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "774fc10d-c962-4968-90de-8f57259b3aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 9,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4405126a-b258-4029-b6fe-336e841771fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 16,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "73cafa93-b6fc-4297-af77-2d6093ed9428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 81,
                "y": 35
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8945c034-96d2-4d49-ac83-55537bb11795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 34,
                "y": 46
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0553fa03-8998-4e29-bfaf-12d05b882ae7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 87,
                "y": 35
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "54a4d4a3-455a-4675-8298-16cfeb56ada9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 3,
                "x": 19,
                "y": 46
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "714d4bbd-1800-4c21-83b4-07820d0bebb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 93,
                "y": 35
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7f2651b5-da73-4149-a03d-f059f9b2d104",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 99,
                "y": 35
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0511977e-9248-4343-8939-8bd1052b91b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 9,
                "offset": -1,
                "shift": 6,
                "w": 7,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d2b90b74-fe8f-4811-a57a-6e7ef6fa201b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 75,
                "y": 35
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "82fdc7d6-f635-4923-9db8-8f9ddf36ab70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "656e61a5-8cde-49ac-bb12-33115ffcfba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 9,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 111,
                "y": 35
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "730abcae-2b03-4077-8d69-b5a9e0496c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 24,
                "y": 46
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c82c42f0-4fcc-4886-885c-737048f81012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 9,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 6,
                "y": 57
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0f8a2fdd-c8be-4c64-b60e-824b4a0e5bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 9,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 49,
                "y": 46
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "055ca886-6b53-49a3-b880-59a18136c922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 9,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 37,
                "y": 24
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 6,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
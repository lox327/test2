{
    "id": "a02f560a-c526-4694-8596-3994f757469a",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_med_reg",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arimo",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d306226d-c5ac-41f7-9fab-c7b9699d432c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 193,
                "y": 62
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "24df7ef5-c1db-4308-8b4a-be57131f1f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 219,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "460430db-a3cc-4122-bb25-9fceb5b64c25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 97,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "48dbc2ea-e0ac-4e35-8a17-1315f04bf076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 145,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "1b40de7b-feff-4584-b6a1-236624e87016",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 90,
                "y": 42
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2170585c-7cef-4cb4-9e15-4b65e652b3e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "06f371d7-d9c3-47a4-8a2a-9484b4867c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "af7dc2dd-2519-4423-a468-8b95459616c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 239,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "310ee9c1-43fa-4970-8634-5b04570d7376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 81,
                "y": 62
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "17aa2803-698b-4707-a673-2cbdd06ef0d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 179,
                "y": 62
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "554a15bf-a308-4ab0-9bb3-149aa111d2fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 72,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "92ed88a2-f34f-448b-8d55-3da7dd41c5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5627ea1a-a95a-4d0d-9a4b-ae335f4d8410",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 209,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "be3f14c1-c706-4c6f-a5d0-8a37e286798a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 186,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e2dfd896-d03f-4c60-96f1-b8f584aedd2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 214,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cc3f1cc1-53a5-47eb-98c3-0021faf24343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 137,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "293cfffe-a8a8-44b7-a6b8-824725f04ad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 79,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "85d2f255-60a9-428c-b71e-90728ec73575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 42,
                "y": 62
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "82825832-07a8-4faf-8944-71bead6c27de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "02273f04-019e-4726-8cf3-d46135e096ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bae14511-1535-420a-837c-7d977c82e193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 123,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9ccf79f8-c5dd-4863-bd2a-77c69cb309d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 134,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "d69917e0-fae2-4b97-9bc1-040a7755db22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 178,
                "y": 42
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "db95e38f-25cb-4228-a35e-7c682ba2387f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 42
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "16466c85-663b-45dd-b4f2-a4ba9e1b8a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 42
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ae258878-7337-4ef2-ab69-3c785933a9fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 244,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "04e819e4-73e6-419b-ad18-d821ce29561e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 224,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e6feeca1-9c06-42f4-9907-7d410d499758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 229,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1bd4fdab-a15d-4fba-a69a-1d5bc076f95a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 189,
                "y": 42
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ab89c31b-7193-4e99-87d3-37874df2f893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 200,
                "y": 42
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d34a05d3-493d-4fd4-990b-a179107c9f6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 42
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "51d23d37-2ded-4e0f-b750-427aab79fa29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 42
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "f65a5b5c-2647-42c6-9930-c2a00e2875d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5834a2d4-97f9-4491-973b-2c81af36b051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "eb4ee028-3fc0-41fd-92bf-3be0599b8f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 77,
                "y": 22
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4c395ef0-ec01-4fae-b3d8-2f5846e2a85a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ea0601f1-47b9-4e13-a3e1-7f61d9701c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 65,
                "y": 22
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "07d51414-48b0-49ba-bd0f-5ab115f1320e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 53,
                "y": 22
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8bfb9e24-0d09-4e71-ac15-64aaa56d4134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 233,
                "y": 42
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "050925c5-d8b5-4e81-9ac7-905f229128bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "807a52b4-1e24-45d8-883c-b0ed8f9a4573",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 41,
                "y": 22
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1c9ddca1-35d7-4980-b04b-62a131035042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 244,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "808f410a-c1e7-41c1-a607-d458b11d0f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "db20b31d-74bd-441f-8156-2412cf2e9708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 28,
                "y": 22
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8f7bc7c7-15ea-49d1-8cc9-c8d7203cdc19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 42
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0b2cd046-029a-4a6b-a33d-34f18cc7173d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "27122c99-f8af-486a-81b1-869e8c73eb37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 89,
                "y": 22
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "02003eef-c6e2-4dff-8a8c-eeeb737b4831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b38b61c9-7070-4cb7-80db-48e75c187fa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 113,
                "y": 22
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "543134dd-20cb-414c-8c5f-eae74c546f0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "12e17cea-8007-4415-ae26-2fadf3f0d249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3d0c4341-d3ed-45dc-892f-4f802a2c511a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "94ce232d-19ba-41ca-812e-4a7c61c152dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 22
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "ad144713-cdb2-45a8-9416-ed4d077a7872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "55085971-7fc4-4619-8b4a-c6c9a22b297b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8090bdb4-669e-4281-bbcc-dfec583906bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9ca4539f-6bc6-4e9b-8253-82a6b2987d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "739492bf-7051-4481-9338-b67c6715269a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 22
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3bb7a1d8-c2c5-420d-9dcf-25ed2a9ed584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 22
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "de1bccf4-944e-4856-aa75-ed359cc84626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 105,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "78e094ca-271f-47a4-9b06-8c85c406048e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 172,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "c580026e-a206-43bc-a42b-94071b46fafb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 165,
                "y": 62
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5c3572a6-ef94-4f08-94d0-7db5fd7464d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 46,
                "y": 42
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "27f17e0b-71c0-434b-b8ee-7ca25962bf04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f7f79101-1495-4f2f-85b3-2767e47b024f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 144,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2317b511-6454-4a61-adbd-85f1ead10ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 182,
                "y": 22
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a193b945-bbda-4008-90e4-c56cda80747f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "40b70377-f8b6-4d7d-9fcc-a4857fd42b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 226,
                "y": 22
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b757d44d-74f2-402d-b58e-d816fdb29715",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 193,
                "y": 22
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ff3e76bc-49f2-4682-ac68-8f7f826687c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 160,
                "y": 22
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e9517703-9b38-422f-b37a-167778d305db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 129,
                "y": 62
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ab5c6392-67b6-4127-863b-099ee8084e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 149,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5ad54707-3f63-4f3c-aaf5-bdffb777652f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 62
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a453c535-98b8-4d80-bcbb-4f398f481aea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "04ade0f8-8403-4f99-98f9-6df95b8b9769",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 151,
                "y": 62
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9b30d0b7-3760-4d4b-8064-8b98c8e1bef8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 32,
                "y": 62
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "04e52668-3c44-407c-ae2e-0d5be764cf46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 199,
                "y": 62
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "212433b7-d649-4533-8ba5-e4af39a83df9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a362ee15-bc5d-4704-9eac-0cfb7bb7fcad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 62
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d4d5422b-d717-4802-a280-2f23296826f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 101,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8afb97f2-6b5d-4588-82f0-e1313c5b5490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 171,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a17b4a9d-7135-4709-bfb4-72b1764a9302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 204,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e8db4cb7-36f9-4fd1-8bd5-2dd79795bf71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 158,
                "y": 62
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "96fed4f2-7aa9-48fb-8872-50571d30526d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 215,
                "y": 22
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "20ff609f-50cc-4705-8f96-70165fbcbb77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 89,
                "y": 62
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d59484f4-d4cf-44bc-8e2a-54a65ee1b529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "560e2c23-dab3-4d31-aa6b-99c7b9a91152",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 237,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5320fbcd-acd6-4175-a2a9-f316c3414b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b880b7f7-8dbf-42c8-a6d5-1df6145a134d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5fabbaf4-6449-4a56-baaa-6a0c279f8a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 42
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7acda826-45ae-4ba0-916e-f3d114639b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c1698e06-91a0-46ca-b5fc-5b21b9f885d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 113,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ea6d5651-4285-4185-b131-d01b0175f003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 234,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d9125884-25df-4ad4-8161-a4d019952cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "827097a9-1a4a-4f4e-89ee-ec3a8977c9cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 57,
                "y": 42
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "242cb45d-2af1-4803-ae92-309e9a162a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "116586e5-087e-41b8-9be6-741de9056eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "7d9fa648-c230-4e57-885a-8d44b1548b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "5aa32ce2-cc3e-4fec-bc2b-53cae229123e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "33c1464e-c2e2-4063-87de-400963b29ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "a56f8e9b-d677-4452-88b7-640990cf26cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "41445d1f-41cb-4138-a7fe-489bab7588c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "c6b6de63-34e4-4fba-8e7e-ff92b8ebee28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "2393649c-0c30-463b-936b-e21439b9e88a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "4ee1b5d6-8b81-4401-bad8-d99f42887886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "8cffa4ca-96ab-4fc4-8a4f-80a875b7e75a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "042d46b4-1d0e-43ad-bb5b-b00f9cdb1474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "16b6971d-9984-4f9a-9949-0e1c2f4abc7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "c092b715-3660-4db8-a2b7-951a0645e8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "cf38570f-515d-4fc3-abae-e0b9d21f7719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "40ebc1f5-73b8-4c30-becb-4bae71b037a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "e02db10f-8874-4aee-83ba-cc580d394a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "cdd3d9e2-6a48-4d4e-a237-e279cf75f1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "8bf37f64-df70-4d47-aa9f-b828cc6aa2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "c47cb252-6142-4b29-ac87-67ca07ab767b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "10473508-0327-41b3-9edf-08862134184a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "5b5cb67b-cdf3-4b51-880a-71335d7fbae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "6ba48f45-32a6-4165-93ef-c9a756f6c333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "6765d8fc-601c-4225-9750-f17f605dffcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "aa346b88-072e-4121-93f9-f31ed3d49d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "64517e26-cb79-46f3-b840-7f6a9881c6c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "05ddaca6-e89d-461e-a9d3-b0e3e01f0674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "ab158c54-20ea-479e-a5a8-1cc02941a6ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "0cc141ea-0c91-4803-915f-02e3c0f07d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "bacd2c77-1404-4300-a2c5-7b559136bbdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "394e6f1f-35de-48ec-aaae-bd1bb718bbe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "3d3bb78a-ab84-4b7b-9b7c-3e8c6a06a4b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "e2f4a08b-1323-4367-be57-ff237ce0ea86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "1ca44d02-089a-47e1-a020-bae11b7f637f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "78428651-3665-4b96-a668-5f6c3ee7cbc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "17f24bb2-a3d4-4ed7-85a2-883ecbd39b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "63cb3046-5a52-49bf-9928-c1e405e6e946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "3d08076b-0f9c-4209-a3ad-bee0bcc54f77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "73654961-2d80-47a6-8609-ec82cd685121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "7de3a648-7662-4e64-a929-a76682e3e57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "97dc043b-b3c2-4fd0-816d-b87bc4956caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "36240643-cb32-4743-bd86-947ac5b79f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "7b05e85b-f03c-40d4-9289-5b14868ca425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "e15e1a1f-b453-499f-a28e-597789de4a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "f27cff67-e083-4832-9aa0-e737bff242b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "4db7a5d6-4350-437e-9548-4470aa13e44a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "749d11b8-a6f2-46af-aa84-767baf6abf57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "992cf9b7-085f-4b95-8f9f-8710126c2e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "4c03ba7b-6917-4bbf-83cf-17feb54d339c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "ce3994c0-1d3f-42ec-a34d-27a57225d32e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "7aad3dc4-3622-47bc-b804-ee21afcc4a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "39aeef3c-97b4-449c-a1b3-8501c40e6a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "44cdae1e-4c65-4f2d-b070-ba20cb4e1e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "3be659bf-fb5d-4a17-bf31-aa5092dda08c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "1d218907-c9ae-44e8-9a4a-278c76fbac03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "a6ef9b96-bf33-41ff-8ed9-2de1e27c22f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "1f1899e1-cff3-497a-ad12-d2fa83aa1cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "4e43b387-ca54-403a-ac55-0215138021e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "9be915b8-4634-4219-9e10-1af10e4a854f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "0832a41e-2d67-41b6-b275-6cdc2ee9f44b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "712f1ab3-48e8-4bd3-8f98-ac43e67e4cbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "9c76791e-e6bc-480d-ac8b-65c80890d90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "26927caa-fdf7-40d5-8ba0-d48c27d0d8fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "0523b2b9-3a89-4ceb-9ec4-86128daaeffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "153ea3b6-3277-4e18-9070-358495f8ec30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "a1f01dff-0d62-427e-8dc5-3745242f1a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "79fa9eb3-6c71-4535-9551-041abdddb0bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "a8bea6f7-637b-4a5f-8b8e-ac3d3148b0e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "415fe20f-47d4-4edc-b99e-329d464b3300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "4384a21c-08d6-4172-bccf-773aec00f140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}
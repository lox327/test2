[Tileset sttuffs for JP]

1) create sprite, using the tilesheet.png of the tiles, buildings...
2a) create a tileset based on the sprite that you created (which contans the tilesheet)
2b) this is where the brushes can be made: Brush Builder
2c) now we're ready to jump into the "rooms" editor...
3) create tile layer (on the left pane; you can see the tileset on the right pane when you do this)


[Version control stuff]
1)  PULL from server (grabs any changes made to the project)
2a) COMMIT to your local to create a save point, with the intent that this will eventually go to the server (once you are happy with your edits...)
2b) PULL againin case there werechanges made since you last pulled
3)  PUSH to the server to sync your changes so that I can then PULL what you've done into my project.


[GM shortcuts]
F5 runs the game
F12 to toggle fullscreen edit mode
CTRL+SHIFT+NUM(1-9) to create a bookmark that can be recalled by CTRL+NUM(1-9)


[Game controls]
WASD for movement
SPACE for shoot
ENTER for action (dialog, open door)
NUMS 1, 2, 3 for weapon changes
	-1 is default weapon
	-2 is power weapon
	-3 is stun weapon


[Misc]
-todo note has my bugs list, some other random shit
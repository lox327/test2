AREA EVENTS (trigger, barrier, scene zoom)
	OBJS
		-objSceneZoom
		-objTrigger
		-objBarrier
	-must assign way to trigger event over (last enemy down, checkpoint...)
	-Barriers layer

objSceneZoom
	-place where you want the zoom/lock to occur, camera will pull xy of the obj
	-obj uses prox script
	-should add params like zoom amount and speed for different effects
	-instance_nearest*
		-use when having more than one obj in a room to distinguish between objs instances
			-better to do this vs. having individual objs?
	-need a way to mark this as one-time use

dialog:
sprites:
-create char sprite
-create char avatar

objects:
-create char obj, copy from other npc to get CREATE and KEYPRESS events
-CREATE: need to edit diag title, diag text, avatar name
-KEYPRESS: need to edit startDialog name

place char in the room

*can use checkpoints to determine which diag to use

different colored key cards (doom, super mario world...) that allow access to different doors

different cyber arms:
	-power capacity
	-mod slots (different colors/level like in WoW, take up more power)
		-a1, a2, a3 (ability, active effect on bullet)
		-p1, p2, p3 (passive)
		-s1, s2 (special ability)
	
[ability ideas] mega man, transistor
-pwr boost
-dmg over time (bleed? fire, acid)
-area of effect (explosive)
-lighting bounces?

passive ideas (can be upgraded?)
-adds a stun chance
-adds a chance to slow target
-adds a chance to hack enemy (robot only?)
-adds a chance to knock back
-adds a chance to add burst dmg
-adds a chance to disarm/hack
-increase dmg or effects
-gain resistance to damage and slowing effects
-increase amo regen
-reduce cooldown on special
-increase duration on special
-increases AOE radius
-increases AOE dmg

special ideas (cooldown or charges?)
-overcharge for dmg boost
-overcharge for speed boost
-create decoy
-turret
-stealth
-contra spread gun

[gameplay ideas]
player starts with basic lvl1 arm, one lvl1 mod slot
gets lvl2 arm, access to lvl2 mods
gets lvl2 arm with extra mod slot, access to lvl2 mods
	-low lvl mods can fit into higher lvl arms
	-high lvl mods need equal+ lvl arm

Arm I, Mod I
Arm II, Mod II
Arm IIe, Mod IIe
Arm III, Mod III
Arm IIIe, Mod IIIe
Arm IIIx, Mod IIIx


char has standard gun
	-uses regen ammo
	-upgradable?
char has mod to enhance weapon
	-uses energy
toggle enhanced weapon on and off




GUNS!!!
guns have different lvls, I, II, III
I - no mod slots, not upgradeable
II - 1 mod slot, upgradeable
III - 2 mod slots, upgradeable
-uses more energy

MOD SLOTS!!
-energy efficiency mod
-power over boost
-elemental


{
    "id": "40b6af7d-c946-4abc-a869-1e262515caad",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tilecars",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 14,
        "SerialiseWidth": 18,
        "TileSerialiseData": [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            21,
            22,
            0,
            24,
            25,
            0,
            28,
            29,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            41,
            42,
            0,
            44,
            45,
            0,
            48,
            49,
            70,
            71,
            72,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            61,
            62,
            0,
            64,
            65,
            0,
            68,
            69,
            90,
            91,
            92,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            81,
            82,
            0,
            84,
            85,
            0,
            108,
            109,
            110,
            111,
            112,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            101,
            102,
            0,
            104,
            105,
            0,
            128,
            129,
            130,
            131,
            132,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            121,
            122,
            0,
            124,
            125,
            0,
            0,
            0,
            0,
            151,
            152,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            144,
            145,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            164,
            165,
            0,
            0,
            115,
            116,
            117,
            157,
            158,
            0,
            14,
            15,
            0,
            17,
            18,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            135,
            136,
            137,
            177,
            178,
            0,
            34,
            35,
            0,
            37,
            38,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            113,
            114,
            0,
            197,
            198,
            0,
            54,
            55,
            0,
            57,
            58,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            133,
            134,
            174,
            175,
            176,
            0,
            74,
            75,
            76,
            77,
            78,
            79,
            0,
            0,
            0,
            0,
            0,
            0,
            153,
            154,
            194,
            195,
            196,
            0,
            94,
            95,
            96,
            97,
            98,
            99
        ]
    },
    "out_columns": 15,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "b35af346-50a8-4b4b-b1d2-03fb625ada7f",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
            36,
            37,
            38,
            39,
            40,
            41,
            42,
            43,
            44,
            45,
            46,
            47,
            48,
            49,
            50,
            51,
            52,
            53,
            54,
            55,
            56,
            57,
            58,
            59,
            60,
            61,
            62,
            63,
            64,
            65,
            66,
            67,
            68,
            69,
            70,
            71,
            72,
            73,
            74,
            75,
            76,
            77,
            78,
            79,
            80,
            81,
            82,
            83,
            84,
            85,
            86,
            87,
            88,
            89,
            90,
            91,
            92,
            93,
            94,
            95,
            96,
            97,
            98,
            99,
            100,
            101,
            102,
            103,
            104,
            105,
            106,
            107,
            108,
            109,
            110,
            111,
            112,
            113,
            114,
            115,
            116,
            117,
            118,
            119,
            120,
            121,
            122,
            123,
            124,
            125,
            126,
            127,
            128,
            129,
            130,
            131,
            132,
            133,
            134,
            135,
            136,
            137,
            138,
            139,
            140,
            141,
            142,
            143,
            144,
            145,
            146,
            147,
            148,
            149,
            150,
            151,
            152,
            153,
            154,
            155,
            156,
            157,
            158,
            159,
            160,
            161,
            162,
            163,
            164,
            165,
            166,
            167,
            168,
            169,
            170,
            171,
            172,
            173,
            174,
            175,
            176,
            177,
            178,
            179,
            180,
            181,
            182,
            183,
            184,
            185,
            186,
            187,
            188,
            189,
            190,
            191,
            192,
            193,
            194,
            195,
            196,
            197,
            198,
            199,
            200,
            201,
            202,
            203,
            204,
            205,
            206,
            207,
            208,
            209,
            210,
            211,
            212,
            213,
            214,
            215,
            216,
            217,
            218,
            219
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 220,
    "tileheight": 24,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 24,
    "tilexoff": 0,
    "tileyoff": 0
}
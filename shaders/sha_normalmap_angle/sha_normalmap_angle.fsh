varying vec2 v_texcoord;
varying vec4 v_color;
varying vec4 v_pos;

uniform sampler2D s_normalmap;
uniform vec3 light_pos;
uniform vec3 light_dif;
//uniform vec3 light_amb;
uniform float angle;

void main()
{
    //setup variables
    vec2 pos = v_pos.xy;
    vec3 light = light_pos;
    vec3 attenuation = vec3(0.5,1.0,1.0);
    vec3 ambient_color = vec3(0.35,0.35,0.35);
    vec3 light_color = light_dif;
    vec2 resolution = vec2(4.0,4.0);

    //sample the textures
    vec4 t_color = texture2D(gm_BaseTexture,v_texcoord.xy);
    vec3 n_color = texture2D(s_normalmap,v_texcoord.xy).xyz;

    // Convert normals to correct range
    vec3 normal = normalize(n_color * 2.0 - 1.0);

    // Find the light vector
    vec2 to_transform = vec2(pos.xy-light.xy);
    
    // Transform the light vector with the angle (probably can use a matrix for this somehow..)
    //save the trig things in variables to speed it up
    float cc = cos(angle);
    float ss = sin(angle);
    to_transform = vec2(-to_transform.x*cc-to_transform.y*ss,-to_transform.x*ss+to_transform.y*cc);
    
    //Normalize, take dot product
    vec3 delta_pos = vec3(to_transform.xy, light.z);
    vec3 light_dir = normalize(delta_pos);
    float lambert = clamp(dot(normal,light_dir), 0.0, 1.0);
   
    // Falloff
    float d = sqrt(dot(delta_pos,delta_pos))/resolution.x;
    float att = 1.0 / (attenuation.x+(attenuation.y*d)+(attenuation.z*d*d));
    
    // Final color
    vec3 result = (ambient_color + (light_color.rgb * lambert) * att) * t_color.rgb;
   
    gl_FragColor = v_color * vec4(result,t_color.a);
}
